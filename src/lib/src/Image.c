#include "../include/Image.h"
#include "../include/BmpLib.h"
#include <stdlib.h>

Image *alloueImage(const int largeur,const  int hauteur){
    //alloue l'image
    Image* img = malloc(sizeof(Image));

    img->hauteur = hauteur;
    img->largeur = largeur;

    //Alloue les colonnes de la matrice
    img->bleu = (short int**) malloc(sizeof(short int*)*largeur);
    img->rouge = (short int**) malloc(sizeof(short int*)*largeur);
    img->vert = (short int**) malloc(sizeof(short int*)*largeur);
    img->gris = (short int**) malloc(sizeof(short int*)*largeur);

    //Alloue les lignes de la matrice
    for(int x = 0; x < largeur; x++){
        img->bleu[x] = (short int*) malloc(sizeof(short int)*hauteur);
        img->rouge[x] = (short int*) malloc(sizeof(short int)*hauteur);
        img->vert[x] = (short int*) malloc(sizeof(short int)*hauteur);
        img->gris[x] = (short int*) malloc(sizeof(short int)*hauteur);
    }

    return img;
}

void libereImage(Image **ptrImage){
    
    for(int x = 0; x < (*ptrImage)->largeur; x++){
        free((*ptrImage)->bleu[x]);
        free((*ptrImage)->rouge[x]);
        free((*ptrImage)->vert[x]);
        free((*ptrImage)->gris[x]);
    }

    free((*ptrImage)->bleu);
    free((*ptrImage)->rouge);
    free((*ptrImage)->vert);
    free((*ptrImage)->gris);

    free(*ptrImage);
    *ptrImage = NULL;
    
}

Image *chargeImage(char *path){

    if(path == NULL)
        return NULL;

    DonneesImageRGB* img = lisBMPRGB(path);
    
    //Si Image non lue
    if(img == NULL)
        return NULL;
    
    Image* result = alloueImage(img->largeurImage,img->hauteurImage);

    int i = 0;
    for(int y = 0; y < img->hauteurImage; y++){
        for(int x = 0; x < img->largeurImage; x++){
            //Remplis les données
            result->rouge[x][y] = img->donneesRGB[i+2];
            result->vert[x][y] = img->donneesRGB[i+1];
            result->bleu[x][y] = img->donneesRGB[i];

            //Cacule le gris
            result->gris[x][y] = 0.2125*result->rouge[x][y] +0.7154*result->vert[x][y] + 0.0721*result->bleu[x][y];
            i += 3;
        }
    }

    free(img->donneesRGB);
    free(img);
    return result;
}

void sauveImage(Image *monImage, char *nom){
    
    DonneesImageRGB* img = (DonneesImageRGB*) malloc(sizeof(DonneesImageRGB));
    
    img->hauteurImage = monImage->hauteur;
    img->largeurImage = monImage->largeur;
    img->donneesRGB = (unsigned char *)malloc((unsigned int)monImage->largeur*(unsigned int)monImage->hauteur*3*sizeof(unsigned char));

    int i = 0;
    for(int y = 0; y < img->hauteurImage; y++){
        for(int x = 0; x < img->largeurImage; x++){
            img->donneesRGB[i] =monImage->bleu[x][y];
            img->donneesRGB[i+1] = monImage->vert[x][y];
            img->donneesRGB[i+2] =monImage->rouge[x][y];
            i +=3;
        }
    }
    
    ecrisBMPRGB_Dans(img,nom);
    free(img->donneesRGB);
    free(img);
}

void sauveImageNG(Image *monImage, char *nom){
    DonneesImageRGB* img = (DonneesImageRGB*) malloc(sizeof(DonneesImageRGB));
    
    img->hauteurImage = monImage->hauteur;
    img->largeurImage = monImage->largeur;
    img->donneesRGB = (unsigned char *)malloc((unsigned int)monImage->largeur*(unsigned int)monImage->hauteur*3);
    int i = 0;
    for(int y = 0; y < img->hauteurImage; y++){
        for(int x = 0; x < img->largeurImage; x++){
            img->donneesRGB[i] = 0.2125*monImage->rouge[x][y] +0.7154*monImage->vert[x][y] + 0.0721*monImage->bleu[x][y];
            img->donneesRGB[i+1] = 0.2125*monImage->rouge[x][y] +0.7154*monImage->vert[x][y] + 0.0721*monImage->bleu[x][y];
            img->donneesRGB[i+2] = 0.2125*monImage->rouge[x][y] +0.7154*monImage->vert[x][y] + 0.0721*monImage->bleu[x][y];
            i +=3;
            
        }
    }

    ecrisBMPRGB_Dans(img,nom);
    free(img);
}

Image *dupliqueImage(Image *monImage){

    Image* img = alloueImage(monImage->largeur,monImage->hauteur);
    
    for(int y = 0; y < monImage->hauteur; y++){
        for(int x = 0; x < monImage->largeur; x++){
            img->rouge[x][y] = monImage->rouge[x][y];
            img->bleu[x][y] = monImage->bleu[x][y];
            img->vert[x][y] = monImage->vert[x][y];
            img->gris[x][y] = monImage->gris[x][y];
        }
    }

    return img;
}

Image *differenceImage(Image *image1, Image *image2){

    Image *img = alloueImage(image1->largeur,image1->hauteur);

    for(int y = 0; y < image1->hauteur; y++){
        for(int x = 0; x < image1->largeur; x++){
            img->rouge[x][y] = image2->rouge[x][y] - image1->rouge[x][y];
            img->bleu[x][y] = image2->bleu[x][y] - image1->bleu[x][y];
            img->vert[x][y] = image2->vert[x][y] - image1->vert[x][y];
            img->gris[x][y] = image2->gris[x][y] - image1->gris[x][y];
        }
    }

    return img;
}