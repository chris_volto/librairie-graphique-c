#ifndef _VOLTO_GUI_H
#define _VOLTO_GUI_H

#include "./containers/View.h"
#include <SDL.h>

typedef struct Gui_Settings{
    const char *title;
    int x, y, w, h;
    Uint32 win_flags, ren_flags;
    View **views;
}Gui_Settings;

typedef struct Gui{
    SDL_bool visible;
    
    int win_heigth, win_width;
    
    SDL_Window *window;
    SDL_Renderer *renderer;

    View **views;
}Gui;

void freeGui(Gui *Gui);
void freeGuis(Gui **guis);
Gui* initGui(Gui_Settings gui_settings);
int addGui(Gui_Settings gui_settings);
int removeGui(Gui* gui);

#endif