#ifndef _VOLTO_DISPLAYSURFACE_H
#define _VOLTO_DISPLAYSURFACE_H

    #include "../Image.h"
    #include <SDL.h>

typedef struct DisplaySurface{
    SDL_bool visible;
    SDL_bool disabled;
    int x,y,w,h;
    Image *bgImage;
    
    void (*callback)(void *parentGui, void *parentFrame);
}DisplaySurface;

    DisplaySurface * initDisplaySurface(SDL_bool visible,SDL_bool disabled, int x, int y, int w, int h, Image *bgImage, void (*callback)(void *parentGui, void *parentFrame));
    DisplaySurface ** initDisplaySurfaces(int nbreArguments, ...);
    void freeDisplaySurface(DisplaySurface *displaySurface);
    void freeDisplaySurfaces(DisplaySurface **displaySurfaces);

    SDL_bool isOverDisplaySurface(DisplaySurface *displaySurface, void *parent);

#endif