#include "../../include/widgets/Button.h"
#include "../../include/AppState.h"
#include "../../include/App.h"

#include <stdlib.h>
#include <stdio.h>

extern AppState *appState;

void simple_callback(void *parentGui, void *parentFrame){
    SDL_Log("bouton appuye\n");
}

void trigger_error_callback(void *parentGui, void *parentFrame){
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
                         "Erreur.",
                         "Invalid pre-set robot positions.",
                         ((Gui*) parentGui)->window);
}

void affiche_custom_win_callback(void *parentGui, void *parentFrame){
        
    Gui *gui = (Gui*) parentGui;
    
    gui->views[0]->frames[2]->visible = SDL_TRUE;
    gui->views[0]->frames[3]->visible = SDL_FALSE;

    SDL_FlushEvent(SDL_MOUSEBUTTONDOWN);
}

void affiche_textfield_win_callback(void *parentGui, void *parentFrame){

    Gui *gui = (Gui*) parentGui;
    if(gui->views[0]->frames[3]->textareas[0]->text)
    {
        //free(gui->views[0]->frames[4]->textareas[0]->text);
        gui->views[0]->frames[3]->textareas[0]->text = createString("RESET\n------ TEST TEXTFIELD ------");
    }
    else
    {
        gui->views[0]->frames[3]->textareas[0]->text = createString("------ TEST TEXTFIELD ------");
    }

    
    TextArea *textArea = gui->views[0]->frames[3]->textareas[0];
    
    gui->views[0]->frames[2]->visible = SDL_FALSE;
    gui->views[0]->frames[3]->visible = SDL_TRUE;

    char textToAdd[500];

    sprintf(textToAdd,"\n\n> Le scroll est possible");
    addText(textArea,textToAdd);
    update();
    sprintf(textToAdd,"\n\n> var example :%d",42);
    addText(textArea,textToAdd);
    update();
    sprintf(textToAdd,"\n\n>\
    Lorem Ipsum is simply dummy text of the printing and typesetting industry.\
    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s");
    addText(textArea,textToAdd);
    update();

    SDL_FlushEvent(SDL_MOUSEBUTTONDOWN);
}

Button* initButton(int x, int y, int width, int heigth, int outlineWidth, char *text, char* fontName, int fontSize, SDL_Color textColor, SDL_Color bgColor, SDL_Color outlineColor, SDL_bool visible, void (*callback)(void *parentGui, void *parentFrame)){
    Button *button = (Button*) malloc(sizeof(Button));

    button->visible = visible;
    button->disabled = SDL_FALSE;
    button->x=x;
    button->y=y;
    button->text = text;
    button->textColor = textColor;
    button->fontName = fontName;
    button->fontSize = fontSize;
    button->width=width;
    button->heigth=heigth;
    button->outlineWidth=outlineWidth;
    button->bgColor=bgColor;
    button->outlineColor=outlineColor;
    button->callback = callback;

    return button;
}

/**
 * @brief Args must be Button*
 * 
 * @param nbreArguments 
 * @param ... 
 * @return Button** 
 */
Button** initButtons(int nbreArguments, ...){
    va_list ap;
    va_start (ap, nbreArguments);

    Button **buttons = (Button**) malloc(sizeof(Button*)*(nbreArguments+1));

    for (int i = 0; i < nbreArguments; i++)
        buttons[i] = va_arg (ap, Button*);
    va_end (ap);
    
    buttons[nbreArguments] = NULL;

    return buttons;
}

SDL_bool isOverButton(Button *button, void *parent)
{
    if(appState->event.button.x >= ((Frame*) parent)->x+button->x && appState->event.button.x <= ((Frame*) parent)->x+button->x+button->width && appState->event.button.y >= ((Frame*) parent)->y+button->y && appState->event.button.y <= ((Frame*) parent)->y+button->y+button->heigth)
        return SDL_TRUE;
    else
        return SDL_FALSE;
}

void freeButton(Button *button)
{
    free(button);
}

void freeButtons(Button **buttons)
{
    for (int i = 0; buttons[i]; i++)
        freeButton(buttons[i]);
    free(buttons);
    
}