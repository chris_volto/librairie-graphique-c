########################################### COMPILER PARAMS ###########################################
CC = gcc #Which compiler we're using
CCFLAGS = -Wall #COMPILER_FLAGS
LINKING_FLAGS = -I ./SDL2/include -L ./SDL2/lib#-static

ifeq ($(OS),Windows_NT)
    CCFLAGS += -D WIN32
	LINKING_FLAGS += -lmingw32 -mwindows
    ifeq ($(PROCESSOR_ARCHITEW6432),AMD64)
        CCFLAGS += -D AMD64
    else
        ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
            CCFLAGS += -D AMD64
        endif
        ifeq ($(PROCESSOR_ARCHITECTURE),x86)
            CCFLAGS += -D IA32
        endif
    endif
else
    UNAME_S := $(shell uname -s)
    ifeq ($(UNAME_S),Linux)
        CCFLAGS += -D LINUX
    endif
    ifeq ($(UNAME_S),Darwin)
        CCFLAGS += -D OSX
    endif
    UNAME_P := $(shell uname -p)
    ifeq ($(UNAME_P),x86_64)
        CCFLAGS += -D AMD64
    endif
    ifneq ($(filter %86,$(UNAME_P)),)
        CCFLAGS += -D IA32
    endif
    ifneq ($(filter arm%,$(UNAME_P)),)
        CCFLAGS += -D ARM
    endif
endif

LINKING_FLAGS += -lSDL2main -lSDL2 -lSDL2_ttf
########################################### BUILD PATH VARS ###########################################
BUILD_I_PATH = ./build/i/
BUILD_S_PATH = ./build/s/
BUILD_O_PATH = ./build/o/
BUILD_BIN_PATH = ./build/bin/
BUILD_LIB_PATH = ./build/lib/

LIBGRAPHIC_SRC_PATH = ./src/lib_graphic/src/
LIBGRAPHIC_WIDGETS_PATH = ./src/lib_graphic/src/widgets/
LIBGRAPHIC_CONTAINERS_PATH = ./src/lib_graphic/src/containers/
LIB_SRC_PATH = ./src/lib/src/
MAIN_SRC_PATH = ./src/
########################################### BINARY BUILD PARAMS ###########################################
BIN_NAME = Application
FULL_BIN_NAME = $(BUILD_BIN_PATH)$(BIN_NAME)
########################################### LINKING STAGE ###########################################
programs: program
	@echo "Les programmes $^ ont ete crees"
program: App.o AppState.o Gui.o View.o Button.o BmpLib.o OutilsLib.o Image.o DisplaySurface.o Frame.o TextArea.o
	gcc $(BUILD_O_PATH)App.o $(BUILD_O_PATH)AppState.o $(BUILD_O_PATH)Gui.o $(BUILD_O_PATH)View.o $(BUILD_O_PATH)Button.o $(BUILD_O_PATH)BmpLib.o $(BUILD_O_PATH)OutilsLib.o $(BUILD_O_PATH)Image.o $(BUILD_O_PATH)DisplaySurface.o $(BUILD_O_PATH)Frame.o $(BUILD_O_PATH)Partie4.o $(BUILD_O_PATH)Partie5.o $(BUILD_O_PATH)TextArea.o $(BUILD_O_PATH)Partie3.o $(BUILD_O_PATH)Partie6.o -o $(FULL_BIN_NAME) $(LINKING_FLAGS)
########################################### COMPILE STAGE ###########################################
App.o: App.s
	gcc -c $(BUILD_S_PATH)App.s -o $(BUILD_O_PATH)App.o $(CCFLAGS)
AppState.o: AppState.s
	gcc -c $(BUILD_S_PATH)AppState.s -o $(BUILD_O_PATH)AppState.o $(CCFLAGS)
Gui.o: Gui.s
	gcc -c $(BUILD_S_PATH)Gui.s -o $(BUILD_O_PATH)Gui.o $(CCFLAGS)
View.o: View.s
	gcc -c $(BUILD_S_PATH)View.s -o $(BUILD_O_PATH)View.o $(CCFLAGS)
Button.o: Button.s
	gcc -c $(BUILD_S_PATH)Button.s -o $(BUILD_O_PATH)Button.o $(CCFLAGS)
BmpLib.o: BmpLib.s
	gcc -c $(BUILD_S_PATH)BmpLib.s -o $(BUILD_O_PATH)BmpLib.o $(CCFLAGS)
OutilsLib.o: OutilsLib.s
	gcc -c $(BUILD_S_PATH)OutilsLib.s -o $(BUILD_O_PATH)OutilsLib.o $(CCFLAGS)
Image.o: Image.s
	gcc -c $(BUILD_S_PATH)Image.s -o $(BUILD_O_PATH)Image.o $(CCFLAGS)
DisplaySurface.o: DisplaySurface.s
	gcc -c $(BUILD_S_PATH)DisplaySurface.s -o $(BUILD_O_PATH)DisplaySurface.o $(CCFLAGS)
Frame.o: Frame.s
	gcc -c $(BUILD_S_PATH)Frame.s -o $(BUILD_O_PATH)Frame.o $(CCFLAGS)
TextArea.o: TextArea.s
	gcc -c $(BUILD_S_PATH)TextArea.s -o $(BUILD_O_PATH)TextArea.o $(CCFLAGS)
########################################### ASSEMBLER STAGE ###########################################
App.s: App.i
	gcc -S $(BUILD_I_PATH)App.i -o $(BUILD_S_PATH)App.s
AppState.s: AppState.i
	gcc -S $(BUILD_I_PATH)AppState.i -o $(BUILD_S_PATH)AppState.s
Gui.s: Gui.i
	gcc -S $(BUILD_I_PATH)Gui.i -o $(BUILD_S_PATH)Gui.s
View.s: View.i
	gcc -S $(BUILD_I_PATH)View.i -o $(BUILD_S_PATH)View.s
Button.s: Button.i
	gcc -S $(BUILD_I_PATH)Button.i -o $(BUILD_S_PATH)Button.s
BmpLib.s: BmpLib.i
	gcc -S $(BUILD_I_PATH)BmpLib.i -o $(BUILD_S_PATH)BmpLib.s
OutilsLib.s: OutilsLib.i
	gcc -S $(BUILD_I_PATH)OutilsLib.i -o $(BUILD_S_PATH)OutilsLib.s
Image.s: Image.i
	gcc -S $(BUILD_I_PATH)Image.i -o $(BUILD_S_PATH)Image.s
DisplaySurface.s: DisplaySurface.i
	gcc -S $(BUILD_I_PATH)DisplaySurface.i -o $(BUILD_S_PATH)DisplaySurface.s
Frame.s: Frame.i
	gcc -S $(BUILD_I_PATH)Frame.i -o $(BUILD_S_PATH)Frame.s
TextArea.s: TextArea.i
	gcc -S $(BUILD_I_PATH)TextArea.i -o $(BUILD_S_PATH)TextArea.s
########################################### PREPROCESSING STAGE ###########################################

### FRONTEND ###
App.i: 
	gcc -E $(MAIN_SRC_PATH)App.c -o $(BUILD_I_PATH)App.i -I ./SDL2/include
AppState.i: 
	gcc -E $(LIBGRAPHIC_SRC_PATH)AppState.c -o $(BUILD_I_PATH)AppState.i -I ./SDL2/include
Gui.i: 
	gcc -E $(LIBGRAPHIC_SRC_PATH)Gui.c -o $(BUILD_I_PATH)Gui.i -I ./SDL2/include
Image.i:
	gcc -E $(LIBGRAPHIC_SRC_PATH)Image.c -o $(BUILD_I_PATH)Image.i
BmpLib.i:
	gcc -E $(LIBGRAPHIC_SRC_PATH)BmpLib.c -o $(BUILD_I_PATH)BmpLib.i
OutilsLib.i:
	gcc -E $(LIBGRAPHIC_SRC_PATH)OutilsLib.c -o $(BUILD_I_PATH)OutilsLib.i

### FRONTEND - CONTAINERS ###
View.i: 
	gcc -E $(LIBGRAPHIC_CONTAINERS_PATH)View.c -o $(BUILD_I_PATH)View.i -I ./SDL2/include
Frame.i:
	gcc -E $(LIBGRAPHIC_CONTAINERS_PATH)Frame.c -o $(BUILD_I_PATH)Frame.i -I ./SDL2/include

### FRONTEND - WIDGETS ###
Button.i: 
	gcc -E $(LIBGRAPHIC_WIDGETS_PATH)Button.c -o $(BUILD_I_PATH)Button.i -I ./SDL2/include
DisplaySurface.i:
	gcc -E $(LIBGRAPHIC_WIDGETS_PATH)DisplaySurface.c -o $(BUILD_I_PATH)DisplaySurface.i -I ./SDL2/include
TextArea.i:
	gcc -E $(LIBGRAPHIC_WIDGETS_PATH)TextArea.c -o $(BUILD_I_PATH)TextArea.i -I ./SDL2/include

### BACKEND ###

clean:
ifeq ($(OS),Windows_NT)
	del /s build\o\*.o
else
	rm -f ./build/i/*.i ./build/o/*.o ./build/s/*.s
endif


deepclean: clean
ifeq ($(OS),Windows_NT)
	del /s build\bin\$(BIN_NAME).exe
else
	rm -f $(BIN_FULL_NAME)
endif