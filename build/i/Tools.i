# 1 "./src/lib/src/Tools.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "./src/lib/src/Tools.c"
# 1 "./src/lib/src/../include/Tools.h" 1



# 1 "./src/lib/src/../include/Image.h" 1



    typedef struct Image {
        int largeur, hauteur;
        short int **rouge, **vert, **bleu, **gris;
    } Image;

    Image *alloueImage(int largeur, int hauteur);
    void libereImage(Image **ptrImage);
    Image *chargeImage(char *nom);
    void sauveImage(Image *monImage, char *nom);
    void sauveImageNG(Image *monImage, char *nom);
    Image *dupliqueImage(Image *monImage);
    Image *differenceImage(Image *image1, Image *image2);
# 5 "./src/lib/src/../include/Tools.h" 2
# 1 "./src/lib/src/../include/FonctionsPartie4.h" 1
# 9 "./src/lib/src/../include/FonctionsPartie4.h"
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdbool.h" 1 3 4
# 10 "./src/lib/src/../include/FonctionsPartie4.h" 2
# 1 "c:\\mingw\\include\\stdio.h" 1 3
# 38 "c:\\mingw\\include\\stdio.h" 3
       
# 39 "c:\\mingw\\include\\stdio.h" 3
# 55 "c:\\mingw\\include\\stdio.h" 3
# 1 "c:\\mingw\\include\\_mingw.h" 1 3
# 55 "c:\\mingw\\include\\_mingw.h" 3
       
# 56 "c:\\mingw\\include\\_mingw.h" 3
# 66 "c:\\mingw\\include\\_mingw.h" 3
# 1 "c:\\mingw\\include\\msvcrtver.h" 1 3
# 35 "c:\\mingw\\include\\msvcrtver.h" 3
       
# 36 "c:\\mingw\\include\\msvcrtver.h" 3
# 67 "c:\\mingw\\include\\_mingw.h" 2 3






# 1 "c:\\mingw\\include\\w32api.h" 1 3
# 35 "c:\\mingw\\include\\w32api.h" 3
       
# 36 "c:\\mingw\\include\\w32api.h" 3
# 59 "c:\\mingw\\include\\w32api.h" 3
# 1 "c:\\mingw\\include\\sdkddkver.h" 1 3
# 35 "c:\\mingw\\include\\sdkddkver.h" 3
       
# 36 "c:\\mingw\\include\\sdkddkver.h" 3
# 60 "c:\\mingw\\include\\w32api.h" 2 3
# 74 "c:\\mingw\\include\\_mingw.h" 2 3
# 174 "c:\\mingw\\include\\_mingw.h" 3
# 1 "c:\\mingw\\include\\features.h" 1 3
# 39 "c:\\mingw\\include\\features.h" 3
       
# 40 "c:\\mingw\\include\\features.h" 3
# 175 "c:\\mingw\\include\\_mingw.h" 2 3
# 56 "c:\\mingw\\include\\stdio.h" 2 3
# 68 "c:\\mingw\\include\\stdio.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 216 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4

# 216 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4
typedef unsigned int size_t;
# 328 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4
typedef short unsigned int wchar_t;
# 357 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4
typedef short unsigned int wint_t;
# 69 "c:\\mingw\\include\\stdio.h" 2 3
# 95 "c:\\mingw\\include\\stdio.h" 3
# 1 "c:\\mingw\\include\\sys/types.h" 1 3
# 34 "c:\\mingw\\include\\sys/types.h" 3
       
# 35 "c:\\mingw\\include\\sys/types.h" 3
# 62 "c:\\mingw\\include\\sys/types.h" 3
  typedef long __off32_t;




  typedef __off32_t _off_t;







  typedef _off_t off_t;
# 91 "c:\\mingw\\include\\sys/types.h" 3
  typedef long long __off64_t;






  typedef __off64_t off64_t;
# 115 "c:\\mingw\\include\\sys/types.h" 3
  typedef int _ssize_t;







  typedef _ssize_t ssize_t;
# 96 "c:\\mingw\\include\\stdio.h" 2 3






# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 1 3 4
# 40 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 3 4
typedef __builtin_va_list __gnuc_va_list;
# 103 "c:\\mingw\\include\\stdio.h" 2 3
# 210 "c:\\mingw\\include\\stdio.h" 3
typedef struct _iobuf
{
  char *_ptr;
  int _cnt;
  char *_base;
  int _flag;
  int _file;
  int _charbuf;
  int _bufsiz;
  char *_tmpfname;
} FILE;
# 239 "c:\\mingw\\include\\stdio.h" 3
extern __attribute__((__dllimport__)) FILE _iob[];
# 252 "c:\\mingw\\include\\stdio.h" 3








 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * fopen (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * freopen (const char *, const char *, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fflush (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fclose (FILE *);






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int remove (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int rename (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * tmpfile (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char * tmpnam (char *);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_tempnam (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _rmtmp (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _unlink (const char *);
# 289 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char * tempnam (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int rmtmp (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int unlink (const char *);



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int setvbuf (FILE *, char *, int, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void setbuf (FILE *, char *);
# 342 "c:\\mingw\\include\\stdio.h" 3
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,2,3))) __mingw_fprintf(FILE*, const char*, ...);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,1,2))) __mingw_printf(const char*, ...);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,2,3))) __mingw_sprintf(char*, const char*, ...);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,3,4))) __mingw_snprintf(char*, size_t, const char*, ...);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,2,0))) __mingw_vfprintf(FILE*, const char*, __builtin_va_list);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,1,0))) __mingw_vprintf(const char*, __builtin_va_list);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,2,0))) __mingw_vsprintf(char*, const char*, __builtin_va_list);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,3,0))) __mingw_vsnprintf(char*, size_t, const char*, __builtin_va_list);
# 376 "c:\\mingw\\include\\stdio.h" 3
extern unsigned int _mingw_output_format_control( unsigned int, unsigned int );
# 461 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fprintf (FILE *, const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int printf (const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int sprintf (char *, const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vfprintf (FILE *, const char *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vprintf (const char *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vsprintf (char *, const char *, __builtin_va_list);
# 479 "c:\\mingw\\include\\stdio.h" 3
static __inline__ __attribute__((__cdecl__)) __attribute__((__nothrow__))
int snprintf (char *__buf, size_t __len, const char *__format, ...)
{
  register int __retval;
  __builtin_va_list __local_argv; __builtin_va_start( __local_argv, __format );
  __retval = __mingw_vsnprintf( __buf, __len, __format, __local_argv );
  __builtin_va_end( __local_argv );
  return __retval;
}

static __inline__ __attribute__((__cdecl__)) __attribute__((__nothrow__))
int vsnprintf (char *__buf, size_t __len, const char *__format, __builtin_va_list __local_argv)
{
  return __mingw_vsnprintf( __buf, __len, __format, __local_argv );
}
# 513 "c:\\mingw\\include\\stdio.h" 3
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,2,3))) __msvcrt_fprintf(FILE *, const char *, ...);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,1,2))) __msvcrt_printf(const char *, ...);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,2,3))) __msvcrt_sprintf(char *, const char *, ...);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,2,0))) __msvcrt_vfprintf(FILE *, const char *, __builtin_va_list);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,1,0))) __msvcrt_vprintf(const char *, __builtin_va_list);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,2,0))) __msvcrt_vsprintf(char *, const char *, __builtin_va_list);






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _snprintf (char *, size_t, const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _vsnprintf (char *, size_t, const char *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _vscprintf (const char *, __builtin_va_list);
# 536 "c:\\mingw\\include\\stdio.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,3,4)))
int snprintf (char *, size_t, const char *, ...);

__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,3,0)))
int vsnprintf (char *, size_t, const char *, __builtin_va_list);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vscanf (const char * __restrict__, __builtin_va_list);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vfscanf (FILE * __restrict__, const char * __restrict__, __builtin_va_list);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vsscanf (const char * __restrict__, const char * __restrict__, __builtin_va_list);
# 679 "c:\\mingw\\include\\stdio.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) ssize_t
getdelim (char ** __restrict__, size_t * __restrict__, int, FILE * __restrict__);

__attribute__((__cdecl__)) __attribute__((__nothrow__)) ssize_t
getline (char ** __restrict__, size_t * __restrict__, FILE * __restrict__);
# 699 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fscanf (FILE *, const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int scanf (const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int sscanf (const char *, const char *, ...);



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fgetc (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char * fgets (char *, int, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fputc (int, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fputs (const char *, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char * gets (char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int puts (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int ungetc (int, FILE *);
# 720 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _filbuf (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _flsbuf (int, FILE *);



extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getc (FILE *);
extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getc (FILE * __F)
{
  return (--__F->_cnt >= 0)
    ? (int) (unsigned char) *__F->_ptr++
    : _filbuf (__F);
}

extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putc (int, FILE *);
extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putc (int __c, FILE * __F)
{
  return (--__F->_cnt >= 0)
    ? (int) (unsigned char) (*__F->_ptr++ = (char)__c)
    : _flsbuf (__c, __F);
}

extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getchar (void);
extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getchar (void)
{
  return (--(&_iob[0])->_cnt >= 0)
    ? (int) (unsigned char) *(&_iob[0])->_ptr++
    : _filbuf ((&_iob[0]));
}

extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putchar(int);
extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putchar(int __c)
{
  return (--(&_iob[1])->_cnt >= 0)
    ? (int) (unsigned char) (*(&_iob[1])->_ptr++ = (char)__c)
    : _flsbuf (__c, (&_iob[1]));}
# 767 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t fread (void *, size_t, size_t, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t fwrite (const void *, size_t, size_t, FILE *);



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fseek (FILE *, long, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long ftell (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void rewind (FILE *);
# 821 "c:\\mingw\\include\\stdio.h" 3
typedef union { long long __value; __off64_t __offset; } fpos_t;




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fgetpos (FILE *, fpos_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fsetpos (FILE *, const fpos_t *);
# 862 "c:\\mingw\\include\\stdio.h" 3
int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __mingw_fseeki64 (FILE *, long long, int);
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fseeki64 (FILE *__f, long long __o, int __w)
{ return __mingw_fseeki64 (__f, __o, __w); }


long long __attribute__((__cdecl__)) __attribute__((__nothrow__)) __mingw_ftelli64 (FILE *);
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__cdecl__)) long long __attribute__((__nothrow__)) _ftelli64 (FILE *__file )
{ return __mingw_ftelli64 (__file); }





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int feof (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int ferror (FILE *);
# 886 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void clearerr (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void perror (const char *);





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _popen (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _pclose (FILE *);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * popen (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int pclose (FILE *);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _flushall (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fgetchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fputchar (int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _fdopen (int, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fileno (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fcloseall (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _fsopen (const char *, const char *, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _getmaxstdio (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _setmaxstdio (int);
# 936 "c:\\mingw\\include\\stdio.h" 3
unsigned int __attribute__((__cdecl__)) __mingw_get_output_format (void);
unsigned int __attribute__((__cdecl__)) __mingw_set_output_format (unsigned int);







int __attribute__((__cdecl__)) __mingw_get_printf_count_output (void);
int __attribute__((__cdecl__)) __mingw_set_printf_count_output (int);
# 962 "c:\\mingw\\include\\stdio.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) unsigned int __attribute__((__cdecl__)) _get_output_format (void)
{ return __mingw_get_output_format (); }

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) unsigned int __attribute__((__cdecl__)) _set_output_format (unsigned int __style)
{ return __mingw_set_output_format (__style); }
# 987 "c:\\mingw\\include\\stdio.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) int __attribute__((__cdecl__)) _get_printf_count_output (void)
{ return 0 ? 1 : __mingw_get_printf_count_output (); }

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) int __attribute__((__cdecl__)) _set_printf_count_output (int __mode)
{ return 0 ? 1 : __mingw_set_printf_count_output (__mode); }



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fgetchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fputchar (int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * fdopen (int, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fileno (FILE *);
# 1007 "c:\\mingw\\include\\stdio.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) FILE * __attribute__((__cdecl__)) __attribute__((__nothrow__)) fopen64 (const char *, const char *);
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
FILE * __attribute__((__cdecl__)) __attribute__((__nothrow__)) fopen64 (const char * filename, const char * mode)
{ return fopen (filename, mode); }

int __attribute__((__cdecl__)) __attribute__((__nothrow__)) fseeko64 (FILE *, __off64_t, int);
# 1028 "c:\\mingw\\include\\stdio.h" 3
__off64_t __attribute__((__cdecl__)) __attribute__((__nothrow__)) ftello64 (FILE *);
# 1041 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fwprintf (FILE *, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wprintf (const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vfwprintf (FILE *, const wchar_t *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vwprintf (const wchar_t *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _snwprintf (wchar_t *, size_t, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _vscwprintf (const wchar_t *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _vsnwprintf (wchar_t *, size_t, const wchar_t *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fwscanf (FILE *, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wscanf (const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int swscanf (const wchar_t *, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t fgetwc (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t fputwc (wchar_t, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t ungetwc (wchar_t, FILE *);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int swprintf (wchar_t *, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vswprintf (wchar_t *, const wchar_t *, __builtin_va_list);



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t * fgetws (wchar_t *, int, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fputws (const wchar_t *, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t getwc (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t getwchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t putwc (wint_t, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t putwchar (wint_t);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t * _getws (wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _putws (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wfdopen(int, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wfopen (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wfreopen (const wchar_t *, const wchar_t *, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wfsopen (const wchar_t *, const wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t * _wtmpnam (wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t * _wtempnam (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wrename (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wremove (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _wperror (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wpopen (const wchar_t *, const wchar_t *);






__attribute__((__cdecl__)) __attribute__((__nothrow__)) int snwprintf (wchar_t *, size_t, const wchar_t *, ...);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int vsnwprintf (wchar_t *, size_t, const wchar_t *, __builtin_va_list);
# 1099 "c:\\mingw\\include\\stdio.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int vwscanf (const wchar_t *__restrict__, __builtin_va_list);
__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vfwscanf (FILE *__restrict__, const wchar_t *__restrict__, __builtin_va_list);
__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vswscanf (const wchar_t *__restrict__, const wchar_t * __restrict__, __builtin_va_list);






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * wpopen (const wchar_t *, const wchar_t *);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t _fgetwchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t _fputwchar (wint_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _getw (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _putw (int, FILE *);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t fgetwchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t fputwchar (wint_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getw (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putw (int, FILE *);





# 11 "./src/lib/src/../include/FonctionsPartie4.h" 2
# 20 "./src/lib/src/../include/FonctionsPartie4.h"
    
# 20 "./src/lib/src/../include/FonctionsPartie4.h"
   typedef struct Couleur{

        short int rouge, vert, bleu;
    }Couleur;






    typedef struct Palette {
        int nb;
        Couleur *pal;
    }Palette;





    typedef struct Motif{

        short int forme;
        short int config;

        int cfond;
        int cobjet;
    }Motif;







    typedef struct noeudMotif{

        struct noeudMotif *NO;
        struct noeudMotif *NE;
        struct noeudMotif *SO;
        struct noeudMotif *SE;

        Motif motif;
        int x,y;
        int largeur;

    }noeudMotif;







    Palette* creePalette(Image *img);
# 85 "./src/lib/src/../include/FonctionsPartie4.h"
    Motif identifieMotif(Image *img, Palette *palette, int x, int y, int largeur);
# 97 "./src/lib/src/../include/FonctionsPartie4.h"
    noeudMotif* creeArbreMotifs(Image *img, Palette *palette, int largeur, int x, int y);





        void fprintfArbre(FILE *fp, noeudMotif* ptrRcn);







    void sauveDescriptionImage(noeudMotif *rcn, Palette* pal);






    void affichePalette(Palette *pal);

    
# 120 "./src/lib/src/../include/FonctionsPartie4.h" 3 4
   _Bool 
# 120 "./src/lib/src/../include/FonctionsPartie4.h"
        compareCouleur (Couleur c1, Couleur c2);
    
# 121 "./src/lib/src/../include/FonctionsPartie4.h" 3 4
   _Bool 
# 121 "./src/lib/src/../include/FonctionsPartie4.h"
        compareCouleurVariadic (Couleur c1, Couleur c2, int nbreArgumentsOptionnels, ...);
    
# 122 "./src/lib/src/../include/FonctionsPartie4.h" 3 4
   _Bool 
# 122 "./src/lib/src/../include/FonctionsPartie4.h"
        sontDesCouleursVariadic(int nbreArguments, ...);
    
# 123 "./src/lib/src/../include/FonctionsPartie4.h" 3 4
   _Bool 
# 123 "./src/lib/src/../include/FonctionsPartie4.h"
        comparePalette(Palette p1, Palette p2);
    
# 124 "./src/lib/src/../include/FonctionsPartie4.h" 3 4
   _Bool 
# 124 "./src/lib/src/../include/FonctionsPartie4.h"
        compareMotif(Motif m1, Motif m2);
    
# 125 "./src/lib/src/../include/FonctionsPartie4.h" 3 4
   _Bool 
# 125 "./src/lib/src/../include/FonctionsPartie4.h"
        estCouleurDans(Couleur *arr, int arrSize, Couleur couleur);
    void libereArbreMotif(noeudMotif *ptrRcn);
    Couleur estCouleurUniformeDansCarre(Image *img, int x1, int y1, int x2, int y2);
    Couleur estCouleurUniforme(Image *img, int depart_x, int depart_y, int largeur, int hauteur);
# 6 "./src/lib/src/../include/Tools.h" 2


    typedef struct Point{
        unsigned int x,y;
    }Point;
    
# 11 "./src/lib/src/../include/Tools.h" 3 4
   _Bool 
# 11 "./src/lib/src/../include/Tools.h"
        comparePoints(Point p1, Point p2);
# 20 "./src/lib/src/../include/Tools.h"
    int positionVoisinage(Point p1, Point p2);
    
# 21 "./src/lib/src/../include/Tools.h" 3 4
   _Bool 
# 21 "./src/lib/src/../include/Tools.h"
        comparePointsVariadic (Point p1, Point p2, int nbreArgumentsOptionnels, ...);

    typedef struct MaillonTraj{
        Point position;
        struct MaillonTraj *next;
    }MaillonTraj;

    MaillonTraj* creeMaillon(const Point position);
    void insererMaillonAvant(MaillonTraj *ptrTete, const int index, MaillonTraj *maillonAInserer);
    void ajouterMaillon(MaillonTraj *ptrTete, MaillonTraj *maillonAAjouter);
    void supprimerMaillonParValeur(MaillonTraj *ptrTete, const Point position);
    void supprimerMaillon(MaillonTraj *ptrTete, MaillonTraj **maillon);
    int trouverIndexParValeur(MaillonTraj *ptrTete, const Point position);
    void libereMaillon(MaillonTraj **maillon);

    typedef struct Robot{
        MaillonTraj *ptrTete;
        Point position;
        Point depart;
        Point arrivee;
    }Robot;

Robot* calculeTrajectoire(Image *img, Image *imgSansTrajectoires, Palette *palette, Point depart, Point arrivee);
    typedef struct Emplacement{
        Point origine;
        unsigned int largeurMotif;
    }Emplacement;

    void dessineMotif1(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 49 "./src/lib/src/../include/Tools.h" 3 4
                                                                                                                        _Bool 
# 49 "./src/lib/src/../include/Tools.h"
                                                                                                                             trajectoire);
    void dessineMotif2(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 50 "./src/lib/src/../include/Tools.h" 3 4
                                                                                                                       _Bool 
# 50 "./src/lib/src/../include/Tools.h"
                                                                                                                            trajectoire);
    void dessineMotif3(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 51 "./src/lib/src/../include/Tools.h" 3 4
                                                                                                                       _Bool 
# 51 "./src/lib/src/../include/Tools.h"
                                                                                                                            trajectoire);
    void dessineMotif4(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 52 "./src/lib/src/../include/Tools.h" 3 4
                                                                                                                       _Bool 
# 52 "./src/lib/src/../include/Tools.h"
                                                                                                                            trajectoire);
    void dessineMotif5(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 53 "./src/lib/src/../include/Tools.h" 3 4
                                                                                                                       _Bool 
# 53 "./src/lib/src/../include/Tools.h"
                                                                                                                            trajectoire);
    void dessineMotif6(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 54 "./src/lib/src/../include/Tools.h" 3 4
                                                                                                                       _Bool 
# 54 "./src/lib/src/../include/Tools.h"
                                                                                                                            trajectoire);

    void dessineMotifNoeud(const Image *img,const Palette palette, const noeudMotif *noeud, const int formeId, const 
# 56 "./src/lib/src/../include/Tools.h" 3 4
                                                                                                                    _Bool 
# 56 "./src/lib/src/../include/Tools.h"
                                                                                                                         trajectoire);

    void dessineCarte(const Image *img, const Palette palette, const noeudMotif *racine, const 
# 58 "./src/lib/src/../include/Tools.h" 3 4
                                                                                              _Bool 
# 58 "./src/lib/src/../include/Tools.h"
                                                                                                   trajectoire);
    void sauveDescriptionChemin(Robot *robot);
# 2 "./src/lib/src/Tools.c" 2
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 1 3 4
# 99 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 3 4

# 99 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 3 4
typedef __gnuc_va_list va_list;
# 3 "./src/lib/src/Tools.c" 2
# 1 "c:\\mingw\\include\\stdlib.h" 1 3
# 34 "c:\\mingw\\include\\stdlib.h" 3
       
# 35 "c:\\mingw\\include\\stdlib.h" 3
# 55 "c:\\mingw\\include\\stdlib.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 56 "c:\\mingw\\include\\stdlib.h" 2 3
# 90 "c:\\mingw\\include\\stdlib.h" 3

# 99 "c:\\mingw\\include\\stdlib.h" 3
extern int _argc;
extern char **_argv;




extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) int *__p___argc(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) char ***__p___argv(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t ***__p___wargv(void);
# 142 "c:\\mingw\\include\\stdlib.h" 3
   extern __attribute__((__dllimport__)) int __mb_cur_max;
# 166 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int *_errno(void);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int *__doserrno(void);







extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) char ***__p__environ(void);

extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t ***__p__wenviron(void);
# 202 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) int _sys_nerr;
# 227 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) char *_sys_errlist[];
# 238 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__osver(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__winver(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__winmajor(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__winminor(void);
# 250 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) unsigned int _osver;
extern __attribute__((__dllimport__)) unsigned int _winver;
extern __attribute__((__dllimport__)) unsigned int _winmajor;
extern __attribute__((__dllimport__)) unsigned int _winminor;
# 289 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char **__p__pgmptr(void);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t **__p__wpgmptr(void);
# 325 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) int _fmode;
# 335 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int atoi (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long atol (const char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double strtod (const char *, char **);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double atof (const char *);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double _wtof (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wtoi (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long _wtol (const wchar_t *);
# 378 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__))
float strtof (const char *__restrict__, char **__restrict__);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
long double strtold (const char *__restrict__, char **__restrict__);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long strtol (const char *, char **, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned long strtoul (const char *, char **, int);







 __attribute__((__cdecl__)) __attribute__((__nothrow__))
long wcstol (const wchar_t *, wchar_t **, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
unsigned long wcstoul (const wchar_t *, wchar_t **, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double wcstod (const wchar_t *, wchar_t **);





__attribute__((__cdecl__)) __attribute__((__nothrow__))
float wcstof (const wchar_t *__restrict__, wchar_t **__restrict__);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
long double wcstold (const wchar_t *__restrict__, wchar_t **__restrict__);
# 451 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_wgetenv (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wputenv (const wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _wsearchenv (const wchar_t *, const wchar_t *, wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wsystem (const wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _wmakepath (wchar_t *, const wchar_t *, const wchar_t *, const wchar_t *,
    const wchar_t *
  );

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _wsplitpath (const wchar_t *, wchar_t *, wchar_t *, wchar_t *, wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
wchar_t *_wfullpath (wchar_t *, const wchar_t *, size_t);





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t wcstombs (char *, const wchar_t *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wctomb (char *, wchar_t);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int mblen (const char *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t mbstowcs (wchar_t *, const char *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int mbtowc (wchar_t *, const char *, size_t);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int rand (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void srand (unsigned int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void abort (void) __attribute__((__noreturn__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void exit (int) __attribute__((__noreturn__));



int __attribute__((__cdecl__)) __attribute__((__nothrow__)) atexit (void (*)(void));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int system (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *getenv (const char *);






# 1 "c:\\mingw\\include\\alloca.h" 1 3
# 43 "c:\\mingw\\include\\alloca.h" 3
       
# 44 "c:\\mingw\\include\\alloca.h" 3
# 54 "c:\\mingw\\include\\alloca.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 55 "c:\\mingw\\include\\alloca.h" 2 3


# 80 "c:\\mingw\\include\\alloca.h" 3
void *alloca( size_t );







void *_alloca( size_t );



# 500 "c:\\mingw\\include\\stdlib.h" 2 3


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *calloc (size_t, size_t) __attribute__((__malloc__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *malloc (size_t) __attribute__((__malloc__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *realloc (void *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void free (void *);
# 514 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) void *__mingw_realloc (void *, size_t);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) void __mingw_free (void *);






 __attribute__((__cdecl__)) void *bsearch
(const void *, const void *, size_t, size_t, int (*)(const void *, const void *));

 __attribute__((__cdecl__)) void qsort
(void *, size_t, size_t, int (*)(const void *, const void *));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int abs (int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long labs (long) __attribute__((__const__));
# 538 "c:\\mingw\\include\\stdlib.h" 3
typedef struct { int quot, rem; } div_t;
typedef struct { long quot, rem; } ldiv_t;

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) div_t div (int, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) ldiv_t ldiv (long, long) __attribute__((__const__));






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _exit (int) __attribute__((__noreturn__));





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long long _atoi64 (const char *);
# 564 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _beep (unsigned int, unsigned int) __attribute__((__deprecated__));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _seterrormode (int) __attribute__((__deprecated__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _sleep (unsigned long) __attribute__((__deprecated__));



typedef int (* _onexit_t)(void);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) _onexit_t _onexit( _onexit_t );

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _putenv (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _searchenv (const char *, const char *, char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_ecvt (double, int, int *, int *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_fcvt (double, int, int *, int *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_gcvt (double, int, char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _makepath (char *, const char *, const char *, const char *, const char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _splitpath (const char *, char *, char *, char *, char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_fullpath (char*, const char*, size_t);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_itoa (int, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_ltoa (long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_ultoa(unsigned long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_itow (int, wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_ltow (long, wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_ultow (unsigned long, wchar_t *, int);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* _i64toa (long long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* _ui64toa (unsigned long long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long long _wtoi64 (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t* _i64tow (long long, wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t* _ui64tow (unsigned long long, wchar_t *, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int (_rotl)(unsigned int, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int (_rotr)(unsigned int, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned long (_lrotl)(unsigned long, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned long (_lrotr)(unsigned long, int) __attribute__((__const__));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _set_error_mode (int);
# 647 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putenv (const char*);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void searchenv (const char*, const char*, char*);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* itoa (int, char*, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* ltoa (long, char*, int);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* ecvt (double, int, int*, int*);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* fcvt (double, int, int*, int*);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* gcvt (double, int, char*);
# 668 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) void _Exit(int) __attribute__((__noreturn__));






typedef struct { long long quot, rem; } lldiv_t;
__attribute__((__cdecl__)) __attribute__((__nothrow__)) lldiv_t lldiv (long long, long long) __attribute__((__const__));

__attribute__((__cdecl__)) __attribute__((__nothrow__)) long long llabs (long long);
# 689 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__))
long long strtoll (const char *__restrict__, char **__restrict, int);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
unsigned long long strtoull (const char *__restrict__, char **__restrict__, int);





__attribute__((__cdecl__)) __attribute__((__nothrow__)) long long atoll (const char *);
# 745 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) long long wtoll (const wchar_t *);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) char *lltoa (long long, char *, int);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) char *ulltoa (unsigned long long , char *, int);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) wchar_t *lltow (long long, wchar_t *, int);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) wchar_t *ulltow (unsigned long long, wchar_t *, int);
# 785 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int mkstemp (char *);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int __mingw_mkstemp (int, char *);
# 827 "c:\\mingw\\include\\stdlib.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int mkstemp (char *__filename_template)
{ return __mingw_mkstemp( 0, __filename_template ); }
# 838 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) char *mkdtemp (char *);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) char *__mingw_mkdtemp (char *);

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) char *mkdtemp (char *__dirname_template)
{ return __mingw_mkdtemp( __dirname_template ); }






__attribute__((__cdecl__)) __attribute__((__nothrow__)) int setenv( const char *, const char *, int );
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int unsetenv( const char * );

__attribute__((__cdecl__)) __attribute__((__nothrow__)) int __mingw_setenv( const char *, const char *, int );

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int setenv( const char *__n, const char *__v, int __f )
{ return __mingw_setenv( __n, __v, __f ); }

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int unsetenv( const char *__name )
{ return __mingw_setenv( __name, ((void *)0), 1 ); }





# 4 "./src/lib/src/Tools.c" 2
# 1 "c:\\mingw\\include\\math.h" 1 3
# 35 "c:\\mingw\\include\\math.h" 3
       
# 36 "c:\\mingw\\include\\math.h" 3
# 111 "c:\\mingw\\include\\math.h" 3

# 151 "c:\\mingw\\include\\math.h" 3
struct _exception
{
  int type;
  char *name;
  double arg1;
  double arg2;
  double retval;
};

 double __attribute__((__cdecl__)) sin (double);
 double __attribute__((__cdecl__)) cos (double);
 double __attribute__((__cdecl__)) tan (double);
 double __attribute__((__cdecl__)) sinh (double);
 double __attribute__((__cdecl__)) cosh (double);
 double __attribute__((__cdecl__)) tanh (double);
 double __attribute__((__cdecl__)) asin (double);
 double __attribute__((__cdecl__)) acos (double);
 double __attribute__((__cdecl__)) atan (double);
 double __attribute__((__cdecl__)) atan2 (double, double);
 double __attribute__((__cdecl__)) exp (double);
 double __attribute__((__cdecl__)) log (double);
 double __attribute__((__cdecl__)) log10 (double);
 double __attribute__((__cdecl__)) pow (double, double);
 double __attribute__((__cdecl__)) sqrt (double);
 double __attribute__((__cdecl__)) ceil (double);
 double __attribute__((__cdecl__)) floor (double);
 double __attribute__((__cdecl__)) fabs (double);
 double __attribute__((__cdecl__)) ldexp (double, int);
 double __attribute__((__cdecl__)) frexp (double, int*);
 double __attribute__((__cdecl__)) modf (double, double*);
 double __attribute__((__cdecl__)) fmod (double, double);
# 225 "c:\\mingw\\include\\math.h" 3
struct _complex
{



  double x;
  double y;
};

 double __attribute__((__cdecl__)) _cabs (struct _complex);

 double __attribute__((__cdecl__)) _hypot (double, double);
 double __attribute__((__cdecl__)) _j0 (double);
 double __attribute__((__cdecl__)) _j1 (double);
 double __attribute__((__cdecl__)) _jn (int, double);
 double __attribute__((__cdecl__)) _y0 (double);
 double __attribute__((__cdecl__)) _y1 (double);
 double __attribute__((__cdecl__)) _yn (int, double);
 int __attribute__((__cdecl__)) _matherr (struct _exception *);
# 252 "c:\\mingw\\include\\math.h" 3
 double __attribute__((__cdecl__)) _chgsign (double);
 double __attribute__((__cdecl__)) _copysign (double, double);
 double __attribute__((__cdecl__)) _logb (double);
 double __attribute__((__cdecl__)) _nextafter (double, double);
 double __attribute__((__cdecl__)) _scalb (double, long);

 int __attribute__((__cdecl__)) _finite (double);
 int __attribute__((__cdecl__)) _fpclass (double);
 int __attribute__((__cdecl__)) _isnan (double);
# 269 "c:\\mingw\\include\\math.h" 3
 double __attribute__((__cdecl__)) j0 (double);
 double __attribute__((__cdecl__)) j1 (double);
 double __attribute__((__cdecl__)) jn (int, double);
 double __attribute__((__cdecl__)) y0 (double);
 double __attribute__((__cdecl__)) y1 (double);
 double __attribute__((__cdecl__)) yn (int, double);

 double __attribute__((__cdecl__)) chgsign (double);
# 285 "c:\\mingw\\include\\math.h" 3
 int __attribute__((__cdecl__)) finite (double);
 int __attribute__((__cdecl__)) fpclass (double);
# 355 "c:\\mingw\\include\\math.h" 3
  typedef long double float_t;
  typedef long double double_t;
# 391 "c:\\mingw\\include\\math.h" 3
extern int __attribute__((__cdecl__)) __fpclassifyf (float);
extern int __attribute__((__cdecl__)) __fpclassify (double);
extern int __attribute__((__cdecl__)) __fpclassifyl (long double);
# 417 "c:\\mingw\\include\\math.h" 3
extern int __attribute__((__cdecl__)) __isnan (double);
extern int __attribute__((__cdecl__)) __isnanf (float);
extern int __attribute__((__cdecl__)) __isnanl (long double);
# 457 "c:\\mingw\\include\\math.h" 3
extern int __attribute__((__cdecl__)) __signbit (double);
extern int __attribute__((__cdecl__)) __signbitf (float);
extern int __attribute__((__cdecl__)) __signbitl (long double);
# 486 "c:\\mingw\\include\\math.h" 3
extern float __attribute__((__cdecl__)) sinf (float);
extern long double __attribute__((__cdecl__)) sinl (long double);

extern float __attribute__((__cdecl__)) cosf (float);
extern long double __attribute__((__cdecl__)) cosl (long double);

extern float __attribute__((__cdecl__)) tanf (float);
extern long double __attribute__((__cdecl__)) tanl (long double);

extern float __attribute__((__cdecl__)) asinf (float);
extern long double __attribute__((__cdecl__)) asinl (long double);

extern float __attribute__((__cdecl__)) acosf (float);
extern long double __attribute__((__cdecl__)) acosl (long double);

extern float __attribute__((__cdecl__)) atanf (float);
extern long double __attribute__((__cdecl__)) atanl (long double);

extern float __attribute__((__cdecl__)) atan2f (float, float);
extern long double __attribute__((__cdecl__)) atan2l (long double, long double);



extern float __attribute__((__cdecl__)) sinhf (float);




extern long double __attribute__((__cdecl__)) sinhl (long double);

extern float __attribute__((__cdecl__)) coshf (float);




extern long double __attribute__((__cdecl__)) coshl (long double);

extern float __attribute__((__cdecl__)) tanhf (float);




extern long double __attribute__((__cdecl__)) tanhl (long double);



extern double __attribute__((__cdecl__)) acosh (double);
extern float __attribute__((__cdecl__)) acoshf (float);
extern long double __attribute__((__cdecl__)) acoshl (long double);


extern double __attribute__((__cdecl__)) asinh (double);
extern float __attribute__((__cdecl__)) asinhf (float);
extern long double __attribute__((__cdecl__)) asinhl (long double);


extern double __attribute__((__cdecl__)) atanh (double);
extern float __attribute__((__cdecl__)) atanhf (float);
extern long double __attribute__((__cdecl__)) atanhl (long double);



extern float __attribute__((__cdecl__)) expf (float);




extern long double __attribute__((__cdecl__)) expl (long double);


extern double __attribute__((__cdecl__)) exp2(double);
extern float __attribute__((__cdecl__)) exp2f(float);
extern long double __attribute__((__cdecl__)) exp2l(long double);



extern double __attribute__((__cdecl__)) expm1(double);
extern float __attribute__((__cdecl__)) expm1f(float);
extern long double __attribute__((__cdecl__)) expm1l(long double);


extern float __attribute__((__cdecl__)) frexpf (float, int*);




extern long double __attribute__((__cdecl__)) frexpl (long double, int*);




extern int __attribute__((__cdecl__)) ilogb (double);
extern int __attribute__((__cdecl__)) ilogbf (float);
extern int __attribute__((__cdecl__)) ilogbl (long double);


extern float __attribute__((__cdecl__)) ldexpf (float, int);




extern long double __attribute__((__cdecl__)) ldexpl (long double, int);


extern float __attribute__((__cdecl__)) logf (float);
extern long double __attribute__((__cdecl__)) logl (long double);


extern float __attribute__((__cdecl__)) log10f (float);
extern long double __attribute__((__cdecl__)) log10l (long double);


extern double __attribute__((__cdecl__)) log1p(double);
extern float __attribute__((__cdecl__)) log1pf(float);
extern long double __attribute__((__cdecl__)) log1pl(long double);


extern double __attribute__((__cdecl__)) log2 (double);
extern float __attribute__((__cdecl__)) log2f (float);
extern long double __attribute__((__cdecl__)) log2l (long double);


extern double __attribute__((__cdecl__)) logb (double);
extern float __attribute__((__cdecl__)) logbf (float);
extern long double __attribute__((__cdecl__)) logbl (long double);
# 644 "c:\\mingw\\include\\math.h" 3
extern float __attribute__((__cdecl__)) modff (float, float*);
extern long double __attribute__((__cdecl__)) modfl (long double, long double*);


extern double __attribute__((__cdecl__)) scalbn (double, int);
extern float __attribute__((__cdecl__)) scalbnf (float, int);
extern long double __attribute__((__cdecl__)) scalbnl (long double, int);

extern double __attribute__((__cdecl__)) scalbln (double, long);
extern float __attribute__((__cdecl__)) scalblnf (float, long);
extern long double __attribute__((__cdecl__)) scalblnl (long double, long);



extern double __attribute__((__cdecl__)) cbrt (double);
extern float __attribute__((__cdecl__)) cbrtf (float);
extern long double __attribute__((__cdecl__)) cbrtl (long double);


extern float __attribute__((__cdecl__)) fabsf (float x);
extern long double __attribute__((__cdecl__)) fabsl (long double x);


extern double __attribute__((__cdecl__)) hypot (double, double);
extern float __attribute__((__cdecl__)) hypotf (float, float);
extern long double __attribute__((__cdecl__)) hypotl (long double, long double);


extern float __attribute__((__cdecl__)) powf (float, float);
extern long double __attribute__((__cdecl__)) powl (long double, long double);


extern float __attribute__((__cdecl__)) sqrtf (float);
extern long double __attribute__((__cdecl__)) sqrtl (long double);


extern double __attribute__((__cdecl__)) erf (double);
extern float __attribute__((__cdecl__)) erff (float);
extern long double __attribute__((__cdecl__)) erfl (long double);


extern double __attribute__((__cdecl__)) erfc (double);
extern float __attribute__((__cdecl__)) erfcf (float);
extern long double __attribute__((__cdecl__)) erfcl (long double);


extern double __attribute__((__cdecl__)) lgamma (double);
extern float __attribute__((__cdecl__)) lgammaf (float);
extern long double __attribute__((__cdecl__)) lgammal (long double);


extern double __attribute__((__cdecl__)) tgamma (double);
extern float __attribute__((__cdecl__)) tgammaf (float);
extern long double __attribute__((__cdecl__)) tgammal (long double);


extern float __attribute__((__cdecl__)) ceilf (float);
extern long double __attribute__((__cdecl__)) ceill (long double);


extern float __attribute__((__cdecl__)) floorf (float);
extern long double __attribute__((__cdecl__)) floorl (long double);


extern double __attribute__((__cdecl__)) nearbyint ( double);
extern float __attribute__((__cdecl__)) nearbyintf (float);
extern long double __attribute__((__cdecl__)) nearbyintl (long double);



extern double __attribute__((__cdecl__)) rint (double);
extern float __attribute__((__cdecl__)) rintf (float);
extern long double __attribute__((__cdecl__)) rintl (long double);


extern long __attribute__((__cdecl__)) lrint (double);
extern long __attribute__((__cdecl__)) lrintf (float);
extern long __attribute__((__cdecl__)) lrintl (long double);

extern long long __attribute__((__cdecl__)) llrint (double);
extern long long __attribute__((__cdecl__)) llrintf (float);
extern long long __attribute__((__cdecl__)) llrintl (long double);
# 805 "c:\\mingw\\include\\math.h" 3
extern double __attribute__((__cdecl__)) round (double);
extern float __attribute__((__cdecl__)) roundf (float);
extern long double __attribute__((__cdecl__)) roundl (long double);


extern long __attribute__((__cdecl__)) lround (double);
extern long __attribute__((__cdecl__)) lroundf (float);
extern long __attribute__((__cdecl__)) lroundl (long double);

extern long long __attribute__((__cdecl__)) llround (double);
extern long long __attribute__((__cdecl__)) llroundf (float);
extern long long __attribute__((__cdecl__)) llroundl (long double);



extern double __attribute__((__cdecl__)) trunc (double);
extern float __attribute__((__cdecl__)) truncf (float);
extern long double __attribute__((__cdecl__)) truncl (long double);


extern float __attribute__((__cdecl__)) fmodf (float, float);
extern long double __attribute__((__cdecl__)) fmodl (long double, long double);


extern double __attribute__((__cdecl__)) remainder (double, double);
extern float __attribute__((__cdecl__)) remainderf (float, float);
extern long double __attribute__((__cdecl__)) remainderl (long double, long double);


extern double __attribute__((__cdecl__)) remquo(double, double, int *);
extern float __attribute__((__cdecl__)) remquof(float, float, int *);
extern long double __attribute__((__cdecl__)) remquol(long double, long double, int *);


extern double __attribute__((__cdecl__)) copysign (double, double);
extern float __attribute__((__cdecl__)) copysignf (float, float);
extern long double __attribute__((__cdecl__)) copysignl (long double, long double);


extern double __attribute__((__cdecl__)) nan(const char *tagp);
extern float __attribute__((__cdecl__)) nanf(const char *tagp);
extern long double __attribute__((__cdecl__)) nanl(const char *tagp);
# 855 "c:\\mingw\\include\\math.h" 3
extern double __attribute__((__cdecl__)) nextafter (double, double);
extern float __attribute__((__cdecl__)) nextafterf (float, float);
extern long double __attribute__((__cdecl__)) nextafterl (long double, long double);


extern double __attribute__((__cdecl__)) nexttoward (double, long double);
extern float __attribute__((__cdecl__)) nexttowardf (float, long double);
extern long double __attribute__((__cdecl__)) nexttowardl (long double, long double);



extern double __attribute__((__cdecl__)) fdim (double x, double y);
extern float __attribute__((__cdecl__)) fdimf (float x, float y);
extern long double __attribute__((__cdecl__)) fdiml (long double x, long double y);







extern double __attribute__((__cdecl__)) fmax (double, double);
extern float __attribute__((__cdecl__)) fmaxf (float, float);
extern long double __attribute__((__cdecl__)) fmaxl (long double, long double);


extern double __attribute__((__cdecl__)) fmin (double, double);
extern float __attribute__((__cdecl__)) fminf (float, float);
extern long double __attribute__((__cdecl__)) fminl (long double, long double);



extern double __attribute__((__cdecl__)) fma (double, double, double);
extern float __attribute__((__cdecl__)) fmaf (float, float, float);
extern long double __attribute__((__cdecl__)) fmal (long double, long double, long double);
# 931 "c:\\mingw\\include\\math.h" 3

# 5 "./src/lib/src/Tools.c" 2
# 1 "include/SDL.h" 1
# 32 "include/SDL.h"
# 1 "include/SDL_main.h" 1
# 25 "include/SDL_main.h"
# 1 "include/SDL_stdinc.h" 1
# 31 "include/SDL_stdinc.h"
# 1 "include/SDL_config.h" 1
# 26 "include/SDL_config.h"
# 1 "include/SDL_platform.h" 1
# 179 "include/SDL_platform.h"
# 1 "include/begin_code.h" 1
# 180 "include/SDL_platform.h" 2
# 188 "include/SDL_platform.h"

# 188 "include/SDL_platform.h"
extern __attribute__((dllexport)) const char * SDL_GetPlatform (void);





# 1 "include/close_code.h" 1
# 195 "include/SDL_platform.h" 2
# 27 "include/SDL_config.h" 2
# 32 "include/SDL_stdinc.h" 2
# 50 "include/SDL_stdinc.h"
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 149 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4

# 149 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4
typedef int ptrdiff_t;
# 426 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4
typedef struct {
  long long __max_align_ll __attribute__((__aligned__(__alignof__(long long))));
  long double __max_align_ld __attribute__((__aligned__(__alignof__(long double))));






  __float128 __max_align_f128 __attribute__((__aligned__(__alignof(__float128))));

} max_align_t;
# 51 "include/SDL_stdinc.h" 2
# 71 "include/SDL_stdinc.h"
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdint.h" 1 3 4
# 9 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdint.h" 3 4
# 1 "c:\\mingw\\include\\stdint.h" 1 3 4
# 34 "c:\\mingw\\include\\stdint.h" 3 4
       
# 35 "c:\\mingw\\include\\stdint.h" 3
# 54 "c:\\mingw\\include\\stdint.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 55 "c:\\mingw\\include\\stdint.h" 2 3



typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef short int16_t;
typedef unsigned short uint16_t;
typedef int int32_t;
typedef unsigned uint32_t;
typedef long long int64_t;
typedef unsigned long long uint64_t;



typedef signed char int_least8_t;
typedef unsigned char uint_least8_t;
typedef short int_least16_t;
typedef unsigned short uint_least16_t;
typedef int int_least32_t;
typedef unsigned uint_least32_t;
typedef long long int_least64_t;
typedef unsigned long long uint_least64_t;





typedef signed char int_fast8_t;
typedef unsigned char uint_fast8_t;
typedef short int_fast16_t;
typedef unsigned short uint_fast16_t;
typedef int int_fast32_t;
typedef unsigned int uint_fast32_t;
typedef long long int_fast64_t;
typedef unsigned long long uint_fast64_t;
# 106 "c:\\mingw\\include\\stdint.h" 3
 typedef int __intptr_t;

typedef __intptr_t intptr_t;
# 118 "c:\\mingw\\include\\stdint.h" 3
 typedef unsigned int __uintptr_t;

typedef __uintptr_t uintptr_t;







typedef long long intmax_t;
typedef unsigned long long uintmax_t;
# 10 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdint.h" 2 3 4
# 72 "include/SDL_stdinc.h" 2
# 161 "include/SDL_stdinc.h"

# 161 "include/SDL_stdinc.h"
typedef enum
{
    SDL_FALSE = 0,
    SDL_TRUE = 1
} SDL_bool;







typedef int8_t Sint8;





typedef uint8_t Uint8;





typedef int16_t Sint16;





typedef uint16_t Uint16;





typedef int32_t Sint32;





typedef uint32_t Uint32;






typedef int64_t Sint64;





typedef uint64_t Uint64;
# 316 "include/SDL_stdinc.h"
typedef int SDL_compile_time_assert_uint8[(sizeof(Uint8) == 1) * 2 - 1];
typedef int SDL_compile_time_assert_sint8[(sizeof(Sint8) == 1) * 2 - 1];
typedef int SDL_compile_time_assert_uint16[(sizeof(Uint16) == 2) * 2 - 1];
typedef int SDL_compile_time_assert_sint16[(sizeof(Sint16) == 2) * 2 - 1];
typedef int SDL_compile_time_assert_uint32[(sizeof(Uint32) == 4) * 2 - 1];
typedef int SDL_compile_time_assert_sint32[(sizeof(Sint32) == 4) * 2 - 1];
typedef int SDL_compile_time_assert_uint64[(sizeof(Uint64) == 8) * 2 - 1];
typedef int SDL_compile_time_assert_sint64[(sizeof(Sint64) == 8) * 2 - 1];
# 337 "include/SDL_stdinc.h"
typedef enum
{
    DUMMY_ENUM_VALUE
} SDL_DUMMY_ENUM;

typedef int SDL_compile_time_assert_enum[(sizeof(SDL_DUMMY_ENUM) == sizeof(int)) * 2 - 1];




# 1 "include/begin_code.h" 1
# 348 "include/SDL_stdinc.h" 2
# 361 "include/SDL_stdinc.h"
extern __attribute__((dllexport)) void * SDL_malloc(size_t size);
extern __attribute__((dllexport)) void * SDL_calloc(size_t nmemb, size_t size);
extern __attribute__((dllexport)) void * SDL_realloc(void *mem, size_t size);
extern __attribute__((dllexport)) void SDL_free(void *mem);

typedef void *( *SDL_malloc_func)(size_t size);
typedef void *( *SDL_calloc_func)(size_t nmemb, size_t size);
typedef void *( *SDL_realloc_func)(void *mem, size_t size);
typedef void ( *SDL_free_func)(void *mem);




extern __attribute__((dllexport)) void SDL_GetMemoryFunctions(SDL_malloc_func *malloc_func,
                                                    SDL_calloc_func *calloc_func,
                                                    SDL_realloc_func *realloc_func,
                                                    SDL_free_func *free_func);
# 387 "include/SDL_stdinc.h"
extern __attribute__((dllexport)) int SDL_SetMemoryFunctions(SDL_malloc_func malloc_func,
                                                   SDL_calloc_func calloc_func,
                                                   SDL_realloc_func realloc_func,
                                                   SDL_free_func free_func);




extern __attribute__((dllexport)) int SDL_GetNumAllocations(void);

extern __attribute__((dllexport)) char * SDL_getenv(const char *name);
extern __attribute__((dllexport)) int SDL_setenv(const char *name, const char *value, int overwrite);

extern __attribute__((dllexport)) void SDL_qsort(void *base, size_t nmemb, size_t size, int (*compare) (const void *, const void *));

extern __attribute__((dllexport)) int SDL_abs(int x);






extern __attribute__((dllexport)) int SDL_isdigit(int x);
extern __attribute__((dllexport)) int SDL_isspace(int x);
extern __attribute__((dllexport)) int SDL_isupper(int x);
extern __attribute__((dllexport)) int SDL_islower(int x);
extern __attribute__((dllexport)) int SDL_toupper(int x);
extern __attribute__((dllexport)) int SDL_tolower(int x);

extern __attribute__((dllexport)) void * SDL_memset( void *dst, int c, size_t len);






__attribute__((always_inline)) static __inline__ void SDL_memset4(void *dst, Uint32 val, size_t dwords)
{



    int u0, u1, u2;
    __asm__ __volatile__ (
        "cld \n\t"
        "rep ; stosl \n\t"
        : "=&D" (u0), "=&a" (u1), "=&c" (u2)
        : "0" (dst), "1" (val), "2" (((Uint32)(dwords)))
        : "memory"
    );
# 451 "include/SDL_stdinc.h"
}

extern __attribute__((dllexport)) void * SDL_memcpy( void *dst, const void *src, size_t len);

extern __attribute__((dllexport)) void * SDL_memmove( void *dst, const void *src, size_t len);
extern __attribute__((dllexport)) int SDL_memcmp(const void *s1, const void *s2, size_t len);

extern __attribute__((dllexport)) size_t SDL_wcslen(const wchar_t *wstr);
extern __attribute__((dllexport)) size_t SDL_wcslcpy( wchar_t *dst, const wchar_t *src, size_t maxlen);
extern __attribute__((dllexport)) size_t SDL_wcslcat( wchar_t *dst, const wchar_t *src, size_t maxlen);
extern __attribute__((dllexport)) wchar_t * SDL_wcsdup(const wchar_t *wstr);
extern __attribute__((dllexport)) wchar_t * SDL_wcsstr(const wchar_t *haystack, const wchar_t *needle);

extern __attribute__((dllexport)) int SDL_wcscmp(const wchar_t *str1, const wchar_t *str2);
extern __attribute__((dllexport)) int SDL_wcsncmp(const wchar_t *str1, const wchar_t *str2, size_t maxlen);

extern __attribute__((dllexport)) size_t SDL_strlen(const char *str);
extern __attribute__((dllexport)) size_t SDL_strlcpy( char *dst, const char *src, size_t maxlen);
extern __attribute__((dllexport)) size_t SDL_utf8strlcpy( char *dst, const char *src, size_t dst_bytes);
extern __attribute__((dllexport)) size_t SDL_strlcat( char *dst, const char *src, size_t maxlen);
extern __attribute__((dllexport)) char * SDL_strdup(const char *str);
extern __attribute__((dllexport)) char * SDL_strrev(char *str);
extern __attribute__((dllexport)) char * SDL_strupr(char *str);
extern __attribute__((dllexport)) char * SDL_strlwr(char *str);
extern __attribute__((dllexport)) char * SDL_strchr(const char *str, int c);
extern __attribute__((dllexport)) char * SDL_strrchr(const char *str, int c);
extern __attribute__((dllexport)) char * SDL_strstr(const char *haystack, const char *needle);
extern __attribute__((dllexport)) char * SDL_strtokr(char *s1, const char *s2, char **saveptr);
extern __attribute__((dllexport)) size_t SDL_utf8strlen(const char *str);

extern __attribute__((dllexport)) char * SDL_itoa(int value, char *str, int radix);
extern __attribute__((dllexport)) char * SDL_uitoa(unsigned int value, char *str, int radix);
extern __attribute__((dllexport)) char * SDL_ltoa(long value, char *str, int radix);
extern __attribute__((dllexport)) char * SDL_ultoa(unsigned long value, char *str, int radix);
extern __attribute__((dllexport)) char * SDL_lltoa(Sint64 value, char *str, int radix);
extern __attribute__((dllexport)) char * SDL_ulltoa(Uint64 value, char *str, int radix);

extern __attribute__((dllexport)) int SDL_atoi(const char *str);
extern __attribute__((dllexport)) double SDL_atof(const char *str);
extern __attribute__((dllexport)) long SDL_strtol(const char *str, char **endp, int base);
extern __attribute__((dllexport)) unsigned long SDL_strtoul(const char *str, char **endp, int base);
extern __attribute__((dllexport)) Sint64 SDL_strtoll(const char *str, char **endp, int base);
extern __attribute__((dllexport)) Uint64 SDL_strtoull(const char *str, char **endp, int base);
extern __attribute__((dllexport)) double SDL_strtod(const char *str, char **endp);

extern __attribute__((dllexport)) int SDL_strcmp(const char *str1, const char *str2);
extern __attribute__((dllexport)) int SDL_strncmp(const char *str1, const char *str2, size_t maxlen);
extern __attribute__((dllexport)) int SDL_strcasecmp(const char *str1, const char *str2);
extern __attribute__((dllexport)) int SDL_strncasecmp(const char *str1, const char *str2, size_t len);

extern __attribute__((dllexport)) int SDL_sscanf(const char *text, const char *fmt, ...) __attribute__ (( format( __scanf__, 2, 2 +1 )));
extern __attribute__((dllexport)) int SDL_vsscanf(const char *text, const char *fmt, va_list ap);
extern __attribute__((dllexport)) int SDL_snprintf( char *text, size_t maxlen, const char *fmt, ... ) __attribute__ (( format( __printf__, 3, 3 +1 )));
extern __attribute__((dllexport)) int SDL_vsnprintf( char *text, size_t maxlen, const char *fmt, va_list ap);







extern __attribute__((dllexport)) double SDL_acos(double x);
extern __attribute__((dllexport)) float SDL_acosf(float x);
extern __attribute__((dllexport)) double SDL_asin(double x);
extern __attribute__((dllexport)) float SDL_asinf(float x);
extern __attribute__((dllexport)) double SDL_atan(double x);
extern __attribute__((dllexport)) float SDL_atanf(float x);
extern __attribute__((dllexport)) double SDL_atan2(double x, double y);
extern __attribute__((dllexport)) float SDL_atan2f(float x, float y);
extern __attribute__((dllexport)) double SDL_ceil(double x);
extern __attribute__((dllexport)) float SDL_ceilf(float x);
extern __attribute__((dllexport)) double SDL_copysign(double x, double y);
extern __attribute__((dllexport)) float SDL_copysignf(float x, float y);
extern __attribute__((dllexport)) double SDL_cos(double x);
extern __attribute__((dllexport)) float SDL_cosf(float x);
extern __attribute__((dllexport)) double SDL_exp(double x);
extern __attribute__((dllexport)) float SDL_expf(float x);
extern __attribute__((dllexport)) double SDL_fabs(double x);
extern __attribute__((dllexport)) float SDL_fabsf(float x);
extern __attribute__((dllexport)) double SDL_floor(double x);
extern __attribute__((dllexport)) float SDL_floorf(float x);
extern __attribute__((dllexport)) double SDL_fmod(double x, double y);
extern __attribute__((dllexport)) float SDL_fmodf(float x, float y);
extern __attribute__((dllexport)) double SDL_log(double x);
extern __attribute__((dllexport)) float SDL_logf(float x);
extern __attribute__((dllexport)) double SDL_log10(double x);
extern __attribute__((dllexport)) float SDL_log10f(float x);
extern __attribute__((dllexport)) double SDL_pow(double x, double y);
extern __attribute__((dllexport)) float SDL_powf(float x, float y);
extern __attribute__((dllexport)) double SDL_scalbn(double x, int n);
extern __attribute__((dllexport)) float SDL_scalbnf(float x, int n);
extern __attribute__((dllexport)) double SDL_sin(double x);
extern __attribute__((dllexport)) float SDL_sinf(float x);
extern __attribute__((dllexport)) double SDL_sqrt(double x);
extern __attribute__((dllexport)) float SDL_sqrtf(float x);
extern __attribute__((dllexport)) double SDL_tan(double x);
extern __attribute__((dllexport)) float SDL_tanf(float x);
# 556 "include/SDL_stdinc.h"
typedef struct _SDL_iconv_t *SDL_iconv_t;
extern __attribute__((dllexport)) SDL_iconv_t SDL_iconv_open(const char *tocode,
                                                   const char *fromcode);
extern __attribute__((dllexport)) int SDL_iconv_close(SDL_iconv_t cd);
extern __attribute__((dllexport)) size_t SDL_iconv(SDL_iconv_t cd, const char **inbuf,
                                         size_t * inbytesleft, char **outbuf,
                                         size_t * outbytesleft);




extern __attribute__((dllexport)) char * SDL_iconv_string(const char *tocode,
                                               const char *fromcode,
                                               const char *inbuf,
                                               size_t inbytesleft);
# 604 "include/SDL_stdinc.h"
__attribute__((always_inline)) static __inline__ void *SDL_memcpy4( void *dst, const void *src, size_t dwords)
{
    return SDL_memcpy(dst, src, dwords * 4);
}





# 1 "include/close_code.h" 1
# 614 "include/SDL_stdinc.h" 2
# 26 "include/SDL_main.h" 2
# 112 "include/SDL_main.h"
# 1 "include/begin_code.h" 1
# 113 "include/SDL_main.h" 2







typedef int (*SDL_main_func)(int argc, char *argv[]);
extern int SDL_main(int argc, char *argv[]);
# 131 "include/SDL_main.h"
extern __attribute__((dllexport)) void SDL_SetMainReady(void);






extern __attribute__((dllexport)) int SDL_RegisterApp(char *name, Uint32 style, void *hInst);
extern __attribute__((dllexport)) void SDL_UnregisterApp(void);
# 176 "include/SDL_main.h"
# 1 "include/close_code.h" 1
# 177 "include/SDL_main.h" 2
# 33 "include/SDL.h" 2

# 1 "include/SDL_assert.h" 1
# 27 "include/SDL_assert.h"
# 1 "include/begin_code.h" 1
# 28 "include/SDL_assert.h" 2
# 102 "include/SDL_assert.h"
typedef enum
{
    SDL_ASSERTION_RETRY,
    SDL_ASSERTION_BREAK,
    SDL_ASSERTION_ABORT,
    SDL_ASSERTION_IGNORE,
    SDL_ASSERTION_ALWAYS_IGNORE
} SDL_AssertState;

typedef struct SDL_AssertData
{
    int always_ignore;
    unsigned int trigger_count;
    const char *condition;
    const char *filename;
    int linenum;
    const char *function;
    const struct SDL_AssertData *next;
} SDL_AssertData;




extern __attribute__((dllexport)) SDL_AssertState SDL_ReportAssertion(SDL_AssertData *,
                                                             const char *,
                                                             const char *, int)
# 136 "include/SDL_assert.h"
;
# 188 "include/SDL_assert.h"
typedef SDL_AssertState ( *SDL_AssertionHandler)(
                                 const SDL_AssertData* data, void* userdata);
# 211 "include/SDL_assert.h"
extern __attribute__((dllexport)) void SDL_SetAssertionHandler(
                                            SDL_AssertionHandler handler,
                                            void *userdata);
# 225 "include/SDL_assert.h"
extern __attribute__((dllexport)) SDL_AssertionHandler SDL_GetDefaultAssertionHandler(void);
# 242 "include/SDL_assert.h"
extern __attribute__((dllexport)) SDL_AssertionHandler SDL_GetAssertionHandler(void **puserdata);
# 266 "include/SDL_assert.h"
extern __attribute__((dllexport)) const SDL_AssertData * SDL_GetAssertionReport(void);
# 275 "include/SDL_assert.h"
extern __attribute__((dllexport)) void SDL_ResetAssertionReport(void);
# 287 "include/SDL_assert.h"
# 1 "include/close_code.h" 1
# 288 "include/SDL_assert.h" 2
# 35 "include/SDL.h" 2
# 1 "include/SDL_atomic.h" 1
# 65 "include/SDL_atomic.h"
# 1 "include/begin_code.h" 1
# 66 "include/SDL_atomic.h" 2
# 89 "include/SDL_atomic.h"
typedef int SDL_SpinLock;
# 98 "include/SDL_atomic.h"
extern __attribute__((dllexport)) SDL_bool SDL_AtomicTryLock(SDL_SpinLock *lock);






extern __attribute__((dllexport)) void SDL_AtomicLock(SDL_SpinLock *lock);






extern __attribute__((dllexport)) void SDL_AtomicUnlock(SDL_SpinLock *lock);
# 155 "include/SDL_atomic.h"
extern __attribute__((dllexport)) void SDL_MemoryBarrierReleaseFunction(void);
extern __attribute__((dllexport)) void SDL_MemoryBarrierAcquireFunction(void);
# 216 "include/SDL_atomic.h"
typedef struct { int value; } SDL_atomic_t;
# 225 "include/SDL_atomic.h"
extern __attribute__((dllexport)) SDL_bool SDL_AtomicCAS(SDL_atomic_t *a, int oldval, int newval);






extern __attribute__((dllexport)) int SDL_AtomicSet(SDL_atomic_t *a, int v);




extern __attribute__((dllexport)) int SDL_AtomicGet(SDL_atomic_t *a);
# 246 "include/SDL_atomic.h"
extern __attribute__((dllexport)) int SDL_AtomicAdd(SDL_atomic_t *a, int v);
# 272 "include/SDL_atomic.h"
extern __attribute__((dllexport)) SDL_bool SDL_AtomicCASPtr(void **a, void *oldval, void *newval);






extern __attribute__((dllexport)) void* SDL_AtomicSetPtr(void **a, void* v);




extern __attribute__((dllexport)) void* SDL_AtomicGetPtr(void **a);






# 1 "include/close_code.h" 1
# 292 "include/SDL_atomic.h" 2
# 36 "include/SDL.h" 2
# 1 "include/SDL_audio.h" 1
# 32 "include/SDL_audio.h"
# 1 "include/SDL_error.h" 1
# 33 "include/SDL_error.h"
# 1 "include/begin_code.h" 1
# 34 "include/SDL_error.h" 2







extern __attribute__((dllexport)) int SDL_SetError( const char *fmt, ...) __attribute__ (( format( __printf__, 1, 1 +1 )));
extern __attribute__((dllexport)) const char * SDL_GetError(void);
extern __attribute__((dllexport)) void SDL_ClearError(void);
# 55 "include/SDL_error.h"
typedef enum
{
    SDL_ENOMEM,
    SDL_EFREAD,
    SDL_EFWRITE,
    SDL_EFSEEK,
    SDL_UNSUPPORTED,
    SDL_LASTERROR
} SDL_errorcode;

extern __attribute__((dllexport)) int SDL_Error(SDL_errorcode code);






# 1 "include/close_code.h" 1
# 73 "include/SDL_error.h" 2
# 33 "include/SDL_audio.h" 2
# 1 "include/SDL_endian.h" 1
# 62 "include/SDL_endian.h"
# 1 "include/begin_code.h" 1
# 63 "include/SDL_endian.h" 2
# 73 "include/SDL_endian.h"
__attribute__((always_inline)) static __inline__ Uint16
SDL_Swap16(Uint16 x)
{
  __asm__("xchgb %b0,%h0": "=q"(x):"0"(x));
    return x;
}
# 117 "include/SDL_endian.h"
__attribute__((always_inline)) static __inline__ Uint32
SDL_Swap32(Uint32 x)
{
  __asm__("bswap %0": "=r"(x):"0"(x));
    return x;
}
# 173 "include/SDL_endian.h"
__attribute__((always_inline)) static __inline__ Uint64
SDL_Swap64(Uint64 x)
{
    union
    {
        struct
        {
            Uint32 a, b;
        } s;
        Uint64 u;
    } v;
    v.u = x;
  __asm__("bswapl %0 ; bswapl %1 ; xchgl %0,%1": "=r"(v.s.a), "=r"(v.s.b):"0"(v.s.a),
            "1"(v.s.
                b));
    return v.u;
}
# 215 "include/SDL_endian.h"
__attribute__((always_inline)) static __inline__ float
SDL_SwapFloat(float x)
{
    union
    {
        float f;
        Uint32 ui32;
    } swapper;
    swapper.f = x;
    swapper.ui32 = SDL_Swap32(swapper.ui32);
    return swapper.f;
}
# 259 "include/SDL_endian.h"
# 1 "include/close_code.h" 1
# 260 "include/SDL_endian.h" 2
# 34 "include/SDL_audio.h" 2
# 1 "include/SDL_mutex.h" 1
# 34 "include/SDL_mutex.h"
# 1 "include/begin_code.h" 1
# 35 "include/SDL_mutex.h" 2
# 58 "include/SDL_mutex.h"
struct SDL_mutex;
typedef struct SDL_mutex SDL_mutex;




extern __attribute__((dllexport)) SDL_mutex * SDL_CreateMutex(void);







extern __attribute__((dllexport)) int SDL_LockMutex(SDL_mutex * mutex);






extern __attribute__((dllexport)) int SDL_TryLockMutex(SDL_mutex * mutex);
# 90 "include/SDL_mutex.h"
extern __attribute__((dllexport)) int SDL_UnlockMutex(SDL_mutex * mutex);




extern __attribute__((dllexport)) void SDL_DestroyMutex(SDL_mutex * mutex);
# 106 "include/SDL_mutex.h"
struct SDL_semaphore;
typedef struct SDL_semaphore SDL_sem;




extern __attribute__((dllexport)) SDL_sem * SDL_CreateSemaphore(Uint32 initial_value);




extern __attribute__((dllexport)) void SDL_DestroySemaphore(SDL_sem * sem);






extern __attribute__((dllexport)) int SDL_SemWait(SDL_sem * sem);







extern __attribute__((dllexport)) int SDL_SemTryWait(SDL_sem * sem);
# 143 "include/SDL_mutex.h"
extern __attribute__((dllexport)) int SDL_SemWaitTimeout(SDL_sem * sem, Uint32 ms);






extern __attribute__((dllexport)) int SDL_SemPost(SDL_sem * sem);




extern __attribute__((dllexport)) Uint32 SDL_SemValue(SDL_sem * sem);
# 166 "include/SDL_mutex.h"
struct SDL_cond;
typedef struct SDL_cond SDL_cond;
# 197 "include/SDL_mutex.h"
extern __attribute__((dllexport)) SDL_cond * SDL_CreateCond(void);




extern __attribute__((dllexport)) void SDL_DestroyCond(SDL_cond * cond);






extern __attribute__((dllexport)) int SDL_CondSignal(SDL_cond * cond);






extern __attribute__((dllexport)) int SDL_CondBroadcast(SDL_cond * cond);
# 227 "include/SDL_mutex.h"
extern __attribute__((dllexport)) int SDL_CondWait(SDL_cond * cond, SDL_mutex * mutex);
# 237 "include/SDL_mutex.h"
extern __attribute__((dllexport)) int SDL_CondWaitTimeout(SDL_cond * cond,
                                                SDL_mutex * mutex, Uint32 ms);
# 247 "include/SDL_mutex.h"
# 1 "include/close_code.h" 1
# 248 "include/SDL_mutex.h" 2
# 35 "include/SDL_audio.h" 2
# 1 "include/SDL_thread.h" 1
# 38 "include/SDL_thread.h"
# 1 "include/begin_code.h" 1
# 39 "include/SDL_thread.h" 2






struct SDL_Thread;
typedef struct SDL_Thread SDL_Thread;


typedef unsigned long SDL_threadID;


typedef unsigned int SDL_TLSID;






typedef enum {
    SDL_THREAD_PRIORITY_LOW,
    SDL_THREAD_PRIORITY_NORMAL,
    SDL_THREAD_PRIORITY_HIGH,
    SDL_THREAD_PRIORITY_TIME_CRITICAL
} SDL_ThreadPriority;





typedef int ( * SDL_ThreadFunction) (void *data);
# 94 "include/SDL_thread.h"
# 1 "c:\\mingw\\include\\process.h" 1 3
# 33 "c:\\mingw\\include\\process.h" 3
       
# 34 "c:\\mingw\\include\\process.h" 3
# 51 "c:\\mingw\\include\\process.h" 3
# 1 "c:\\mingw\\include\\sys\\types.h" 1 3
# 34 "c:\\mingw\\include\\sys\\types.h" 3
       
# 35 "c:\\mingw\\include\\sys\\types.h" 3
# 62 "c:\\mingw\\include\\sys\\types.h" 3
  
# 62 "c:\\mingw\\include\\sys\\types.h" 3
 typedef long __off32_t;




  typedef __off32_t _off_t;







  typedef _off_t off_t;
# 91 "c:\\mingw\\include\\sys\\types.h" 3
  typedef long long __off64_t;






  typedef __off64_t off64_t;
# 115 "c:\\mingw\\include\\sys\\types.h" 3
  typedef int _ssize_t;







  typedef _ssize_t ssize_t;
# 139 "c:\\mingw\\include\\sys\\types.h" 3
  typedef long __time32_t;
  typedef long long __time64_t;
# 149 "c:\\mingw\\include\\sys\\types.h" 3
   typedef __time32_t time_t;
# 174 "c:\\mingw\\include\\sys\\types.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 175 "c:\\mingw\\include\\sys\\types.h" 2 3
# 184 "c:\\mingw\\include\\sys\\types.h" 3
typedef unsigned int _dev_t;
# 195 "c:\\mingw\\include\\sys\\types.h" 3
typedef short _ino_t;
typedef unsigned short _mode_t;
typedef int _pid_t;
typedef int _sigset_t;
# 207 "c:\\mingw\\include\\sys\\types.h" 3
typedef _dev_t dev_t;
typedef _ino_t ino_t;
typedef _mode_t mode_t;
typedef _pid_t pid_t;
typedef _sigset_t sigset_t;


typedef long long fpos64_t;






typedef unsigned long useconds_t __attribute__((__deprecated__));
# 52 "c:\\mingw\\include\\process.h" 2 3
# 91 "c:\\mingw\\include\\process.h" 3
# 1 "c:\\mingw\\include\\stdint.h" 1 3
# 92 "c:\\mingw\\include\\process.h" 2 3




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _cexit (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _c_exit (void);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _cwait (int *, _pid_t, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) _pid_t _getpid (void);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _execl (const char *, const char *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _execle (const char *, const char *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _execlp (const char *, const char *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _execlpe (const char *, const char *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _execv (const char *, const char * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _execve (const char *, const char * const *, const char * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _execvp (const char *, const char * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _execvpe (const char *, const char * const *, const char * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _spawnl (int, const char *, const char *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _spawnle (int, const char *, const char *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _spawnlp (int, const char *, const char *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _spawnlpe (int, const char *, const char *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _spawnv (int, const char *, const char * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _spawnve (int, const char *, const char * const *, const char * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _spawnvp (int, const char *, const char * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _spawnvpe (int, const char *, const char * const *, const char * const *);
# 161 "c:\\mingw\\include\\process.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__))
unsigned long _beginthread (void (*)(void *), unsigned, void *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _endthread (void);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned long _beginthreadex
(void *, unsigned, unsigned (__attribute__((__stdcall__)) *) (void *), void *, unsigned, unsigned *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _endthreadex (unsigned);






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int cwait (int *, pid_t, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) pid_t getpid (void);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t execl (const char *, const char *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t execle (const char *, const char *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t execlp (const char *, const char *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t execlpe (const char *, const char *,...);
# 201 "c:\\mingw\\include\\process.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t execv (const char *, const char * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t execve (const char *, const char * const *, const char * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t execvp (const char *, const char * const *);







 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t execvpe (const char *, const char * const *, const char * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t spawnl (int, const char *, const char *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t spawnle (int, const char *, const char *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t spawnlp (int, const char *, const char *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t spawnlpe (int, const char *, const char *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t spawnv (int, const char *, const char * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t spawnve (int, const char *, const char * const *, const char * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t spawnvp (int, const char *, const char * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t spawnvpe (int, const char *, const char * const *, const char * const *);
# 255 "c:\\mingw\\include\\process.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _wexecl (const wchar_t *, const wchar_t *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _wexecle (const wchar_t *, const wchar_t *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _wexeclp (const wchar_t *, const wchar_t *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _wexeclpe (const wchar_t *, const wchar_t *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _wexecv (const wchar_t *, const wchar_t * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) intptr_t _wexecve
(const wchar_t *, const wchar_t * const *, const wchar_t * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _wexecvp (const wchar_t *, const wchar_t * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) intptr_t _wexecvpe
(const wchar_t *, const wchar_t * const *, const wchar_t * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _wspawnl (int, const wchar_t *, const wchar_t *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _wspawnle (int, const wchar_t *, const wchar_t *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _wspawnlp (int, const wchar_t *, const wchar_t *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _wspawnlpe (int, const wchar_t *, const wchar_t *, ...);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _wspawnv (int, const wchar_t *, const wchar_t * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) intptr_t _wspawnve
(int, const wchar_t *, const wchar_t * const *, const wchar_t * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
intptr_t _wspawnvp (int, const wchar_t *, const wchar_t * const *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) intptr_t _wspawnvpe
(int, const wchar_t *, const wchar_t * const *, const wchar_t * const *);




# 95 "include/SDL_thread.h" 2


# 96 "include/SDL_thread.h"
typedef uintptr_t (__attribute__((__cdecl__)) * pfnSDL_CurrentBeginThread)
                   (void *, unsigned, unsigned (__attribute__((__stdcall__)) *func)(void *),
                    void * , unsigned, unsigned * );
typedef void (__attribute__((__cdecl__)) * pfnSDL_CurrentEndThread) (unsigned code);
# 111 "include/SDL_thread.h"
extern __attribute__((dllexport)) SDL_Thread *
SDL_CreateThread(SDL_ThreadFunction fn, const char *name, void *data,
                 pfnSDL_CurrentBeginThread pfnBeginThread,
                 pfnSDL_CurrentEndThread pfnEndThread);

extern __attribute__((dllexport)) SDL_Thread *
SDL_CreateThreadWithStackSize(int ( * fn) (void *),
                 const char *name, const size_t stacksize, void *data,
                 pfnSDL_CurrentBeginThread pfnBeginThread,
                 pfnSDL_CurrentEndThread pfnEndThread);
# 227 "include/SDL_thread.h"
extern __attribute__((dllexport)) const char * SDL_GetThreadName(SDL_Thread *thread);




extern __attribute__((dllexport)) SDL_threadID SDL_ThreadID(void);






extern __attribute__((dllexport)) SDL_threadID SDL_GetThreadID(SDL_Thread * thread);




extern __attribute__((dllexport)) int SDL_SetThreadPriority(SDL_ThreadPriority priority);
# 264 "include/SDL_thread.h"
extern __attribute__((dllexport)) void SDL_WaitThread(SDL_Thread * thread, int *status);
# 292 "include/SDL_thread.h"
extern __attribute__((dllexport)) void SDL_DetachThread(SDL_Thread * thread);
# 324 "include/SDL_thread.h"
extern __attribute__((dllexport)) SDL_TLSID SDL_TLSCreate(void);
# 336 "include/SDL_thread.h"
extern __attribute__((dllexport)) void * SDL_TLSGet(SDL_TLSID id);
# 350 "include/SDL_thread.h"
extern __attribute__((dllexport)) int SDL_TLSSet(SDL_TLSID id, const void *value, void ( *destructor)(void*));






# 1 "include/close_code.h" 1
# 358 "include/SDL_thread.h" 2
# 36 "include/SDL_audio.h" 2
# 1 "include/SDL_rwops.h" 1
# 35 "include/SDL_rwops.h"
# 1 "include/begin_code.h" 1
# 36 "include/SDL_rwops.h" 2
# 52 "include/SDL_rwops.h"
typedef struct SDL_RWops
{



    Sint64 ( * size) (struct SDL_RWops * context);







    Sint64 ( * seek) (struct SDL_RWops * context, Sint64 offset,
                             int whence);







    size_t ( * read) (struct SDL_RWops * context, void *ptr,
                             size_t size, size_t maxnum);







    size_t ( * write) (struct SDL_RWops * context, const void *ptr,
                              size_t size, size_t num);






    int ( * close) (struct SDL_RWops * context);

    Uint32 type;
    union
    {
# 110 "include/SDL_rwops.h"
        struct
        {
            SDL_bool append;
            void *h;
            struct
            {
                void *data;
                size_t size;
                size_t left;
            } buffer;
        } windowsio;
# 130 "include/SDL_rwops.h"
        struct
        {
            Uint8 *base;
            Uint8 *here;
            Uint8 *stop;
        } mem;
        struct
        {
            void *data1;
            void *data2;
        } unknown;
    } hidden;

} SDL_RWops;
# 153 "include/SDL_rwops.h"
extern __attribute__((dllexport)) SDL_RWops * SDL_RWFromFile(const char *file,
                                                  const char *mode);





extern __attribute__((dllexport)) SDL_RWops * SDL_RWFromFP(void * fp,
                                                SDL_bool autoclose);


extern __attribute__((dllexport)) SDL_RWops * SDL_RWFromMem(void *mem, int size);
extern __attribute__((dllexport)) SDL_RWops * SDL_RWFromConstMem(const void *mem,
                                                      int size);




extern __attribute__((dllexport)) SDL_RWops * SDL_AllocRW(void);
extern __attribute__((dllexport)) void SDL_FreeRW(SDL_RWops * area);
# 181 "include/SDL_rwops.h"
extern __attribute__((dllexport)) Sint64 SDL_RWsize(SDL_RWops *context);







extern __attribute__((dllexport)) Sint64 SDL_RWseek(SDL_RWops *context,
                                          Sint64 offset, int whence);




extern __attribute__((dllexport)) Sint64 SDL_RWtell(SDL_RWops *context);







extern __attribute__((dllexport)) size_t SDL_RWread(SDL_RWops *context,
                                          void *ptr, size_t size, size_t maxnum);







extern __attribute__((dllexport)) size_t SDL_RWwrite(SDL_RWops *context,
                                           const void *ptr, size_t size, size_t num);






extern __attribute__((dllexport)) int SDL_RWclose(SDL_RWops *context);
# 235 "include/SDL_rwops.h"
extern __attribute__((dllexport)) void * SDL_LoadFile_RW(SDL_RWops * src, size_t *datasize,
                                                    int freesrc);
# 251 "include/SDL_rwops.h"
extern __attribute__((dllexport)) void * SDL_LoadFile(const char *file, size_t *datasize);







extern __attribute__((dllexport)) Uint8 SDL_ReadU8(SDL_RWops * src);
extern __attribute__((dllexport)) Uint16 SDL_ReadLE16(SDL_RWops * src);
extern __attribute__((dllexport)) Uint16 SDL_ReadBE16(SDL_RWops * src);
extern __attribute__((dllexport)) Uint32 SDL_ReadLE32(SDL_RWops * src);
extern __attribute__((dllexport)) Uint32 SDL_ReadBE32(SDL_RWops * src);
extern __attribute__((dllexport)) Uint64 SDL_ReadLE64(SDL_RWops * src);
extern __attribute__((dllexport)) Uint64 SDL_ReadBE64(SDL_RWops * src);
# 274 "include/SDL_rwops.h"
extern __attribute__((dllexport)) size_t SDL_WriteU8(SDL_RWops * dst, Uint8 value);
extern __attribute__((dllexport)) size_t SDL_WriteLE16(SDL_RWops * dst, Uint16 value);
extern __attribute__((dllexport)) size_t SDL_WriteBE16(SDL_RWops * dst, Uint16 value);
extern __attribute__((dllexport)) size_t SDL_WriteLE32(SDL_RWops * dst, Uint32 value);
extern __attribute__((dllexport)) size_t SDL_WriteBE32(SDL_RWops * dst, Uint32 value);
extern __attribute__((dllexport)) size_t SDL_WriteLE64(SDL_RWops * dst, Uint64 value);
extern __attribute__((dllexport)) size_t SDL_WriteBE64(SDL_RWops * dst, Uint64 value);






# 1 "include/close_code.h" 1
# 288 "include/SDL_rwops.h" 2
# 37 "include/SDL_audio.h" 2

# 1 "include/begin_code.h" 1
# 39 "include/SDL_audio.h" 2
# 64 "include/SDL_audio.h"
typedef Uint16 SDL_AudioFormat;
# 163 "include/SDL_audio.h"
typedef void ( * SDL_AudioCallback) (void *userdata, Uint8 * stream,
                                            int len);
# 178 "include/SDL_audio.h"
typedef struct SDL_AudioSpec
{
    int freq;
    SDL_AudioFormat format;
    Uint8 channels;
    Uint8 silence;
    Uint16 samples;
    Uint16 padding;
    Uint32 size;
    SDL_AudioCallback callback;
    void *userdata;
} SDL_AudioSpec;


struct SDL_AudioCVT;
typedef void ( * SDL_AudioFilter) (struct SDL_AudioCVT * cvt,
                                          SDL_AudioFormat format);
# 226 "include/SDL_audio.h"
typedef struct SDL_AudioCVT
{
    int needed;
    SDL_AudioFormat src_format;
    SDL_AudioFormat dst_format;
    double rate_incr;
    Uint8 *buf;
    int len;
    int len_cvt;
    int len_mult;
    double len_ratio;
    SDL_AudioFilter filters[9 + 1];
    int filter_index;
} __attribute__((packed)) SDL_AudioCVT;
# 251 "include/SDL_audio.h"
extern __attribute__((dllexport)) int SDL_GetNumAudioDrivers(void);
extern __attribute__((dllexport)) const char * SDL_GetAudioDriver(int index);
# 263 "include/SDL_audio.h"
extern __attribute__((dllexport)) int SDL_AudioInit(const char *driver_name);
extern __attribute__((dllexport)) void SDL_AudioQuit(void);






extern __attribute__((dllexport)) const char * SDL_GetCurrentAudioDriver(void);
# 318 "include/SDL_audio.h"
extern __attribute__((dllexport)) int SDL_OpenAudio(SDL_AudioSpec * desired,
                                          SDL_AudioSpec * obtained);
# 330 "include/SDL_audio.h"
typedef Uint32 SDL_AudioDeviceID;
# 344 "include/SDL_audio.h"
extern __attribute__((dllexport)) int SDL_GetNumAudioDevices(int iscapture);
# 359 "include/SDL_audio.h"
extern __attribute__((dllexport)) const char * SDL_GetAudioDeviceName(int index,
                                                           int iscapture);
# 376 "include/SDL_audio.h"
extern __attribute__((dllexport)) SDL_AudioDeviceID SDL_OpenAudioDevice(const char
                                                              *device,
                                                              int iscapture,
                                                              const
                                                              SDL_AudioSpec *
                                                              desired,
                                                              SDL_AudioSpec *
                                                              obtained,
                                                              int
                                                              allowed_changes);
# 395 "include/SDL_audio.h"
typedef enum
{
    SDL_AUDIO_STOPPED = 0,
    SDL_AUDIO_PLAYING,
    SDL_AUDIO_PAUSED
} SDL_AudioStatus;
extern __attribute__((dllexport)) SDL_AudioStatus SDL_GetAudioStatus(void);

extern __attribute__((dllexport)) SDL_AudioStatus
SDL_GetAudioDeviceStatus(SDL_AudioDeviceID dev);
# 417 "include/SDL_audio.h"
extern __attribute__((dllexport)) void SDL_PauseAudio(int pause_on);
extern __attribute__((dllexport)) void SDL_PauseAudioDevice(SDL_AudioDeviceID dev,
                                                  int pause_on);
# 474 "include/SDL_audio.h"
extern __attribute__((dllexport)) SDL_AudioSpec * SDL_LoadWAV_RW(SDL_RWops * src,
                                                      int freesrc,
                                                      SDL_AudioSpec * spec,
                                                      Uint8 ** audio_buf,
                                                      Uint32 * audio_len);
# 490 "include/SDL_audio.h"
extern __attribute__((dllexport)) void SDL_FreeWAV(Uint8 * audio_buf);
# 501 "include/SDL_audio.h"
extern __attribute__((dllexport)) int SDL_BuildAudioCVT(SDL_AudioCVT * cvt,
                                              SDL_AudioFormat src_format,
                                              Uint8 src_channels,
                                              int src_rate,
                                              SDL_AudioFormat dst_format,
                                              Uint8 dst_channels,
                                              int dst_rate);
# 521 "include/SDL_audio.h"
extern __attribute__((dllexport)) int SDL_ConvertAudio(SDL_AudioCVT * cvt);
# 531 "include/SDL_audio.h"
struct _SDL_AudioStream;
typedef struct _SDL_AudioStream SDL_AudioStream;
# 552 "include/SDL_audio.h"
extern __attribute__((dllexport)) SDL_AudioStream * SDL_NewAudioStream(const SDL_AudioFormat src_format,
                                           const Uint8 src_channels,
                                           const int src_rate,
                                           const SDL_AudioFormat dst_format,
                                           const Uint8 dst_channels,
                                           const int dst_rate);
# 574 "include/SDL_audio.h"
extern __attribute__((dllexport)) int SDL_AudioStreamPut(SDL_AudioStream *stream, const void *buf, int len);
# 591 "include/SDL_audio.h"
extern __attribute__((dllexport)) int SDL_AudioStreamGet(SDL_AudioStream *stream, void *buf, int len);
# 606 "include/SDL_audio.h"
extern __attribute__((dllexport)) int SDL_AudioStreamAvailable(SDL_AudioStream *stream);
# 623 "include/SDL_audio.h"
extern __attribute__((dllexport)) int SDL_AudioStreamFlush(SDL_AudioStream *stream);
# 635 "include/SDL_audio.h"
extern __attribute__((dllexport)) void SDL_AudioStreamClear(SDL_AudioStream *stream);
# 647 "include/SDL_audio.h"
extern __attribute__((dllexport)) void SDL_FreeAudioStream(SDL_AudioStream *stream);
# 657 "include/SDL_audio.h"
extern __attribute__((dllexport)) void SDL_MixAudio(Uint8 * dst, const Uint8 * src,
                                          Uint32 len, int volume);






extern __attribute__((dllexport)) void SDL_MixAudioFormat(Uint8 * dst,
                                                const Uint8 * src,
                                                SDL_AudioFormat format,
                                                Uint32 len, int volume);
# 709 "include/SDL_audio.h"
extern __attribute__((dllexport)) int SDL_QueueAudio(SDL_AudioDeviceID dev, const void *data, Uint32 len);
# 755 "include/SDL_audio.h"
extern __attribute__((dllexport)) Uint32 SDL_DequeueAudio(SDL_AudioDeviceID dev, void *data, Uint32 len);
# 791 "include/SDL_audio.h"
extern __attribute__((dllexport)) Uint32 SDL_GetQueuedAudioSize(SDL_AudioDeviceID dev);
# 827 "include/SDL_audio.h"
extern __attribute__((dllexport)) void SDL_ClearQueuedAudio(SDL_AudioDeviceID dev);
# 839 "include/SDL_audio.h"
extern __attribute__((dllexport)) void SDL_LockAudio(void);
extern __attribute__((dllexport)) void SDL_LockAudioDevice(SDL_AudioDeviceID dev);
extern __attribute__((dllexport)) void SDL_UnlockAudio(void);
extern __attribute__((dllexport)) void SDL_UnlockAudioDevice(SDL_AudioDeviceID dev);





extern __attribute__((dllexport)) void SDL_CloseAudio(void);
extern __attribute__((dllexport)) void SDL_CloseAudioDevice(SDL_AudioDeviceID dev);





# 1 "include/close_code.h" 1
# 856 "include/SDL_audio.h" 2
# 37 "include/SDL.h" 2
# 1 "include/SDL_clipboard.h" 1
# 33 "include/SDL_clipboard.h"
# 1 "include/begin_code.h" 1
# 34 "include/SDL_clipboard.h" 2
# 46 "include/SDL_clipboard.h"
extern __attribute__((dllexport)) int SDL_SetClipboardText(const char *text);






extern __attribute__((dllexport)) char * SDL_GetClipboardText(void);






extern __attribute__((dllexport)) SDL_bool SDL_HasClipboardText(void);






# 1 "include/close_code.h" 1
# 68 "include/SDL_clipboard.h" 2
# 38 "include/SDL.h" 2
# 1 "include/SDL_cpuinfo.h" 1
# 103 "include/SDL_cpuinfo.h"
# 1 "include/begin_code.h" 1
# 104 "include/SDL_cpuinfo.h" 2
# 119 "include/SDL_cpuinfo.h"
extern __attribute__((dllexport)) int SDL_GetCPUCount(void);







extern __attribute__((dllexport)) int SDL_GetCPUCacheLineSize(void);




extern __attribute__((dllexport)) SDL_bool SDL_HasRDTSC(void);




extern __attribute__((dllexport)) SDL_bool SDL_HasAltiVec(void);




extern __attribute__((dllexport)) SDL_bool SDL_HasMMX(void);




extern __attribute__((dllexport)) SDL_bool SDL_Has3DNow(void);




extern __attribute__((dllexport)) SDL_bool SDL_HasSSE(void);




extern __attribute__((dllexport)) SDL_bool SDL_HasSSE2(void);




extern __attribute__((dllexport)) SDL_bool SDL_HasSSE3(void);




extern __attribute__((dllexport)) SDL_bool SDL_HasSSE41(void);




extern __attribute__((dllexport)) SDL_bool SDL_HasSSE42(void);




extern __attribute__((dllexport)) SDL_bool SDL_HasAVX(void);




extern __attribute__((dllexport)) SDL_bool SDL_HasAVX2(void);




extern __attribute__((dllexport)) SDL_bool SDL_HasAVX512F(void);




extern __attribute__((dllexport)) SDL_bool SDL_HasARMSIMD(void);




extern __attribute__((dllexport)) SDL_bool SDL_HasNEON(void);




extern __attribute__((dllexport)) int SDL_GetSystemRAM(void);
# 216 "include/SDL_cpuinfo.h"
extern __attribute__((dllexport)) size_t SDL_SIMDGetAlignment(void);
# 251 "include/SDL_cpuinfo.h"
extern __attribute__((dllexport)) void * SDL_SIMDAlloc(const size_t len);
# 264 "include/SDL_cpuinfo.h"
extern __attribute__((dllexport)) void SDL_SIMDFree(void *ptr);






# 1 "include/close_code.h" 1
# 272 "include/SDL_cpuinfo.h" 2
# 39 "include/SDL.h" 2


# 1 "include/SDL_events.h" 1
# 33 "include/SDL_events.h"
# 1 "include/SDL_video.h" 1
# 32 "include/SDL_video.h"
# 1 "include/SDL_pixels.h" 1
# 34 "include/SDL_pixels.h"
# 1 "include/begin_code.h" 1
# 35 "include/SDL_pixels.h" 2
# 51 "include/SDL_pixels.h"
typedef enum
{
    SDL_PIXELTYPE_UNKNOWN,
    SDL_PIXELTYPE_INDEX1,
    SDL_PIXELTYPE_INDEX4,
    SDL_PIXELTYPE_INDEX8,
    SDL_PIXELTYPE_PACKED8,
    SDL_PIXELTYPE_PACKED16,
    SDL_PIXELTYPE_PACKED32,
    SDL_PIXELTYPE_ARRAYU8,
    SDL_PIXELTYPE_ARRAYU16,
    SDL_PIXELTYPE_ARRAYU32,
    SDL_PIXELTYPE_ARRAYF16,
    SDL_PIXELTYPE_ARRAYF32
} SDL_PixelType;


typedef enum
{
    SDL_BITMAPORDER_NONE,
    SDL_BITMAPORDER_4321,
    SDL_BITMAPORDER_1234
} SDL_BitmapOrder;


typedef enum
{
    SDL_PACKEDORDER_NONE,
    SDL_PACKEDORDER_XRGB,
    SDL_PACKEDORDER_RGBX,
    SDL_PACKEDORDER_ARGB,
    SDL_PACKEDORDER_RGBA,
    SDL_PACKEDORDER_XBGR,
    SDL_PACKEDORDER_BGRX,
    SDL_PACKEDORDER_ABGR,
    SDL_PACKEDORDER_BGRA
} SDL_PackedOrder;




typedef enum
{
    SDL_ARRAYORDER_NONE,
    SDL_ARRAYORDER_RGB,
    SDL_ARRAYORDER_RGBA,
    SDL_ARRAYORDER_ARGB,
    SDL_ARRAYORDER_BGR,
    SDL_ARRAYORDER_BGRA,
    SDL_ARRAYORDER_ABGR
} SDL_ArrayOrder;


typedef enum
{
    SDL_PACKEDLAYOUT_NONE,
    SDL_PACKEDLAYOUT_332,
    SDL_PACKEDLAYOUT_4444,
    SDL_PACKEDLAYOUT_1555,
    SDL_PACKEDLAYOUT_5551,
    SDL_PACKEDLAYOUT_565,
    SDL_PACKEDLAYOUT_8888,
    SDL_PACKEDLAYOUT_2101010,
    SDL_PACKEDLAYOUT_1010102
} SDL_PackedLayout;
# 171 "include/SDL_pixels.h"
typedef enum
{
    SDL_PIXELFORMAT_UNKNOWN,
    SDL_PIXELFORMAT_INDEX1LSB =
        ((1 << 28) | ((SDL_PIXELTYPE_INDEX1) << 24) | ((SDL_BITMAPORDER_4321) << 20) | ((0) << 16) | ((1) << 8) | ((0) << 0))
                                    ,
    SDL_PIXELFORMAT_INDEX1MSB =
        ((1 << 28) | ((SDL_PIXELTYPE_INDEX1) << 24) | ((SDL_BITMAPORDER_1234) << 20) | ((0) << 16) | ((1) << 8) | ((0) << 0))
                                    ,
    SDL_PIXELFORMAT_INDEX4LSB =
        ((1 << 28) | ((SDL_PIXELTYPE_INDEX4) << 24) | ((SDL_BITMAPORDER_4321) << 20) | ((0) << 16) | ((4) << 8) | ((0) << 0))
                                    ,
    SDL_PIXELFORMAT_INDEX4MSB =
        ((1 << 28) | ((SDL_PIXELTYPE_INDEX4) << 24) | ((SDL_BITMAPORDER_1234) << 20) | ((0) << 16) | ((4) << 8) | ((0) << 0))
                                    ,
    SDL_PIXELFORMAT_INDEX8 =
        ((1 << 28) | ((SDL_PIXELTYPE_INDEX8) << 24) | ((0) << 20) | ((0) << 16) | ((8) << 8) | ((1) << 0)),
    SDL_PIXELFORMAT_RGB332 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED8) << 24) | ((SDL_PACKEDORDER_XRGB) << 20) | ((SDL_PACKEDLAYOUT_332) << 16) | ((8) << 8) | ((1) << 0))
                                                          ,
    SDL_PIXELFORMAT_RGB444 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED16) << 24) | ((SDL_PACKEDORDER_XRGB) << 20) | ((SDL_PACKEDLAYOUT_4444) << 16) | ((12) << 8) | ((2) << 0))
                                                            ,
    SDL_PIXELFORMAT_BGR444 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED16) << 24) | ((SDL_PACKEDORDER_XBGR) << 20) | ((SDL_PACKEDLAYOUT_4444) << 16) | ((12) << 8) | ((2) << 0))
                                                            ,
    SDL_PIXELFORMAT_RGB555 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED16) << 24) | ((SDL_PACKEDORDER_XRGB) << 20) | ((SDL_PACKEDLAYOUT_1555) << 16) | ((15) << 8) | ((2) << 0))
                                                            ,
    SDL_PIXELFORMAT_BGR555 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED16) << 24) | ((SDL_PACKEDORDER_XBGR) << 20) | ((SDL_PACKEDLAYOUT_1555) << 16) | ((15) << 8) | ((2) << 0))
                                                            ,
    SDL_PIXELFORMAT_ARGB4444 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED16) << 24) | ((SDL_PACKEDORDER_ARGB) << 20) | ((SDL_PACKEDLAYOUT_4444) << 16) | ((16) << 8) | ((2) << 0))
                                                            ,
    SDL_PIXELFORMAT_RGBA4444 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED16) << 24) | ((SDL_PACKEDORDER_RGBA) << 20) | ((SDL_PACKEDLAYOUT_4444) << 16) | ((16) << 8) | ((2) << 0))
                                                            ,
    SDL_PIXELFORMAT_ABGR4444 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED16) << 24) | ((SDL_PACKEDORDER_ABGR) << 20) | ((SDL_PACKEDLAYOUT_4444) << 16) | ((16) << 8) | ((2) << 0))
                                                            ,
    SDL_PIXELFORMAT_BGRA4444 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED16) << 24) | ((SDL_PACKEDORDER_BGRA) << 20) | ((SDL_PACKEDLAYOUT_4444) << 16) | ((16) << 8) | ((2) << 0))
                                                            ,
    SDL_PIXELFORMAT_ARGB1555 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED16) << 24) | ((SDL_PACKEDORDER_ARGB) << 20) | ((SDL_PACKEDLAYOUT_1555) << 16) | ((16) << 8) | ((2) << 0))
                                                            ,
    SDL_PIXELFORMAT_RGBA5551 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED16) << 24) | ((SDL_PACKEDORDER_RGBA) << 20) | ((SDL_PACKEDLAYOUT_5551) << 16) | ((16) << 8) | ((2) << 0))
                                                            ,
    SDL_PIXELFORMAT_ABGR1555 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED16) << 24) | ((SDL_PACKEDORDER_ABGR) << 20) | ((SDL_PACKEDLAYOUT_1555) << 16) | ((16) << 8) | ((2) << 0))
                                                            ,
    SDL_PIXELFORMAT_BGRA5551 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED16) << 24) | ((SDL_PACKEDORDER_BGRA) << 20) | ((SDL_PACKEDLAYOUT_5551) << 16) | ((16) << 8) | ((2) << 0))
                                                            ,
    SDL_PIXELFORMAT_RGB565 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED16) << 24) | ((SDL_PACKEDORDER_XRGB) << 20) | ((SDL_PACKEDLAYOUT_565) << 16) | ((16) << 8) | ((2) << 0))
                                                           ,
    SDL_PIXELFORMAT_BGR565 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED16) << 24) | ((SDL_PACKEDORDER_XBGR) << 20) | ((SDL_PACKEDLAYOUT_565) << 16) | ((16) << 8) | ((2) << 0))
                                                           ,
    SDL_PIXELFORMAT_RGB24 =
        ((1 << 28) | ((SDL_PIXELTYPE_ARRAYU8) << 24) | ((SDL_ARRAYORDER_RGB) << 20) | ((0) << 16) | ((24) << 8) | ((3) << 0))
                                     ,
    SDL_PIXELFORMAT_BGR24 =
        ((1 << 28) | ((SDL_PIXELTYPE_ARRAYU8) << 24) | ((SDL_ARRAYORDER_BGR) << 20) | ((0) << 16) | ((24) << 8) | ((3) << 0))
                                     ,
    SDL_PIXELFORMAT_RGB888 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED32) << 24) | ((SDL_PACKEDORDER_XRGB) << 20) | ((SDL_PACKEDLAYOUT_8888) << 16) | ((24) << 8) | ((4) << 0))
                                                            ,
    SDL_PIXELFORMAT_RGBX8888 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED32) << 24) | ((SDL_PACKEDORDER_RGBX) << 20) | ((SDL_PACKEDLAYOUT_8888) << 16) | ((24) << 8) | ((4) << 0))
                                                            ,
    SDL_PIXELFORMAT_BGR888 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED32) << 24) | ((SDL_PACKEDORDER_XBGR) << 20) | ((SDL_PACKEDLAYOUT_8888) << 16) | ((24) << 8) | ((4) << 0))
                                                            ,
    SDL_PIXELFORMAT_BGRX8888 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED32) << 24) | ((SDL_PACKEDORDER_BGRX) << 20) | ((SDL_PACKEDLAYOUT_8888) << 16) | ((24) << 8) | ((4) << 0))
                                                            ,
    SDL_PIXELFORMAT_ARGB8888 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED32) << 24) | ((SDL_PACKEDORDER_ARGB) << 20) | ((SDL_PACKEDLAYOUT_8888) << 16) | ((32) << 8) | ((4) << 0))
                                                            ,
    SDL_PIXELFORMAT_RGBA8888 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED32) << 24) | ((SDL_PACKEDORDER_RGBA) << 20) | ((SDL_PACKEDLAYOUT_8888) << 16) | ((32) << 8) | ((4) << 0))
                                                            ,
    SDL_PIXELFORMAT_ABGR8888 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED32) << 24) | ((SDL_PACKEDORDER_ABGR) << 20) | ((SDL_PACKEDLAYOUT_8888) << 16) | ((32) << 8) | ((4) << 0))
                                                            ,
    SDL_PIXELFORMAT_BGRA8888 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED32) << 24) | ((SDL_PACKEDORDER_BGRA) << 20) | ((SDL_PACKEDLAYOUT_8888) << 16) | ((32) << 8) | ((4) << 0))
                                                            ,
    SDL_PIXELFORMAT_ARGB2101010 =
        ((1 << 28) | ((SDL_PIXELTYPE_PACKED32) << 24) | ((SDL_PACKEDORDER_ARGB) << 20) | ((SDL_PACKEDLAYOUT_2101010) << 16) | ((32) << 8) | ((4) << 0))
                                                               ,
# 274 "include/SDL_pixels.h"
    SDL_PIXELFORMAT_RGBA32 = SDL_PIXELFORMAT_ABGR8888,
    SDL_PIXELFORMAT_ARGB32 = SDL_PIXELFORMAT_BGRA8888,
    SDL_PIXELFORMAT_BGRA32 = SDL_PIXELFORMAT_ARGB8888,
    SDL_PIXELFORMAT_ABGR32 = SDL_PIXELFORMAT_RGBA8888,


    SDL_PIXELFORMAT_YV12 =
        ((((Uint32)(((Uint8)(('Y'))))) << 0) | (((Uint32)(((Uint8)(('V'))))) << 8) | (((Uint32)(((Uint8)(('1'))))) << 16) | (((Uint32)(((Uint8)(('2'))))) << 24)),
    SDL_PIXELFORMAT_IYUV =
        ((((Uint32)(((Uint8)(('I'))))) << 0) | (((Uint32)(((Uint8)(('Y'))))) << 8) | (((Uint32)(((Uint8)(('U'))))) << 16) | (((Uint32)(((Uint8)(('V'))))) << 24)),
    SDL_PIXELFORMAT_YUY2 =
        ((((Uint32)(((Uint8)(('Y'))))) << 0) | (((Uint32)(((Uint8)(('U'))))) << 8) | (((Uint32)(((Uint8)(('Y'))))) << 16) | (((Uint32)(((Uint8)(('2'))))) << 24)),
    SDL_PIXELFORMAT_UYVY =
        ((((Uint32)(((Uint8)(('U'))))) << 0) | (((Uint32)(((Uint8)(('Y'))))) << 8) | (((Uint32)(((Uint8)(('V'))))) << 16) | (((Uint32)(((Uint8)(('Y'))))) << 24)),
    SDL_PIXELFORMAT_YVYU =
        ((((Uint32)(((Uint8)(('Y'))))) << 0) | (((Uint32)(((Uint8)(('V'))))) << 8) | (((Uint32)(((Uint8)(('Y'))))) << 16) | (((Uint32)(((Uint8)(('U'))))) << 24)),
    SDL_PIXELFORMAT_NV12 =
        ((((Uint32)(((Uint8)(('N'))))) << 0) | (((Uint32)(((Uint8)(('V'))))) << 8) | (((Uint32)(((Uint8)(('1'))))) << 16) | (((Uint32)(((Uint8)(('2'))))) << 24)),
    SDL_PIXELFORMAT_NV21 =
        ((((Uint32)(((Uint8)(('N'))))) << 0) | (((Uint32)(((Uint8)(('V'))))) << 8) | (((Uint32)(((Uint8)(('2'))))) << 16) | (((Uint32)(((Uint8)(('1'))))) << 24)),
    SDL_PIXELFORMAT_EXTERNAL_OES =
        ((((Uint32)(((Uint8)(('O'))))) << 0) | (((Uint32)(((Uint8)(('E'))))) << 8) | (((Uint32)(((Uint8)(('S'))))) << 16) | (((Uint32)(((Uint8)((' '))))) << 24))
} SDL_PixelFormatEnum;

typedef struct SDL_Color
{
    Uint8 r;
    Uint8 g;
    Uint8 b;
    Uint8 a;
} SDL_Color;


typedef struct SDL_Palette
{
    int ncolors;
    SDL_Color *colors;
    Uint32 version;
    int refcount;
} SDL_Palette;




typedef struct SDL_PixelFormat
{
    Uint32 format;
    SDL_Palette *palette;
    Uint8 BitsPerPixel;
    Uint8 BytesPerPixel;
    Uint8 padding[2];
    Uint32 Rmask;
    Uint32 Gmask;
    Uint32 Bmask;
    Uint32 Amask;
    Uint8 Rloss;
    Uint8 Gloss;
    Uint8 Bloss;
    Uint8 Aloss;
    Uint8 Rshift;
    Uint8 Gshift;
    Uint8 Bshift;
    Uint8 Ashift;
    int refcount;
    struct SDL_PixelFormat *next;
} SDL_PixelFormat;




extern __attribute__((dllexport)) const char* SDL_GetPixelFormatName(Uint32 format);
# 353 "include/SDL_pixels.h"
extern __attribute__((dllexport)) SDL_bool SDL_PixelFormatEnumToMasks(Uint32 format,
                                                            int *bpp,
                                                            Uint32 * Rmask,
                                                            Uint32 * Gmask,
                                                            Uint32 * Bmask,
                                                            Uint32 * Amask);
# 368 "include/SDL_pixels.h"
extern __attribute__((dllexport)) Uint32 SDL_MasksToPixelFormatEnum(int bpp,
                                                          Uint32 Rmask,
                                                          Uint32 Gmask,
                                                          Uint32 Bmask,
                                                          Uint32 Amask);




extern __attribute__((dllexport)) SDL_PixelFormat * SDL_AllocFormat(Uint32 pixel_format);




extern __attribute__((dllexport)) void SDL_FreeFormat(SDL_PixelFormat *format);
# 394 "include/SDL_pixels.h"
extern __attribute__((dllexport)) SDL_Palette * SDL_AllocPalette(int ncolors);




extern __attribute__((dllexport)) int SDL_SetPixelFormatPalette(SDL_PixelFormat * format,
                                                      SDL_Palette *palette);
# 412 "include/SDL_pixels.h"
extern __attribute__((dllexport)) int SDL_SetPaletteColors(SDL_Palette * palette,
                                                 const SDL_Color * colors,
                                                 int firstcolor, int ncolors);






extern __attribute__((dllexport)) void SDL_FreePalette(SDL_Palette * palette);






extern __attribute__((dllexport)) Uint32 SDL_MapRGB(const SDL_PixelFormat * format,
                                          Uint8 r, Uint8 g, Uint8 b);






extern __attribute__((dllexport)) Uint32 SDL_MapRGBA(const SDL_PixelFormat * format,
                                           Uint8 r, Uint8 g, Uint8 b,
                                           Uint8 a);






extern __attribute__((dllexport)) void SDL_GetRGB(Uint32 pixel,
                                        const SDL_PixelFormat * format,
                                        Uint8 * r, Uint8 * g, Uint8 * b);






extern __attribute__((dllexport)) void SDL_GetRGBA(Uint32 pixel,
                                         const SDL_PixelFormat * format,
                                         Uint8 * r, Uint8 * g, Uint8 * b,
                                         Uint8 * a);




extern __attribute__((dllexport)) void SDL_CalculateGammaRamp(float gamma, Uint16 * ramp);






# 1 "include/close_code.h" 1
# 470 "include/SDL_pixels.h" 2
# 33 "include/SDL_video.h" 2
# 1 "include/SDL_rect.h" 1
# 36 "include/SDL_rect.h"
# 1 "include/begin_code.h" 1
# 37 "include/SDL_rect.h" 2
# 48 "include/SDL_rect.h"
typedef struct SDL_Point
{
    int x;
    int y;
} SDL_Point;







typedef struct SDL_FPoint
{
    float x;
    float y;
} SDL_FPoint;
# 77 "include/SDL_rect.h"
typedef struct SDL_Rect
{
    int x, y;
    int w, h;
} SDL_Rect;





typedef struct SDL_FRect
{
    float x;
    float y;
    float w;
    float h;
} SDL_FRect;





__attribute__((always_inline)) static __inline__ SDL_bool SDL_PointInRect(const SDL_Point *p, const SDL_Rect *r)
{
    return ( (p->x >= r->x) && (p->x < (r->x + r->w)) &&
             (p->y >= r->y) && (p->y < (r->y + r->h)) ) ? SDL_TRUE : SDL_FALSE;
}




__attribute__((always_inline)) static __inline__ SDL_bool SDL_RectEmpty(const SDL_Rect *r)
{
    return ((!r) || (r->w <= 0) || (r->h <= 0)) ? SDL_TRUE : SDL_FALSE;
}




__attribute__((always_inline)) static __inline__ SDL_bool SDL_RectEquals(const SDL_Rect *a, const SDL_Rect *b)
{
    return (a && b && (a->x == b->x) && (a->y == b->y) &&
            (a->w == b->w) && (a->h == b->h)) ? SDL_TRUE : SDL_FALSE;
}






extern __attribute__((dllexport)) SDL_bool SDL_HasIntersection(const SDL_Rect * A,
                                                     const SDL_Rect * B);






extern __attribute__((dllexport)) SDL_bool SDL_IntersectRect(const SDL_Rect * A,
                                                   const SDL_Rect * B,
                                                   SDL_Rect * result);




extern __attribute__((dllexport)) void SDL_UnionRect(const SDL_Rect * A,
                                           const SDL_Rect * B,
                                           SDL_Rect * result);






extern __attribute__((dllexport)) SDL_bool SDL_EnclosePoints(const SDL_Point * points,
                                                   int count,
                                                   const SDL_Rect * clip,
                                                   SDL_Rect * result);






extern __attribute__((dllexport)) SDL_bool SDL_IntersectRectAndLine(const SDL_Rect *
                                                          rect, int *X1,
                                                          int *Y1, int *X2,
                                                          int *Y2);





# 1 "include/close_code.h" 1
# 171 "include/SDL_rect.h" 2
# 34 "include/SDL_video.h" 2
# 1 "include/SDL_surface.h" 1
# 34 "include/SDL_surface.h"
# 1 "include/SDL_blendmode.h" 1
# 31 "include/SDL_blendmode.h"
# 1 "include/begin_code.h" 1
# 32 "include/SDL_blendmode.h" 2
# 40 "include/SDL_blendmode.h"
typedef enum
{
    SDL_BLENDMODE_NONE = 0x00000000,

    SDL_BLENDMODE_BLEND = 0x00000001,


    SDL_BLENDMODE_ADD = 0x00000002,


    SDL_BLENDMODE_MOD = 0x00000004,


    SDL_BLENDMODE_MUL = 0x00000008,


    SDL_BLENDMODE_INVALID = 0x7FFFFFFF



} SDL_BlendMode;




typedef enum
{
    SDL_BLENDOPERATION_ADD = 0x1,
    SDL_BLENDOPERATION_SUBTRACT = 0x2,
    SDL_BLENDOPERATION_REV_SUBTRACT = 0x3,
    SDL_BLENDOPERATION_MINIMUM = 0x4,
    SDL_BLENDOPERATION_MAXIMUM = 0x5

} SDL_BlendOperation;




typedef enum
{
    SDL_BLENDFACTOR_ZERO = 0x1,
    SDL_BLENDFACTOR_ONE = 0x2,
    SDL_BLENDFACTOR_SRC_COLOR = 0x3,
    SDL_BLENDFACTOR_ONE_MINUS_SRC_COLOR = 0x4,
    SDL_BLENDFACTOR_SRC_ALPHA = 0x5,
    SDL_BLENDFACTOR_ONE_MINUS_SRC_ALPHA = 0x6,
    SDL_BLENDFACTOR_DST_COLOR = 0x7,
    SDL_BLENDFACTOR_ONE_MINUS_DST_COLOR = 0x8,
    SDL_BLENDFACTOR_DST_ALPHA = 0x9,
    SDL_BLENDFACTOR_ONE_MINUS_DST_ALPHA = 0xA

} SDL_BlendFactor;
# 108 "include/SDL_blendmode.h"
extern __attribute__((dllexport)) SDL_BlendMode SDL_ComposeCustomBlendMode(SDL_BlendFactor srcColorFactor,
                                                                 SDL_BlendFactor dstColorFactor,
                                                                 SDL_BlendOperation colorOperation,
                                                                 SDL_BlendFactor srcAlphaFactor,
                                                                 SDL_BlendFactor dstAlphaFactor,
                                                                 SDL_BlendOperation alphaOperation);





# 1 "include/close_code.h" 1
# 120 "include/SDL_blendmode.h" 2
# 35 "include/SDL_surface.h" 2


# 1 "include/begin_code.h" 1
# 38 "include/SDL_surface.h" 2
# 70 "include/SDL_surface.h"
typedef struct SDL_Surface
{
    Uint32 flags;
    SDL_PixelFormat *format;
    int w, h;
    int pitch;
    void *pixels;


    void *userdata;


    int locked;
    void *lock_data;


    SDL_Rect clip_rect;


    struct SDL_BlitMap *map;


    int refcount;
} SDL_Surface;




typedef int ( *SDL_blit) (struct SDL_Surface * src, SDL_Rect * srcrect,
                                 struct SDL_Surface * dst, SDL_Rect * dstrect);




typedef enum
{
    SDL_YUV_CONVERSION_JPEG,
    SDL_YUV_CONVERSION_BT601,
    SDL_YUV_CONVERSION_BT709,
    SDL_YUV_CONVERSION_AUTOMATIC
} SDL_YUV_CONVERSION_MODE;
# 130 "include/SDL_surface.h"
extern __attribute__((dllexport)) SDL_Surface * SDL_CreateRGBSurface
    (Uint32 flags, int width, int height, int depth,
     Uint32 Rmask, Uint32 Gmask, Uint32 Bmask, Uint32 Amask);


extern __attribute__((dllexport)) SDL_Surface * SDL_CreateRGBSurfaceWithFormat
    (Uint32 flags, int width, int height, int depth, Uint32 format);

extern __attribute__((dllexport)) SDL_Surface * SDL_CreateRGBSurfaceFrom(void *pixels,
                                                              int width,
                                                              int height,
                                                              int depth,
                                                              int pitch,
                                                              Uint32 Rmask,
                                                              Uint32 Gmask,
                                                              Uint32 Bmask,
                                                              Uint32 Amask);
extern __attribute__((dllexport)) SDL_Surface * SDL_CreateRGBSurfaceWithFormatFrom
    (void *pixels, int width, int height, int depth, int pitch, Uint32 format);
extern __attribute__((dllexport)) void SDL_FreeSurface(SDL_Surface * surface);
# 158 "include/SDL_surface.h"
extern __attribute__((dllexport)) int SDL_SetSurfacePalette(SDL_Surface * surface,
                                                  SDL_Palette * palette);
# 180 "include/SDL_surface.h"
extern __attribute__((dllexport)) int SDL_LockSurface(SDL_Surface * surface);

extern __attribute__((dllexport)) void SDL_UnlockSurface(SDL_Surface * surface);
# 193 "include/SDL_surface.h"
extern __attribute__((dllexport)) SDL_Surface * SDL_LoadBMP_RW(SDL_RWops * src,
                                                    int freesrc);
# 216 "include/SDL_surface.h"
extern __attribute__((dllexport)) int SDL_SaveBMP_RW
    (SDL_Surface * surface, SDL_RWops * dst, int freedst);
# 235 "include/SDL_surface.h"
extern __attribute__((dllexport)) int SDL_SetSurfaceRLE(SDL_Surface * surface,
                                              int flag);
# 249 "include/SDL_surface.h"
extern __attribute__((dllexport)) int SDL_SetColorKey(SDL_Surface * surface,
                                            int flag, Uint32 key);






extern __attribute__((dllexport)) SDL_bool SDL_HasColorKey(SDL_Surface * surface);
# 269 "include/SDL_surface.h"
extern __attribute__((dllexport)) int SDL_GetColorKey(SDL_Surface * surface,
                                            Uint32 * key);
# 284 "include/SDL_surface.h"
extern __attribute__((dllexport)) int SDL_SetSurfaceColorMod(SDL_Surface * surface,
                                                   Uint8 r, Uint8 g, Uint8 b);
# 300 "include/SDL_surface.h"
extern __attribute__((dllexport)) int SDL_GetSurfaceColorMod(SDL_Surface * surface,
                                                   Uint8 * r, Uint8 * g,
                                                   Uint8 * b);
# 314 "include/SDL_surface.h"
extern __attribute__((dllexport)) int SDL_SetSurfaceAlphaMod(SDL_Surface * surface,
                                                   Uint8 alpha);
# 327 "include/SDL_surface.h"
extern __attribute__((dllexport)) int SDL_GetSurfaceAlphaMod(SDL_Surface * surface,
                                                   Uint8 * alpha);
# 340 "include/SDL_surface.h"
extern __attribute__((dllexport)) int SDL_SetSurfaceBlendMode(SDL_Surface * surface,
                                                    SDL_BlendMode blendMode);
# 353 "include/SDL_surface.h"
extern __attribute__((dllexport)) int SDL_GetSurfaceBlendMode(SDL_Surface * surface,
                                                    SDL_BlendMode *blendMode);
# 369 "include/SDL_surface.h"
extern __attribute__((dllexport)) SDL_bool SDL_SetClipRect(SDL_Surface * surface,
                                                 const SDL_Rect * rect);







extern __attribute__((dllexport)) void SDL_GetClipRect(SDL_Surface * surface,
                                             SDL_Rect * rect);




extern __attribute__((dllexport)) SDL_Surface * SDL_DuplicateSurface(SDL_Surface * surface);
# 396 "include/SDL_surface.h"
extern __attribute__((dllexport)) SDL_Surface * SDL_ConvertSurface
    (SDL_Surface * src, const SDL_PixelFormat * fmt, Uint32 flags);
extern __attribute__((dllexport)) SDL_Surface * SDL_ConvertSurfaceFormat
    (SDL_Surface * src, Uint32 pixel_format, Uint32 flags);






extern __attribute__((dllexport)) int SDL_ConvertPixels(int width, int height,
                                              Uint32 src_format,
                                              const void * src, int src_pitch,
                                              Uint32 dst_format,
                                              void * dst, int dst_pitch);
# 422 "include/SDL_surface.h"
extern __attribute__((dllexport)) int SDL_FillRect
    (SDL_Surface * dst, const SDL_Rect * rect, Uint32 color);
extern __attribute__((dllexport)) int SDL_FillRects
    (SDL_Surface * dst, const SDL_Rect * rects, int count, Uint32 color);
# 490 "include/SDL_surface.h"
extern __attribute__((dllexport)) int SDL_UpperBlit
    (SDL_Surface * src, const SDL_Rect * srcrect,
     SDL_Surface * dst, SDL_Rect * dstrect);





extern __attribute__((dllexport)) int SDL_LowerBlit
    (SDL_Surface * src, SDL_Rect * srcrect,
     SDL_Surface * dst, SDL_Rect * dstrect);







extern __attribute__((dllexport)) int SDL_SoftStretch(SDL_Surface * src,
                                            const SDL_Rect * srcrect,
                                            SDL_Surface * dst,
                                            const SDL_Rect * dstrect);







extern __attribute__((dllexport)) int SDL_UpperBlitScaled
    (SDL_Surface * src, const SDL_Rect * srcrect,
    SDL_Surface * dst, SDL_Rect * dstrect);





extern __attribute__((dllexport)) int SDL_LowerBlitScaled
    (SDL_Surface * src, SDL_Rect * srcrect,
    SDL_Surface * dst, SDL_Rect * dstrect);




extern __attribute__((dllexport)) void SDL_SetYUVConversionMode(SDL_YUV_CONVERSION_MODE mode);




extern __attribute__((dllexport)) SDL_YUV_CONVERSION_MODE SDL_GetYUVConversionMode(void);




extern __attribute__((dllexport)) SDL_YUV_CONVERSION_MODE SDL_GetYUVConversionModeForResolution(int width, int height);





# 1 "include/close_code.h" 1
# 551 "include/SDL_surface.h" 2
# 35 "include/SDL_video.h" 2

# 1 "include/begin_code.h" 1
# 37 "include/SDL_video.h" 2
# 53 "include/SDL_video.h"
typedef struct
{
    Uint32 format;
    int w;
    int h;
    int refresh_rate;
    void *driverdata;
} SDL_DisplayMode;
# 90 "include/SDL_video.h"
typedef struct SDL_Window SDL_Window;






typedef enum
{
    SDL_WINDOW_FULLSCREEN = 0x00000001,
    SDL_WINDOW_OPENGL = 0x00000002,
    SDL_WINDOW_SHOWN = 0x00000004,
    SDL_WINDOW_HIDDEN = 0x00000008,
    SDL_WINDOW_BORDERLESS = 0x00000010,
    SDL_WINDOW_RESIZABLE = 0x00000020,
    SDL_WINDOW_MINIMIZED = 0x00000040,
    SDL_WINDOW_MAXIMIZED = 0x00000080,
    SDL_WINDOW_INPUT_GRABBED = 0x00000100,
    SDL_WINDOW_INPUT_FOCUS = 0x00000200,
    SDL_WINDOW_MOUSE_FOCUS = 0x00000400,
    SDL_WINDOW_FULLSCREEN_DESKTOP = ( SDL_WINDOW_FULLSCREEN | 0x00001000 ),
    SDL_WINDOW_FOREIGN = 0x00000800,
    SDL_WINDOW_ALLOW_HIGHDPI = 0x00002000,


    SDL_WINDOW_MOUSE_CAPTURE = 0x00004000,
    SDL_WINDOW_ALWAYS_ON_TOP = 0x00008000,
    SDL_WINDOW_SKIP_TASKBAR = 0x00010000,
    SDL_WINDOW_UTILITY = 0x00020000,
    SDL_WINDOW_TOOLTIP = 0x00040000,
    SDL_WINDOW_POPUP_MENU = 0x00080000,
    SDL_WINDOW_VULKAN = 0x10000000
} SDL_WindowFlags;
# 145 "include/SDL_video.h"
typedef enum
{
    SDL_WINDOWEVENT_NONE,
    SDL_WINDOWEVENT_SHOWN,
    SDL_WINDOWEVENT_HIDDEN,
    SDL_WINDOWEVENT_EXPOSED,

    SDL_WINDOWEVENT_MOVED,

    SDL_WINDOWEVENT_RESIZED,
    SDL_WINDOWEVENT_SIZE_CHANGED,


    SDL_WINDOWEVENT_MINIMIZED,
    SDL_WINDOWEVENT_MAXIMIZED,
    SDL_WINDOWEVENT_RESTORED,

    SDL_WINDOWEVENT_ENTER,
    SDL_WINDOWEVENT_LEAVE,
    SDL_WINDOWEVENT_FOCUS_GAINED,
    SDL_WINDOWEVENT_FOCUS_LOST,
    SDL_WINDOWEVENT_CLOSE,
    SDL_WINDOWEVENT_TAKE_FOCUS,
    SDL_WINDOWEVENT_HIT_TEST
} SDL_WindowEventID;




typedef enum
{
    SDL_DISPLAYEVENT_NONE,
    SDL_DISPLAYEVENT_ORIENTATION
} SDL_DisplayEventID;

typedef enum
{
    SDL_ORIENTATION_UNKNOWN,
    SDL_ORIENTATION_LANDSCAPE,
    SDL_ORIENTATION_LANDSCAPE_FLIPPED,
    SDL_ORIENTATION_PORTRAIT,
    SDL_ORIENTATION_PORTRAIT_FLIPPED
} SDL_DisplayOrientation;




typedef void *SDL_GLContext;




typedef enum
{
    SDL_GL_RED_SIZE,
    SDL_GL_GREEN_SIZE,
    SDL_GL_BLUE_SIZE,
    SDL_GL_ALPHA_SIZE,
    SDL_GL_BUFFER_SIZE,
    SDL_GL_DOUBLEBUFFER,
    SDL_GL_DEPTH_SIZE,
    SDL_GL_STENCIL_SIZE,
    SDL_GL_ACCUM_RED_SIZE,
    SDL_GL_ACCUM_GREEN_SIZE,
    SDL_GL_ACCUM_BLUE_SIZE,
    SDL_GL_ACCUM_ALPHA_SIZE,
    SDL_GL_STEREO,
    SDL_GL_MULTISAMPLEBUFFERS,
    SDL_GL_MULTISAMPLESAMPLES,
    SDL_GL_ACCELERATED_VISUAL,
    SDL_GL_RETAINED_BACKING,
    SDL_GL_CONTEXT_MAJOR_VERSION,
    SDL_GL_CONTEXT_MINOR_VERSION,
    SDL_GL_CONTEXT_EGL,
    SDL_GL_CONTEXT_FLAGS,
    SDL_GL_CONTEXT_PROFILE_MASK,
    SDL_GL_SHARE_WITH_CURRENT_CONTEXT,
    SDL_GL_FRAMEBUFFER_SRGB_CAPABLE,
    SDL_GL_CONTEXT_RELEASE_BEHAVIOR,
    SDL_GL_CONTEXT_RESET_NOTIFICATION,
    SDL_GL_CONTEXT_NO_ERROR
} SDL_GLattr;

typedef enum
{
    SDL_GL_CONTEXT_PROFILE_CORE = 0x0001,
    SDL_GL_CONTEXT_PROFILE_COMPATIBILITY = 0x0002,
    SDL_GL_CONTEXT_PROFILE_ES = 0x0004
} SDL_GLprofile;

typedef enum
{
    SDL_GL_CONTEXT_DEBUG_FLAG = 0x0001,
    SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG = 0x0002,
    SDL_GL_CONTEXT_ROBUST_ACCESS_FLAG = 0x0004,
    SDL_GL_CONTEXT_RESET_ISOLATION_FLAG = 0x0008
} SDL_GLcontextFlag;

typedef enum
{
    SDL_GL_CONTEXT_RELEASE_BEHAVIOR_NONE = 0x0000,
    SDL_GL_CONTEXT_RELEASE_BEHAVIOR_FLUSH = 0x0001
} SDL_GLcontextReleaseFlag;

typedef enum
{
    SDL_GL_CONTEXT_RESET_NO_NOTIFICATION = 0x0000,
    SDL_GL_CONTEXT_RESET_LOSE_CONTEXT = 0x0001
} SDL_GLContextResetNotification;
# 262 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_GetNumVideoDrivers(void);
# 272 "include/SDL_video.h"
extern __attribute__((dllexport)) const char * SDL_GetVideoDriver(int index);
# 288 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_VideoInit(const char *driver_name);
# 297 "include/SDL_video.h"
extern __attribute__((dllexport)) void SDL_VideoQuit(void);
# 308 "include/SDL_video.h"
extern __attribute__((dllexport)) const char * SDL_GetCurrentVideoDriver(void);






extern __attribute__((dllexport)) int SDL_GetNumVideoDisplays(void);
# 324 "include/SDL_video.h"
extern __attribute__((dllexport)) const char * SDL_GetDisplayName(int displayIndex);
# 334 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_GetDisplayBounds(int displayIndex, SDL_Rect * rect);
# 353 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_GetDisplayUsableBounds(int displayIndex, SDL_Rect * rect);
# 365 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_GetDisplayDPI(int displayIndex, float * ddpi, float * hdpi, float * vdpi);
# 374 "include/SDL_video.h"
extern __attribute__((dllexport)) SDL_DisplayOrientation SDL_GetDisplayOrientation(int displayIndex);






extern __attribute__((dllexport)) int SDL_GetNumDisplayModes(int displayIndex);
# 394 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_GetDisplayMode(int displayIndex, int modeIndex,
                                               SDL_DisplayMode * mode);




extern __attribute__((dllexport)) int SDL_GetDesktopDisplayMode(int displayIndex, SDL_DisplayMode * mode);




extern __attribute__((dllexport)) int SDL_GetCurrentDisplayMode(int displayIndex, SDL_DisplayMode * mode);
# 429 "include/SDL_video.h"
extern __attribute__((dllexport)) SDL_DisplayMode * SDL_GetClosestDisplayMode(int displayIndex, const SDL_DisplayMode * mode, SDL_DisplayMode * closest);







extern __attribute__((dllexport)) int SDL_GetWindowDisplayIndex(SDL_Window * window);
# 453 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_SetWindowDisplayMode(SDL_Window * window,
                                                     const SDL_DisplayMode
                                                         * mode);
# 464 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_GetWindowDisplayMode(SDL_Window * window,
                                                     SDL_DisplayMode * mode);




extern __attribute__((dllexport)) Uint32 SDL_GetWindowPixelFormat(SDL_Window * window);
# 514 "include/SDL_video.h"
extern __attribute__((dllexport)) SDL_Window * SDL_CreateWindow(const char *title,
                                                      int x, int y, int w,
                                                      int h, Uint32 flags);
# 527 "include/SDL_video.h"
extern __attribute__((dllexport)) SDL_Window * SDL_CreateWindowFrom(const void *data);




extern __attribute__((dllexport)) Uint32 SDL_GetWindowID(SDL_Window * window);




extern __attribute__((dllexport)) SDL_Window * SDL_GetWindowFromID(Uint32 id);




extern __attribute__((dllexport)) Uint32 SDL_GetWindowFlags(SDL_Window * window);






extern __attribute__((dllexport)) void SDL_SetWindowTitle(SDL_Window * window,
                                                const char *title);






extern __attribute__((dllexport)) const char * SDL_GetWindowTitle(SDL_Window * window);







extern __attribute__((dllexport)) void SDL_SetWindowIcon(SDL_Window * window,
                                               SDL_Surface * icon);
# 581 "include/SDL_video.h"
extern __attribute__((dllexport)) void* SDL_SetWindowData(SDL_Window * window,
                                                const char *name,
                                                void *userdata);
# 595 "include/SDL_video.h"
extern __attribute__((dllexport)) void * SDL_GetWindowData(SDL_Window * window,
                                                const char *name);
# 611 "include/SDL_video.h"
extern __attribute__((dllexport)) void SDL_SetWindowPosition(SDL_Window * window,
                                                   int x, int y);
# 625 "include/SDL_video.h"
extern __attribute__((dllexport)) void SDL_GetWindowPosition(SDL_Window * window,
                                                   int *x, int *y);
# 646 "include/SDL_video.h"
extern __attribute__((dllexport)) void SDL_SetWindowSize(SDL_Window * window, int w,
                                               int h);
# 665 "include/SDL_video.h"
extern __attribute__((dllexport)) void SDL_GetWindowSize(SDL_Window * window, int *w,
                                               int *h);
# 683 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_GetWindowBordersSize(SDL_Window * window,
                                                     int *top, int *left,
                                                     int *bottom, int *right);
# 700 "include/SDL_video.h"
extern __attribute__((dllexport)) void SDL_SetWindowMinimumSize(SDL_Window * window,
                                                      int min_w, int min_h);
# 713 "include/SDL_video.h"
extern __attribute__((dllexport)) void SDL_GetWindowMinimumSize(SDL_Window * window,
                                                      int *w, int *h);
# 729 "include/SDL_video.h"
extern __attribute__((dllexport)) void SDL_SetWindowMaximumSize(SDL_Window * window,
                                                      int max_w, int max_h);
# 742 "include/SDL_video.h"
extern __attribute__((dllexport)) void SDL_GetWindowMaximumSize(SDL_Window * window,
                                                      int *w, int *h);
# 759 "include/SDL_video.h"
extern __attribute__((dllexport)) void SDL_SetWindowBordered(SDL_Window * window,
                                                   SDL_bool bordered);
# 776 "include/SDL_video.h"
extern __attribute__((dllexport)) void SDL_SetWindowResizable(SDL_Window * window,
                                                    SDL_bool resizable);






extern __attribute__((dllexport)) void SDL_ShowWindow(SDL_Window * window);






extern __attribute__((dllexport)) void SDL_HideWindow(SDL_Window * window);




extern __attribute__((dllexport)) void SDL_RaiseWindow(SDL_Window * window);






extern __attribute__((dllexport)) void SDL_MaximizeWindow(SDL_Window * window);






extern __attribute__((dllexport)) void SDL_MinimizeWindow(SDL_Window * window);







extern __attribute__((dllexport)) void SDL_RestoreWindow(SDL_Window * window);
# 828 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_SetWindowFullscreen(SDL_Window * window,
                                                    Uint32 flags);
# 844 "include/SDL_video.h"
extern __attribute__((dllexport)) SDL_Surface * SDL_GetWindowSurface(SDL_Window * window);
# 854 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_UpdateWindowSurface(SDL_Window * window);
# 864 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_UpdateWindowSurfaceRects(SDL_Window * window,
                                                         const SDL_Rect * rects,
                                                         int numrects);
# 879 "include/SDL_video.h"
extern __attribute__((dllexport)) void SDL_SetWindowGrab(SDL_Window * window,
                                               SDL_bool grabbed);
# 889 "include/SDL_video.h"
extern __attribute__((dllexport)) SDL_bool SDL_GetWindowGrab(SDL_Window * window);
# 898 "include/SDL_video.h"
extern __attribute__((dllexport)) SDL_Window * SDL_GetGrabbedWindow(void);
# 908 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_SetWindowBrightness(SDL_Window * window, float brightness);
# 917 "include/SDL_video.h"
extern __attribute__((dllexport)) float SDL_GetWindowBrightness(SDL_Window * window);
# 930 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_SetWindowOpacity(SDL_Window * window, float opacity);
# 945 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_GetWindowOpacity(SDL_Window * window, float * out_opacity);
# 955 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_SetWindowModalFor(SDL_Window * modal_window, SDL_Window * parent_window);
# 969 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_SetWindowInputFocus(SDL_Window * window);
# 989 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_SetWindowGammaRamp(SDL_Window * window,
                                                   const Uint16 * red,
                                                   const Uint16 * green,
                                                   const Uint16 * blue);
# 1009 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_GetWindowGammaRamp(SDL_Window * window,
                                                   Uint16 * red,
                                                   Uint16 * green,
                                                   Uint16 * blue);






typedef enum
{
    SDL_HITTEST_NORMAL,
    SDL_HITTEST_DRAGGABLE,
    SDL_HITTEST_RESIZE_TOPLEFT,
    SDL_HITTEST_RESIZE_TOP,
    SDL_HITTEST_RESIZE_TOPRIGHT,
    SDL_HITTEST_RESIZE_RIGHT,
    SDL_HITTEST_RESIZE_BOTTOMRIGHT,
    SDL_HITTEST_RESIZE_BOTTOM,
    SDL_HITTEST_RESIZE_BOTTOMLEFT,
    SDL_HITTEST_RESIZE_LEFT
} SDL_HitTestResult;






typedef SDL_HitTestResult ( *SDL_HitTest)(SDL_Window *win,
                                                 const SDL_Point *area,
                                                 void *data);
# 1079 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_SetWindowHitTest(SDL_Window * window,
                                                 SDL_HitTest callback,
                                                 void *callback_data);




extern __attribute__((dllexport)) void SDL_DestroyWindow(SDL_Window * window);
# 1095 "include/SDL_video.h"
extern __attribute__((dllexport)) SDL_bool SDL_IsScreenSaverEnabled(void);







extern __attribute__((dllexport)) void SDL_EnableScreenSaver(void);







extern __attribute__((dllexport)) void SDL_DisableScreenSaver(void);
# 1137 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_GL_LoadLibrary(const char *path);




extern __attribute__((dllexport)) void * SDL_GL_GetProcAddress(const char *proc);






extern __attribute__((dllexport)) void SDL_GL_UnloadLibrary(void);





extern __attribute__((dllexport)) SDL_bool SDL_GL_ExtensionSupported(const char
                                                           *extension);




extern __attribute__((dllexport)) void SDL_GL_ResetAttributes(void);






extern __attribute__((dllexport)) int SDL_GL_SetAttribute(SDL_GLattr attr, int value);







extern __attribute__((dllexport)) int SDL_GL_GetAttribute(SDL_GLattr attr, int *value);







extern __attribute__((dllexport)) SDL_GLContext SDL_GL_CreateContext(SDL_Window *
                                                           window);






extern __attribute__((dllexport)) int SDL_GL_MakeCurrent(SDL_Window * window,
                                               SDL_GLContext context);




extern __attribute__((dllexport)) SDL_Window* SDL_GL_GetCurrentWindow(void);




extern __attribute__((dllexport)) SDL_GLContext SDL_GL_GetCurrentContext(void);
# 1221 "include/SDL_video.h"
extern __attribute__((dllexport)) void SDL_GL_GetDrawableSize(SDL_Window * window, int *w,
                                                    int *h);
# 1236 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_GL_SetSwapInterval(int interval);
# 1249 "include/SDL_video.h"
extern __attribute__((dllexport)) int SDL_GL_GetSwapInterval(void);





extern __attribute__((dllexport)) void SDL_GL_SwapWindow(SDL_Window * window);






extern __attribute__((dllexport)) void SDL_GL_DeleteContext(SDL_GLContext context);
# 1271 "include/SDL_video.h"
# 1 "include/close_code.h" 1
# 1272 "include/SDL_video.h" 2
# 34 "include/SDL_events.h" 2
# 1 "include/SDL_keyboard.h" 1
# 33 "include/SDL_keyboard.h"
# 1 "include/SDL_keycode.h" 1
# 32 "include/SDL_keycode.h"
# 1 "include/SDL_scancode.h" 1
# 43 "include/SDL_scancode.h"
typedef enum
{
    SDL_SCANCODE_UNKNOWN = 0,
# 54 "include/SDL_scancode.h"
    SDL_SCANCODE_A = 4,
    SDL_SCANCODE_B = 5,
    SDL_SCANCODE_C = 6,
    SDL_SCANCODE_D = 7,
    SDL_SCANCODE_E = 8,
    SDL_SCANCODE_F = 9,
    SDL_SCANCODE_G = 10,
    SDL_SCANCODE_H = 11,
    SDL_SCANCODE_I = 12,
    SDL_SCANCODE_J = 13,
    SDL_SCANCODE_K = 14,
    SDL_SCANCODE_L = 15,
    SDL_SCANCODE_M = 16,
    SDL_SCANCODE_N = 17,
    SDL_SCANCODE_O = 18,
    SDL_SCANCODE_P = 19,
    SDL_SCANCODE_Q = 20,
    SDL_SCANCODE_R = 21,
    SDL_SCANCODE_S = 22,
    SDL_SCANCODE_T = 23,
    SDL_SCANCODE_U = 24,
    SDL_SCANCODE_V = 25,
    SDL_SCANCODE_W = 26,
    SDL_SCANCODE_X = 27,
    SDL_SCANCODE_Y = 28,
    SDL_SCANCODE_Z = 29,

    SDL_SCANCODE_1 = 30,
    SDL_SCANCODE_2 = 31,
    SDL_SCANCODE_3 = 32,
    SDL_SCANCODE_4 = 33,
    SDL_SCANCODE_5 = 34,
    SDL_SCANCODE_6 = 35,
    SDL_SCANCODE_7 = 36,
    SDL_SCANCODE_8 = 37,
    SDL_SCANCODE_9 = 38,
    SDL_SCANCODE_0 = 39,

    SDL_SCANCODE_RETURN = 40,
    SDL_SCANCODE_ESCAPE = 41,
    SDL_SCANCODE_BACKSPACE = 42,
    SDL_SCANCODE_TAB = 43,
    SDL_SCANCODE_SPACE = 44,

    SDL_SCANCODE_MINUS = 45,
    SDL_SCANCODE_EQUALS = 46,
    SDL_SCANCODE_LEFTBRACKET = 47,
    SDL_SCANCODE_RIGHTBRACKET = 48,
    SDL_SCANCODE_BACKSLASH = 49,
# 116 "include/SDL_scancode.h"
    SDL_SCANCODE_NONUSHASH = 50,
# 128 "include/SDL_scancode.h"
    SDL_SCANCODE_SEMICOLON = 51,
    SDL_SCANCODE_APOSTROPHE = 52,
    SDL_SCANCODE_GRAVE = 53,
# 147 "include/SDL_scancode.h"
    SDL_SCANCODE_COMMA = 54,
    SDL_SCANCODE_PERIOD = 55,
    SDL_SCANCODE_SLASH = 56,

    SDL_SCANCODE_CAPSLOCK = 57,

    SDL_SCANCODE_F1 = 58,
    SDL_SCANCODE_F2 = 59,
    SDL_SCANCODE_F3 = 60,
    SDL_SCANCODE_F4 = 61,
    SDL_SCANCODE_F5 = 62,
    SDL_SCANCODE_F6 = 63,
    SDL_SCANCODE_F7 = 64,
    SDL_SCANCODE_F8 = 65,
    SDL_SCANCODE_F9 = 66,
    SDL_SCANCODE_F10 = 67,
    SDL_SCANCODE_F11 = 68,
    SDL_SCANCODE_F12 = 69,

    SDL_SCANCODE_PRINTSCREEN = 70,
    SDL_SCANCODE_SCROLLLOCK = 71,
    SDL_SCANCODE_PAUSE = 72,
    SDL_SCANCODE_INSERT = 73,

    SDL_SCANCODE_HOME = 74,
    SDL_SCANCODE_PAGEUP = 75,
    SDL_SCANCODE_DELETE = 76,
    SDL_SCANCODE_END = 77,
    SDL_SCANCODE_PAGEDOWN = 78,
    SDL_SCANCODE_RIGHT = 79,
    SDL_SCANCODE_LEFT = 80,
    SDL_SCANCODE_DOWN = 81,
    SDL_SCANCODE_UP = 82,

    SDL_SCANCODE_NUMLOCKCLEAR = 83,

    SDL_SCANCODE_KP_DIVIDE = 84,
    SDL_SCANCODE_KP_MULTIPLY = 85,
    SDL_SCANCODE_KP_MINUS = 86,
    SDL_SCANCODE_KP_PLUS = 87,
    SDL_SCANCODE_KP_ENTER = 88,
    SDL_SCANCODE_KP_1 = 89,
    SDL_SCANCODE_KP_2 = 90,
    SDL_SCANCODE_KP_3 = 91,
    SDL_SCANCODE_KP_4 = 92,
    SDL_SCANCODE_KP_5 = 93,
    SDL_SCANCODE_KP_6 = 94,
    SDL_SCANCODE_KP_7 = 95,
    SDL_SCANCODE_KP_8 = 96,
    SDL_SCANCODE_KP_9 = 97,
    SDL_SCANCODE_KP_0 = 98,
    SDL_SCANCODE_KP_PERIOD = 99,

    SDL_SCANCODE_NONUSBACKSLASH = 100,
# 210 "include/SDL_scancode.h"
    SDL_SCANCODE_APPLICATION = 101,
    SDL_SCANCODE_POWER = 102,


    SDL_SCANCODE_KP_EQUALS = 103,
    SDL_SCANCODE_F13 = 104,
    SDL_SCANCODE_F14 = 105,
    SDL_SCANCODE_F15 = 106,
    SDL_SCANCODE_F16 = 107,
    SDL_SCANCODE_F17 = 108,
    SDL_SCANCODE_F18 = 109,
    SDL_SCANCODE_F19 = 110,
    SDL_SCANCODE_F20 = 111,
    SDL_SCANCODE_F21 = 112,
    SDL_SCANCODE_F22 = 113,
    SDL_SCANCODE_F23 = 114,
    SDL_SCANCODE_F24 = 115,
    SDL_SCANCODE_EXECUTE = 116,
    SDL_SCANCODE_HELP = 117,
    SDL_SCANCODE_MENU = 118,
    SDL_SCANCODE_SELECT = 119,
    SDL_SCANCODE_STOP = 120,
    SDL_SCANCODE_AGAIN = 121,
    SDL_SCANCODE_UNDO = 122,
    SDL_SCANCODE_CUT = 123,
    SDL_SCANCODE_COPY = 124,
    SDL_SCANCODE_PASTE = 125,
    SDL_SCANCODE_FIND = 126,
    SDL_SCANCODE_MUTE = 127,
    SDL_SCANCODE_VOLUMEUP = 128,
    SDL_SCANCODE_VOLUMEDOWN = 129,




    SDL_SCANCODE_KP_COMMA = 133,
    SDL_SCANCODE_KP_EQUALSAS400 = 134,

    SDL_SCANCODE_INTERNATIONAL1 = 135,

    SDL_SCANCODE_INTERNATIONAL2 = 136,
    SDL_SCANCODE_INTERNATIONAL3 = 137,
    SDL_SCANCODE_INTERNATIONAL4 = 138,
    SDL_SCANCODE_INTERNATIONAL5 = 139,
    SDL_SCANCODE_INTERNATIONAL6 = 140,
    SDL_SCANCODE_INTERNATIONAL7 = 141,
    SDL_SCANCODE_INTERNATIONAL8 = 142,
    SDL_SCANCODE_INTERNATIONAL9 = 143,
    SDL_SCANCODE_LANG1 = 144,
    SDL_SCANCODE_LANG2 = 145,
    SDL_SCANCODE_LANG3 = 146,
    SDL_SCANCODE_LANG4 = 147,
    SDL_SCANCODE_LANG5 = 148,
    SDL_SCANCODE_LANG6 = 149,
    SDL_SCANCODE_LANG7 = 150,
    SDL_SCANCODE_LANG8 = 151,
    SDL_SCANCODE_LANG9 = 152,

    SDL_SCANCODE_ALTERASE = 153,
    SDL_SCANCODE_SYSREQ = 154,
    SDL_SCANCODE_CANCEL = 155,
    SDL_SCANCODE_CLEAR = 156,
    SDL_SCANCODE_PRIOR = 157,
    SDL_SCANCODE_RETURN2 = 158,
    SDL_SCANCODE_SEPARATOR = 159,
    SDL_SCANCODE_OUT = 160,
    SDL_SCANCODE_OPER = 161,
    SDL_SCANCODE_CLEARAGAIN = 162,
    SDL_SCANCODE_CRSEL = 163,
    SDL_SCANCODE_EXSEL = 164,

    SDL_SCANCODE_KP_00 = 176,
    SDL_SCANCODE_KP_000 = 177,
    SDL_SCANCODE_THOUSANDSSEPARATOR = 178,
    SDL_SCANCODE_DECIMALSEPARATOR = 179,
    SDL_SCANCODE_CURRENCYUNIT = 180,
    SDL_SCANCODE_CURRENCYSUBUNIT = 181,
    SDL_SCANCODE_KP_LEFTPAREN = 182,
    SDL_SCANCODE_KP_RIGHTPAREN = 183,
    SDL_SCANCODE_KP_LEFTBRACE = 184,
    SDL_SCANCODE_KP_RIGHTBRACE = 185,
    SDL_SCANCODE_KP_TAB = 186,
    SDL_SCANCODE_KP_BACKSPACE = 187,
    SDL_SCANCODE_KP_A = 188,
    SDL_SCANCODE_KP_B = 189,
    SDL_SCANCODE_KP_C = 190,
    SDL_SCANCODE_KP_D = 191,
    SDL_SCANCODE_KP_E = 192,
    SDL_SCANCODE_KP_F = 193,
    SDL_SCANCODE_KP_XOR = 194,
    SDL_SCANCODE_KP_POWER = 195,
    SDL_SCANCODE_KP_PERCENT = 196,
    SDL_SCANCODE_KP_LESS = 197,
    SDL_SCANCODE_KP_GREATER = 198,
    SDL_SCANCODE_KP_AMPERSAND = 199,
    SDL_SCANCODE_KP_DBLAMPERSAND = 200,
    SDL_SCANCODE_KP_VERTICALBAR = 201,
    SDL_SCANCODE_KP_DBLVERTICALBAR = 202,
    SDL_SCANCODE_KP_COLON = 203,
    SDL_SCANCODE_KP_HASH = 204,
    SDL_SCANCODE_KP_SPACE = 205,
    SDL_SCANCODE_KP_AT = 206,
    SDL_SCANCODE_KP_EXCLAM = 207,
    SDL_SCANCODE_KP_MEMSTORE = 208,
    SDL_SCANCODE_KP_MEMRECALL = 209,
    SDL_SCANCODE_KP_MEMCLEAR = 210,
    SDL_SCANCODE_KP_MEMADD = 211,
    SDL_SCANCODE_KP_MEMSUBTRACT = 212,
    SDL_SCANCODE_KP_MEMMULTIPLY = 213,
    SDL_SCANCODE_KP_MEMDIVIDE = 214,
    SDL_SCANCODE_KP_PLUSMINUS = 215,
    SDL_SCANCODE_KP_CLEAR = 216,
    SDL_SCANCODE_KP_CLEARENTRY = 217,
    SDL_SCANCODE_KP_BINARY = 218,
    SDL_SCANCODE_KP_OCTAL = 219,
    SDL_SCANCODE_KP_DECIMAL = 220,
    SDL_SCANCODE_KP_HEXADECIMAL = 221,

    SDL_SCANCODE_LCTRL = 224,
    SDL_SCANCODE_LSHIFT = 225,
    SDL_SCANCODE_LALT = 226,
    SDL_SCANCODE_LGUI = 227,
    SDL_SCANCODE_RCTRL = 228,
    SDL_SCANCODE_RSHIFT = 229,
    SDL_SCANCODE_RALT = 230,
    SDL_SCANCODE_RGUI = 231,

    SDL_SCANCODE_MODE = 257,
# 351 "include/SDL_scancode.h"
    SDL_SCANCODE_AUDIONEXT = 258,
    SDL_SCANCODE_AUDIOPREV = 259,
    SDL_SCANCODE_AUDIOSTOP = 260,
    SDL_SCANCODE_AUDIOPLAY = 261,
    SDL_SCANCODE_AUDIOMUTE = 262,
    SDL_SCANCODE_MEDIASELECT = 263,
    SDL_SCANCODE_WWW = 264,
    SDL_SCANCODE_MAIL = 265,
    SDL_SCANCODE_CALCULATOR = 266,
    SDL_SCANCODE_COMPUTER = 267,
    SDL_SCANCODE_AC_SEARCH = 268,
    SDL_SCANCODE_AC_HOME = 269,
    SDL_SCANCODE_AC_BACK = 270,
    SDL_SCANCODE_AC_FORWARD = 271,
    SDL_SCANCODE_AC_STOP = 272,
    SDL_SCANCODE_AC_REFRESH = 273,
    SDL_SCANCODE_AC_BOOKMARKS = 274,
# 378 "include/SDL_scancode.h"
    SDL_SCANCODE_BRIGHTNESSDOWN = 275,
    SDL_SCANCODE_BRIGHTNESSUP = 276,
    SDL_SCANCODE_DISPLAYSWITCH = 277,

    SDL_SCANCODE_KBDILLUMTOGGLE = 278,
    SDL_SCANCODE_KBDILLUMDOWN = 279,
    SDL_SCANCODE_KBDILLUMUP = 280,
    SDL_SCANCODE_EJECT = 281,
    SDL_SCANCODE_SLEEP = 282,

    SDL_SCANCODE_APP1 = 283,
    SDL_SCANCODE_APP2 = 284,
# 400 "include/SDL_scancode.h"
    SDL_SCANCODE_AUDIOREWIND = 285,
    SDL_SCANCODE_AUDIOFASTFORWARD = 286,





    SDL_NUM_SCANCODES = 512

} SDL_Scancode;
# 33 "include/SDL_keycode.h" 2
# 45 "include/SDL_keycode.h"
typedef Sint32 SDL_Keycode;




typedef enum
{
    SDLK_UNKNOWN = 0,

    SDLK_RETURN = '\r',
    SDLK_ESCAPE = '\033',
    SDLK_BACKSPACE = '\b',
    SDLK_TAB = '\t',
    SDLK_SPACE = ' ',
    SDLK_EXCLAIM = '!',
    SDLK_QUOTEDBL = '"',
    SDLK_HASH = '#',
    SDLK_PERCENT = '%',
    SDLK_DOLLAR = '$',
    SDLK_AMPERSAND = '&',
    SDLK_QUOTE = '\'',
    SDLK_LEFTPAREN = '(',
    SDLK_RIGHTPAREN = ')',
    SDLK_ASTERISK = '*',
    SDLK_PLUS = '+',
    SDLK_COMMA = ',',
    SDLK_MINUS = '-',
    SDLK_PERIOD = '.',
    SDLK_SLASH = '/',
    SDLK_0 = '0',
    SDLK_1 = '1',
    SDLK_2 = '2',
    SDLK_3 = '3',
    SDLK_4 = '4',
    SDLK_5 = '5',
    SDLK_6 = '6',
    SDLK_7 = '7',
    SDLK_8 = '8',
    SDLK_9 = '9',
    SDLK_COLON = ':',
    SDLK_SEMICOLON = ';',
    SDLK_LESS = '<',
    SDLK_EQUALS = '=',
    SDLK_GREATER = '>',
    SDLK_QUESTION = '?',
    SDLK_AT = '@',



    SDLK_LEFTBRACKET = '[',
    SDLK_BACKSLASH = '\\',
    SDLK_RIGHTBRACKET = ']',
    SDLK_CARET = '^',
    SDLK_UNDERSCORE = '_',
    SDLK_BACKQUOTE = '`',
    SDLK_a = 'a',
    SDLK_b = 'b',
    SDLK_c = 'c',
    SDLK_d = 'd',
    SDLK_e = 'e',
    SDLK_f = 'f',
    SDLK_g = 'g',
    SDLK_h = 'h',
    SDLK_i = 'i',
    SDLK_j = 'j',
    SDLK_k = 'k',
    SDLK_l = 'l',
    SDLK_m = 'm',
    SDLK_n = 'n',
    SDLK_o = 'o',
    SDLK_p = 'p',
    SDLK_q = 'q',
    SDLK_r = 'r',
    SDLK_s = 's',
    SDLK_t = 't',
    SDLK_u = 'u',
    SDLK_v = 'v',
    SDLK_w = 'w',
    SDLK_x = 'x',
    SDLK_y = 'y',
    SDLK_z = 'z',

    SDLK_CAPSLOCK = (SDL_SCANCODE_CAPSLOCK | (1<<30)),

    SDLK_F1 = (SDL_SCANCODE_F1 | (1<<30)),
    SDLK_F2 = (SDL_SCANCODE_F2 | (1<<30)),
    SDLK_F3 = (SDL_SCANCODE_F3 | (1<<30)),
    SDLK_F4 = (SDL_SCANCODE_F4 | (1<<30)),
    SDLK_F5 = (SDL_SCANCODE_F5 | (1<<30)),
    SDLK_F6 = (SDL_SCANCODE_F6 | (1<<30)),
    SDLK_F7 = (SDL_SCANCODE_F7 | (1<<30)),
    SDLK_F8 = (SDL_SCANCODE_F8 | (1<<30)),
    SDLK_F9 = (SDL_SCANCODE_F9 | (1<<30)),
    SDLK_F10 = (SDL_SCANCODE_F10 | (1<<30)),
    SDLK_F11 = (SDL_SCANCODE_F11 | (1<<30)),
    SDLK_F12 = (SDL_SCANCODE_F12 | (1<<30)),

    SDLK_PRINTSCREEN = (SDL_SCANCODE_PRINTSCREEN | (1<<30)),
    SDLK_SCROLLLOCK = (SDL_SCANCODE_SCROLLLOCK | (1<<30)),
    SDLK_PAUSE = (SDL_SCANCODE_PAUSE | (1<<30)),
    SDLK_INSERT = (SDL_SCANCODE_INSERT | (1<<30)),
    SDLK_HOME = (SDL_SCANCODE_HOME | (1<<30)),
    SDLK_PAGEUP = (SDL_SCANCODE_PAGEUP | (1<<30)),
    SDLK_DELETE = '\177',
    SDLK_END = (SDL_SCANCODE_END | (1<<30)),
    SDLK_PAGEDOWN = (SDL_SCANCODE_PAGEDOWN | (1<<30)),
    SDLK_RIGHT = (SDL_SCANCODE_RIGHT | (1<<30)),
    SDLK_LEFT = (SDL_SCANCODE_LEFT | (1<<30)),
    SDLK_DOWN = (SDL_SCANCODE_DOWN | (1<<30)),
    SDLK_UP = (SDL_SCANCODE_UP | (1<<30)),

    SDLK_NUMLOCKCLEAR = (SDL_SCANCODE_NUMLOCKCLEAR | (1<<30)),
    SDLK_KP_DIVIDE = (SDL_SCANCODE_KP_DIVIDE | (1<<30)),
    SDLK_KP_MULTIPLY = (SDL_SCANCODE_KP_MULTIPLY | (1<<30)),
    SDLK_KP_MINUS = (SDL_SCANCODE_KP_MINUS | (1<<30)),
    SDLK_KP_PLUS = (SDL_SCANCODE_KP_PLUS | (1<<30)),
    SDLK_KP_ENTER = (SDL_SCANCODE_KP_ENTER | (1<<30)),
    SDLK_KP_1 = (SDL_SCANCODE_KP_1 | (1<<30)),
    SDLK_KP_2 = (SDL_SCANCODE_KP_2 | (1<<30)),
    SDLK_KP_3 = (SDL_SCANCODE_KP_3 | (1<<30)),
    SDLK_KP_4 = (SDL_SCANCODE_KP_4 | (1<<30)),
    SDLK_KP_5 = (SDL_SCANCODE_KP_5 | (1<<30)),
    SDLK_KP_6 = (SDL_SCANCODE_KP_6 | (1<<30)),
    SDLK_KP_7 = (SDL_SCANCODE_KP_7 | (1<<30)),
    SDLK_KP_8 = (SDL_SCANCODE_KP_8 | (1<<30)),
    SDLK_KP_9 = (SDL_SCANCODE_KP_9 | (1<<30)),
    SDLK_KP_0 = (SDL_SCANCODE_KP_0 | (1<<30)),
    SDLK_KP_PERIOD = (SDL_SCANCODE_KP_PERIOD | (1<<30)),

    SDLK_APPLICATION = (SDL_SCANCODE_APPLICATION | (1<<30)),
    SDLK_POWER = (SDL_SCANCODE_POWER | (1<<30)),
    SDLK_KP_EQUALS = (SDL_SCANCODE_KP_EQUALS | (1<<30)),
    SDLK_F13 = (SDL_SCANCODE_F13 | (1<<30)),
    SDLK_F14 = (SDL_SCANCODE_F14 | (1<<30)),
    SDLK_F15 = (SDL_SCANCODE_F15 | (1<<30)),
    SDLK_F16 = (SDL_SCANCODE_F16 | (1<<30)),
    SDLK_F17 = (SDL_SCANCODE_F17 | (1<<30)),
    SDLK_F18 = (SDL_SCANCODE_F18 | (1<<30)),
    SDLK_F19 = (SDL_SCANCODE_F19 | (1<<30)),
    SDLK_F20 = (SDL_SCANCODE_F20 | (1<<30)),
    SDLK_F21 = (SDL_SCANCODE_F21 | (1<<30)),
    SDLK_F22 = (SDL_SCANCODE_F22 | (1<<30)),
    SDLK_F23 = (SDL_SCANCODE_F23 | (1<<30)),
    SDLK_F24 = (SDL_SCANCODE_F24 | (1<<30)),
    SDLK_EXECUTE = (SDL_SCANCODE_EXECUTE | (1<<30)),
    SDLK_HELP = (SDL_SCANCODE_HELP | (1<<30)),
    SDLK_MENU = (SDL_SCANCODE_MENU | (1<<30)),
    SDLK_SELECT = (SDL_SCANCODE_SELECT | (1<<30)),
    SDLK_STOP = (SDL_SCANCODE_STOP | (1<<30)),
    SDLK_AGAIN = (SDL_SCANCODE_AGAIN | (1<<30)),
    SDLK_UNDO = (SDL_SCANCODE_UNDO | (1<<30)),
    SDLK_CUT = (SDL_SCANCODE_CUT | (1<<30)),
    SDLK_COPY = (SDL_SCANCODE_COPY | (1<<30)),
    SDLK_PASTE = (SDL_SCANCODE_PASTE | (1<<30)),
    SDLK_FIND = (SDL_SCANCODE_FIND | (1<<30)),
    SDLK_MUTE = (SDL_SCANCODE_MUTE | (1<<30)),
    SDLK_VOLUMEUP = (SDL_SCANCODE_VOLUMEUP | (1<<30)),
    SDLK_VOLUMEDOWN = (SDL_SCANCODE_VOLUMEDOWN | (1<<30)),
    SDLK_KP_COMMA = (SDL_SCANCODE_KP_COMMA | (1<<30)),
    SDLK_KP_EQUALSAS400 =
        (SDL_SCANCODE_KP_EQUALSAS400 | (1<<30)),

    SDLK_ALTERASE = (SDL_SCANCODE_ALTERASE | (1<<30)),
    SDLK_SYSREQ = (SDL_SCANCODE_SYSREQ | (1<<30)),
    SDLK_CANCEL = (SDL_SCANCODE_CANCEL | (1<<30)),
    SDLK_CLEAR = (SDL_SCANCODE_CLEAR | (1<<30)),
    SDLK_PRIOR = (SDL_SCANCODE_PRIOR | (1<<30)),
    SDLK_RETURN2 = (SDL_SCANCODE_RETURN2 | (1<<30)),
    SDLK_SEPARATOR = (SDL_SCANCODE_SEPARATOR | (1<<30)),
    SDLK_OUT = (SDL_SCANCODE_OUT | (1<<30)),
    SDLK_OPER = (SDL_SCANCODE_OPER | (1<<30)),
    SDLK_CLEARAGAIN = (SDL_SCANCODE_CLEARAGAIN | (1<<30)),
    SDLK_CRSEL = (SDL_SCANCODE_CRSEL | (1<<30)),
    SDLK_EXSEL = (SDL_SCANCODE_EXSEL | (1<<30)),

    SDLK_KP_00 = (SDL_SCANCODE_KP_00 | (1<<30)),
    SDLK_KP_000 = (SDL_SCANCODE_KP_000 | (1<<30)),
    SDLK_THOUSANDSSEPARATOR =
        (SDL_SCANCODE_THOUSANDSSEPARATOR | (1<<30)),
    SDLK_DECIMALSEPARATOR =
        (SDL_SCANCODE_DECIMALSEPARATOR | (1<<30)),
    SDLK_CURRENCYUNIT = (SDL_SCANCODE_CURRENCYUNIT | (1<<30)),
    SDLK_CURRENCYSUBUNIT =
        (SDL_SCANCODE_CURRENCYSUBUNIT | (1<<30)),
    SDLK_KP_LEFTPAREN = (SDL_SCANCODE_KP_LEFTPAREN | (1<<30)),
    SDLK_KP_RIGHTPAREN = (SDL_SCANCODE_KP_RIGHTPAREN | (1<<30)),
    SDLK_KP_LEFTBRACE = (SDL_SCANCODE_KP_LEFTBRACE | (1<<30)),
    SDLK_KP_RIGHTBRACE = (SDL_SCANCODE_KP_RIGHTBRACE | (1<<30)),
    SDLK_KP_TAB = (SDL_SCANCODE_KP_TAB | (1<<30)),
    SDLK_KP_BACKSPACE = (SDL_SCANCODE_KP_BACKSPACE | (1<<30)),
    SDLK_KP_A = (SDL_SCANCODE_KP_A | (1<<30)),
    SDLK_KP_B = (SDL_SCANCODE_KP_B | (1<<30)),
    SDLK_KP_C = (SDL_SCANCODE_KP_C | (1<<30)),
    SDLK_KP_D = (SDL_SCANCODE_KP_D | (1<<30)),
    SDLK_KP_E = (SDL_SCANCODE_KP_E | (1<<30)),
    SDLK_KP_F = (SDL_SCANCODE_KP_F | (1<<30)),
    SDLK_KP_XOR = (SDL_SCANCODE_KP_XOR | (1<<30)),
    SDLK_KP_POWER = (SDL_SCANCODE_KP_POWER | (1<<30)),
    SDLK_KP_PERCENT = (SDL_SCANCODE_KP_PERCENT | (1<<30)),
    SDLK_KP_LESS = (SDL_SCANCODE_KP_LESS | (1<<30)),
    SDLK_KP_GREATER = (SDL_SCANCODE_KP_GREATER | (1<<30)),
    SDLK_KP_AMPERSAND = (SDL_SCANCODE_KP_AMPERSAND | (1<<30)),
    SDLK_KP_DBLAMPERSAND =
        (SDL_SCANCODE_KP_DBLAMPERSAND | (1<<30)),
    SDLK_KP_VERTICALBAR =
        (SDL_SCANCODE_KP_VERTICALBAR | (1<<30)),
    SDLK_KP_DBLVERTICALBAR =
        (SDL_SCANCODE_KP_DBLVERTICALBAR | (1<<30)),
    SDLK_KP_COLON = (SDL_SCANCODE_KP_COLON | (1<<30)),
    SDLK_KP_HASH = (SDL_SCANCODE_KP_HASH | (1<<30)),
    SDLK_KP_SPACE = (SDL_SCANCODE_KP_SPACE | (1<<30)),
    SDLK_KP_AT = (SDL_SCANCODE_KP_AT | (1<<30)),
    SDLK_KP_EXCLAM = (SDL_SCANCODE_KP_EXCLAM | (1<<30)),
    SDLK_KP_MEMSTORE = (SDL_SCANCODE_KP_MEMSTORE | (1<<30)),
    SDLK_KP_MEMRECALL = (SDL_SCANCODE_KP_MEMRECALL | (1<<30)),
    SDLK_KP_MEMCLEAR = (SDL_SCANCODE_KP_MEMCLEAR | (1<<30)),
    SDLK_KP_MEMADD = (SDL_SCANCODE_KP_MEMADD | (1<<30)),
    SDLK_KP_MEMSUBTRACT =
        (SDL_SCANCODE_KP_MEMSUBTRACT | (1<<30)),
    SDLK_KP_MEMMULTIPLY =
        (SDL_SCANCODE_KP_MEMMULTIPLY | (1<<30)),
    SDLK_KP_MEMDIVIDE = (SDL_SCANCODE_KP_MEMDIVIDE | (1<<30)),
    SDLK_KP_PLUSMINUS = (SDL_SCANCODE_KP_PLUSMINUS | (1<<30)),
    SDLK_KP_CLEAR = (SDL_SCANCODE_KP_CLEAR | (1<<30)),
    SDLK_KP_CLEARENTRY = (SDL_SCANCODE_KP_CLEARENTRY | (1<<30)),
    SDLK_KP_BINARY = (SDL_SCANCODE_KP_BINARY | (1<<30)),
    SDLK_KP_OCTAL = (SDL_SCANCODE_KP_OCTAL | (1<<30)),
    SDLK_KP_DECIMAL = (SDL_SCANCODE_KP_DECIMAL | (1<<30)),
    SDLK_KP_HEXADECIMAL =
        (SDL_SCANCODE_KP_HEXADECIMAL | (1<<30)),

    SDLK_LCTRL = (SDL_SCANCODE_LCTRL | (1<<30)),
    SDLK_LSHIFT = (SDL_SCANCODE_LSHIFT | (1<<30)),
    SDLK_LALT = (SDL_SCANCODE_LALT | (1<<30)),
    SDLK_LGUI = (SDL_SCANCODE_LGUI | (1<<30)),
    SDLK_RCTRL = (SDL_SCANCODE_RCTRL | (1<<30)),
    SDLK_RSHIFT = (SDL_SCANCODE_RSHIFT | (1<<30)),
    SDLK_RALT = (SDL_SCANCODE_RALT | (1<<30)),
    SDLK_RGUI = (SDL_SCANCODE_RGUI | (1<<30)),

    SDLK_MODE = (SDL_SCANCODE_MODE | (1<<30)),

    SDLK_AUDIONEXT = (SDL_SCANCODE_AUDIONEXT | (1<<30)),
    SDLK_AUDIOPREV = (SDL_SCANCODE_AUDIOPREV | (1<<30)),
    SDLK_AUDIOSTOP = (SDL_SCANCODE_AUDIOSTOP | (1<<30)),
    SDLK_AUDIOPLAY = (SDL_SCANCODE_AUDIOPLAY | (1<<30)),
    SDLK_AUDIOMUTE = (SDL_SCANCODE_AUDIOMUTE | (1<<30)),
    SDLK_MEDIASELECT = (SDL_SCANCODE_MEDIASELECT | (1<<30)),
    SDLK_WWW = (SDL_SCANCODE_WWW | (1<<30)),
    SDLK_MAIL = (SDL_SCANCODE_MAIL | (1<<30)),
    SDLK_CALCULATOR = (SDL_SCANCODE_CALCULATOR | (1<<30)),
    SDLK_COMPUTER = (SDL_SCANCODE_COMPUTER | (1<<30)),
    SDLK_AC_SEARCH = (SDL_SCANCODE_AC_SEARCH | (1<<30)),
    SDLK_AC_HOME = (SDL_SCANCODE_AC_HOME | (1<<30)),
    SDLK_AC_BACK = (SDL_SCANCODE_AC_BACK | (1<<30)),
    SDLK_AC_FORWARD = (SDL_SCANCODE_AC_FORWARD | (1<<30)),
    SDLK_AC_STOP = (SDL_SCANCODE_AC_STOP | (1<<30)),
    SDLK_AC_REFRESH = (SDL_SCANCODE_AC_REFRESH | (1<<30)),
    SDLK_AC_BOOKMARKS = (SDL_SCANCODE_AC_BOOKMARKS | (1<<30)),

    SDLK_BRIGHTNESSDOWN =
        (SDL_SCANCODE_BRIGHTNESSDOWN | (1<<30)),
    SDLK_BRIGHTNESSUP = (SDL_SCANCODE_BRIGHTNESSUP | (1<<30)),
    SDLK_DISPLAYSWITCH = (SDL_SCANCODE_DISPLAYSWITCH | (1<<30)),
    SDLK_KBDILLUMTOGGLE =
        (SDL_SCANCODE_KBDILLUMTOGGLE | (1<<30)),
    SDLK_KBDILLUMDOWN = (SDL_SCANCODE_KBDILLUMDOWN | (1<<30)),
    SDLK_KBDILLUMUP = (SDL_SCANCODE_KBDILLUMUP | (1<<30)),
    SDLK_EJECT = (SDL_SCANCODE_EJECT | (1<<30)),
    SDLK_SLEEP = (SDL_SCANCODE_SLEEP | (1<<30)),
    SDLK_APP1 = (SDL_SCANCODE_APP1 | (1<<30)),
    SDLK_APP2 = (SDL_SCANCODE_APP2 | (1<<30)),

    SDLK_AUDIOREWIND = (SDL_SCANCODE_AUDIOREWIND | (1<<30)),
    SDLK_AUDIOFASTFORWARD = (SDL_SCANCODE_AUDIOFASTFORWARD | (1<<30))
} SDL_KeyCode;




typedef enum
{
    KMOD_NONE = 0x0000,
    KMOD_LSHIFT = 0x0001,
    KMOD_RSHIFT = 0x0002,
    KMOD_LCTRL = 0x0040,
    KMOD_RCTRL = 0x0080,
    KMOD_LALT = 0x0100,
    KMOD_RALT = 0x0200,
    KMOD_LGUI = 0x0400,
    KMOD_RGUI = 0x0800,
    KMOD_NUM = 0x1000,
    KMOD_CAPS = 0x2000,
    KMOD_MODE = 0x4000,
    KMOD_RESERVED = 0x8000
} SDL_Keymod;
# 34 "include/SDL_keyboard.h" 2


# 1 "include/begin_code.h" 1
# 37 "include/SDL_keyboard.h" 2
# 47 "include/SDL_keyboard.h"
typedef struct SDL_Keysym
{
    SDL_Scancode scancode;
    SDL_Keycode sym;
    Uint16 mod;
    Uint32 unused;
} SDL_Keysym;






extern __attribute__((dllexport)) SDL_Window * SDL_GetKeyboardFocus(void);
# 77 "include/SDL_keyboard.h"
extern __attribute__((dllexport)) const Uint8 * SDL_GetKeyboardState(int *numkeys);




extern __attribute__((dllexport)) SDL_Keymod SDL_GetModState(void);






extern __attribute__((dllexport)) void SDL_SetModState(SDL_Keymod modstate);
# 99 "include/SDL_keyboard.h"
extern __attribute__((dllexport)) SDL_Keycode SDL_GetKeyFromScancode(SDL_Scancode scancode);
# 109 "include/SDL_keyboard.h"
extern __attribute__((dllexport)) SDL_Scancode SDL_GetScancodeFromKey(SDL_Keycode key);
# 120 "include/SDL_keyboard.h"
extern __attribute__((dllexport)) const char * SDL_GetScancodeName(SDL_Scancode scancode);
# 129 "include/SDL_keyboard.h"
extern __attribute__((dllexport)) SDL_Scancode SDL_GetScancodeFromName(const char *name);
# 141 "include/SDL_keyboard.h"
extern __attribute__((dllexport)) const char * SDL_GetKeyName(SDL_Keycode key);
# 150 "include/SDL_keyboard.h"
extern __attribute__((dllexport)) SDL_Keycode SDL_GetKeyFromName(const char *name);
# 160 "include/SDL_keyboard.h"
extern __attribute__((dllexport)) void SDL_StartTextInput(void);







extern __attribute__((dllexport)) SDL_bool SDL_IsTextInputActive(void);
# 177 "include/SDL_keyboard.h"
extern __attribute__((dllexport)) void SDL_StopTextInput(void);







extern __attribute__((dllexport)) void SDL_SetTextInputRect(SDL_Rect *rect);
# 196 "include/SDL_keyboard.h"
extern __attribute__((dllexport)) SDL_bool SDL_HasScreenKeyboardSupport(void);
# 207 "include/SDL_keyboard.h"
extern __attribute__((dllexport)) SDL_bool SDL_IsScreenKeyboardShown(SDL_Window *window);





# 1 "include/close_code.h" 1
# 214 "include/SDL_keyboard.h" 2
# 35 "include/SDL_events.h" 2
# 1 "include/SDL_mouse.h" 1
# 35 "include/SDL_mouse.h"
# 1 "include/begin_code.h" 1
# 36 "include/SDL_mouse.h" 2





typedef struct SDL_Cursor SDL_Cursor;




typedef enum
{
    SDL_SYSTEM_CURSOR_ARROW,
    SDL_SYSTEM_CURSOR_IBEAM,
    SDL_SYSTEM_CURSOR_WAIT,
    SDL_SYSTEM_CURSOR_CROSSHAIR,
    SDL_SYSTEM_CURSOR_WAITARROW,
    SDL_SYSTEM_CURSOR_SIZENWSE,
    SDL_SYSTEM_CURSOR_SIZENESW,
    SDL_SYSTEM_CURSOR_SIZEWE,
    SDL_SYSTEM_CURSOR_SIZENS,
    SDL_SYSTEM_CURSOR_SIZEALL,
    SDL_SYSTEM_CURSOR_NO,
    SDL_SYSTEM_CURSOR_HAND,
    SDL_NUM_SYSTEM_CURSORS
} SDL_SystemCursor;




typedef enum
{
    SDL_MOUSEWHEEL_NORMAL,
    SDL_MOUSEWHEEL_FLIPPED
} SDL_MouseWheelDirection;






extern __attribute__((dllexport)) SDL_Window * SDL_GetMouseFocus(void);
# 87 "include/SDL_mouse.h"
extern __attribute__((dllexport)) Uint32 SDL_GetMouseState(int *x, int *y);
# 112 "include/SDL_mouse.h"
extern __attribute__((dllexport)) Uint32 SDL_GetGlobalMouseState(int *x, int *y);
# 121 "include/SDL_mouse.h"
extern __attribute__((dllexport)) Uint32 SDL_GetRelativeMouseState(int *x, int *y);
# 132 "include/SDL_mouse.h"
extern __attribute__((dllexport)) void SDL_WarpMouseInWindow(SDL_Window * window,
                                                   int x, int y);
# 144 "include/SDL_mouse.h"
extern __attribute__((dllexport)) int SDL_WarpMouseGlobal(int x, int y);
# 162 "include/SDL_mouse.h"
extern __attribute__((dllexport)) int SDL_SetRelativeMouseMode(SDL_bool enabled);
# 193 "include/SDL_mouse.h"
extern __attribute__((dllexport)) int SDL_CaptureMouse(SDL_bool enabled);






extern __attribute__((dllexport)) SDL_bool SDL_GetRelativeMouseMode(void);
# 220 "include/SDL_mouse.h"
extern __attribute__((dllexport)) SDL_Cursor * SDL_CreateCursor(const Uint8 * data,
                                                     const Uint8 * mask,
                                                     int w, int h, int hot_x,
                                                     int hot_y);






extern __attribute__((dllexport)) SDL_Cursor * SDL_CreateColorCursor(SDL_Surface *surface,
                                                          int hot_x,
                                                          int hot_y);






extern __attribute__((dllexport)) SDL_Cursor * SDL_CreateSystemCursor(SDL_SystemCursor id);




extern __attribute__((dllexport)) void SDL_SetCursor(SDL_Cursor * cursor);




extern __attribute__((dllexport)) SDL_Cursor * SDL_GetCursor(void);




extern __attribute__((dllexport)) SDL_Cursor * SDL_GetDefaultCursor(void);
# 263 "include/SDL_mouse.h"
extern __attribute__((dllexport)) void SDL_FreeCursor(SDL_Cursor * cursor);
# 273 "include/SDL_mouse.h"
extern __attribute__((dllexport)) int SDL_ShowCursor(int toggle);
# 298 "include/SDL_mouse.h"
# 1 "include/close_code.h" 1
# 299 "include/SDL_mouse.h" 2
# 36 "include/SDL_events.h" 2
# 1 "include/SDL_joystick.h" 1
# 45 "include/SDL_joystick.h"
# 1 "include/begin_code.h" 1
# 46 "include/SDL_joystick.h" 2
# 66 "include/SDL_joystick.h"
struct _SDL_Joystick;
typedef struct _SDL_Joystick SDL_Joystick;


typedef struct {
    Uint8 data[16];
} SDL_JoystickGUID;
# 81 "include/SDL_joystick.h"
typedef Sint32 SDL_JoystickID;

typedef enum
{
    SDL_JOYSTICK_TYPE_UNKNOWN,
    SDL_JOYSTICK_TYPE_GAMECONTROLLER,
    SDL_JOYSTICK_TYPE_WHEEL,
    SDL_JOYSTICK_TYPE_ARCADE_STICK,
    SDL_JOYSTICK_TYPE_FLIGHT_STICK,
    SDL_JOYSTICK_TYPE_DANCE_PAD,
    SDL_JOYSTICK_TYPE_GUITAR,
    SDL_JOYSTICK_TYPE_DRUM_KIT,
    SDL_JOYSTICK_TYPE_ARCADE_PAD,
    SDL_JOYSTICK_TYPE_THROTTLE
} SDL_JoystickType;

typedef enum
{
    SDL_JOYSTICK_POWER_UNKNOWN = -1,
    SDL_JOYSTICK_POWER_EMPTY,
    SDL_JOYSTICK_POWER_LOW,
    SDL_JOYSTICK_POWER_MEDIUM,
    SDL_JOYSTICK_POWER_FULL,
    SDL_JOYSTICK_POWER_WIRED,
    SDL_JOYSTICK_POWER_MAX
} SDL_JoystickPowerLevel;
# 120 "include/SDL_joystick.h"
extern __attribute__((dllexport)) void SDL_LockJoysticks(void);
extern __attribute__((dllexport)) void SDL_UnlockJoysticks(void);




extern __attribute__((dllexport)) int SDL_NumJoysticks(void);






extern __attribute__((dllexport)) const char * SDL_JoystickNameForIndex(int device_index);





extern __attribute__((dllexport)) int SDL_JoystickGetDevicePlayerIndex(int device_index);





extern __attribute__((dllexport)) SDL_JoystickGUID SDL_JoystickGetDeviceGUID(int device_index);






extern __attribute__((dllexport)) Uint16 SDL_JoystickGetDeviceVendor(int device_index);






extern __attribute__((dllexport)) Uint16 SDL_JoystickGetDeviceProduct(int device_index);






extern __attribute__((dllexport)) Uint16 SDL_JoystickGetDeviceProductVersion(int device_index);





extern __attribute__((dllexport)) SDL_JoystickType SDL_JoystickGetDeviceType(int device_index);






extern __attribute__((dllexport)) SDL_JoystickID SDL_JoystickGetDeviceInstanceID(int device_index);
# 190 "include/SDL_joystick.h"
extern __attribute__((dllexport)) SDL_Joystick * SDL_JoystickOpen(int device_index);




extern __attribute__((dllexport)) SDL_Joystick * SDL_JoystickFromInstanceID(SDL_JoystickID instance_id);




extern __attribute__((dllexport)) SDL_Joystick * SDL_JoystickFromPlayerIndex(int player_index);





extern __attribute__((dllexport)) const char * SDL_JoystickName(SDL_Joystick * joystick);






extern __attribute__((dllexport)) int SDL_JoystickGetPlayerIndex(SDL_Joystick * joystick);




extern __attribute__((dllexport)) void SDL_JoystickSetPlayerIndex(SDL_Joystick * joystick, int player_index);




extern __attribute__((dllexport)) SDL_JoystickGUID SDL_JoystickGetGUID(SDL_Joystick * joystick);





extern __attribute__((dllexport)) Uint16 SDL_JoystickGetVendor(SDL_Joystick * joystick);





extern __attribute__((dllexport)) Uint16 SDL_JoystickGetProduct(SDL_Joystick * joystick);





extern __attribute__((dllexport)) Uint16 SDL_JoystickGetProductVersion(SDL_Joystick * joystick);




extern __attribute__((dllexport)) SDL_JoystickType SDL_JoystickGetType(SDL_Joystick * joystick);





extern __attribute__((dllexport)) void SDL_JoystickGetGUIDString(SDL_JoystickGUID guid, char *pszGUID, int cbGUID);




extern __attribute__((dllexport)) SDL_JoystickGUID SDL_JoystickGetGUIDFromString(const char *pchGUID);




extern __attribute__((dllexport)) SDL_bool SDL_JoystickGetAttached(SDL_Joystick * joystick);




extern __attribute__((dllexport)) SDL_JoystickID SDL_JoystickInstanceID(SDL_Joystick * joystick);




extern __attribute__((dllexport)) int SDL_JoystickNumAxes(SDL_Joystick * joystick);







extern __attribute__((dllexport)) int SDL_JoystickNumBalls(SDL_Joystick * joystick);




extern __attribute__((dllexport)) int SDL_JoystickNumHats(SDL_Joystick * joystick);




extern __attribute__((dllexport)) int SDL_JoystickNumButtons(SDL_Joystick * joystick);







extern __attribute__((dllexport)) void SDL_JoystickUpdate(void);
# 309 "include/SDL_joystick.h"
extern __attribute__((dllexport)) int SDL_JoystickEventState(int state);
# 320 "include/SDL_joystick.h"
extern __attribute__((dllexport)) Sint16 SDL_JoystickGetAxis(SDL_Joystick * joystick,
                                                   int axis);
# 332 "include/SDL_joystick.h"
extern __attribute__((dllexport)) SDL_bool SDL_JoystickGetAxisInitialState(SDL_Joystick * joystick,
                                                   int axis, Sint16 *state);
# 366 "include/SDL_joystick.h"
extern __attribute__((dllexport)) Uint8 SDL_JoystickGetHat(SDL_Joystick * joystick,
                                                 int hat);
# 376 "include/SDL_joystick.h"
extern __attribute__((dllexport)) int SDL_JoystickGetBall(SDL_Joystick * joystick,
                                                int ball, int *dx, int *dy);






extern __attribute__((dllexport)) Uint8 SDL_JoystickGetButton(SDL_Joystick * joystick,
                                                    int button);
# 398 "include/SDL_joystick.h"
extern __attribute__((dllexport)) int SDL_JoystickRumble(SDL_Joystick * joystick, Uint16 low_frequency_rumble, Uint16 high_frequency_rumble, Uint32 duration_ms);




extern __attribute__((dllexport)) void SDL_JoystickClose(SDL_Joystick * joystick);




extern __attribute__((dllexport)) SDL_JoystickPowerLevel SDL_JoystickCurrentPowerLevel(SDL_Joystick * joystick);





# 1 "include/close_code.h" 1
# 415 "include/SDL_joystick.h" 2
# 37 "include/SDL_events.h" 2
# 1 "include/SDL_gamecontroller.h" 1
# 36 "include/SDL_gamecontroller.h"
# 1 "include/begin_code.h" 1
# 37 "include/SDL_gamecontroller.h" 2
# 57 "include/SDL_gamecontroller.h"
struct _SDL_GameController;
typedef struct _SDL_GameController SDL_GameController;

typedef enum
{
    SDL_CONTROLLER_TYPE_UNKNOWN = 0,
    SDL_CONTROLLER_TYPE_XBOX360,
    SDL_CONTROLLER_TYPE_XBOXONE,
    SDL_CONTROLLER_TYPE_PS3,
    SDL_CONTROLLER_TYPE_PS4,
    SDL_CONTROLLER_TYPE_NINTENDO_SWITCH_PRO
} SDL_GameControllerType;

typedef enum
{
    SDL_CONTROLLER_BINDTYPE_NONE = 0,
    SDL_CONTROLLER_BINDTYPE_BUTTON,
    SDL_CONTROLLER_BINDTYPE_AXIS,
    SDL_CONTROLLER_BINDTYPE_HAT
} SDL_GameControllerBindType;




typedef struct SDL_GameControllerButtonBind
{
    SDL_GameControllerBindType bindType;
    union
    {
        int button;
        int axis;
        struct {
            int hat;
            int hat_mask;
        } hat;
    } value;

} SDL_GameControllerButtonBind;
# 131 "include/SDL_gamecontroller.h"
extern __attribute__((dllexport)) int SDL_GameControllerAddMappingsFromRW(SDL_RWops * rw, int freerw);
# 145 "include/SDL_gamecontroller.h"
extern __attribute__((dllexport)) int SDL_GameControllerAddMapping(const char* mappingString);






extern __attribute__((dllexport)) int SDL_GameControllerNumMappings(void);






extern __attribute__((dllexport)) char * SDL_GameControllerMappingForIndex(int mapping_index);






extern __attribute__((dllexport)) char * SDL_GameControllerMappingForGUID(SDL_JoystickGUID guid);






extern __attribute__((dllexport)) char * SDL_GameControllerMapping(SDL_GameController * gamecontroller);




extern __attribute__((dllexport)) SDL_bool SDL_IsGameController(int joystick_index);






extern __attribute__((dllexport)) const char * SDL_GameControllerNameForIndex(int joystick_index);





extern __attribute__((dllexport)) SDL_GameControllerType SDL_GameControllerTypeForIndex(int joystick_index);







extern __attribute__((dllexport)) char * SDL_GameControllerMappingForDeviceIndex(int joystick_index);
# 210 "include/SDL_gamecontroller.h"
extern __attribute__((dllexport)) SDL_GameController * SDL_GameControllerOpen(int joystick_index);




extern __attribute__((dllexport)) SDL_GameController * SDL_GameControllerFromInstanceID(SDL_JoystickID joyid);




extern __attribute__((dllexport)) SDL_GameController * SDL_GameControllerFromPlayerIndex(int player_index);




extern __attribute__((dllexport)) const char * SDL_GameControllerName(SDL_GameController *gamecontroller);




extern __attribute__((dllexport)) SDL_GameControllerType SDL_GameControllerGetType(SDL_GameController *gamecontroller);






extern __attribute__((dllexport)) int SDL_GameControllerGetPlayerIndex(SDL_GameController *gamecontroller);




extern __attribute__((dllexport)) void SDL_GameControllerSetPlayerIndex(SDL_GameController *gamecontroller, int player_index);





extern __attribute__((dllexport)) Uint16 SDL_GameControllerGetVendor(SDL_GameController * gamecontroller);





extern __attribute__((dllexport)) Uint16 SDL_GameControllerGetProduct(SDL_GameController * gamecontroller);





extern __attribute__((dllexport)) Uint16 SDL_GameControllerGetProductVersion(SDL_GameController * gamecontroller);





extern __attribute__((dllexport)) SDL_bool SDL_GameControllerGetAttached(SDL_GameController *gamecontroller);




extern __attribute__((dllexport)) SDL_Joystick * SDL_GameControllerGetJoystick(SDL_GameController *gamecontroller);
# 282 "include/SDL_gamecontroller.h"
extern __attribute__((dllexport)) int SDL_GameControllerEventState(int state);







extern __attribute__((dllexport)) void SDL_GameControllerUpdate(void);
# 302 "include/SDL_gamecontroller.h"
typedef enum
{
    SDL_CONTROLLER_AXIS_INVALID = -1,
    SDL_CONTROLLER_AXIS_LEFTX,
    SDL_CONTROLLER_AXIS_LEFTY,
    SDL_CONTROLLER_AXIS_RIGHTX,
    SDL_CONTROLLER_AXIS_RIGHTY,
    SDL_CONTROLLER_AXIS_TRIGGERLEFT,
    SDL_CONTROLLER_AXIS_TRIGGERRIGHT,
    SDL_CONTROLLER_AXIS_MAX
} SDL_GameControllerAxis;




extern __attribute__((dllexport)) SDL_GameControllerAxis SDL_GameControllerGetAxisFromString(const char *pchString);




extern __attribute__((dllexport)) const char* SDL_GameControllerGetStringForAxis(SDL_GameControllerAxis axis);




extern __attribute__((dllexport)) SDL_GameControllerButtonBind
SDL_GameControllerGetBindForAxis(SDL_GameController *gamecontroller,
                                 SDL_GameControllerAxis axis);
# 339 "include/SDL_gamecontroller.h"
extern __attribute__((dllexport)) Sint16
SDL_GameControllerGetAxis(SDL_GameController *gamecontroller,
                          SDL_GameControllerAxis axis);




typedef enum
{
    SDL_CONTROLLER_BUTTON_INVALID = -1,
    SDL_CONTROLLER_BUTTON_A,
    SDL_CONTROLLER_BUTTON_B,
    SDL_CONTROLLER_BUTTON_X,
    SDL_CONTROLLER_BUTTON_Y,
    SDL_CONTROLLER_BUTTON_BACK,
    SDL_CONTROLLER_BUTTON_GUIDE,
    SDL_CONTROLLER_BUTTON_START,
    SDL_CONTROLLER_BUTTON_LEFTSTICK,
    SDL_CONTROLLER_BUTTON_RIGHTSTICK,
    SDL_CONTROLLER_BUTTON_LEFTSHOULDER,
    SDL_CONTROLLER_BUTTON_RIGHTSHOULDER,
    SDL_CONTROLLER_BUTTON_DPAD_UP,
    SDL_CONTROLLER_BUTTON_DPAD_DOWN,
    SDL_CONTROLLER_BUTTON_DPAD_LEFT,
    SDL_CONTROLLER_BUTTON_DPAD_RIGHT,
    SDL_CONTROLLER_BUTTON_MAX
} SDL_GameControllerButton;




extern __attribute__((dllexport)) SDL_GameControllerButton SDL_GameControllerGetButtonFromString(const char *pchString);




extern __attribute__((dllexport)) const char* SDL_GameControllerGetStringForButton(SDL_GameControllerButton button);




extern __attribute__((dllexport)) SDL_GameControllerButtonBind
SDL_GameControllerGetBindForButton(SDL_GameController *gamecontroller,
                                   SDL_GameControllerButton button);







extern __attribute__((dllexport)) Uint8 SDL_GameControllerGetButton(SDL_GameController *gamecontroller,
                                                          SDL_GameControllerButton button);
# 404 "include/SDL_gamecontroller.h"
extern __attribute__((dllexport)) int SDL_GameControllerRumble(SDL_GameController *gamecontroller, Uint16 low_frequency_rumble, Uint16 high_frequency_rumble, Uint32 duration_ms);




extern __attribute__((dllexport)) void SDL_GameControllerClose(SDL_GameController *gamecontroller);






# 1 "include/close_code.h" 1
# 417 "include/SDL_gamecontroller.h" 2
# 38 "include/SDL_events.h" 2
# 1 "include/SDL_quit.h" 1
# 39 "include/SDL_events.h" 2
# 1 "include/SDL_gesture.h" 1
# 35 "include/SDL_gesture.h"
# 1 "include/SDL_touch.h" 1
# 35 "include/SDL_touch.h"
# 1 "include/begin_code.h" 1
# 36 "include/SDL_touch.h" 2





typedef Sint64 SDL_TouchID;
typedef Sint64 SDL_FingerID;

typedef enum
{
    SDL_TOUCH_DEVICE_INVALID = -1,
    SDL_TOUCH_DEVICE_DIRECT,
    SDL_TOUCH_DEVICE_INDIRECT_ABSOLUTE,
    SDL_TOUCH_DEVICE_INDIRECT_RELATIVE
} SDL_TouchDeviceType;

typedef struct SDL_Finger
{
    SDL_FingerID id;
    float x;
    float y;
    float pressure;
} SDL_Finger;
# 72 "include/SDL_touch.h"
extern __attribute__((dllexport)) int SDL_GetNumTouchDevices(void);




extern __attribute__((dllexport)) SDL_TouchID SDL_GetTouchDevice(int index);




extern __attribute__((dllexport)) SDL_TouchDeviceType SDL_GetTouchDeviceType(SDL_TouchID touchID);




extern __attribute__((dllexport)) int SDL_GetNumTouchFingers(SDL_TouchID touchID);




extern __attribute__((dllexport)) SDL_Finger * SDL_GetTouchFinger(SDL_TouchID touchID, int index);





# 1 "include/close_code.h" 1
# 99 "include/SDL_touch.h" 2
# 36 "include/SDL_gesture.h" 2


# 1 "include/begin_code.h" 1
# 39 "include/SDL_gesture.h" 2





typedef Sint64 SDL_GestureID;
# 53 "include/SDL_gesture.h"
extern __attribute__((dllexport)) int SDL_RecordGesture(SDL_TouchID touchId);







extern __attribute__((dllexport)) int SDL_SaveAllDollarTemplates(SDL_RWops *dst);






extern __attribute__((dllexport)) int SDL_SaveDollarTemplate(SDL_GestureID gestureId,SDL_RWops *dst);







extern __attribute__((dllexport)) int SDL_LoadDollarTemplates(SDL_TouchID touchId, SDL_RWops *src);






# 1 "include/close_code.h" 1
# 84 "include/SDL_gesture.h" 2
# 40 "include/SDL_events.h" 2


# 1 "include/begin_code.h" 1
# 43 "include/SDL_events.h" 2
# 55 "include/SDL_events.h"
typedef enum
{
    SDL_FIRSTEVENT = 0,


    SDL_QUIT = 0x100,


    SDL_APP_TERMINATING,



    SDL_APP_LOWMEMORY,



    SDL_APP_WILLENTERBACKGROUND,



    SDL_APP_DIDENTERBACKGROUND,



    SDL_APP_WILLENTERFOREGROUND,



    SDL_APP_DIDENTERFOREGROUND,





    SDL_DISPLAYEVENT = 0x150,


    SDL_WINDOWEVENT = 0x200,
    SDL_SYSWMEVENT,


    SDL_KEYDOWN = 0x300,
    SDL_KEYUP,
    SDL_TEXTEDITING,
    SDL_TEXTINPUT,
    SDL_KEYMAPCHANGED,




    SDL_MOUSEMOTION = 0x400,
    SDL_MOUSEBUTTONDOWN,
    SDL_MOUSEBUTTONUP,
    SDL_MOUSEWHEEL,


    SDL_JOYAXISMOTION = 0x600,
    SDL_JOYBALLMOTION,
    SDL_JOYHATMOTION,
    SDL_JOYBUTTONDOWN,
    SDL_JOYBUTTONUP,
    SDL_JOYDEVICEADDED,
    SDL_JOYDEVICEREMOVED,


    SDL_CONTROLLERAXISMOTION = 0x650,
    SDL_CONTROLLERBUTTONDOWN,
    SDL_CONTROLLERBUTTONUP,
    SDL_CONTROLLERDEVICEADDED,
    SDL_CONTROLLERDEVICEREMOVED,
    SDL_CONTROLLERDEVICEREMAPPED,


    SDL_FINGERDOWN = 0x700,
    SDL_FINGERUP,
    SDL_FINGERMOTION,


    SDL_DOLLARGESTURE = 0x800,
    SDL_DOLLARRECORD,
    SDL_MULTIGESTURE,


    SDL_CLIPBOARDUPDATE = 0x900,


    SDL_DROPFILE = 0x1000,
    SDL_DROPTEXT,
    SDL_DROPBEGIN,
    SDL_DROPCOMPLETE,


    SDL_AUDIODEVICEADDED = 0x1100,
    SDL_AUDIODEVICEREMOVED,


    SDL_SENSORUPDATE = 0x1200,


    SDL_RENDER_TARGETS_RESET = 0x2000,
    SDL_RENDER_DEVICE_RESET,




    SDL_USEREVENT = 0x8000,




    SDL_LASTEVENT = 0xFFFF
} SDL_EventType;




typedef struct SDL_CommonEvent
{
    Uint32 type;
    Uint32 timestamp;
} SDL_CommonEvent;




typedef struct SDL_DisplayEvent
{
    Uint32 type;
    Uint32 timestamp;
    Uint32 display;
    Uint8 event;
    Uint8 padding1;
    Uint8 padding2;
    Uint8 padding3;
    Sint32 data1;
} SDL_DisplayEvent;




typedef struct SDL_WindowEvent
{
    Uint32 type;
    Uint32 timestamp;
    Uint32 windowID;
    Uint8 event;
    Uint8 padding1;
    Uint8 padding2;
    Uint8 padding3;
    Sint32 data1;
    Sint32 data2;
} SDL_WindowEvent;




typedef struct SDL_KeyboardEvent
{
    Uint32 type;
    Uint32 timestamp;
    Uint32 windowID;
    Uint8 state;
    Uint8 repeat;
    Uint8 padding2;
    Uint8 padding3;
    SDL_Keysym keysym;
} SDL_KeyboardEvent;





typedef struct SDL_TextEditingEvent
{
    Uint32 type;
    Uint32 timestamp;
    Uint32 windowID;
    char text[(32)];
    Sint32 start;
    Sint32 length;
} SDL_TextEditingEvent;






typedef struct SDL_TextInputEvent
{
    Uint32 type;
    Uint32 timestamp;
    Uint32 windowID;
    char text[(32)];
} SDL_TextInputEvent;




typedef struct SDL_MouseMotionEvent
{
    Uint32 type;
    Uint32 timestamp;
    Uint32 windowID;
    Uint32 which;
    Uint32 state;
    Sint32 x;
    Sint32 y;
    Sint32 xrel;
    Sint32 yrel;
} SDL_MouseMotionEvent;




typedef struct SDL_MouseButtonEvent
{
    Uint32 type;
    Uint32 timestamp;
    Uint32 windowID;
    Uint32 which;
    Uint8 button;
    Uint8 state;
    Uint8 clicks;
    Uint8 padding1;
    Sint32 x;
    Sint32 y;
} SDL_MouseButtonEvent;




typedef struct SDL_MouseWheelEvent
{
    Uint32 type;
    Uint32 timestamp;
    Uint32 windowID;
    Uint32 which;
    Sint32 x;
    Sint32 y;
    Uint32 direction;
} SDL_MouseWheelEvent;




typedef struct SDL_JoyAxisEvent
{
    Uint32 type;
    Uint32 timestamp;
    SDL_JoystickID which;
    Uint8 axis;
    Uint8 padding1;
    Uint8 padding2;
    Uint8 padding3;
    Sint16 value;
    Uint16 padding4;
} SDL_JoyAxisEvent;




typedef struct SDL_JoyBallEvent
{
    Uint32 type;
    Uint32 timestamp;
    SDL_JoystickID which;
    Uint8 ball;
    Uint8 padding1;
    Uint8 padding2;
    Uint8 padding3;
    Sint16 xrel;
    Sint16 yrel;
} SDL_JoyBallEvent;




typedef struct SDL_JoyHatEvent
{
    Uint32 type;
    Uint32 timestamp;
    SDL_JoystickID which;
    Uint8 hat;
    Uint8 value;






    Uint8 padding1;
    Uint8 padding2;
} SDL_JoyHatEvent;




typedef struct SDL_JoyButtonEvent
{
    Uint32 type;
    Uint32 timestamp;
    SDL_JoystickID which;
    Uint8 button;
    Uint8 state;
    Uint8 padding1;
    Uint8 padding2;
} SDL_JoyButtonEvent;




typedef struct SDL_JoyDeviceEvent
{
    Uint32 type;
    Uint32 timestamp;
    Sint32 which;
} SDL_JoyDeviceEvent;





typedef struct SDL_ControllerAxisEvent
{
    Uint32 type;
    Uint32 timestamp;
    SDL_JoystickID which;
    Uint8 axis;
    Uint8 padding1;
    Uint8 padding2;
    Uint8 padding3;
    Sint16 value;
    Uint16 padding4;
} SDL_ControllerAxisEvent;





typedef struct SDL_ControllerButtonEvent
{
    Uint32 type;
    Uint32 timestamp;
    SDL_JoystickID which;
    Uint8 button;
    Uint8 state;
    Uint8 padding1;
    Uint8 padding2;
} SDL_ControllerButtonEvent;





typedef struct SDL_ControllerDeviceEvent
{
    Uint32 type;
    Uint32 timestamp;
    Sint32 which;
} SDL_ControllerDeviceEvent;




typedef struct SDL_AudioDeviceEvent
{
    Uint32 type;
    Uint32 timestamp;
    Uint32 which;
    Uint8 iscapture;
    Uint8 padding1;
    Uint8 padding2;
    Uint8 padding3;
} SDL_AudioDeviceEvent;





typedef struct SDL_TouchFingerEvent
{
    Uint32 type;
    Uint32 timestamp;
    SDL_TouchID touchId;
    SDL_FingerID fingerId;
    float x;
    float y;
    float dx;
    float dy;
    float pressure;
    Uint32 windowID;
} SDL_TouchFingerEvent;





typedef struct SDL_MultiGestureEvent
{
    Uint32 type;
    Uint32 timestamp;
    SDL_TouchID touchId;
    float dTheta;
    float dDist;
    float x;
    float y;
    Uint16 numFingers;
    Uint16 padding;
} SDL_MultiGestureEvent;





typedef struct SDL_DollarGestureEvent
{
    Uint32 type;
    Uint32 timestamp;
    SDL_TouchID touchId;
    SDL_GestureID gestureId;
    Uint32 numFingers;
    float error;
    float x;
    float y;
} SDL_DollarGestureEvent;







typedef struct SDL_DropEvent
{
    Uint32 type;
    Uint32 timestamp;
    char *file;
    Uint32 windowID;
} SDL_DropEvent;





typedef struct SDL_SensorEvent
{
    Uint32 type;
    Uint32 timestamp;
    Sint32 which;
    float data[6];
} SDL_SensorEvent;




typedef struct SDL_QuitEvent
{
    Uint32 type;
    Uint32 timestamp;
} SDL_QuitEvent;




typedef struct SDL_OSEvent
{
    Uint32 type;
    Uint32 timestamp;
} SDL_OSEvent;




typedef struct SDL_UserEvent
{
    Uint32 type;
    Uint32 timestamp;
    Uint32 windowID;
    Sint32 code;
    void *data1;
    void *data2;
} SDL_UserEvent;


struct SDL_SysWMmsg;
typedef struct SDL_SysWMmsg SDL_SysWMmsg;







typedef struct SDL_SysWMEvent
{
    Uint32 type;
    Uint32 timestamp;
    SDL_SysWMmsg *msg;
} SDL_SysWMEvent;




typedef union SDL_Event
{
    Uint32 type;
    SDL_CommonEvent common;
    SDL_DisplayEvent display;
    SDL_WindowEvent window;
    SDL_KeyboardEvent key;
    SDL_TextEditingEvent edit;
    SDL_TextInputEvent text;
    SDL_MouseMotionEvent motion;
    SDL_MouseButtonEvent button;
    SDL_MouseWheelEvent wheel;
    SDL_JoyAxisEvent jaxis;
    SDL_JoyBallEvent jball;
    SDL_JoyHatEvent jhat;
    SDL_JoyButtonEvent jbutton;
    SDL_JoyDeviceEvent jdevice;
    SDL_ControllerAxisEvent caxis;
    SDL_ControllerButtonEvent cbutton;
    SDL_ControllerDeviceEvent cdevice;
    SDL_AudioDeviceEvent adevice;
    SDL_SensorEvent sensor;
    SDL_QuitEvent quit;
    SDL_UserEvent user;
    SDL_SysWMEvent syswm;
    SDL_TouchFingerEvent tfinger;
    SDL_MultiGestureEvent mgesture;
    SDL_DollarGestureEvent dgesture;
    SDL_DropEvent drop;
# 595 "include/SDL_events.h"
    Uint8 padding[56];
} SDL_Event;


typedef int SDL_compile_time_assert_SDL_Event[(sizeof(SDL_Event) == 56) * 2 - 1];
# 611 "include/SDL_events.h"
extern __attribute__((dllexport)) void SDL_PumpEvents(void);


typedef enum
{
    SDL_ADDEVENT,
    SDL_PEEKEVENT,
    SDL_GETEVENT
} SDL_eventaction;
# 639 "include/SDL_events.h"
extern __attribute__((dllexport)) int SDL_PeepEvents(SDL_Event * events, int numevents,
                                           SDL_eventaction action,
                                           Uint32 minType, Uint32 maxType);





extern __attribute__((dllexport)) SDL_bool SDL_HasEvent(Uint32 type);
extern __attribute__((dllexport)) SDL_bool SDL_HasEvents(Uint32 minType, Uint32 maxType);







extern __attribute__((dllexport)) void SDL_FlushEvent(Uint32 type);
extern __attribute__((dllexport)) void SDL_FlushEvents(Uint32 minType, Uint32 maxType);
# 667 "include/SDL_events.h"
extern __attribute__((dllexport)) int SDL_PollEvent(SDL_Event * event);
# 677 "include/SDL_events.h"
extern __attribute__((dllexport)) int SDL_WaitEvent(SDL_Event * event);
# 689 "include/SDL_events.h"
extern __attribute__((dllexport)) int SDL_WaitEventTimeout(SDL_Event * event,
                                                 int timeout);







extern __attribute__((dllexport)) int SDL_PushEvent(SDL_Event * event);

typedef int ( * SDL_EventFilter) (void *userdata, SDL_Event * event);
# 727 "include/SDL_events.h"
extern __attribute__((dllexport)) void SDL_SetEventFilter(SDL_EventFilter filter,
                                                void *userdata);





extern __attribute__((dllexport)) SDL_bool SDL_GetEventFilter(SDL_EventFilter * filter,
                                                    void **userdata);




extern __attribute__((dllexport)) void SDL_AddEventWatch(SDL_EventFilter filter,
                                               void *userdata);




extern __attribute__((dllexport)) void SDL_DelEventWatch(SDL_EventFilter filter,
                                               void *userdata);





extern __attribute__((dllexport)) void SDL_FilterEvents(SDL_EventFilter filter,
                                              void *userdata);
# 771 "include/SDL_events.h"
extern __attribute__((dllexport)) Uint8 SDL_EventState(Uint32 type, int state);
# 782 "include/SDL_events.h"
extern __attribute__((dllexport)) Uint32 SDL_RegisterEvents(int numevents);





# 1 "include/close_code.h" 1
# 789 "include/SDL_events.h" 2
# 42 "include/SDL.h" 2
# 1 "include/SDL_filesystem.h" 1
# 33 "include/SDL_filesystem.h"
# 1 "include/begin_code.h" 1
# 34 "include/SDL_filesystem.h" 2
# 63 "include/SDL_filesystem.h"
extern __attribute__((dllexport)) char * SDL_GetBasePath(void);
# 126 "include/SDL_filesystem.h"
extern __attribute__((dllexport)) char * SDL_GetPrefPath(const char *org, const char *app);





# 1 "include/close_code.h" 1
# 133 "include/SDL_filesystem.h" 2
# 43 "include/SDL.h" 2

# 1 "include/SDL_haptic.h" 1
# 114 "include/SDL_haptic.h"
# 1 "include/begin_code.h" 1
# 115 "include/SDL_haptic.h" 2
# 140 "include/SDL_haptic.h"
struct _SDL_Haptic;
typedef struct _SDL_Haptic SDL_Haptic;
# 450 "include/SDL_haptic.h"
typedef struct SDL_HapticDirection
{
    Uint8 type;
    Sint32 dir[3];
} SDL_HapticDirection;
# 468 "include/SDL_haptic.h"
typedef struct SDL_HapticConstant
{

    Uint16 type;
    SDL_HapticDirection direction;


    Uint32 length;
    Uint16 delay;


    Uint16 button;
    Uint16 interval;


    Sint16 level;


    Uint16 attack_length;
    Uint16 attack_level;
    Uint16 fade_length;
    Uint16 fade_level;
} SDL_HapticConstant;
# 549 "include/SDL_haptic.h"
typedef struct SDL_HapticPeriodic
{

    Uint16 type;


    SDL_HapticDirection direction;


    Uint32 length;
    Uint16 delay;


    Uint16 button;
    Uint16 interval;


    Uint16 period;
    Sint16 magnitude;
    Sint16 offset;
    Uint16 phase;


    Uint16 attack_length;
    Uint16 attack_level;
    Uint16 fade_length;
    Uint16 fade_level;
} SDL_HapticPeriodic;
# 602 "include/SDL_haptic.h"
typedef struct SDL_HapticCondition
{

    Uint16 type;

    SDL_HapticDirection direction;


    Uint32 length;
    Uint16 delay;


    Uint16 button;
    Uint16 interval;


    Uint16 right_sat[3];
    Uint16 left_sat[3];
    Sint16 right_coeff[3];
    Sint16 left_coeff[3];
    Uint16 deadband[3];
    Sint16 center[3];
} SDL_HapticCondition;
# 639 "include/SDL_haptic.h"
typedef struct SDL_HapticRamp
{

    Uint16 type;
    SDL_HapticDirection direction;


    Uint32 length;
    Uint16 delay;


    Uint16 button;
    Uint16 interval;


    Sint16 start;
    Sint16 end;


    Uint16 attack_length;
    Uint16 attack_level;
    Uint16 fade_length;
    Uint16 fade_level;
} SDL_HapticRamp;
# 676 "include/SDL_haptic.h"
typedef struct SDL_HapticLeftRight
{

    Uint16 type;


    Uint32 length;


    Uint16 large_magnitude;
    Uint16 small_magnitude;
} SDL_HapticLeftRight;
# 704 "include/SDL_haptic.h"
typedef struct SDL_HapticCustom
{

    Uint16 type;
    SDL_HapticDirection direction;


    Uint32 length;
    Uint16 delay;


    Uint16 button;
    Uint16 interval;


    Uint8 channels;
    Uint16 period;
    Uint16 samples;
    Uint16 *data;


    Uint16 attack_length;
    Uint16 attack_level;
    Uint16 fade_length;
    Uint16 fade_level;
} SDL_HapticCustom;
# 800 "include/SDL_haptic.h"
typedef union SDL_HapticEffect
{

    Uint16 type;
    SDL_HapticConstant constant;
    SDL_HapticPeriodic periodic;
    SDL_HapticCondition condition;
    SDL_HapticRamp ramp;
    SDL_HapticLeftRight leftright;
    SDL_HapticCustom custom;
} SDL_HapticEffect;
# 819 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_NumHaptics(void);
# 832 "include/SDL_haptic.h"
extern __attribute__((dllexport)) const char * SDL_HapticName(int device_index);
# 856 "include/SDL_haptic.h"
extern __attribute__((dllexport)) SDL_Haptic * SDL_HapticOpen(int device_index);
# 867 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticOpened(int device_index);
# 878 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticIndex(SDL_Haptic * haptic);
# 887 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_MouseIsHaptic(void);
# 897 "include/SDL_haptic.h"
extern __attribute__((dllexport)) SDL_Haptic * SDL_HapticOpenFromMouse(void);
# 908 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_JoystickIsHaptic(SDL_Joystick * joystick);
# 927 "include/SDL_haptic.h"
extern __attribute__((dllexport)) SDL_Haptic * SDL_HapticOpenFromJoystick(SDL_Joystick *
                                                               joystick);






extern __attribute__((dllexport)) void SDL_HapticClose(SDL_Haptic * haptic);
# 951 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticNumEffects(SDL_Haptic * haptic);
# 967 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticNumEffectsPlaying(SDL_Haptic * haptic);
# 985 "include/SDL_haptic.h"
extern __attribute__((dllexport)) unsigned int SDL_HapticQuery(SDL_Haptic * haptic);







extern __attribute__((dllexport)) int SDL_HapticNumAxes(SDL_Haptic * haptic);
# 1005 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticEffectSupported(SDL_Haptic * haptic,
                                                      SDL_HapticEffect *
                                                      effect);
# 1020 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticNewEffect(SDL_Haptic * haptic,
                                                SDL_HapticEffect * effect);
# 1040 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticUpdateEffect(SDL_Haptic * haptic,
                                                   int effect,
                                                   SDL_HapticEffect * data);
# 1062 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticRunEffect(SDL_Haptic * haptic,
                                                int effect,
                                                Uint32 iterations);
# 1076 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticStopEffect(SDL_Haptic * haptic,
                                                 int effect);
# 1090 "include/SDL_haptic.h"
extern __attribute__((dllexport)) void SDL_HapticDestroyEffect(SDL_Haptic * haptic,
                                                     int effect);
# 1105 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticGetEffectStatus(SDL_Haptic * haptic,
                                                      int effect);
# 1124 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticSetGain(SDL_Haptic * haptic, int gain);
# 1140 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticSetAutocenter(SDL_Haptic * haptic,
                                                    int autocenter);
# 1157 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticPause(SDL_Haptic * haptic);
# 1169 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticUnpause(SDL_Haptic * haptic);







extern __attribute__((dllexport)) int SDL_HapticStopAll(SDL_Haptic * haptic);
# 1189 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticRumbleSupported(SDL_Haptic * haptic);
# 1202 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticRumbleInit(SDL_Haptic * haptic);
# 1216 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticRumblePlay(SDL_Haptic * haptic, float strength, Uint32 length );
# 1228 "include/SDL_haptic.h"
extern __attribute__((dllexport)) int SDL_HapticRumbleStop(SDL_Haptic * haptic);





# 1 "include/close_code.h" 1
# 1235 "include/SDL_haptic.h" 2
# 45 "include/SDL.h" 2
# 1 "include/SDL_hints.h" 1
# 44 "include/SDL_hints.h"
# 1 "include/begin_code.h" 1
# 45 "include/SDL_hints.h" 2
# 1278 "include/SDL_hints.h"
typedef enum
{
    SDL_HINT_DEFAULT,
    SDL_HINT_NORMAL,
    SDL_HINT_OVERRIDE
} SDL_HintPriority;
# 1295 "include/SDL_hints.h"
extern __attribute__((dllexport)) SDL_bool SDL_SetHintWithPriority(const char *name,
                                                         const char *value,
                                                         SDL_HintPriority priority);






extern __attribute__((dllexport)) SDL_bool SDL_SetHint(const char *name,
                                             const char *value);






extern __attribute__((dllexport)) const char * SDL_GetHint(const char *name);






extern __attribute__((dllexport)) SDL_bool SDL_GetHintBoolean(const char *name, SDL_bool default_value);




typedef void ( *SDL_HintCallback)(void *userdata, const char *name, const char *oldValue, const char *newValue);
# 1333 "include/SDL_hints.h"
extern __attribute__((dllexport)) void SDL_AddHintCallback(const char *name,
                                                 SDL_HintCallback callback,
                                                 void *userdata);
# 1344 "include/SDL_hints.h"
extern __attribute__((dllexport)) void SDL_DelHintCallback(const char *name,
                                                 SDL_HintCallback callback,
                                                 void *userdata);






extern __attribute__((dllexport)) void SDL_ClearHints(void);






# 1 "include/close_code.h" 1
# 1361 "include/SDL_hints.h" 2
# 46 "include/SDL.h" 2

# 1 "include/SDL_loadso.h" 1
# 47 "include/SDL_loadso.h"
# 1 "include/begin_code.h" 1
# 48 "include/SDL_loadso.h" 2
# 58 "include/SDL_loadso.h"
extern __attribute__((dllexport)) void * SDL_LoadObject(const char *sofile);






extern __attribute__((dllexport)) void * SDL_LoadFunction(void *handle,
                                               const char *name);




extern __attribute__((dllexport)) void SDL_UnloadObject(void *handle);





# 1 "include/close_code.h" 1
# 78 "include/SDL_loadso.h" 2
# 48 "include/SDL.h" 2
# 1 "include/SDL_log.h" 1
# 42 "include/SDL_log.h"
# 1 "include/begin_code.h" 1
# 43 "include/SDL_log.h" 2
# 64 "include/SDL_log.h"
typedef enum
{
    SDL_LOG_CATEGORY_APPLICATION,
    SDL_LOG_CATEGORY_ERROR,
    SDL_LOG_CATEGORY_ASSERT,
    SDL_LOG_CATEGORY_SYSTEM,
    SDL_LOG_CATEGORY_AUDIO,
    SDL_LOG_CATEGORY_VIDEO,
    SDL_LOG_CATEGORY_RENDER,
    SDL_LOG_CATEGORY_INPUT,
    SDL_LOG_CATEGORY_TEST,


    SDL_LOG_CATEGORY_RESERVED1,
    SDL_LOG_CATEGORY_RESERVED2,
    SDL_LOG_CATEGORY_RESERVED3,
    SDL_LOG_CATEGORY_RESERVED4,
    SDL_LOG_CATEGORY_RESERVED5,
    SDL_LOG_CATEGORY_RESERVED6,
    SDL_LOG_CATEGORY_RESERVED7,
    SDL_LOG_CATEGORY_RESERVED8,
    SDL_LOG_CATEGORY_RESERVED9,
    SDL_LOG_CATEGORY_RESERVED10,
# 96 "include/SDL_log.h"
    SDL_LOG_CATEGORY_CUSTOM
} SDL_LogCategory;




typedef enum
{
    SDL_LOG_PRIORITY_VERBOSE = 1,
    SDL_LOG_PRIORITY_DEBUG,
    SDL_LOG_PRIORITY_INFO,
    SDL_LOG_PRIORITY_WARN,
    SDL_LOG_PRIORITY_ERROR,
    SDL_LOG_PRIORITY_CRITICAL,
    SDL_NUM_LOG_PRIORITIES
} SDL_LogPriority;





extern __attribute__((dllexport)) void SDL_LogSetAllPriority(SDL_LogPriority priority);




extern __attribute__((dllexport)) void SDL_LogSetPriority(int category,
                                                SDL_LogPriority priority);




extern __attribute__((dllexport)) SDL_LogPriority SDL_LogGetPriority(int category);






extern __attribute__((dllexport)) void SDL_LogResetPriorities(void);




extern __attribute__((dllexport)) void SDL_Log( const char *fmt, ...) __attribute__ (( format( __printf__, 1, 1 +1 )));




extern __attribute__((dllexport)) void SDL_LogVerbose(int category, const char *fmt, ...) __attribute__ (( format( __printf__, 2, 2 +1 )));




extern __attribute__((dllexport)) void SDL_LogDebug(int category, const char *fmt, ...) __attribute__ (( format( __printf__, 2, 2 +1 )));




extern __attribute__((dllexport)) void SDL_LogInfo(int category, const char *fmt, ...) __attribute__ (( format( __printf__, 2, 2 +1 )));




extern __attribute__((dllexport)) void SDL_LogWarn(int category, const char *fmt, ...) __attribute__ (( format( __printf__, 2, 2 +1 )));




extern __attribute__((dllexport)) void SDL_LogError(int category, const char *fmt, ...) __attribute__ (( format( __printf__, 2, 2 +1 )));




extern __attribute__((dllexport)) void SDL_LogCritical(int category, const char *fmt, ...) __attribute__ (( format( __printf__, 2, 2 +1 )));




extern __attribute__((dllexport)) void SDL_LogMessage(int category,
                                            SDL_LogPriority priority,
                                            const char *fmt, ...) __attribute__ (( format( __printf__, 3, 3 +1 )));




extern __attribute__((dllexport)) void SDL_LogMessageV(int category,
                                             SDL_LogPriority priority,
                                             const char *fmt, va_list ap);




typedef void ( *SDL_LogOutputFunction)(void *userdata, int category, SDL_LogPriority priority, const char *message);




extern __attribute__((dllexport)) void SDL_LogGetOutputFunction(SDL_LogOutputFunction *callback, void **userdata);





extern __attribute__((dllexport)) void SDL_LogSetOutputFunction(SDL_LogOutputFunction callback, void *userdata);






# 1 "include/close_code.h" 1
# 208 "include/SDL_log.h" 2
# 49 "include/SDL.h" 2
# 1 "include/SDL_messagebox.h" 1
# 28 "include/SDL_messagebox.h"
# 1 "include/begin_code.h" 1
# 29 "include/SDL_messagebox.h" 2
# 37 "include/SDL_messagebox.h"
typedef enum
{
    SDL_MESSAGEBOX_ERROR = 0x00000010,
    SDL_MESSAGEBOX_WARNING = 0x00000020,
    SDL_MESSAGEBOX_INFORMATION = 0x00000040,
    SDL_MESSAGEBOX_BUTTONS_LEFT_TO_RIGHT = 0x00000080,
    SDL_MESSAGEBOX_BUTTONS_RIGHT_TO_LEFT = 0x00000100
} SDL_MessageBoxFlags;




typedef enum
{
    SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT = 0x00000001,
    SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT = 0x00000002
} SDL_MessageBoxButtonFlags;




typedef struct
{
    Uint32 flags;
    int buttonid;
    const char * text;
} SDL_MessageBoxButtonData;




typedef struct
{
    Uint8 r, g, b;
} SDL_MessageBoxColor;

typedef enum
{
    SDL_MESSAGEBOX_COLOR_BACKGROUND,
    SDL_MESSAGEBOX_COLOR_TEXT,
    SDL_MESSAGEBOX_COLOR_BUTTON_BORDER,
    SDL_MESSAGEBOX_COLOR_BUTTON_BACKGROUND,
    SDL_MESSAGEBOX_COLOR_BUTTON_SELECTED,
    SDL_MESSAGEBOX_COLOR_MAX
} SDL_MessageBoxColorType;




typedef struct
{
    SDL_MessageBoxColor colors[SDL_MESSAGEBOX_COLOR_MAX];
} SDL_MessageBoxColorScheme;




typedef struct
{
    Uint32 flags;
    SDL_Window *window;
    const char *title;
    const char *message;

    int numbuttons;
    const SDL_MessageBoxButtonData *buttons;

    const SDL_MessageBoxColorScheme *colorScheme;
} SDL_MessageBoxData;
# 121 "include/SDL_messagebox.h"
extern __attribute__((dllexport)) int SDL_ShowMessageBox(const SDL_MessageBoxData *messageboxdata, int *buttonid);
# 135 "include/SDL_messagebox.h"
extern __attribute__((dllexport)) int SDL_ShowSimpleMessageBox(Uint32 flags, const char *title, const char *message, SDL_Window *window);






# 1 "include/close_code.h" 1
# 143 "include/SDL_messagebox.h" 2
# 50 "include/SDL.h" 2
# 1 "include/SDL_metal.h" 1
# 33 "include/SDL_metal.h"
# 1 "include/begin_code.h" 1
# 34 "include/SDL_metal.h" 2
# 44 "include/SDL_metal.h"
typedef void *SDL_MetalView;
# 71 "include/SDL_metal.h"
extern __attribute__((dllexport)) SDL_MetalView SDL_Metal_CreateView(SDL_Window * window);
# 81 "include/SDL_metal.h"
extern __attribute__((dllexport)) void SDL_Metal_DestroyView(SDL_MetalView view);







# 1 "include/close_code.h" 1
# 90 "include/SDL_metal.h" 2
# 51 "include/SDL.h" 2

# 1 "include/SDL_power.h" 1
# 33 "include/SDL_power.h"
# 1 "include/begin_code.h" 1
# 34 "include/SDL_power.h" 2
# 42 "include/SDL_power.h"
typedef enum
{
    SDL_POWERSTATE_UNKNOWN,
    SDL_POWERSTATE_ON_BATTERY,
    SDL_POWERSTATE_NO_BATTERY,
    SDL_POWERSTATE_CHARGING,
    SDL_POWERSTATE_CHARGED
} SDL_PowerState;
# 65 "include/SDL_power.h"
extern __attribute__((dllexport)) SDL_PowerState SDL_GetPowerInfo(int *secs, int *pct);





# 1 "include/close_code.h" 1
# 72 "include/SDL_power.h" 2
# 53 "include/SDL.h" 2
# 1 "include/SDL_render.h" 1
# 55 "include/SDL_render.h"
# 1 "include/begin_code.h" 1
# 56 "include/SDL_render.h" 2
# 64 "include/SDL_render.h"
typedef enum
{
    SDL_RENDERER_SOFTWARE = 0x00000001,
    SDL_RENDERER_ACCELERATED = 0x00000002,

    SDL_RENDERER_PRESENTVSYNC = 0x00000004,

    SDL_RENDERER_TARGETTEXTURE = 0x00000008

} SDL_RendererFlags;




typedef struct SDL_RendererInfo
{
    const char *name;
    Uint32 flags;
    Uint32 num_texture_formats;
    Uint32 texture_formats[16];
    int max_texture_width;
    int max_texture_height;
} SDL_RendererInfo;




typedef enum
{
    SDL_ScaleModeNearest,
    SDL_ScaleModeLinear,
    SDL_ScaleModeBest
} SDL_ScaleMode;




typedef enum
{
    SDL_TEXTUREACCESS_STATIC,
    SDL_TEXTUREACCESS_STREAMING,
    SDL_TEXTUREACCESS_TARGET
} SDL_TextureAccess;




typedef enum
{
    SDL_TEXTUREMODULATE_NONE = 0x00000000,
    SDL_TEXTUREMODULATE_COLOR = 0x00000001,
    SDL_TEXTUREMODULATE_ALPHA = 0x00000002
} SDL_TextureModulate;




typedef enum
{
    SDL_FLIP_NONE = 0x00000000,
    SDL_FLIP_HORIZONTAL = 0x00000001,
    SDL_FLIP_VERTICAL = 0x00000002
} SDL_RendererFlip;




struct SDL_Renderer;
typedef struct SDL_Renderer SDL_Renderer;




struct SDL_Texture;
typedef struct SDL_Texture SDL_Texture;
# 154 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_GetNumRenderDrivers(void);
# 168 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_GetRenderDriverInfo(int index,
                                                    SDL_RendererInfo * info);
# 182 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_CreateWindowAndRenderer(
                                int width, int height, Uint32 window_flags,
                                SDL_Window **window, SDL_Renderer **renderer);
# 201 "include/SDL_render.h"
extern __attribute__((dllexport)) SDL_Renderer * SDL_CreateRenderer(SDL_Window * window,
                                               int index, Uint32 flags);
# 214 "include/SDL_render.h"
extern __attribute__((dllexport)) SDL_Renderer * SDL_CreateSoftwareRenderer(SDL_Surface * surface);




extern __attribute__((dllexport)) SDL_Renderer * SDL_GetRenderer(SDL_Window * window);




extern __attribute__((dllexport)) int SDL_GetRendererInfo(SDL_Renderer * renderer,
                                                SDL_RendererInfo * info);




extern __attribute__((dllexport)) int SDL_GetRendererOutputSize(SDL_Renderer * renderer,
                                                      int *w, int *h);
# 252 "include/SDL_render.h"
extern __attribute__((dllexport)) SDL_Texture * SDL_CreateTexture(SDL_Renderer * renderer,
                                                        Uint32 format,
                                                        int access, int w,
                                                        int h);
# 270 "include/SDL_render.h"
extern __attribute__((dllexport)) SDL_Texture * SDL_CreateTextureFromSurface(SDL_Renderer * renderer, SDL_Surface * surface);
# 285 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_QueryTexture(SDL_Texture * texture,
                                             Uint32 * format, int *access,
                                             int *w, int *h);
# 302 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_SetTextureColorMod(SDL_Texture * texture,
                                                   Uint8 r, Uint8 g, Uint8 b);
# 318 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_GetTextureColorMod(SDL_Texture * texture,
                                                   Uint8 * r, Uint8 * g,
                                                   Uint8 * b);
# 333 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_SetTextureAlphaMod(SDL_Texture * texture,
                                                   Uint8 alpha);
# 346 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_GetTextureAlphaMod(SDL_Texture * texture,
                                                   Uint8 * alpha);
# 363 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_SetTextureBlendMode(SDL_Texture * texture,
                                                    SDL_BlendMode blendMode);
# 376 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_GetTextureBlendMode(SDL_Texture * texture,
                                                    SDL_BlendMode *blendMode);
# 392 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_SetTextureScaleMode(SDL_Texture * texture,
                                                    SDL_ScaleMode scaleMode);
# 405 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_GetTextureScaleMode(SDL_Texture * texture,
                                                    SDL_ScaleMode *scaleMode);
# 424 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_UpdateTexture(SDL_Texture * texture,
                                              const SDL_Rect * rect,
                                              const void *pixels, int pitch);
# 447 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_UpdateYUVTexture(SDL_Texture * texture,
                                                 const SDL_Rect * rect,
                                                 const Uint8 *Yplane, int Ypitch,
                                                 const Uint8 *Uplane, int Upitch,
                                                 const Uint8 *Vplane, int Vpitch);
# 468 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_LockTexture(SDL_Texture * texture,
                                            const SDL_Rect * rect,
                                            void **pixels, int *pitch);
# 487 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_LockTextureToSurface(SDL_Texture *texture,
                                            const SDL_Rect *rect,
                                            SDL_Surface **surface);
# 498 "include/SDL_render.h"
extern __attribute__((dllexport)) void SDL_UnlockTexture(SDL_Texture * texture);
# 507 "include/SDL_render.h"
extern __attribute__((dllexport)) SDL_bool SDL_RenderTargetSupported(SDL_Renderer *renderer);
# 519 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_SetRenderTarget(SDL_Renderer *renderer,
                                                SDL_Texture *texture);
# 529 "include/SDL_render.h"
extern __attribute__((dllexport)) SDL_Texture * SDL_GetRenderTarget(SDL_Renderer *renderer);
# 554 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderSetLogicalSize(SDL_Renderer * renderer, int w, int h);
# 565 "include/SDL_render.h"
extern __attribute__((dllexport)) void SDL_RenderGetLogicalSize(SDL_Renderer * renderer, int *w, int *h);
# 579 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderSetIntegerScale(SDL_Renderer * renderer,
                                                      SDL_bool enable);
# 589 "include/SDL_render.h"
extern __attribute__((dllexport)) SDL_bool SDL_RenderGetIntegerScale(SDL_Renderer * renderer);
# 606 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderSetViewport(SDL_Renderer * renderer,
                                                  const SDL_Rect * rect);






extern __attribute__((dllexport)) void SDL_RenderGetViewport(SDL_Renderer * renderer,
                                                   SDL_Rect * rect);
# 628 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderSetClipRect(SDL_Renderer * renderer,
                                                  const SDL_Rect * rect);
# 640 "include/SDL_render.h"
extern __attribute__((dllexport)) void SDL_RenderGetClipRect(SDL_Renderer * renderer,
                                                   SDL_Rect * rect);
# 650 "include/SDL_render.h"
extern __attribute__((dllexport)) SDL_bool SDL_RenderIsClipEnabled(SDL_Renderer * renderer);
# 671 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderSetScale(SDL_Renderer * renderer,
                                               float scaleX, float scaleY);
# 683 "include/SDL_render.h"
extern __attribute__((dllexport)) void SDL_RenderGetScale(SDL_Renderer * renderer,
                                               float *scaleX, float *scaleY);
# 698 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_SetRenderDrawColor(SDL_Renderer * renderer,
                                           Uint8 r, Uint8 g, Uint8 b,
                                           Uint8 a);
# 714 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_GetRenderDrawColor(SDL_Renderer * renderer,
                                           Uint8 * r, Uint8 * g, Uint8 * b,
                                           Uint8 * a);
# 731 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_SetRenderDrawBlendMode(SDL_Renderer * renderer,
                                                       SDL_BlendMode blendMode);
# 744 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_GetRenderDrawBlendMode(SDL_Renderer * renderer,
                                                       SDL_BlendMode *blendMode);
# 755 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderClear(SDL_Renderer * renderer);
# 766 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderDrawPoint(SDL_Renderer * renderer,
                                                int x, int y);
# 778 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderDrawPoints(SDL_Renderer * renderer,
                                                 const SDL_Point * points,
                                                 int count);
# 793 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderDrawLine(SDL_Renderer * renderer,
                                               int x1, int y1, int x2, int y2);
# 805 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderDrawLines(SDL_Renderer * renderer,
                                                const SDL_Point * points,
                                                int count);
# 817 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderDrawRect(SDL_Renderer * renderer,
                                               const SDL_Rect * rect);
# 829 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderDrawRects(SDL_Renderer * renderer,
                                                const SDL_Rect * rects,
                                                int count);
# 842 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderFillRect(SDL_Renderer * renderer,
                                               const SDL_Rect * rect);
# 854 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderFillRects(SDL_Renderer * renderer,
                                                const SDL_Rect * rects,
                                                int count);
# 870 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderCopy(SDL_Renderer * renderer,
                                           SDL_Texture * texture,
                                           const SDL_Rect * srcrect,
                                           const SDL_Rect * dstrect);
# 890 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderCopyEx(SDL_Renderer * renderer,
                                           SDL_Texture * texture,
                                           const SDL_Rect * srcrect,
                                           const SDL_Rect * dstrect,
                                           const double angle,
                                           const SDL_Point *center,
                                           const SDL_RendererFlip flip);
# 908 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderDrawPointF(SDL_Renderer * renderer,
                                                 float x, float y);
# 920 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderDrawPointsF(SDL_Renderer * renderer,
                                                  const SDL_FPoint * points,
                                                  int count);
# 935 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderDrawLineF(SDL_Renderer * renderer,
                                                float x1, float y1, float x2, float y2);
# 947 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderDrawLinesF(SDL_Renderer * renderer,
                                                const SDL_FPoint * points,
                                                int count);
# 959 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderDrawRectF(SDL_Renderer * renderer,
                                               const SDL_FRect * rect);
# 971 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderDrawRectsF(SDL_Renderer * renderer,
                                                 const SDL_FRect * rects,
                                                 int count);
# 984 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderFillRectF(SDL_Renderer * renderer,
                                                const SDL_FRect * rect);
# 996 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderFillRectsF(SDL_Renderer * renderer,
                                                 const SDL_FRect * rects,
                                                 int count);
# 1012 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderCopyF(SDL_Renderer * renderer,
                                            SDL_Texture * texture,
                                            const SDL_Rect * srcrect,
                                            const SDL_FRect * dstrect);
# 1032 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderCopyExF(SDL_Renderer * renderer,
                                            SDL_Texture * texture,
                                            const SDL_Rect * srcrect,
                                            const SDL_FRect * dstrect,
                                            const double angle,
                                            const SDL_FPoint *center,
                                            const SDL_RendererFlip flip);
# 1055 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderReadPixels(SDL_Renderer * renderer,
                                                 const SDL_Rect * rect,
                                                 Uint32 format,
                                                 void *pixels, int pitch);




extern __attribute__((dllexport)) void SDL_RenderPresent(SDL_Renderer * renderer);







extern __attribute__((dllexport)) void SDL_DestroyTexture(SDL_Texture * texture);







extern __attribute__((dllexport)) void SDL_DestroyRenderer(SDL_Renderer * renderer);
# 1104 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_RenderFlush(SDL_Renderer * renderer);
# 1117 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_GL_BindTexture(SDL_Texture *texture, float *texw, float *texh);
# 1126 "include/SDL_render.h"
extern __attribute__((dllexport)) int SDL_GL_UnbindTexture(SDL_Texture *texture);
# 1137 "include/SDL_render.h"
extern __attribute__((dllexport)) void * SDL_RenderGetMetalLayer(SDL_Renderer * renderer);
# 1148 "include/SDL_render.h"
extern __attribute__((dllexport)) void * SDL_RenderGetMetalCommandEncoder(SDL_Renderer * renderer);





# 1 "include/close_code.h" 1
# 1155 "include/SDL_render.h" 2
# 54 "include/SDL.h" 2

# 1 "include/SDL_sensor.h" 1
# 35 "include/SDL_sensor.h"
# 1 "include/begin_code.h" 1
# 36 "include/SDL_sensor.h" 2
# 51 "include/SDL_sensor.h"
struct _SDL_Sensor;
typedef struct _SDL_Sensor SDL_Sensor;







typedef Sint32 SDL_SensorID;
# 69 "include/SDL_sensor.h"
typedef enum
{
    SDL_SENSOR_INVALID = -1,
    SDL_SENSOR_UNKNOWN,
    SDL_SENSOR_ACCEL,
    SDL_SENSOR_GYRO
} SDL_SensorType;
# 127 "include/SDL_sensor.h"
extern __attribute__((dllexport)) int SDL_NumSensors(void);
# 136 "include/SDL_sensor.h"
extern __attribute__((dllexport)) const char * SDL_SensorGetDeviceName(int device_index);
# 145 "include/SDL_sensor.h"
extern __attribute__((dllexport)) SDL_SensorType SDL_SensorGetDeviceType(int device_index);
# 154 "include/SDL_sensor.h"
extern __attribute__((dllexport)) int SDL_SensorGetDeviceNonPortableType(int device_index);
# 163 "include/SDL_sensor.h"
extern __attribute__((dllexport)) SDL_SensorID SDL_SensorGetDeviceInstanceID(int device_index);
# 172 "include/SDL_sensor.h"
extern __attribute__((dllexport)) SDL_Sensor * SDL_SensorOpen(int device_index);




extern __attribute__((dllexport)) SDL_Sensor * SDL_SensorFromInstanceID(SDL_SensorID instance_id);






extern __attribute__((dllexport)) const char * SDL_SensorGetName(SDL_Sensor *sensor);
# 193 "include/SDL_sensor.h"
extern __attribute__((dllexport)) SDL_SensorType SDL_SensorGetType(SDL_Sensor *sensor);
# 202 "include/SDL_sensor.h"
extern __attribute__((dllexport)) int SDL_SensorGetNonPortableType(SDL_Sensor *sensor);
# 211 "include/SDL_sensor.h"
extern __attribute__((dllexport)) SDL_SensorID SDL_SensorGetInstanceID(SDL_Sensor *sensor);
# 224 "include/SDL_sensor.h"
extern __attribute__((dllexport)) int SDL_SensorGetData(SDL_Sensor * sensor, float *data, int num_values);




extern __attribute__((dllexport)) void SDL_SensorClose(SDL_Sensor * sensor);
# 238 "include/SDL_sensor.h"
extern __attribute__((dllexport)) void SDL_SensorUpdate(void);
# 247 "include/SDL_sensor.h"
# 1 "include/close_code.h" 1
# 248 "include/SDL_sensor.h" 2
# 56 "include/SDL.h" 2
# 1 "include/SDL_shape.h" 1
# 31 "include/SDL_shape.h"
# 1 "include/begin_code.h" 1
# 32 "include/SDL_shape.h" 2
# 66 "include/SDL_shape.h"
extern __attribute__((dllexport)) SDL_Window * SDL_CreateShapedWindow(const char *title,unsigned int x,unsigned int y,unsigned int w,unsigned int h,Uint32 flags);
# 77 "include/SDL_shape.h"
extern __attribute__((dllexport)) SDL_bool SDL_IsShapedWindow(const SDL_Window *window);


typedef enum {

    ShapeModeDefault,

    ShapeModeBinarizeAlpha,

    ShapeModeReverseBinarizeAlpha,

    ShapeModeColorKey
} WindowShapeMode;




typedef union {

    Uint8 binarizationCutoff;
    SDL_Color colorKey;
} SDL_WindowShapeParams;


typedef struct SDL_WindowShapeMode {

    WindowShapeMode mode;

    SDL_WindowShapeParams parameters;
} SDL_WindowShapeMode;
# 121 "include/SDL_shape.h"
extern __attribute__((dllexport)) int SDL_SetWindowShape(SDL_Window *window,SDL_Surface *shape,SDL_WindowShapeMode *shape_mode);
# 136 "include/SDL_shape.h"
extern __attribute__((dllexport)) int SDL_GetShapedWindowMode(SDL_Window *window,SDL_WindowShapeMode *shape_mode);





# 1 "include/close_code.h" 1
# 143 "include/SDL_shape.h" 2
# 57 "include/SDL.h" 2
# 1 "include/SDL_system.h" 1
# 36 "include/SDL_system.h"
# 1 "include/begin_code.h" 1
# 37 "include/SDL_system.h" 2
# 49 "include/SDL_system.h"
typedef void ( * SDL_WindowsMessageHook)(void *userdata, void *hWnd, unsigned int message, Uint64 wParam, Sint64 lParam);
extern __attribute__((dllexport)) void SDL_SetWindowsMessageHook(SDL_WindowsMessageHook callback, void *userdata);







extern __attribute__((dllexport)) int SDL_Direct3D9GetAdapterIndex( int displayIndex );

typedef struct IDirect3DDevice9 IDirect3DDevice9;





extern __attribute__((dllexport)) IDirect3DDevice9* SDL_RenderGetD3D9Device(SDL_Renderer * renderer);







extern __attribute__((dllexport)) SDL_bool SDL_DXGIGetOutputInfo( int displayIndex, int *adapterIndex, int *outputIndex );
# 295 "include/SDL_system.h"
extern __attribute__((dllexport)) SDL_bool SDL_IsTablet(void);


extern __attribute__((dllexport)) void SDL_OnApplicationWillTerminate(void);
extern __attribute__((dllexport)) void SDL_OnApplicationDidReceiveMemoryWarning(void);
extern __attribute__((dllexport)) void SDL_OnApplicationWillResignActive(void);
extern __attribute__((dllexport)) void SDL_OnApplicationDidEnterBackground(void);
extern __attribute__((dllexport)) void SDL_OnApplicationWillEnterForeground(void);
extern __attribute__((dllexport)) void SDL_OnApplicationDidBecomeActive(void);
# 312 "include/SDL_system.h"
# 1 "include/close_code.h" 1
# 313 "include/SDL_system.h" 2
# 58 "include/SDL.h" 2

# 1 "include/SDL_timer.h" 1
# 34 "include/SDL_timer.h"
# 1 "include/begin_code.h" 1
# 35 "include/SDL_timer.h" 2
# 45 "include/SDL_timer.h"
extern __attribute__((dllexport)) Uint32 SDL_GetTicks(void);
# 61 "include/SDL_timer.h"
extern __attribute__((dllexport)) Uint64 SDL_GetPerformanceCounter(void);




extern __attribute__((dllexport)) Uint64 SDL_GetPerformanceFrequency(void);




extern __attribute__((dllexport)) void SDL_Delay(Uint32 ms);
# 81 "include/SDL_timer.h"
typedef Uint32 ( * SDL_TimerCallback) (Uint32 interval, void *param);




typedef int SDL_TimerID;






extern __attribute__((dllexport)) SDL_TimerID SDL_AddTimer(Uint32 interval,
                                                 SDL_TimerCallback callback,
                                                 void *param);
# 104 "include/SDL_timer.h"
extern __attribute__((dllexport)) SDL_bool SDL_RemoveTimer(SDL_TimerID id);






# 1 "include/close_code.h" 1
# 112 "include/SDL_timer.h" 2
# 60 "include/SDL.h" 2
# 1 "include/SDL_version.h" 1
# 33 "include/SDL_version.h"
# 1 "include/begin_code.h" 1
# 34 "include/SDL_version.h" 2
# 51 "include/SDL_version.h"
typedef struct SDL_version
{
    Uint8 major;
    Uint8 minor;
    Uint8 patch;
} SDL_version;
# 133 "include/SDL_version.h"
extern __attribute__((dllexport)) void SDL_GetVersion(SDL_version * ver);
# 142 "include/SDL_version.h"
extern __attribute__((dllexport)) const char * SDL_GetRevision(void);
# 151 "include/SDL_version.h"
extern __attribute__((dllexport)) int SDL_GetRevisionNumber(void);






# 1 "include/close_code.h" 1
# 159 "include/SDL_version.h" 2
# 61 "include/SDL.h" 2


# 1 "include/begin_code.h" 1
# 64 "include/SDL.h" 2
# 96 "include/SDL.h"
extern __attribute__((dllexport)) int SDL_Init(Uint32 flags);
# 107 "include/SDL.h"
extern __attribute__((dllexport)) int SDL_InitSubSystem(Uint32 flags);




extern __attribute__((dllexport)) void SDL_QuitSubSystem(Uint32 flags);







extern __attribute__((dllexport)) Uint32 SDL_WasInit(Uint32 flags);





extern __attribute__((dllexport)) void SDL_Quit(void);





# 1 "include/close_code.h" 1
# 133 "include/SDL.h" 2
# 6 "./src/lib/src/Tools.c" 2

# 6 "./src/lib/src/Tools.c" 3 4
_Bool 
# 6 "./src/lib/src/Tools.c"
    comparePoints(Point p1, Point p2){
    if(p1.x!=p2.x||p1.y!=p2.y) return 
# 7 "./src/lib/src/Tools.c" 3 4
                                     0
# 7 "./src/lib/src/Tools.c"
                                          ;
    return 
# 8 "./src/lib/src/Tools.c" 3 4
          1
# 8 "./src/lib/src/Tools.c"
              ;
}

int positionVoisinage(Point p1, Point p2){
    Point haut_gauche = (Point) {p2.x-1,p2.y+1},
    haut = (Point) {p2.x,p2.y+1},
    haut_droite = (Point) {p2.x+1,p2.y+1},
    droite = (Point) {p2.x+1,p2.y},
    bas_droite = (Point) {p2.x+1,p2.y-1},
    bas = (Point) {p2.x,p2.y-1},
    bas_gauche = (Point) {p2.x-1,p2.y-1},
    gauche = (Point) {p2.x-1,p2.y};

    if(comparePoints(p1,haut_gauche)) return 0;
    if(comparePoints(p1,haut)) return 1;
    if(comparePoints(p1,haut_droite)) return 2;
    if(comparePoints(p1,droite)) return 3;
    if(comparePoints(p1,bas_droite)) return 4;
    if(comparePoints(p1,bas)) return 5;
    if(comparePoints(p1,bas_gauche)) return 6;
    if(comparePoints(p1,gauche)) return 7;
    if(comparePoints(p1,p2)) return 8;

    return -1;
}


# 34 "./src/lib/src/Tools.c" 3 4
_Bool 
# 34 "./src/lib/src/Tools.c"
    comparePointsVariadic (Point p1, Point p2, int nbreArgumentsOptionnels, ...){
    if(p1.x!=p2.x||p1.y!=p2.y) return 
# 35 "./src/lib/src/Tools.c" 3 4
                                     0
# 35 "./src/lib/src/Tools.c"
                                          ;

    va_list ap;
    
# 38 "./src/lib/src/Tools.c" 3 4
   __builtin_va_start(
# 38 "./src/lib/src/Tools.c"
   ap
# 38 "./src/lib/src/Tools.c" 3 4
   ,
# 38 "./src/lib/src/Tools.c"
   nbreArgumentsOptionnels
# 38 "./src/lib/src/Tools.c" 3 4
   )
# 38 "./src/lib/src/Tools.c"
                                         ;
    Point tmp;
    for (int i = 0; i < nbreArgumentsOptionnels; i++){
        tmp = 
# 41 "./src/lib/src/Tools.c" 3 4
             __builtin_va_arg(
# 41 "./src/lib/src/Tools.c"
             ap
# 41 "./src/lib/src/Tools.c" 3 4
             ,
# 41 "./src/lib/src/Tools.c"
             Point
# 41 "./src/lib/src/Tools.c" 3 4
             )
# 41 "./src/lib/src/Tools.c"
                               ;
        if(p1.x!=tmp.x||p1.y!=tmp.y) return 
# 42 "./src/lib/src/Tools.c" 3 4
                                           0
# 42 "./src/lib/src/Tools.c"
                                                ;
    }
    
# 44 "./src/lib/src/Tools.c" 3 4
   __builtin_va_end(
# 44 "./src/lib/src/Tools.c"
   ap
# 44 "./src/lib/src/Tools.c" 3 4
   )
# 44 "./src/lib/src/Tools.c"
              ;
    return 
# 45 "./src/lib/src/Tools.c" 3 4
          1
# 45 "./src/lib/src/Tools.c"
              ;
}

MaillonTraj* creeMaillon(const Point position){
    MaillonTraj *ret = (MaillonTraj*) malloc(sizeof(MaillonTraj));
    ret->position = (Point){position.x,position.y};
    ret->next = 
# 51 "./src/lib/src/Tools.c" 3 4
               ((void *)0)
# 51 "./src/lib/src/Tools.c"
                   ;
    return ret;
}

void insererMaillonAvant(MaillonTraj *ptrTete, const int index, MaillonTraj *maillonAInserer){
    MaillonTraj *maillonCourant = ptrTete;
    int i = 0;
    while(maillonCourant)
    {
        if(i+1 == index)
        {
            MaillonTraj *tmp = maillonCourant->next;
            maillonCourant->next = maillonAInserer;
            maillonAInserer->next = tmp;
            return;
        }

        i++;
        maillonCourant = maillonCourant->next;
    }
}
void ajouterMaillon(MaillonTraj *ptrTete, MaillonTraj *maillonAAjouter){
    MaillonTraj *maillonCourant = ptrTete;
    while(maillonCourant->next)
    {
        maillonCourant = maillonCourant->next;
    }
    maillonCourant->next = maillonAAjouter;
}
void libereMaillon(MaillonTraj **maillon){

    if(*maillon) free(*maillon);
    maillon = 
# 83 "./src/lib/src/Tools.c" 3 4
             ((void *)0)
# 83 "./src/lib/src/Tools.c"
                 ;
}

int trouverIndexParValeur(MaillonTraj *ptrTete, const Point position){
    MaillonTraj *maillonCourant = ptrTete;
    int i = 0;
    while(maillonCourant->next)
    {
        if(maillonCourant->position.x == position.x && maillonCourant->position.y == position.y) return i;

        i++;
        maillonCourant = maillonCourant->next;
    }

    return -1;
}
void supprimerMaillonParValeur(MaillonTraj *ptrTete, const Point position){
    MaillonTraj *maillonCourant = ptrTete;
    while(maillonCourant->next)
    {
        MaillonTraj *next = maillonCourant->next;
        if(next->position.x == position.x && next->position.y == position.y)
        {
            MaillonTraj *target = maillonCourant->next;
            maillonCourant->next = maillonCourant->next->next;
            libereMaillon(&target);
            return;
        }
        maillonCourant = maillonCourant->next;
    }
}

void supprimerMaillon(MaillonTraj *ptrTete, MaillonTraj **maillon){
    MaillonTraj *maillonCourant = ptrTete;

    while(maillonCourant->next != *maillon)
    {
        maillonCourant = maillonCourant->next;
    }
    MaillonTraj *tmp = maillonCourant->next->next;
    libereMaillon(maillon);
    maillonCourant->next = tmp;
}

void dessineCarre(const Image *img, const Couleur couleur, const Emplacement emplacement){
    for (int x = emplacement.origine.x; x < emplacement.origine.x+emplacement.largeurMotif; x++)
    {
        for (int y = emplacement.origine.y; y < emplacement.origine.y+emplacement.largeurMotif; y++)
        {
            img->rouge[x][y] = couleur.rouge;
            img->vert[x][y] = couleur.vert;
            img->bleu[x][y] = couleur.bleu;
            img->gris[x][y] = 0.2125*couleur.rouge +0.7154*couleur.vert + 0.0721*couleur.bleu;
        }
    }
}

Robot* calculeTrajectoire(Image *img, Image *imgSansTrajectoires, Palette *palette, Point depart, Point arrivee){

    if( depart.x+3 > img->largeur-1 || depart.x-3 < 0 ||
        depart.y+3 > img->hauteur-1 || depart.y-3 < 0 ||
        arrivee.x+3 > img->largeur-1 || arrivee.x-3 <0 ||
        arrivee.y+3 > img->hauteur-1 || arrivee.y-3 <0 ||
        !compareCouleur(palette->pal[0], estCouleurUniforme(imgSansTrajectoires,depart.x-3,depart.y-3,7,7)) ||
        !compareCouleur(palette->pal[0], estCouleurUniforme(imgSansTrajectoires,arrivee.x-3,arrivee.y-3,7,7)))
        {
            return 
# 149 "./src/lib/src/Tools.c" 3 4
                  ((void *)0)
# 149 "./src/lib/src/Tools.c"
                      ;
        }


    Robot* robot = (Robot*) malloc(sizeof(Robot));
    robot->depart = (Point){depart.x,depart.y};
    robot->arrivee = (Point){arrivee.x,arrivee.y};
    robot->position = (Point) {depart.x,depart.y};
    robot->ptrTete = creeMaillon(robot->depart);

    if(depart.x == arrivee.x)
    {
        Point positionCourante = depart;
        while (!comparePoints(positionCourante,arrivee))
        {
            if(positionCourante.x < arrivee.x){
                positionCourante = (Point){positionCourante.x+1,positionCourante.y};
                ajouterMaillon(robot->ptrTete,creeMaillon(positionCourante));

            }
            else if( positionCourante.x > arrivee.x){
                positionCourante = (Point){positionCourante.x-1,positionCourante.y};
                ajouterMaillon(robot->ptrTete,creeMaillon(positionCourante));

            }
            else if(positionCourante.y < arrivee.y){
                positionCourante = (Point){positionCourante.x,positionCourante.y+1};
                ajouterMaillon(robot->ptrTete,creeMaillon(positionCourante));

            }
            else if(positionCourante.y > arrivee.y){
                positionCourante = (Point){positionCourante.x,positionCourante.y-1};
                ajouterMaillon(robot->ptrTete,creeMaillon(positionCourante));
            }
            else break;
        }

    }
    else
    {
        float a = ((float) arrivee.y-(float) depart.y)/((float) arrivee.x-(float) depart.x);
        float b = (float) depart.y - (float) a* (float) depart.x;

        MaillonTraj *maillonPrecedent = robot->ptrTete;
        for (int x=depart.x;(depart.x>arrivee.x)?x>=arrivee.x:x<=arrivee.x;(depart.x>arrivee.x)?x--:x++)
        {
            int y = a*x+b;

            MaillonTraj *nouveauMaillon = creeMaillon((Point){x,y});

            Point positionPrecedente = (Point){maillonPrecedent->position.x,maillonPrecedent->position.y};
            Point positionCourante = (Point){x,y};

            if(positionVoisinage(positionPrecedente, positionCourante) != 8){

                while( positionVoisinage(positionPrecedente, positionCourante) != 1 &&
                        positionVoisinage(positionPrecedente, positionCourante) != 3 &&
                        positionVoisinage(positionPrecedente, positionCourante) != 5 &&
                        positionVoisinage(positionPrecedente, positionCourante) != 7){
                    if(positionPrecedente.x < positionCourante.x){
                        positionPrecedente = (Point){positionPrecedente.x+1,positionPrecedente.y};
                        ajouterMaillon(robot->ptrTete,creeMaillon(positionPrecedente));

                    }
                    else if( positionPrecedente.x > positionCourante.x){
                        positionPrecedente = (Point){positionPrecedente.x-1,positionPrecedente.y};
                        ajouterMaillon(robot->ptrTete,creeMaillon(positionPrecedente));

                    }
                    else if(positionPrecedente.y < positionCourante.y){
                        positionPrecedente = (Point){positionPrecedente.x,positionPrecedente.y+1};
                        ajouterMaillon(robot->ptrTete,creeMaillon(positionPrecedente));

                    }
                    else if( positionPrecedente.y > positionCourante.y){
                        positionPrecedente = (Point){positionPrecedente.x,positionPrecedente.y-1};
                        ajouterMaillon(robot->ptrTete,creeMaillon(positionPrecedente));
                    }
                    else break;
                }
            }

            ajouterMaillon(robot->ptrTete,nouveauMaillon);
            maillonPrecedent = nouveauMaillon;

        }
    }

    MaillonTraj *current = robot->ptrTete;

    reloop:
    while(current->next)
    {


        if(!compareCouleur(palette->pal[0], estCouleurUniforme(imgSansTrajectoires,current->next->position.x-4,current->next->position.y-4,9,9))){
            MaillonTraj *firstSeg = current->next;
            MaillonTraj *currentT = firstSeg;
            while (!compareCouleur(palette->pal[0], estCouleurUniforme(imgSansTrajectoires,currentT->next->position.x-4,currentT->next->position.y-4,9,9)))
            {
                currentT=currentT->next;
                if(currentT->next == 
# 250 "./src/lib/src/Tools.c" 3 4
                                    ((void *)0)
# 250 "./src/lib/src/Tools.c"
                                        )
                    return 
# 251 "./src/lib/src/Tools.c" 3 4
                          ((void *)0)
# 251 "./src/lib/src/Tools.c"
                              ;
            }
            MaillonTraj *secondSeg= currentT;

            Point previousPosition1 = (Point){current->next->position.x,current->next->position.y},
            previousPosition2 = (Point){current->next->position.x,current->next->position.y},
            previousPosition3 = (Point){current->next->position.x,current->next->position.y},
            previousPosition4 = (Point){current->next->position.x,current->next->position.y};

            MaillonTraj *ptrTetePath1 = creeMaillon((Point){current->next->position.x,current->next->position.y}),
            *ptrTetePath2 = creeMaillon((Point){current->next->position.x,current->next->position.y}),
            *ptrTetePath3 = creeMaillon((Point){current->next->position.x,current->next->position.y}),
            *ptrTetePath4 = creeMaillon((Point){current->next->position.x,current->next->position.y}),

            *current1 = ptrTetePath1,
            *current2 = ptrTetePath2,
            *current3 = ptrTetePath3,
            *current4 = ptrTetePath4;

            int passage = 0;
            
# 271 "./src/lib/src/Tools.c" 3 4
           _Bool 
# 271 "./src/lib/src/Tools.c"
                pathFind = 
# 271 "./src/lib/src/Tools.c" 3 4
                           0
# 271 "./src/lib/src/Tools.c"
                                ;
            while(pathFind == 
# 272 "./src/lib/src/Tools.c" 3 4
                             0
# 272 "./src/lib/src/Tools.c"
                                  )
            {
                Point up1,right1,down1,left1,up2,right2,down2,left2,up3,right3,down3,left3,up4,right4,down4,left4;
                Couleur cUp1 ,cRight1,cDown1,cLeft1,cUp2 ,cRight2,cDown2,cLeft2,cUp3,cRight3,cDown3,cLeft3,cUp4 ,cRight4,cDown4,cLeft4;

                if(current1)
                {
                    up1=(Point){current1->position.x,current1->position.y+1},
                    right1=(Point){current1->position.x+1,current1->position.y},
                    down1=(Point){current1->position.x,current1->position.y-1},
                    left1=(Point){current1->position.x-1,current1->position.y};

                    cUp1 = (Couleur){img->rouge[up1.x][up1.y],img->vert[up1.x][up1.y],img->bleu[up1.x][up1.y]},
                    cRight1 = (Couleur){img->rouge[right1.x][right1.y],img->vert[right1.x][right1.y],img->bleu[right1.x][right1.y]},
                    cDown1 = (Couleur){img->rouge[down1.x][down1.y],img->vert[down1.x][down1.y],img->bleu[down1.x][down1.y]},
                    cLeft1 = (Couleur){img->rouge[left1.x][left1.y],img->vert[left1.x][left1.y],img->bleu[left1.x][left1.y]};
                }

                if(current2)
                {
                    up2=(Point){current2->position.x,current2->position.y+1},
                    right2=(Point){current2->position.x+1,current2->position.y},
                    down2=(Point){current2->position.x,current2->position.y-1},
                    left2=(Point){current2->position.x-1,current2->position.y};

                    cUp2 = (Couleur){img->rouge[up2.x][up2.y],img->vert[up2.x][up2.y],img->bleu[up2.x][up2.y]},
                    cRight2 = (Couleur){img->rouge[right2.x][right2.y],img->vert[right2.x][right2.y],img->bleu[right2.x][right2.y]},
                    cDown2 = (Couleur){img->rouge[down2.x][down2.y],img->vert[down2.x][down2.y],img->bleu[down2.x][down2.y]},
                    cLeft2 = (Couleur){img->rouge[left2.x][left2.y],img->vert[left2.x][left2.y],img->bleu[left2.x][left2.y]};
                }

                if(current3)
                {
                    up3=(Point){current3->position.x,current3->position.y+1},
                    right3=(Point){current3->position.x+1,current3->position.y},
                    down3=(Point){current3->position.x,current3->position.y-1},
                    left3=(Point){current3->position.x-1,current3->position.y};

                    cUp3 = (Couleur){img->rouge[up3.x][up3.y],img->vert[up3.x][up3.y],img->bleu[up3.x][up3.y]},
                    cRight3 = (Couleur){img->rouge[right3.x][right3.y],img->vert[right3.x][right3.y],img->bleu[right3.x][right3.y]},
                    cDown3 = (Couleur){img->rouge[down3.x][down3.y],img->vert[down3.x][down3.y],img->bleu[down3.x][down3.y]},
                    cLeft3 = (Couleur){img->rouge[left3.x][left3.y],img->vert[left3.x][left3.y],img->bleu[left3.x][left3.y]};
                }

                if(current4)
                {
                    up4=(Point){current4->position.x,current4->position.y+1},
                    right4=(Point){current4->position.x+1,current4->position.y},
                    down4=(Point){current4->position.x,current4->position.y-1},
                    left4=(Point){current4->position.x-1,current4->position.y};

                    cUp4 = (Couleur){img->rouge[up4.x][up4.y],img->vert[up4.x][up4.y],img->bleu[up4.x][up4.y]},
                    cRight4 = (Couleur){img->rouge[right4.x][right4.y],img->vert[right4.x][right4.y],img->bleu[right4.x][right4.y]},
                    cDown4 = (Couleur){img->rouge[down4.x][down4.y],img->vert[down4.x][down4.y],img->bleu[down4.x][down4.y]},
                    cLeft4 = (Couleur){img->rouge[left4.x][left4.y],img->vert[left4.x][left4.y],img->bleu[left4.x][left4.y]};
                }

                if(ptrTetePath1){
                    if(compareCouleur(cUp1,palette->pal[0]) == 
# 330 "./src/lib/src/Tools.c" 3 4
                                                              0 
# 330 "./src/lib/src/Tools.c"
                                                                    && comparePoints(up1,previousPosition1) == 
# 330 "./src/lib/src/Tools.c" 3 4
                                                                                                               0
# 330 "./src/lib/src/Tools.c"
                                                                                                                    ){
                        ajouterMaillon(ptrTetePath1,creeMaillon(up1));
                    }
                    else if(compareCouleur(cRight1,palette->pal[0]) == 
# 333 "./src/lib/src/Tools.c" 3 4
                                                                      0 
# 333 "./src/lib/src/Tools.c"
                                                                            && comparePoints(right1,previousPosition1) == 
# 333 "./src/lib/src/Tools.c" 3 4
                                                                                                                          0
# 333 "./src/lib/src/Tools.c"
                                                                                                                               )
                    {
                        ajouterMaillon(ptrTetePath1,creeMaillon(right1));
                    }
                    else if(compareCouleur(cDown1,palette->pal[0]) == 
# 337 "./src/lib/src/Tools.c" 3 4
                                                                     0 
# 337 "./src/lib/src/Tools.c"
                                                                           && comparePoints(down1,previousPosition1) == 
# 337 "./src/lib/src/Tools.c" 3 4
                                                                                                                        0
# 337 "./src/lib/src/Tools.c"
                                                                                                                             )
                    {
                        ajouterMaillon(ptrTetePath1,creeMaillon(down1));
                    }
                    else if(compareCouleur(cLeft1,palette->pal[0]) == 
# 341 "./src/lib/src/Tools.c" 3 4
                                                                     0 
# 341 "./src/lib/src/Tools.c"
                                                                           && comparePoints(left1,previousPosition1) == 
# 341 "./src/lib/src/Tools.c" 3 4
                                                                                                                        0
# 341 "./src/lib/src/Tools.c"
                                                                                                                             )
                    {
                        ajouterMaillon(ptrTetePath1,creeMaillon(left1));
                    }

                    if(current1->next == 
# 346 "./src/lib/src/Tools.c" 3 4
                                        ((void *)0)
# 346 "./src/lib/src/Tools.c"
                                            )
                    {
                        while(ptrTetePath1){
                            MaillonTraj *tmp = ptrTetePath1->next;
                            libereMaillon(&ptrTetePath1);
                            ptrTetePath1=tmp;
                        }
                        current1 = 
# 353 "./src/lib/src/Tools.c" 3 4
                                  ((void *)0)
# 353 "./src/lib/src/Tools.c"
                                      ;
                        previousPosition1 = (Point){0,0};
                    }
                    else
                    {
                        previousPosition1 = (Point){current1->position.x,current1->position.y};
                        current1 = current1->next;
                    }
                }

                if(ptrTetePath2)
                {
                    if(compareCouleur(cLeft2,palette->pal[0]) == 
# 365 "./src/lib/src/Tools.c" 3 4
                                                                0 
# 365 "./src/lib/src/Tools.c"
                                                                      && comparePoints(left2,previousPosition2) == 
# 365 "./src/lib/src/Tools.c" 3 4
                                                                                                                   0
# 365 "./src/lib/src/Tools.c"
                                                                                                                        )
                    {
                        if (current1 == 
# 367 "./src/lib/src/Tools.c" 3 4
                                       ((void *)0) 
# 367 "./src/lib/src/Tools.c"
                                            || (comparePoints(left2,current1->position) == 
# 367 "./src/lib/src/Tools.c" 3 4
                                                                                           0
# 367 "./src/lib/src/Tools.c"
                                                                                                ))
                        {
                            ajouterMaillon(ptrTetePath2,creeMaillon(left2));
                        }
                    }
                    else if(compareCouleur(cDown2,palette->pal[0]) == 
# 372 "./src/lib/src/Tools.c" 3 4
                                                                     0 
# 372 "./src/lib/src/Tools.c"
                                                                           && comparePoints(down2,previousPosition2) == 
# 372 "./src/lib/src/Tools.c" 3 4
                                                                                                                        0
# 372 "./src/lib/src/Tools.c"
                                                                                                                             )
                    {
                        if (current1 == 
# 374 "./src/lib/src/Tools.c" 3 4
                                       ((void *)0) 
# 374 "./src/lib/src/Tools.c"
                                            || (comparePoints(down2,current1->position) == 
# 374 "./src/lib/src/Tools.c" 3 4
                                                                                           0
# 374 "./src/lib/src/Tools.c"
                                                                                                ))
                        {
                            ajouterMaillon(ptrTetePath2,creeMaillon(down2));
                        }
                    }
                    else if(compareCouleur(cRight2,palette->pal[0]) == 
# 379 "./src/lib/src/Tools.c" 3 4
                                                                      0 
# 379 "./src/lib/src/Tools.c"
                                                                            && comparePoints(right2,previousPosition2) == 
# 379 "./src/lib/src/Tools.c" 3 4
                                                                                                                          0
# 379 "./src/lib/src/Tools.c"
                                                                                                                               )
                    {
                        if (current1 == 
# 381 "./src/lib/src/Tools.c" 3 4
                                       ((void *)0) 
# 381 "./src/lib/src/Tools.c"
                                            || (comparePoints(right2,current1->position) == 
# 381 "./src/lib/src/Tools.c" 3 4
                                                                                            0
# 381 "./src/lib/src/Tools.c"
                                                                                                 ))
                        {
                            ajouterMaillon(ptrTetePath2,creeMaillon(right2));
                        }
                    }
                    else if(compareCouleur(cUp2,palette->pal[0]) == 
# 386 "./src/lib/src/Tools.c" 3 4
                                                                   0 
# 386 "./src/lib/src/Tools.c"
                                                                         && comparePoints(up2,previousPosition2) == 
# 386 "./src/lib/src/Tools.c" 3 4
                                                                                                                    0
# 386 "./src/lib/src/Tools.c"
                                                                                                                         )
                    {
                        if (current1 == 
# 388 "./src/lib/src/Tools.c" 3 4
                                       ((void *)0) 
# 388 "./src/lib/src/Tools.c"
                                            || (comparePoints(up2,current1->position) == 
# 388 "./src/lib/src/Tools.c" 3 4
                                                                                         0
# 388 "./src/lib/src/Tools.c"
                                                                                              ))
                        {
                            ajouterMaillon(ptrTetePath2,creeMaillon(up2));
                        }
                    }

                    if(current2->next == 
# 394 "./src/lib/src/Tools.c" 3 4
                                        ((void *)0)
# 394 "./src/lib/src/Tools.c"
                                            )
                    {
                        while(ptrTetePath2){
                            MaillonTraj *tmp = ptrTetePath2->next;
                            libereMaillon(&ptrTetePath2);
                            ptrTetePath2=tmp;
                        }
                        current2 = 
# 401 "./src/lib/src/Tools.c" 3 4
                                  ((void *)0)
# 401 "./src/lib/src/Tools.c"
                                      ;
                        previousPosition2 = (Point){0,0};
                    }
                    else
                    {
                        previousPosition2 = (Point){current2->position.x,current2->position.y};
                        current2 = current2->next;
                    }
                }

                if(ptrTetePath3)
                {
                    if(compareCouleur(cLeft3,palette->pal[0]) == 
# 413 "./src/lib/src/Tools.c" 3 4
                                                                0 
# 413 "./src/lib/src/Tools.c"
                                                                      && comparePoints(left3,previousPosition3) == 
# 413 "./src/lib/src/Tools.c" 3 4
                                                                                                                   0
# 413 "./src/lib/src/Tools.c"
                                                                                                                        )
                    {
                        if (current1 == 
# 415 "./src/lib/src/Tools.c" 3 4
                                       ((void *)0) 
# 415 "./src/lib/src/Tools.c"
                                            || (comparePoints(left3,current1->position) == 
# 415 "./src/lib/src/Tools.c" 3 4
                                                                                           0
# 415 "./src/lib/src/Tools.c"
                                                                                                ))
                        {
                            if (current2 == 
# 417 "./src/lib/src/Tools.c" 3 4
                                           ((void *)0) 
# 417 "./src/lib/src/Tools.c"
                                                || (comparePoints(left3,current2->position) == 
# 417 "./src/lib/src/Tools.c" 3 4
                                                                                               0
# 417 "./src/lib/src/Tools.c"
                                                                                                    ))
                            {
                                ajouterMaillon(ptrTetePath3,creeMaillon(left3));
                            }
                        }


                    }
                    else if(compareCouleur(cDown3,palette->pal[0]) == 
# 425 "./src/lib/src/Tools.c" 3 4
                                                                     0 
# 425 "./src/lib/src/Tools.c"
                                                                           && comparePoints(down3,previousPosition3) == 
# 425 "./src/lib/src/Tools.c" 3 4
                                                                                                                        0
# 425 "./src/lib/src/Tools.c"
                                                                                                                             )
                    {
                        if (current1 == 
# 427 "./src/lib/src/Tools.c" 3 4
                                       ((void *)0) 
# 427 "./src/lib/src/Tools.c"
                                            || (comparePoints(down3,current1->position) == 
# 427 "./src/lib/src/Tools.c" 3 4
                                                                                           0
# 427 "./src/lib/src/Tools.c"
                                                                                                ))
                        {
                            if (current2 == 
# 429 "./src/lib/src/Tools.c" 3 4
                                           ((void *)0) 
# 429 "./src/lib/src/Tools.c"
                                                || (comparePoints(down3,current2->position) == 
# 429 "./src/lib/src/Tools.c" 3 4
                                                                                               0
# 429 "./src/lib/src/Tools.c"
                                                                                                    ))
                            {
                                ajouterMaillon(ptrTetePath3,creeMaillon(down3));
                            }
                        }
                    }
                    else if(compareCouleur(cRight3,palette->pal[0]) == 
# 435 "./src/lib/src/Tools.c" 3 4
                                                                      0 
# 435 "./src/lib/src/Tools.c"
                                                                            && comparePoints(right3,previousPosition3) == 
# 435 "./src/lib/src/Tools.c" 3 4
                                                                                                                          0
# 435 "./src/lib/src/Tools.c"
                                                                                                                               )
                    {
                        if (current1 == 
# 437 "./src/lib/src/Tools.c" 3 4
                                       ((void *)0) 
# 437 "./src/lib/src/Tools.c"
                                            || (comparePoints(right3,current1->position) == 
# 437 "./src/lib/src/Tools.c" 3 4
                                                                                            0
# 437 "./src/lib/src/Tools.c"
                                                                                                 ))
                        {
                            if (current2 == 
# 439 "./src/lib/src/Tools.c" 3 4
                                           ((void *)0) 
# 439 "./src/lib/src/Tools.c"
                                                || (comparePoints(right3,current2->position) == 
# 439 "./src/lib/src/Tools.c" 3 4
                                                                                                0
# 439 "./src/lib/src/Tools.c"
                                                                                                     ))
                            {
                                ajouterMaillon(ptrTetePath3,creeMaillon(right3));
                            }
                        }
                    }
                    else if(compareCouleur(cUp3,palette->pal[0]) == 
# 445 "./src/lib/src/Tools.c" 3 4
                                                                   0 
# 445 "./src/lib/src/Tools.c"
                                                                         && comparePoints(up3,previousPosition3) == 
# 445 "./src/lib/src/Tools.c" 3 4
                                                                                                                    0
# 445 "./src/lib/src/Tools.c"
                                                                                                                         )
                    {
                        if (current1 == 
# 447 "./src/lib/src/Tools.c" 3 4
                                       ((void *)0) 
# 447 "./src/lib/src/Tools.c"
                                            || (comparePoints(up3,current1->position) == 
# 447 "./src/lib/src/Tools.c" 3 4
                                                                                         0
# 447 "./src/lib/src/Tools.c"
                                                                                              ))
                        {
                            if (current2 == 
# 449 "./src/lib/src/Tools.c" 3 4
                                           ((void *)0) 
# 449 "./src/lib/src/Tools.c"
                                                || (comparePoints(up3,current2->position) == 
# 449 "./src/lib/src/Tools.c" 3 4
                                                                                             0
# 449 "./src/lib/src/Tools.c"
                                                                                                  ))
                            {
                                ajouterMaillon(ptrTetePath3,creeMaillon(up3));
                            }
                        }
                    }
                    if(current3->next == 
# 455 "./src/lib/src/Tools.c" 3 4
                                        ((void *)0)
# 455 "./src/lib/src/Tools.c"
                                            ){
                        while(ptrTetePath3){
                            MaillonTraj *tmp = ptrTetePath3->next;
                            libereMaillon(&ptrTetePath3);
                            ptrTetePath3=tmp;
                        }
                        current3 = 
# 461 "./src/lib/src/Tools.c" 3 4
                                  ((void *)0)
# 461 "./src/lib/src/Tools.c"
                                      ;
                        previousPosition3 = (Point){0,0};
                    }
                    else
                    {
                        previousPosition3 = (Point){current3->position.x,current3->position.y};
                        current3 = current3->next;
                    }

                }

                if(ptrTetePath4)
                {
                   if(compareCouleur(cLeft4,palette->pal[0]) == 
# 474 "./src/lib/src/Tools.c" 3 4
                                                               0 
# 474 "./src/lib/src/Tools.c"
                                                                     && comparePoints(left4,previousPosition4) == 
# 474 "./src/lib/src/Tools.c" 3 4
                                                                                                                  0
# 474 "./src/lib/src/Tools.c"
                                                                                                                       )
                    {
                        if (current1 == 
# 476 "./src/lib/src/Tools.c" 3 4
                                       ((void *)0) 
# 476 "./src/lib/src/Tools.c"
                                            || (comparePoints(left4,current1->position) == 
# 476 "./src/lib/src/Tools.c" 3 4
                                                                                           0
# 476 "./src/lib/src/Tools.c"
                                                                                                ))
                        {
                            if (current2 == 
# 478 "./src/lib/src/Tools.c" 3 4
                                           ((void *)0) 
# 478 "./src/lib/src/Tools.c"
                                                || (comparePoints(left4,current2->position) == 
# 478 "./src/lib/src/Tools.c" 3 4
                                                                                               0
# 478 "./src/lib/src/Tools.c"
                                                                                                    ))
                            {
                                if (current3 == 
# 480 "./src/lib/src/Tools.c" 3 4
                                               ((void *)0) 
# 480 "./src/lib/src/Tools.c"
                                                    || (comparePoints(left4,current3->position) == 
# 480 "./src/lib/src/Tools.c" 3 4
                                                                                                   0
# 480 "./src/lib/src/Tools.c"
                                                                                                        ))
                                {
                                    ajouterMaillon(ptrTetePath4,creeMaillon(left4));
                                }
                            }
                        }
                    }
                    else if(compareCouleur(cDown4,palette->pal[0]) == 
# 487 "./src/lib/src/Tools.c" 3 4
                                                                     0 
# 487 "./src/lib/src/Tools.c"
                                                                           && comparePoints(down4,previousPosition4) == 
# 487 "./src/lib/src/Tools.c" 3 4
                                                                                                                        0
# 487 "./src/lib/src/Tools.c"
                                                                                                                             )
                    {
                        if (current1 == 
# 489 "./src/lib/src/Tools.c" 3 4
                                       ((void *)0) 
# 489 "./src/lib/src/Tools.c"
                                            || (comparePoints(down4,current1->position) == 
# 489 "./src/lib/src/Tools.c" 3 4
                                                                                           0
# 489 "./src/lib/src/Tools.c"
                                                                                                ))
                        {
                            if (current2 == 
# 491 "./src/lib/src/Tools.c" 3 4
                                           ((void *)0) 
# 491 "./src/lib/src/Tools.c"
                                                || (comparePoints(down4,current2->position) == 
# 491 "./src/lib/src/Tools.c" 3 4
                                                                                               0
# 491 "./src/lib/src/Tools.c"
                                                                                                    ))
                            {
                                if (current3 == 
# 493 "./src/lib/src/Tools.c" 3 4
                                               ((void *)0) 
# 493 "./src/lib/src/Tools.c"
                                                    || (comparePoints(down4,current3->position) == 
# 493 "./src/lib/src/Tools.c" 3 4
                                                                                                   0
# 493 "./src/lib/src/Tools.c"
                                                                                                        ))
                                {
                                    ajouterMaillon(ptrTetePath4,creeMaillon(down4));
                                }
                            }
                        }
                    }
                    else if(compareCouleur(cRight4,palette->pal[0]) == 
# 500 "./src/lib/src/Tools.c" 3 4
                                                                      0 
# 500 "./src/lib/src/Tools.c"
                                                                            && comparePoints(right4,previousPosition4) == 
# 500 "./src/lib/src/Tools.c" 3 4
                                                                                                                          0
# 500 "./src/lib/src/Tools.c"
                                                                                                                               )
                    {
                        if (current1 == 
# 502 "./src/lib/src/Tools.c" 3 4
                                       ((void *)0) 
# 502 "./src/lib/src/Tools.c"
                                            || (comparePoints(right4,current1->position) == 
# 502 "./src/lib/src/Tools.c" 3 4
                                                                                            0
# 502 "./src/lib/src/Tools.c"
                                                                                                 ))
                        {
                            if (current2 == 
# 504 "./src/lib/src/Tools.c" 3 4
                                           ((void *)0) 
# 504 "./src/lib/src/Tools.c"
                                                || (comparePoints(right4,current2->position) == 
# 504 "./src/lib/src/Tools.c" 3 4
                                                                                                0
# 504 "./src/lib/src/Tools.c"
                                                                                                     ))
                            {
                                if (current3 == 
# 506 "./src/lib/src/Tools.c" 3 4
                                               ((void *)0) 
# 506 "./src/lib/src/Tools.c"
                                                    || (comparePoints(right4,current3->position) == 
# 506 "./src/lib/src/Tools.c" 3 4
                                                                                                    0
# 506 "./src/lib/src/Tools.c"
                                                                                                         ))
                                {
                                    ajouterMaillon(ptrTetePath4,creeMaillon(right4));
                                }
                            }
                        }
                    }
                    else if(compareCouleur(cUp4,palette->pal[0]) == 
# 513 "./src/lib/src/Tools.c" 3 4
                                                                   0 
# 513 "./src/lib/src/Tools.c"
                                                                         && comparePoints(up4,previousPosition4) == 
# 513 "./src/lib/src/Tools.c" 3 4
                                                                                                                    0
# 513 "./src/lib/src/Tools.c"
                                                                                                                         )
                    {
                        if (current1 == 
# 515 "./src/lib/src/Tools.c" 3 4
                                       ((void *)0) 
# 515 "./src/lib/src/Tools.c"
                                            || (comparePoints(up4,current1->position) == 
# 515 "./src/lib/src/Tools.c" 3 4
                                                                                         0
# 515 "./src/lib/src/Tools.c"
                                                                                              ))
                        {
                            if (current2 == 
# 517 "./src/lib/src/Tools.c" 3 4
                                           ((void *)0) 
# 517 "./src/lib/src/Tools.c"
                                                || (comparePoints(up4,current2->position) == 
# 517 "./src/lib/src/Tools.c" 3 4
                                                                                             0
# 517 "./src/lib/src/Tools.c"
                                                                                                  ))
                            {
                                if (current3 == 
# 519 "./src/lib/src/Tools.c" 3 4
                                               ((void *)0) 
# 519 "./src/lib/src/Tools.c"
                                                    || (comparePoints(up4,current3->position) == 
# 519 "./src/lib/src/Tools.c" 3 4
                                                                                                 0
# 519 "./src/lib/src/Tools.c"
                                                                                                      ))
                                {
                                    ajouterMaillon(ptrTetePath4,creeMaillon(up4));
                                }
                            }
                        }
                    }

                    if(current4->next == 
# 527 "./src/lib/src/Tools.c" 3 4
                                        ((void *)0)
# 527 "./src/lib/src/Tools.c"
                                            )
                    {
                        while(ptrTetePath4){
                            MaillonTraj *tmp = ptrTetePath4->next;
                            libereMaillon(&ptrTetePath4);
                            ptrTetePath4=tmp;
                        }
                        current4 = 
# 534 "./src/lib/src/Tools.c" 3 4
                                  ((void *)0)
# 534 "./src/lib/src/Tools.c"
                                      ;
                        previousPosition4 = (Point){0,0};
                    }
                    else
                    {
                        previousPosition4 = (Point){current4->position.x,current4->position.y};
                        current4 = current4->next;
                    }

                }

                MaillonTraj *parcoursMainSegment = current->next;
                while (parcoursMainSegment)
                {

                    if(current1 && comparePoints(current1->position,secondSeg->position))
                    {

                        current->next = ptrTetePath1;
                        current1->next = secondSeg->next;
                        secondSeg->next =
# 554 "./src/lib/src/Tools.c" 3 4
                                        ((void *)0)
# 554 "./src/lib/src/Tools.c"
                                            ;
                        while(firstSeg){
                            MaillonTraj *tmp = firstSeg->next;
                            libereMaillon(&firstSeg);
                            firstSeg=tmp;
                        }
                        current = current1->next;

                        while(ptrTetePath2){
                            MaillonTraj *tmp = ptrTetePath2->next;
                            libereMaillon(&ptrTetePath2);
                            ptrTetePath2=tmp;
                        }
                        while(ptrTetePath3){
                            MaillonTraj *tmp = ptrTetePath3->next;
                            libereMaillon(&ptrTetePath3);
                            ptrTetePath3=tmp;
                        }
                        while(ptrTetePath4){
                            MaillonTraj *tmp = ptrTetePath4->next;
                            libereMaillon(&ptrTetePath4);
                            ptrTetePath4=tmp;
                        }

                        goto reloop;
                    }
                    else if(current2 && comparePoints(current2->position,secondSeg->position))
                    {

                        current->next = ptrTetePath2;
                        current2->next = secondSeg->next;
                        secondSeg->next =
# 585 "./src/lib/src/Tools.c" 3 4
                                        ((void *)0)
# 585 "./src/lib/src/Tools.c"
                                            ;
                        while(firstSeg){
                            MaillonTraj *tmp = firstSeg->next;
                            libereMaillon(&firstSeg);
                            firstSeg=tmp;
                        }
                        current = current2->next;

                        while(ptrTetePath1){
                            MaillonTraj *tmp = ptrTetePath1->next;
                            libereMaillon(&ptrTetePath1);
                            ptrTetePath1=tmp;
                        }
                        while(ptrTetePath3){
                            MaillonTraj *tmp = ptrTetePath3->next;
                            libereMaillon(&ptrTetePath3);
                            ptrTetePath3=tmp;
                        }
                        while(ptrTetePath4){
                            MaillonTraj *tmp = ptrTetePath4->next;
                            libereMaillon(&ptrTetePath4);
                            ptrTetePath4=tmp;
                        }



                        goto reloop;
                    }
                    else if(current3 && comparePoints(current3->position,secondSeg->position))
                    {

                        current->next = ptrTetePath3;
                        current3->next = secondSeg->next;

                        secondSeg->next =
# 619 "./src/lib/src/Tools.c" 3 4
                                        ((void *)0)
# 619 "./src/lib/src/Tools.c"
                                            ;
                        while(firstSeg){
                            MaillonTraj *tmp = firstSeg->next;
                            libereMaillon(&firstSeg);
                            firstSeg=tmp;
                        }
                        current = current3->next;

                        while(ptrTetePath1){
                            MaillonTraj *tmp = ptrTetePath1->next;
                            libereMaillon(&ptrTetePath1);
                            ptrTetePath1=tmp;
                        }
                        while(ptrTetePath2){
                            MaillonTraj *tmp = ptrTetePath2->next;
                            libereMaillon(&ptrTetePath2);
                            ptrTetePath2=tmp;
                        }
                        while(ptrTetePath4){
                            MaillonTraj *tmp = ptrTetePath4->next;
                            libereMaillon(&ptrTetePath4);
                            ptrTetePath4=tmp;
                        }

                        goto reloop;
                    }
                    else if(current4 && comparePoints(current4->position,secondSeg->position))
                    {

                        current->next = ptrTetePath4;
                        current4->next = secondSeg->next;

                        secondSeg->next =
# 651 "./src/lib/src/Tools.c" 3 4
                                        ((void *)0)
# 651 "./src/lib/src/Tools.c"
                                            ;
                        while(firstSeg){
                            MaillonTraj *tmp = firstSeg->next;
                            libereMaillon(&firstSeg);
                            firstSeg=tmp;
                        }
                        current = current4->next;

                        while(ptrTetePath1){
                            MaillonTraj *tmp = ptrTetePath1->next;
                            libereMaillon(&ptrTetePath1);
                            ptrTetePath1=tmp;
                        }
                        while(ptrTetePath2){
                            MaillonTraj *tmp = ptrTetePath2->next;
                            libereMaillon(&ptrTetePath2);
                            ptrTetePath2=tmp;
                        }
                        while(ptrTetePath3){
                            MaillonTraj *tmp = ptrTetePath3->next;
                            libereMaillon(&ptrTetePath3);
                            ptrTetePath3=tmp;
                        }

                        goto reloop;
                    }
                    parcoursMainSegment=parcoursMainSegment->next;
                }
            }
        }
        current=current->next;
    }

    MaillonTraj *optiTraj = creeMaillon(robot->ptrTete->position);
    MaillonTraj *maillonPrecedent = optiTraj;
    current = robot->ptrTete;
    while (current)
    {

        MaillonTraj *currentBis = current->next;
        if(currentBis && currentBis->next)
        {
            MaillonTraj *lastAvailable = currentBis->next;
            while(currentBis->next)
            {
                if(compareCouleur(palette->pal[0],estCouleurUniformeDansCarre(imgSansTrajectoires,current->position.x,current->position.y,currentBis->next->position.x,currentBis->next->position.y)))
                {
                    lastAvailable = currentBis;
                }
                currentBis=currentBis->next;
            }
            if(lastAvailable || lastAvailable && comparePoints(arrivee,lastAvailable->position))
            {
                depart = current->position;
                arrivee = lastAvailable->position;

                if(depart.x == arrivee.x)
                {
                    Point positionCourante = depart;
                    while (!comparePoints(positionCourante,arrivee))
                    {
                        if(positionCourante.x < arrivee.x){
                            positionCourante = (Point){positionCourante.x+1,positionCourante.y};
                            ajouterMaillon(optiTraj,creeMaillon(positionCourante));

                        }
                        else if( positionCourante.x > arrivee.x){
                            positionCourante = (Point){positionCourante.x-1,positionCourante.y};
                            ajouterMaillon(optiTraj,creeMaillon(positionCourante));

                        }
                        else if(positionCourante.y < arrivee.y){
                            positionCourante = (Point){positionCourante.x,positionCourante.y+1};
                            ajouterMaillon(optiTraj,creeMaillon(positionCourante));

                        }
                        else if(positionCourante.y > arrivee.y){
                            positionCourante = (Point){positionCourante.x,positionCourante.y-1};
                            ajouterMaillon(optiTraj,creeMaillon(positionCourante));
                        }
                        else break;
                    }

                }
                else
                {
                    float a = ((float) arrivee.y-(float) depart.y)/((float) arrivee.x-(float) depart.x);
                    float b = (float) depart.y - (float) a* (float) depart.x;
                    for (int x=depart.x;(depart.x>arrivee.x)?x>=arrivee.x:x<=arrivee.x;(depart.x>arrivee.x)?x--:x++)
                    {
                        int y = a*x+b;
                        MaillonTraj *nouveauMaillon = creeMaillon((Point){x,y});

                        Point positionPrecedente = (Point){maillonPrecedent->position.x,maillonPrecedent->position.y};
                        Point positionCourante = (Point){x,y};


                        if(positionVoisinage(positionPrecedente, positionCourante) != 8){

                            while( positionVoisinage(positionPrecedente, positionCourante) != 1 &&
                                    positionVoisinage(positionPrecedente, positionCourante) != 3 &&
                                    positionVoisinage(positionPrecedente, positionCourante) != 5 &&
                                    positionVoisinage(positionPrecedente, positionCourante) != 7){

                                if(positionPrecedente.x < positionCourante.x){
                                    positionPrecedente = (Point){positionPrecedente.x+1,positionPrecedente.y};
                                    ajouterMaillon(optiTraj,creeMaillon(positionPrecedente));

                                }
                                else if( positionPrecedente.x > positionCourante.x){
                                    positionPrecedente = (Point){positionPrecedente.x-1,positionPrecedente.y};
                                    ajouterMaillon(optiTraj,creeMaillon(positionPrecedente));

                                }
                                else if(positionPrecedente.y < positionCourante.y){
                                    positionPrecedente = (Point){positionPrecedente.x,positionPrecedente.y+1};
                                    ajouterMaillon(optiTraj,creeMaillon(positionPrecedente));

                                }
                                else if( positionPrecedente.y > positionCourante.y){
                                    positionPrecedente = (Point){positionPrecedente.x,positionPrecedente.y-1};
                                    ajouterMaillon(optiTraj,creeMaillon(positionPrecedente));
                                }
                                else break;
                            }
                        }

                        ajouterMaillon(optiTraj,nouveauMaillon);
                        maillonPrecedent = nouveauMaillon;

                    }
                }
                for(;maillonPrecedent->next;maillonPrecedent=maillonPrecedent->next);
                current = lastAvailable;
            }
        }
        current=current->next;
    }
    while (robot->ptrTete)
    {
        MaillonTraj *tmp = robot->ptrTete->next;
        libereMaillon(&robot->ptrTete);
        robot->ptrTete = tmp;
    }

    robot->ptrTete = optiTraj;
    return robot;
}

void dessineMotif0(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 800 "./src/lib/src/Tools.c" 3 4
                                                                                                                    _Bool 
# 800 "./src/lib/src/Tools.c"
                                                                                                                         trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        if(trajectoire)
        {

        }
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);

    }
    else
    {
        if(trajectoire)
        {

        }
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);

    }
}

void dessineMotif1(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 832 "./src/lib/src/Tools.c" 3 4
                                                                                                                    _Bool 
# 832 "./src/lib/src/Tools.c"
                                                                                                                         trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y+carreSO.largeurMotif+3;
            for (int x = carreSO.origine.x; x < carreSO.origine.x +2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y+carreSO.largeurMotif-4;
            for (int x = carreSO.origine.x; x < carreSO.origine.x+ 2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }


    }
}

void dessineMotif2(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 878 "./src/lib/src/Tools.c" 3 4
                                                                                                                    _Bool 
# 878 "./src/lib/src/Tools.c"
                                                                                                                         trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif-4;
            for (int y = carreSO.origine.y; y < carreSO.origine.y+ 2*carreSO.largeurMotif; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif+3;
            for (int y = carreSO.origine.y; y < carreSO.origine.y+ 2*carreSO.largeurMotif; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
}

void dessineMotif3(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 922 "./src/lib/src/Tools.c" 3 4
                                                                                                                    _Bool 
# 922 "./src/lib/src/Tools.c"
                                                                                                                         trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif-4;
            for (int y = carreSO.origine.y; y < carreSO.origine.y+ carreSO.largeurMotif+3; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int y = carreSO.origine.y+carreSO.largeurMotif+3;
            for (x = carreSO.origine.x+carreSO.largeurMotif-4; x < carreSO.origine.x+ 2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif+3;
            for (int y = carreSO.origine.y; y < carreSO.origine.y+ carreSO.largeurMotif-4; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int y = carreSO.origine.y+carreSO.largeurMotif-4;
            for (x = carreSO.origine.x+carreSO.largeurMotif+3; x < carreSO.origine.x+ 2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
}

void dessineMotif4(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 984 "./src/lib/src/Tools.c" 3 4
                                                                                                                    _Bool 
# 984 "./src/lib/src/Tools.c"
                                                                                                                         trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif-4;
            for (int y = carreSO.origine.y+2*carreSO.largeurMotif; y > carreSO.origine.y + carreSO.largeurMotif-4; y--)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int y = carreSO.origine.y + carreSO.largeurMotif-4;
            for (x = carreSO.origine.x+carreSO.largeurMotif-4; x < carreSO.origine.x+ 2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif+3;
            for (int y = carreSO.origine.y+2*carreSO.largeurMotif; y > carreSO.origine.y+carreSO.largeurMotif+3; y--)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int y = carreSO.origine.y+carreSO.largeurMotif+3;
            for (x = carreSO.origine.x+carreSO.largeurMotif+3; x < carreSO.origine.x+ 2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
}

void dessineMotif5(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 1046 "./src/lib/src/Tools.c" 3 4
                                                                                                                    _Bool 
# 1046 "./src/lib/src/Tools.c"
                                                                                                                         trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y + carreSO.largeurMotif-4;
            for (int x = carreSO.origine.x; x < carreSO.origine.x+carreSO.largeurMotif+3; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int x = carreSO.origine.x+carreSO.largeurMotif+3;
            for (y = carreSO.origine.y + carreSO.largeurMotif-4; y < carreSO.origine.y + 2*carreSO.largeurMotif; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

        }
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y+carreSO.largeurMotif+3;
            for (int x = carreSO.origine.x; x < carreSO.origine.x+carreSO.largeurMotif-4; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int x = carreSO.origine.x+carreSO.largeurMotif-4;
            for (int y = carreSO.origine.y+carreSO.largeurMotif+3; y < carreSO.origine.y+ 2*carreSO.largeurMotif; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
}

void dessineMotif6(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 1109 "./src/lib/src/Tools.c" 3 4
                                                                                                                    _Bool 
# 1109 "./src/lib/src/Tools.c"
                                                                                                                         trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y+carreSO.largeurMotif+3;
            for (int x = carreSO.origine.x; x < carreSO.origine.x+carreSO.largeurMotif+3; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int x = carreSO.origine.x+carreSO.largeurMotif+3;
            for (int y = carreSO.origine.y+carreSO.largeurMotif+3; y >= carreSO.origine.y; y--)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

        }

    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y+carreSO.largeurMotif-4;
            for (int x = carreSO.origine.x; x < carreSO.origine.x+carreSO.largeurMotif-4; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int x = carreSO.origine.x+carreSO.largeurMotif-4;
            for (int y = carreSO.origine.y+carreSO.largeurMotif-4; y >= carreSO.origine.y; y--)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
}

void dessineMotifNoeud(const Image *img,const Palette palette, const noeudMotif *noeud, const int formeId, const 
# 1173 "./src/lib/src/Tools.c" 3 4
                                                                                                                _Bool 
# 1173 "./src/lib/src/Tools.c"
                                                                                                                     trajectoire){
    Emplacement emplacement = (Emplacement) {(Point){noeud->x,noeud->y},32};
    switch (formeId)
    {
    case 0:
        dessineMotif0(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 1:
        dessineMotif1(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 2:
        dessineMotif2(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 3:
        dessineMotif3(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 4:
        dessineMotif4(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 5:
        dessineMotif5(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 6:
        dessineMotif6(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    default:
        break;
    }
}

void dessineCarte(const Image *img, const Palette palette, const noeudMotif *racine, const 
# 1203 "./src/lib/src/Tools.c" 3 4
                                                                                          _Bool 
# 1203 "./src/lib/src/Tools.c"
                                                                                               trajectoire){
    if(racine == 
# 1204 "./src/lib/src/Tools.c" 3 4
                ((void *)0)
# 1204 "./src/lib/src/Tools.c"
                    )
        return;
    dessineMotifNoeud(img,palette,racine,racine->motif.forme,trajectoire);
    dessineCarte(img,palette,racine->NO,trajectoire);
    dessineCarte(img,palette,racine->NE,trajectoire);
    dessineCarte(img,palette,racine->SO,trajectoire);
    dessineCarte(img,palette,racine->SE,trajectoire);
}

void sauveDescriptionChemin(Robot *robot){

    FILE * fp;


    fp = fopen ("./resources/file/description_robopath.txt","w");
    int i = 0;
    MaillonTraj *current = robot->ptrTete;
    while (current)
    {
        i++;
        current=current->next;
    }


    fprintf (fp, "%d mouvements\n",i);
    fprintf (fp, "Depart: x=%d y=%d\n",robot->depart.x,robot->depart.y);
    fprintf (fp, "Arrivee: x=%d y=%d",robot->arrivee.x,robot->arrivee.y);


    current = robot->ptrTete;
    char mouvementCode;
    while (current->next)
    {
        switch (positionVoisinage(current->next->position,current->position))
        {
        case 1:
            mouvementCode = '1';
            break;

        case 3:
            mouvementCode = '3';
            break;
        case 5:
            mouvementCode = '2';
            break;
        case 7:
            mouvementCode = '4';
            break;
        case 8:
            current=current->next;
            continue;
        default:
            mouvementCode = '?';
            break;
        }
        fprintf (fp, "\n%c",mouvementCode);
        current=current->next;
    }


    fclose (fp);
}
