# 1 "./src/lib/src/Partie5.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "./src/lib/src/Partie5.c"
# 1 "./src/lib/src/../include/Partie5.h" 1



# 1 "./src/lib/src/../include/Image.h" 1




    typedef struct Image {
        int largeur, hauteur;

        short int **rouge, **vert, **bleu, **gris;
    } Image;
# 18 "./src/lib/src/../include/Image.h"
    Image *alloueImage(int largeur, int hauteur);






    void libereImage(Image **ptrImage);







    Image *chargeImage(char *nom);






    void sauveImage(Image *monImage, char *nom);






    void sauveImageNG(Image *monImage, char *nom);






    Image *dupliqueImage(Image *monImage);







    Image *differenceImage(Image *image1, Image *image2);
# 5 "./src/lib/src/../include/Partie5.h" 2
# 1 "./src/lib/src/../include/Partie4.h" 1
# 9 "./src/lib/src/../include/Partie4.h"
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdbool.h" 1 3 4
# 10 "./src/lib/src/../include/Partie4.h" 2
# 1 "c:\\mingw\\include\\stdio.h" 1 3
# 38 "c:\\mingw\\include\\stdio.h" 3
       
# 39 "c:\\mingw\\include\\stdio.h" 3
# 55 "c:\\mingw\\include\\stdio.h" 3
# 1 "c:\\mingw\\include\\_mingw.h" 1 3
# 55 "c:\\mingw\\include\\_mingw.h" 3
       
# 56 "c:\\mingw\\include\\_mingw.h" 3
# 66 "c:\\mingw\\include\\_mingw.h" 3
# 1 "c:\\mingw\\include\\msvcrtver.h" 1 3
# 35 "c:\\mingw\\include\\msvcrtver.h" 3
       
# 36 "c:\\mingw\\include\\msvcrtver.h" 3
# 67 "c:\\mingw\\include\\_mingw.h" 2 3






# 1 "c:\\mingw\\include\\w32api.h" 1 3
# 35 "c:\\mingw\\include\\w32api.h" 3
       
# 36 "c:\\mingw\\include\\w32api.h" 3
# 59 "c:\\mingw\\include\\w32api.h" 3
# 1 "c:\\mingw\\include\\sdkddkver.h" 1 3
# 35 "c:\\mingw\\include\\sdkddkver.h" 3
       
# 36 "c:\\mingw\\include\\sdkddkver.h" 3
# 60 "c:\\mingw\\include\\w32api.h" 2 3
# 74 "c:\\mingw\\include\\_mingw.h" 2 3
# 174 "c:\\mingw\\include\\_mingw.h" 3
# 1 "c:\\mingw\\include\\features.h" 1 3
# 39 "c:\\mingw\\include\\features.h" 3
       
# 40 "c:\\mingw\\include\\features.h" 3
# 175 "c:\\mingw\\include\\_mingw.h" 2 3
# 56 "c:\\mingw\\include\\stdio.h" 2 3
# 68 "c:\\mingw\\include\\stdio.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 216 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4

# 216 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4
typedef unsigned int size_t;
# 328 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4
typedef short unsigned int wchar_t;
# 357 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4
typedef short unsigned int wint_t;
# 69 "c:\\mingw\\include\\stdio.h" 2 3
# 95 "c:\\mingw\\include\\stdio.h" 3
# 1 "c:\\mingw\\include\\sys/types.h" 1 3
# 34 "c:\\mingw\\include\\sys/types.h" 3
       
# 35 "c:\\mingw\\include\\sys/types.h" 3
# 62 "c:\\mingw\\include\\sys/types.h" 3
  typedef long __off32_t;




  typedef __off32_t _off_t;







  typedef _off_t off_t;
# 91 "c:\\mingw\\include\\sys/types.h" 3
  typedef long long __off64_t;






  typedef __off64_t off64_t;
# 115 "c:\\mingw\\include\\sys/types.h" 3
  typedef int _ssize_t;







  typedef _ssize_t ssize_t;
# 96 "c:\\mingw\\include\\stdio.h" 2 3






# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 1 3 4
# 40 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 3 4
typedef __builtin_va_list __gnuc_va_list;
# 103 "c:\\mingw\\include\\stdio.h" 2 3
# 210 "c:\\mingw\\include\\stdio.h" 3
typedef struct _iobuf
{
  char *_ptr;
  int _cnt;
  char *_base;
  int _flag;
  int _file;
  int _charbuf;
  int _bufsiz;
  char *_tmpfname;
} FILE;
# 239 "c:\\mingw\\include\\stdio.h" 3
extern __attribute__((__dllimport__)) FILE _iob[];
# 252 "c:\\mingw\\include\\stdio.h" 3








 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * fopen (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * freopen (const char *, const char *, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fflush (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fclose (FILE *);






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int remove (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int rename (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * tmpfile (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char * tmpnam (char *);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_tempnam (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _rmtmp (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _unlink (const char *);
# 289 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char * tempnam (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int rmtmp (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int unlink (const char *);



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int setvbuf (FILE *, char *, int, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void setbuf (FILE *, char *);
# 342 "c:\\mingw\\include\\stdio.h" 3
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,2,3))) __mingw_fprintf(FILE*, const char*, ...);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,1,2))) __mingw_printf(const char*, ...);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,2,3))) __mingw_sprintf(char*, const char*, ...);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,3,4))) __mingw_snprintf(char*, size_t, const char*, ...);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,2,0))) __mingw_vfprintf(FILE*, const char*, __builtin_va_list);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,1,0))) __mingw_vprintf(const char*, __builtin_va_list);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,2,0))) __mingw_vsprintf(char*, const char*, __builtin_va_list);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,3,0))) __mingw_vsnprintf(char*, size_t, const char*, __builtin_va_list);
# 376 "c:\\mingw\\include\\stdio.h" 3
extern unsigned int _mingw_output_format_control( unsigned int, unsigned int );
# 461 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fprintf (FILE *, const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int printf (const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int sprintf (char *, const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vfprintf (FILE *, const char *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vprintf (const char *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vsprintf (char *, const char *, __builtin_va_list);
# 479 "c:\\mingw\\include\\stdio.h" 3
static __inline__ __attribute__((__cdecl__)) __attribute__((__nothrow__))
int snprintf (char *__buf, size_t __len, const char *__format, ...)
{
  register int __retval;
  __builtin_va_list __local_argv; __builtin_va_start( __local_argv, __format );
  __retval = __mingw_vsnprintf( __buf, __len, __format, __local_argv );
  __builtin_va_end( __local_argv );
  return __retval;
}

static __inline__ __attribute__((__cdecl__)) __attribute__((__nothrow__))
int vsnprintf (char *__buf, size_t __len, const char *__format, __builtin_va_list __local_argv)
{
  return __mingw_vsnprintf( __buf, __len, __format, __local_argv );
}
# 513 "c:\\mingw\\include\\stdio.h" 3
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,2,3))) __msvcrt_fprintf(FILE *, const char *, ...);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,1,2))) __msvcrt_printf(const char *, ...);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,2,3))) __msvcrt_sprintf(char *, const char *, ...);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,2,0))) __msvcrt_vfprintf(FILE *, const char *, __builtin_va_list);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,1,0))) __msvcrt_vprintf(const char *, __builtin_va_list);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,2,0))) __msvcrt_vsprintf(char *, const char *, __builtin_va_list);






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _snprintf (char *, size_t, const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _vsnprintf (char *, size_t, const char *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _vscprintf (const char *, __builtin_va_list);
# 536 "c:\\mingw\\include\\stdio.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,3,4)))
int snprintf (char *, size_t, const char *, ...);

__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,3,0)))
int vsnprintf (char *, size_t, const char *, __builtin_va_list);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vscanf (const char * __restrict__, __builtin_va_list);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vfscanf (FILE * __restrict__, const char * __restrict__, __builtin_va_list);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vsscanf (const char * __restrict__, const char * __restrict__, __builtin_va_list);
# 679 "c:\\mingw\\include\\stdio.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) ssize_t
getdelim (char ** __restrict__, size_t * __restrict__, int, FILE * __restrict__);

__attribute__((__cdecl__)) __attribute__((__nothrow__)) ssize_t
getline (char ** __restrict__, size_t * __restrict__, FILE * __restrict__);
# 699 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fscanf (FILE *, const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int scanf (const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int sscanf (const char *, const char *, ...);



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fgetc (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char * fgets (char *, int, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fputc (int, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fputs (const char *, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char * gets (char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int puts (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int ungetc (int, FILE *);
# 720 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _filbuf (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _flsbuf (int, FILE *);



extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getc (FILE *);
extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getc (FILE * __F)
{
  return (--__F->_cnt >= 0)
    ? (int) (unsigned char) *__F->_ptr++
    : _filbuf (__F);
}

extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putc (int, FILE *);
extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putc (int __c, FILE * __F)
{
  return (--__F->_cnt >= 0)
    ? (int) (unsigned char) (*__F->_ptr++ = (char)__c)
    : _flsbuf (__c, __F);
}

extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getchar (void);
extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getchar (void)
{
  return (--(&_iob[0])->_cnt >= 0)
    ? (int) (unsigned char) *(&_iob[0])->_ptr++
    : _filbuf ((&_iob[0]));
}

extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putchar(int);
extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putchar(int __c)
{
  return (--(&_iob[1])->_cnt >= 0)
    ? (int) (unsigned char) (*(&_iob[1])->_ptr++ = (char)__c)
    : _flsbuf (__c, (&_iob[1]));}
# 767 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t fread (void *, size_t, size_t, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t fwrite (const void *, size_t, size_t, FILE *);



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fseek (FILE *, long, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long ftell (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void rewind (FILE *);
# 821 "c:\\mingw\\include\\stdio.h" 3
typedef union { long long __value; __off64_t __offset; } fpos_t;




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fgetpos (FILE *, fpos_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fsetpos (FILE *, const fpos_t *);
# 862 "c:\\mingw\\include\\stdio.h" 3
int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __mingw_fseeki64 (FILE *, long long, int);
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fseeki64 (FILE *__f, long long __o, int __w)
{ return __mingw_fseeki64 (__f, __o, __w); }


long long __attribute__((__cdecl__)) __attribute__((__nothrow__)) __mingw_ftelli64 (FILE *);
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__cdecl__)) long long __attribute__((__nothrow__)) _ftelli64 (FILE *__file )
{ return __mingw_ftelli64 (__file); }





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int feof (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int ferror (FILE *);
# 886 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void clearerr (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void perror (const char *);





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _popen (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _pclose (FILE *);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * popen (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int pclose (FILE *);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _flushall (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fgetchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fputchar (int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _fdopen (int, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fileno (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fcloseall (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _fsopen (const char *, const char *, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _getmaxstdio (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _setmaxstdio (int);
# 936 "c:\\mingw\\include\\stdio.h" 3
unsigned int __attribute__((__cdecl__)) __mingw_get_output_format (void);
unsigned int __attribute__((__cdecl__)) __mingw_set_output_format (unsigned int);







int __attribute__((__cdecl__)) __mingw_get_printf_count_output (void);
int __attribute__((__cdecl__)) __mingw_set_printf_count_output (int);
# 962 "c:\\mingw\\include\\stdio.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) unsigned int __attribute__((__cdecl__)) _get_output_format (void)
{ return __mingw_get_output_format (); }

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) unsigned int __attribute__((__cdecl__)) _set_output_format (unsigned int __style)
{ return __mingw_set_output_format (__style); }
# 987 "c:\\mingw\\include\\stdio.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) int __attribute__((__cdecl__)) _get_printf_count_output (void)
{ return 0 ? 1 : __mingw_get_printf_count_output (); }

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) int __attribute__((__cdecl__)) _set_printf_count_output (int __mode)
{ return 0 ? 1 : __mingw_set_printf_count_output (__mode); }



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fgetchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fputchar (int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * fdopen (int, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fileno (FILE *);
# 1007 "c:\\mingw\\include\\stdio.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) FILE * __attribute__((__cdecl__)) __attribute__((__nothrow__)) fopen64 (const char *, const char *);
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
FILE * __attribute__((__cdecl__)) __attribute__((__nothrow__)) fopen64 (const char * filename, const char * mode)
{ return fopen (filename, mode); }

int __attribute__((__cdecl__)) __attribute__((__nothrow__)) fseeko64 (FILE *, __off64_t, int);
# 1028 "c:\\mingw\\include\\stdio.h" 3
__off64_t __attribute__((__cdecl__)) __attribute__((__nothrow__)) ftello64 (FILE *);
# 1041 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fwprintf (FILE *, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wprintf (const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vfwprintf (FILE *, const wchar_t *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vwprintf (const wchar_t *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _snwprintf (wchar_t *, size_t, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _vscwprintf (const wchar_t *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _vsnwprintf (wchar_t *, size_t, const wchar_t *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fwscanf (FILE *, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wscanf (const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int swscanf (const wchar_t *, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t fgetwc (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t fputwc (wchar_t, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t ungetwc (wchar_t, FILE *);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int swprintf (wchar_t *, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vswprintf (wchar_t *, const wchar_t *, __builtin_va_list);



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t * fgetws (wchar_t *, int, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fputws (const wchar_t *, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t getwc (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t getwchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t putwc (wint_t, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t putwchar (wint_t);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t * _getws (wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _putws (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wfdopen(int, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wfopen (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wfreopen (const wchar_t *, const wchar_t *, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wfsopen (const wchar_t *, const wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t * _wtmpnam (wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t * _wtempnam (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wrename (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wremove (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _wperror (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wpopen (const wchar_t *, const wchar_t *);






__attribute__((__cdecl__)) __attribute__((__nothrow__)) int snwprintf (wchar_t *, size_t, const wchar_t *, ...);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int vsnwprintf (wchar_t *, size_t, const wchar_t *, __builtin_va_list);
# 1099 "c:\\mingw\\include\\stdio.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int vwscanf (const wchar_t *__restrict__, __builtin_va_list);
__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vfwscanf (FILE *__restrict__, const wchar_t *__restrict__, __builtin_va_list);
__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vswscanf (const wchar_t *__restrict__, const wchar_t * __restrict__, __builtin_va_list);






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * wpopen (const wchar_t *, const wchar_t *);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t _fgetwchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t _fputwchar (wint_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _getw (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _putw (int, FILE *);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t fgetwchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t fputwchar (wint_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getw (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putw (int, FILE *);





# 11 "./src/lib/src/../include/Partie4.h" 2
# 24 "./src/lib/src/../include/Partie4.h"
    
# 24 "./src/lib/src/../include/Partie4.h"
   typedef struct Couleur{

        short int rouge, vert, bleu;
    }Couleur;





    typedef struct Palette {
        int nb;
        Couleur *pal;
    }Palette;





    typedef struct Motif{

        short int forme;
        short int config;

        int cfond;
        int cobjet;
    }Motif;






    typedef struct noeudMotif{

        struct noeudMotif *NO;
        struct noeudMotif *NE;
        struct noeudMotif *SO;
        struct noeudMotif *SE;

        Motif motif;
        int x,y;
        int largeur;

    }noeudMotif;







    Palette* creePalette(Image *img);
# 87 "./src/lib/src/../include/Partie4.h"
    Motif identifieMotif(Image *img, Palette *palette, int x, int y, int largeur);
# 99 "./src/lib/src/../include/Partie4.h"
    noeudMotif* creeArbreMotifs(Image *img, Palette *palette, int largeur, int x, int y);
# 108 "./src/lib/src/../include/Partie4.h"
    void sauveDescriptionImage(noeudMotif *rcn, Palette* pal,char *path);






    void affichePalette(Palette *pal);






    void libereArbreMotif(noeudMotif *ptrRcn);
# 136 "./src/lib/src/../include/Partie4.h"
    
# 136 "./src/lib/src/../include/Partie4.h" 3 4
   _Bool 
# 136 "./src/lib/src/../include/Partie4.h"
        compareCouleur (Couleur c1, Couleur c2);
# 148 "./src/lib/src/../include/Partie4.h"
    
# 148 "./src/lib/src/../include/Partie4.h" 3 4
   _Bool 
# 148 "./src/lib/src/../include/Partie4.h"
        compareCouleurVariadic (Couleur c1, Couleur c2, int nbreArgumentsOptionnels, ...);
# 158 "./src/lib/src/../include/Partie4.h"
    
# 158 "./src/lib/src/../include/Partie4.h" 3 4
   _Bool 
# 158 "./src/lib/src/../include/Partie4.h"
        sontDesCouleursVariadic(int nbreArguments, ...);
# 168 "./src/lib/src/../include/Partie4.h"
    
# 168 "./src/lib/src/../include/Partie4.h" 3 4
   _Bool 
# 168 "./src/lib/src/../include/Partie4.h"
        comparePalette(Palette p1, Palette p2);
# 178 "./src/lib/src/../include/Partie4.h"
    
# 178 "./src/lib/src/../include/Partie4.h" 3 4
   _Bool 
# 178 "./src/lib/src/../include/Partie4.h"
        compareMotif(Motif m1, Motif m2);
# 189 "./src/lib/src/../include/Partie4.h"
    
# 189 "./src/lib/src/../include/Partie4.h" 3 4
   _Bool 
# 189 "./src/lib/src/../include/Partie4.h"
        estCouleurDans(Couleur *arr, int arrSize, Couleur couleur);
# 201 "./src/lib/src/../include/Partie4.h"
    Couleur estCouleurUniforme(Image *img, int depart_x, int depart_y, int largeur, int hauteur);
# 213 "./src/lib/src/../include/Partie4.h"
    Couleur estCouleurUniformeDansCarre(Image *img, int x1, int y1, int x2, int y2);
# 6 "./src/lib/src/../include/Partie5.h" 2


    typedef struct Point{
        unsigned int x,y;
    }Point;





    typedef struct MaillonTraj{
        Point position;
        struct MaillonTraj *next;
    }MaillonTraj;





    typedef struct Robot{
        MaillonTraj *ptrTete;
        Point position;
        Point depart;
        Point arrivee;
    }Robot;





    typedef struct Emplacement{
        Point origine;
        unsigned int largeurMotif;
    }Emplacement;
# 50 "./src/lib/src/../include/Partie5.h"
    void dessineMotif1(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 50 "./src/lib/src/../include/Partie5.h" 3 4
                                                                                                                        _Bool 
# 50 "./src/lib/src/../include/Partie5.h"
                                                                                                                             trajectoire);
# 60 "./src/lib/src/../include/Partie5.h"
    void dessineMotif2(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 60 "./src/lib/src/../include/Partie5.h" 3 4
                                                                                                                       _Bool 
# 60 "./src/lib/src/../include/Partie5.h"
                                                                                                                            trajectoire);
# 70 "./src/lib/src/../include/Partie5.h"
    void dessineMotif3(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 70 "./src/lib/src/../include/Partie5.h" 3 4
                                                                                                                       _Bool 
# 70 "./src/lib/src/../include/Partie5.h"
                                                                                                                            trajectoire);
# 80 "./src/lib/src/../include/Partie5.h"
    void dessineMotif4(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 80 "./src/lib/src/../include/Partie5.h" 3 4
                                                                                                                       _Bool 
# 80 "./src/lib/src/../include/Partie5.h"
                                                                                                                            trajectoire);
# 90 "./src/lib/src/../include/Partie5.h"
    void dessineMotif5(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 90 "./src/lib/src/../include/Partie5.h" 3 4
                                                                                                                       _Bool 
# 90 "./src/lib/src/../include/Partie5.h"
                                                                                                                            trajectoire);
# 100 "./src/lib/src/../include/Partie5.h"
    void dessineMotif6(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 100 "./src/lib/src/../include/Partie5.h" 3 4
                                                                                                                       _Bool 
# 100 "./src/lib/src/../include/Partie5.h"
                                                                                                                            trajectoire);
# 111 "./src/lib/src/../include/Partie5.h"
    void dessineMotifNoeud(const Image *img,const Palette palette, const noeudMotif *noeud, const int formeId, const 
# 111 "./src/lib/src/../include/Partie5.h" 3 4
                                                                                                                    _Bool 
# 111 "./src/lib/src/../include/Partie5.h"
                                                                                                                         trajectoire);
# 121 "./src/lib/src/../include/Partie5.h"
    void dessineCarte(const Image *img, const Palette palette, const noeudMotif *racine, const 
# 121 "./src/lib/src/../include/Partie5.h" 3 4
                                                                                              _Bool 
# 121 "./src/lib/src/../include/Partie5.h"
                                                                                                   trajectoire);
# 133 "./src/lib/src/../include/Partie5.h"
    Robot* calculeTrajectoire(Image *img, Image *imgSansTrajectoires, Palette *palette, Point depart, Point arrivee);







    void sauveDescriptionChemin(Robot *robot, char* path);
# 151 "./src/lib/src/../include/Partie5.h"
    
# 151 "./src/lib/src/../include/Partie5.h" 3 4
   _Bool 
# 151 "./src/lib/src/../include/Partie5.h"
        comparePoints(Point p1, Point p2);
# 163 "./src/lib/src/../include/Partie5.h"
    
# 163 "./src/lib/src/../include/Partie5.h" 3 4
   _Bool 
# 163 "./src/lib/src/../include/Partie5.h"
        comparePointsVariadic (Point p1, Point p2, int nbreArgumentsOptionnels, ...);
# 172 "./src/lib/src/../include/Partie5.h"
    int positionVoisinage(Point p1, Point p2);
# 181 "./src/lib/src/../include/Partie5.h"
    void dessineCarre(const Image *img, const Couleur couleur, const Emplacement emplacement);







    MaillonTraj* creeMaillon(const Point position);
# 198 "./src/lib/src/../include/Partie5.h"
    void insererMaillonAvant(MaillonTraj *ptrTete, const int index, MaillonTraj *maillonAInserer);







    void ajouterMaillon(MaillonTraj *ptrTete, MaillonTraj *maillonAAjouter);







    void supprimerMaillonParValeur(MaillonTraj *ptrTete, const Point position);







    void supprimerMaillon(MaillonTraj *ptrTete, MaillonTraj **maillon);
# 231 "./src/lib/src/../include/Partie5.h"
    int trouverIndexParValeur(MaillonTraj *ptrTete, const Point position);






    void libereMaillon(MaillonTraj **maillon);
# 2 "./src/lib/src/Partie5.c" 2
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 1 3 4
# 99 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 3 4

# 99 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 3 4
typedef __gnuc_va_list va_list;
# 3 "./src/lib/src/Partie5.c" 2
# 1 "c:\\mingw\\include\\stdlib.h" 1 3
# 34 "c:\\mingw\\include\\stdlib.h" 3
       
# 35 "c:\\mingw\\include\\stdlib.h" 3
# 55 "c:\\mingw\\include\\stdlib.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 56 "c:\\mingw\\include\\stdlib.h" 2 3
# 90 "c:\\mingw\\include\\stdlib.h" 3

# 99 "c:\\mingw\\include\\stdlib.h" 3
extern int _argc;
extern char **_argv;




extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) int *__p___argc(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) char ***__p___argv(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t ***__p___wargv(void);
# 142 "c:\\mingw\\include\\stdlib.h" 3
   extern __attribute__((__dllimport__)) int __mb_cur_max;
# 166 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int *_errno(void);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int *__doserrno(void);







extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) char ***__p__environ(void);

extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t ***__p__wenviron(void);
# 202 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) int _sys_nerr;
# 227 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) char *_sys_errlist[];
# 238 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__osver(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__winver(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__winmajor(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__winminor(void);
# 250 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) unsigned int _osver;
extern __attribute__((__dllimport__)) unsigned int _winver;
extern __attribute__((__dllimport__)) unsigned int _winmajor;
extern __attribute__((__dllimport__)) unsigned int _winminor;
# 289 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char **__p__pgmptr(void);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t **__p__wpgmptr(void);
# 325 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) int _fmode;
# 335 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int atoi (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long atol (const char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double strtod (const char *, char **);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double atof (const char *);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double _wtof (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wtoi (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long _wtol (const wchar_t *);
# 378 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__))
float strtof (const char *__restrict__, char **__restrict__);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
long double strtold (const char *__restrict__, char **__restrict__);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long strtol (const char *, char **, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned long strtoul (const char *, char **, int);







 __attribute__((__cdecl__)) __attribute__((__nothrow__))
long wcstol (const wchar_t *, wchar_t **, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
unsigned long wcstoul (const wchar_t *, wchar_t **, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double wcstod (const wchar_t *, wchar_t **);





__attribute__((__cdecl__)) __attribute__((__nothrow__))
float wcstof (const wchar_t *__restrict__, wchar_t **__restrict__);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
long double wcstold (const wchar_t *__restrict__, wchar_t **__restrict__);
# 451 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_wgetenv (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wputenv (const wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _wsearchenv (const wchar_t *, const wchar_t *, wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wsystem (const wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _wmakepath (wchar_t *, const wchar_t *, const wchar_t *, const wchar_t *,
    const wchar_t *
  );

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _wsplitpath (const wchar_t *, wchar_t *, wchar_t *, wchar_t *, wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
wchar_t *_wfullpath (wchar_t *, const wchar_t *, size_t);





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t wcstombs (char *, const wchar_t *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wctomb (char *, wchar_t);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int mblen (const char *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t mbstowcs (wchar_t *, const char *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int mbtowc (wchar_t *, const char *, size_t);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int rand (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void srand (unsigned int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void abort (void) __attribute__((__noreturn__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void exit (int) __attribute__((__noreturn__));



int __attribute__((__cdecl__)) __attribute__((__nothrow__)) atexit (void (*)(void));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int system (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *getenv (const char *);






# 1 "c:\\mingw\\include\\alloca.h" 1 3
# 43 "c:\\mingw\\include\\alloca.h" 3
       
# 44 "c:\\mingw\\include\\alloca.h" 3
# 54 "c:\\mingw\\include\\alloca.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 55 "c:\\mingw\\include\\alloca.h" 2 3


# 80 "c:\\mingw\\include\\alloca.h" 3
void *alloca( size_t );







void *_alloca( size_t );



# 500 "c:\\mingw\\include\\stdlib.h" 2 3


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *calloc (size_t, size_t) __attribute__((__malloc__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *malloc (size_t) __attribute__((__malloc__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *realloc (void *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void free (void *);
# 514 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) void *__mingw_realloc (void *, size_t);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) void __mingw_free (void *);






 __attribute__((__cdecl__)) void *bsearch
(const void *, const void *, size_t, size_t, int (*)(const void *, const void *));

 __attribute__((__cdecl__)) void qsort
(void *, size_t, size_t, int (*)(const void *, const void *));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int abs (int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long labs (long) __attribute__((__const__));
# 538 "c:\\mingw\\include\\stdlib.h" 3
typedef struct { int quot, rem; } div_t;
typedef struct { long quot, rem; } ldiv_t;

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) div_t div (int, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) ldiv_t ldiv (long, long) __attribute__((__const__));






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _exit (int) __attribute__((__noreturn__));





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long long _atoi64 (const char *);
# 564 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _beep (unsigned int, unsigned int) __attribute__((__deprecated__));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _seterrormode (int) __attribute__((__deprecated__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _sleep (unsigned long) __attribute__((__deprecated__));



typedef int (* _onexit_t)(void);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) _onexit_t _onexit( _onexit_t );

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _putenv (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _searchenv (const char *, const char *, char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_ecvt (double, int, int *, int *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_fcvt (double, int, int *, int *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_gcvt (double, int, char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _makepath (char *, const char *, const char *, const char *, const char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _splitpath (const char *, char *, char *, char *, char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_fullpath (char*, const char*, size_t);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_itoa (int, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_ltoa (long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_ultoa(unsigned long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_itow (int, wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_ltow (long, wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_ultow (unsigned long, wchar_t *, int);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* _i64toa (long long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* _ui64toa (unsigned long long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long long _wtoi64 (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t* _i64tow (long long, wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t* _ui64tow (unsigned long long, wchar_t *, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int (_rotl)(unsigned int, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int (_rotr)(unsigned int, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned long (_lrotl)(unsigned long, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned long (_lrotr)(unsigned long, int) __attribute__((__const__));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _set_error_mode (int);
# 647 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putenv (const char*);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void searchenv (const char*, const char*, char*);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* itoa (int, char*, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* ltoa (long, char*, int);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* ecvt (double, int, int*, int*);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* fcvt (double, int, int*, int*);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* gcvt (double, int, char*);
# 668 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) void _Exit(int) __attribute__((__noreturn__));






typedef struct { long long quot, rem; } lldiv_t;
__attribute__((__cdecl__)) __attribute__((__nothrow__)) lldiv_t lldiv (long long, long long) __attribute__((__const__));

__attribute__((__cdecl__)) __attribute__((__nothrow__)) long long llabs (long long);
# 689 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__))
long long strtoll (const char *__restrict__, char **__restrict, int);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
unsigned long long strtoull (const char *__restrict__, char **__restrict__, int);





__attribute__((__cdecl__)) __attribute__((__nothrow__)) long long atoll (const char *);
# 745 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) long long wtoll (const wchar_t *);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) char *lltoa (long long, char *, int);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) char *ulltoa (unsigned long long , char *, int);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) wchar_t *lltow (long long, wchar_t *, int);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) wchar_t *ulltow (unsigned long long, wchar_t *, int);
# 785 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int mkstemp (char *);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int __mingw_mkstemp (int, char *);
# 827 "c:\\mingw\\include\\stdlib.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int mkstemp (char *__filename_template)
{ return __mingw_mkstemp( 0, __filename_template ); }
# 838 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) char *mkdtemp (char *);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) char *__mingw_mkdtemp (char *);

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) char *mkdtemp (char *__dirname_template)
{ return __mingw_mkdtemp( __dirname_template ); }






__attribute__((__cdecl__)) __attribute__((__nothrow__)) int setenv( const char *, const char *, int );
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int unsetenv( const char * );

__attribute__((__cdecl__)) __attribute__((__nothrow__)) int __mingw_setenv( const char *, const char *, int );

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int setenv( const char *__n, const char *__v, int __f )
{ return __mingw_setenv( __n, __v, __f ); }

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int unsetenv( const char *__name )
{ return __mingw_setenv( __name, ((void *)0), 1 ); }





# 4 "./src/lib/src/Partie5.c" 2

_Bool 
# 5 "./src/lib/src/Partie5.c"
    comparePoints(Point p1, Point p2){
    if(p1.x!=p2.x||p1.y!=p2.y) return 
# 6 "./src/lib/src/Partie5.c" 3 4
                                     0
# 6 "./src/lib/src/Partie5.c"
                                          ;
    return 
# 7 "./src/lib/src/Partie5.c" 3 4
          1
# 7 "./src/lib/src/Partie5.c"
              ;
}

int positionVoisinage(Point p1, Point p2){
    Point haut_gauche = (Point) {p2.x-1,p2.y+1},
    haut = (Point) {p2.x,p2.y+1},
    haut_droite = (Point) {p2.x+1,p2.y+1},
    droite = (Point) {p2.x+1,p2.y},
    bas_droite = (Point) {p2.x+1,p2.y-1},
    bas = (Point) {p2.x,p2.y-1},
    bas_gauche = (Point) {p2.x-1,p2.y-1},
    gauche = (Point) {p2.x-1,p2.y};

    if(comparePoints(p1,haut_gauche)) return 0;
    if(comparePoints(p1,haut)) return 1;
    if(comparePoints(p1,haut_droite)) return 2;
    if(comparePoints(p1,droite)) return 3;
    if(comparePoints(p1,bas_droite)) return 4;
    if(comparePoints(p1,bas)) return 5;
    if(comparePoints(p1,bas_gauche)) return 6;
    if(comparePoints(p1,gauche)) return 7;
    if(comparePoints(p1,p2)) return 8;

    return -1;
}


# 33 "./src/lib/src/Partie5.c" 3 4
_Bool 
# 33 "./src/lib/src/Partie5.c"
    comparePointsVariadic (Point p1, Point p2, int nbreArgumentsOptionnels, ...){
    if(p1.x!=p2.x||p1.y!=p2.y) return 
# 34 "./src/lib/src/Partie5.c" 3 4
                                     0
# 34 "./src/lib/src/Partie5.c"
                                          ;

    va_list ap;
    
# 37 "./src/lib/src/Partie5.c" 3 4
   __builtin_va_start(
# 37 "./src/lib/src/Partie5.c"
   ap
# 37 "./src/lib/src/Partie5.c" 3 4
   ,
# 37 "./src/lib/src/Partie5.c"
   nbreArgumentsOptionnels
# 37 "./src/lib/src/Partie5.c" 3 4
   )
# 37 "./src/lib/src/Partie5.c"
                                         ;
    Point tmp;
    for (int i = 0; i < nbreArgumentsOptionnels; i++){
        tmp = 
# 40 "./src/lib/src/Partie5.c" 3 4
             __builtin_va_arg(
# 40 "./src/lib/src/Partie5.c"
             ap
# 40 "./src/lib/src/Partie5.c" 3 4
             ,
# 40 "./src/lib/src/Partie5.c"
             Point
# 40 "./src/lib/src/Partie5.c" 3 4
             )
# 40 "./src/lib/src/Partie5.c"
                               ;
        if(p1.x!=tmp.x||p1.y!=tmp.y) return 
# 41 "./src/lib/src/Partie5.c" 3 4
                                           0
# 41 "./src/lib/src/Partie5.c"
                                                ;
    }
    
# 43 "./src/lib/src/Partie5.c" 3 4
   __builtin_va_end(
# 43 "./src/lib/src/Partie5.c"
   ap
# 43 "./src/lib/src/Partie5.c" 3 4
   )
# 43 "./src/lib/src/Partie5.c"
              ;
    return 
# 44 "./src/lib/src/Partie5.c" 3 4
          1
# 44 "./src/lib/src/Partie5.c"
              ;
}

MaillonTraj* creeMaillon(const Point position){
    MaillonTraj *ret = (MaillonTraj*) malloc(sizeof(MaillonTraj));
    ret->position = (Point){position.x,position.y};
    ret->next = 
# 50 "./src/lib/src/Partie5.c" 3 4
               ((void *)0)
# 50 "./src/lib/src/Partie5.c"
                   ;
    return ret;
}

void insererMaillonAvant(MaillonTraj *ptrTete, const int index, MaillonTraj *maillonAInserer){
    MaillonTraj *maillonCourant = ptrTete;
    int i = 0;
    while(maillonCourant)
    {
        if(i+1 == index)
        {
            MaillonTraj *tmp = maillonCourant->next;
            maillonCourant->next = maillonAInserer;
            maillonAInserer->next = tmp;
            return;
        }

        i++;
        maillonCourant = maillonCourant->next;
    }
}

void ajouterMaillon(MaillonTraj *ptrTete, MaillonTraj *maillonAAjouter){
    MaillonTraj *maillonCourant = ptrTete;
    while(maillonCourant->next)
    {
        maillonCourant = maillonCourant->next;
    }
    maillonCourant->next = maillonAAjouter;
}

void libereMaillon(MaillonTraj **maillon){

    if(*maillon) free(*maillon);
    maillon = 
# 84 "./src/lib/src/Partie5.c" 3 4
             ((void *)0)
# 84 "./src/lib/src/Partie5.c"
                 ;
}

int trouverIndexParValeur(MaillonTraj *ptrTete, const Point position){
    MaillonTraj *maillonCourant = ptrTete;
    int i = 0;
    while(maillonCourant->next)
    {
        if(maillonCourant->position.x == position.x && maillonCourant->position.y == position.y) return i;

        i++;
        maillonCourant = maillonCourant->next;
    }

    return -1;
}

void supprimerMaillonParValeur(MaillonTraj *ptrTete, const Point position){
    MaillonTraj *maillonCourant = ptrTete;
    while(maillonCourant->next)
    {
        MaillonTraj *next = maillonCourant->next;
        if(next->position.x == position.x && next->position.y == position.y)
        {
            MaillonTraj *target = maillonCourant->next;
            maillonCourant->next = maillonCourant->next->next;
            libereMaillon(&target);
            return;
        }
        maillonCourant = maillonCourant->next;
    }
}

void supprimerMaillon(MaillonTraj *ptrTete, MaillonTraj **maillon){
    MaillonTraj *maillonCourant = ptrTete;

    while(maillonCourant->next != *maillon)
    {
        maillonCourant = maillonCourant->next;
    }
    MaillonTraj *tmp = maillonCourant->next->next;
    libereMaillon(maillon);
    maillonCourant->next = tmp;
}

void dessineCarre(const Image *img, const Couleur couleur, const Emplacement emplacement){
    for (int x = emplacement.origine.x; x < emplacement.origine.x+emplacement.largeurMotif; x++)
    {
        for (int y = emplacement.origine.y; y < emplacement.origine.y+emplacement.largeurMotif; y++)
        {
            img->rouge[x][y] = couleur.rouge;
            img->vert[x][y] = couleur.vert;
            img->bleu[x][y] = couleur.bleu;
            img->gris[x][y] = 0.2125*couleur.rouge +0.7154*couleur.vert + 0.0721*couleur.bleu;
        }
    }
}

Robot* calculeTrajectoire(Image *img, Image *imgSansTrajectoires, Palette *palette, Point depart, Point arrivee){


    if( depart.x+3 > img->largeur-1 || depart.x-3 < 0 ||
        depart.y+3 > img->hauteur-1 || depart.y-3 < 0 ||
        arrivee.x+3 > img->largeur-1 || arrivee.x-3 <0 ||
        arrivee.y+3 > img->hauteur-1 || arrivee.y-3 <0 ||
        !compareCouleur(palette->pal[0], estCouleurUniforme(imgSansTrajectoires,depart.x-3,depart.y-3,7,7)) ||
        !compareCouleur(palette->pal[0], estCouleurUniforme(imgSansTrajectoires,arrivee.x-3,arrivee.y-3,7,7)))
        {
            return 
# 152 "./src/lib/src/Partie5.c" 3 4
                  ((void *)0)
# 152 "./src/lib/src/Partie5.c"
                      ;
        }


    Robot* robot = (Robot*) malloc(sizeof(Robot));
    robot->depart = (Point){depart.x,depart.y};
    robot->arrivee = (Point){arrivee.x,arrivee.y};
    robot->position = (Point) {depart.x,depart.y};
    robot->ptrTete = creeMaillon(robot->depart);


    if(depart.x == arrivee.x)
    {
        Point positionCourante = depart;
        while (!comparePoints(positionCourante,arrivee))
        {
            if(positionCourante.x < arrivee.x){
                positionCourante = (Point){positionCourante.x+1,positionCourante.y};
                ajouterMaillon(robot->ptrTete,creeMaillon(positionCourante));

            }
            else if( positionCourante.x > arrivee.x){
                positionCourante = (Point){positionCourante.x-1,positionCourante.y};
                ajouterMaillon(robot->ptrTete,creeMaillon(positionCourante));

            }
            else if(positionCourante.y < arrivee.y){
                positionCourante = (Point){positionCourante.x,positionCourante.y+1};
                ajouterMaillon(robot->ptrTete,creeMaillon(positionCourante));

            }
            else if(positionCourante.y > arrivee.y){
                positionCourante = (Point){positionCourante.x,positionCourante.y-1};
                ajouterMaillon(robot->ptrTete,creeMaillon(positionCourante));
            }
            else break;
        }

    }

    else
    {
        float a = ((float) arrivee.y-(float) depart.y)/((float) arrivee.x-(float) depart.x);
        float b = (float) depart.y - (float) a* (float) depart.x;

        MaillonTraj *maillonPrecedent = robot->ptrTete;
        for (int x=depart.x;(depart.x>arrivee.x)?x>=arrivee.x:x<=arrivee.x;(depart.x>arrivee.x)?x--:x++)
        {
            int y = a*x+b;

            MaillonTraj *nouveauMaillon = creeMaillon((Point){x,y});

            Point positionPrecedente = (Point){maillonPrecedent->position.x,maillonPrecedent->position.y};
            Point positionCourante = (Point){x,y};

            if(positionVoisinage(positionPrecedente, positionCourante) != 8){

                while( positionVoisinage(positionPrecedente, positionCourante) != 1 &&
                        positionVoisinage(positionPrecedente, positionCourante) != 3 &&
                        positionVoisinage(positionPrecedente, positionCourante) != 5 &&
                        positionVoisinage(positionPrecedente, positionCourante) != 7){
                    if(positionPrecedente.x < positionCourante.x){
                        positionPrecedente = (Point){positionPrecedente.x+1,positionPrecedente.y};
                        ajouterMaillon(robot->ptrTete,creeMaillon(positionPrecedente));

                    }
                    else if( positionPrecedente.x > positionCourante.x){
                        positionPrecedente = (Point){positionPrecedente.x-1,positionPrecedente.y};
                        ajouterMaillon(robot->ptrTete,creeMaillon(positionPrecedente));

                    }
                    else if(positionPrecedente.y < positionCourante.y){
                        positionPrecedente = (Point){positionPrecedente.x,positionPrecedente.y+1};
                        ajouterMaillon(robot->ptrTete,creeMaillon(positionPrecedente));

                    }
                    else if( positionPrecedente.y > positionCourante.y){
                        positionPrecedente = (Point){positionPrecedente.x,positionPrecedente.y-1};
                        ajouterMaillon(robot->ptrTete,creeMaillon(positionPrecedente));
                    }
                    else break;
                }
            }

            ajouterMaillon(robot->ptrTete,nouveauMaillon);
            maillonPrecedent = nouveauMaillon;

        }
    }

    MaillonTraj *current = robot->ptrTete;

    reloop:


    while(current->next)
    {


        if(!compareCouleur(palette->pal[0], estCouleurUniforme(imgSansTrajectoires,current->next->position.x-4,current->next->position.y-4,9,9))){

            MaillonTraj *firstSeg = current->next;
            MaillonTraj *currentT = firstSeg;


            while (!compareCouleur(palette->pal[0], estCouleurUniforme(imgSansTrajectoires,currentT->next->position.x-4,currentT->next->position.y-4,9,9)))
            {
                currentT=currentT->next;
                if(currentT->next == 
# 260 "./src/lib/src/Partie5.c" 3 4
                                    ((void *)0)
# 260 "./src/lib/src/Partie5.c"
                                        )
                    return 
# 261 "./src/lib/src/Partie5.c" 3 4
                          ((void *)0)
# 261 "./src/lib/src/Partie5.c"
                              ;
            }
            MaillonTraj *secondSeg= currentT;


            Point previousPosition1 = (Point){current->next->position.x,current->next->position.y},
            previousPosition2 = (Point){current->next->position.x,current->next->position.y},
            previousPosition3 = (Point){current->next->position.x,current->next->position.y},
            previousPosition4 = (Point){current->next->position.x,current->next->position.y};

            MaillonTraj *ptrTetePath1 = creeMaillon((Point){current->next->position.x,current->next->position.y}),
            *ptrTetePath2 = creeMaillon((Point){current->next->position.x,current->next->position.y}),
            *ptrTetePath3 = creeMaillon((Point){current->next->position.x,current->next->position.y}),
            *ptrTetePath4 = creeMaillon((Point){current->next->position.x,current->next->position.y}),

            *current1 = ptrTetePath1,
            *current2 = ptrTetePath2,
            *current3 = ptrTetePath3,
            *current4 = ptrTetePath4;

            int passage = 0;
            
# 282 "./src/lib/src/Partie5.c" 3 4
           _Bool 
# 282 "./src/lib/src/Partie5.c"
                pathFind = 
# 282 "./src/lib/src/Partie5.c" 3 4
                           0
# 282 "./src/lib/src/Partie5.c"
                                ;


            while(pathFind == 
# 285 "./src/lib/src/Partie5.c" 3 4
                             0
# 285 "./src/lib/src/Partie5.c"
                                  )
            {

                Point up1,right1,down1,left1,up2,right2,down2,left2,up3,right3,down3,left3,up4,right4,down4,left4;
                Couleur cUp1 ,cRight1,cDown1,cLeft1,cUp2 ,cRight2,cDown2,cLeft2,cUp3,cRight3,cDown3,cLeft3,cUp4 ,cRight4,cDown4,cLeft4;

                if(current1)
                {
                    up1=(Point){current1->position.x,current1->position.y+1},
                    right1=(Point){current1->position.x+1,current1->position.y},
                    down1=(Point){current1->position.x,current1->position.y-1},
                    left1=(Point){current1->position.x-1,current1->position.y};

                    cUp1 = (Couleur){img->rouge[up1.x][up1.y],img->vert[up1.x][up1.y],img->bleu[up1.x][up1.y]},
                    cRight1 = (Couleur){img->rouge[right1.x][right1.y],img->vert[right1.x][right1.y],img->bleu[right1.x][right1.y]},
                    cDown1 = (Couleur){img->rouge[down1.x][down1.y],img->vert[down1.x][down1.y],img->bleu[down1.x][down1.y]},
                    cLeft1 = (Couleur){img->rouge[left1.x][left1.y],img->vert[left1.x][left1.y],img->bleu[left1.x][left1.y]};
                }

                if(current2)
                {
                    up2=(Point){current2->position.x,current2->position.y+1},
                    right2=(Point){current2->position.x+1,current2->position.y},
                    down2=(Point){current2->position.x,current2->position.y-1},
                    left2=(Point){current2->position.x-1,current2->position.y};

                    cUp2 = (Couleur){img->rouge[up2.x][up2.y],img->vert[up2.x][up2.y],img->bleu[up2.x][up2.y]},
                    cRight2 = (Couleur){img->rouge[right2.x][right2.y],img->vert[right2.x][right2.y],img->bleu[right2.x][right2.y]},
                    cDown2 = (Couleur){img->rouge[down2.x][down2.y],img->vert[down2.x][down2.y],img->bleu[down2.x][down2.y]},
                    cLeft2 = (Couleur){img->rouge[left2.x][left2.y],img->vert[left2.x][left2.y],img->bleu[left2.x][left2.y]};
                }

                if(current3)
                {
                    up3=(Point){current3->position.x,current3->position.y+1},
                    right3=(Point){current3->position.x+1,current3->position.y},
                    down3=(Point){current3->position.x,current3->position.y-1},
                    left3=(Point){current3->position.x-1,current3->position.y};

                    cUp3 = (Couleur){img->rouge[up3.x][up3.y],img->vert[up3.x][up3.y],img->bleu[up3.x][up3.y]},
                    cRight3 = (Couleur){img->rouge[right3.x][right3.y],img->vert[right3.x][right3.y],img->bleu[right3.x][right3.y]},
                    cDown3 = (Couleur){img->rouge[down3.x][down3.y],img->vert[down3.x][down3.y],img->bleu[down3.x][down3.y]},
                    cLeft3 = (Couleur){img->rouge[left3.x][left3.y],img->vert[left3.x][left3.y],img->bleu[left3.x][left3.y]};
                }

                if(current4)
                {
                    up4=(Point){current4->position.x,current4->position.y+1},
                    right4=(Point){current4->position.x+1,current4->position.y},
                    down4=(Point){current4->position.x,current4->position.y-1},
                    left4=(Point){current4->position.x-1,current4->position.y};

                    cUp4 = (Couleur){img->rouge[up4.x][up4.y],img->vert[up4.x][up4.y],img->bleu[up4.x][up4.y]},
                    cRight4 = (Couleur){img->rouge[right4.x][right4.y],img->vert[right4.x][right4.y],img->bleu[right4.x][right4.y]},
                    cDown4 = (Couleur){img->rouge[down4.x][down4.y],img->vert[down4.x][down4.y],img->bleu[down4.x][down4.y]},
                    cLeft4 = (Couleur){img->rouge[left4.x][left4.y],img->vert[left4.x][left4.y],img->bleu[left4.x][left4.y]};
                }

                if(ptrTetePath1){
                    if(compareCouleur(cUp1,palette->pal[0]) == 
# 344 "./src/lib/src/Partie5.c" 3 4
                                                              0 
# 344 "./src/lib/src/Partie5.c"
                                                                    && comparePoints(up1,previousPosition1) == 
# 344 "./src/lib/src/Partie5.c" 3 4
                                                                                                               0
# 344 "./src/lib/src/Partie5.c"
                                                                                                                    ){
                        ajouterMaillon(ptrTetePath1,creeMaillon(up1));
                    }
                    else if(compareCouleur(cRight1,palette->pal[0]) == 
# 347 "./src/lib/src/Partie5.c" 3 4
                                                                      0 
# 347 "./src/lib/src/Partie5.c"
                                                                            && comparePoints(right1,previousPosition1) == 
# 347 "./src/lib/src/Partie5.c" 3 4
                                                                                                                          0
# 347 "./src/lib/src/Partie5.c"
                                                                                                                               )
                    {
                        ajouterMaillon(ptrTetePath1,creeMaillon(right1));
                    }
                    else if(compareCouleur(cDown1,palette->pal[0]) == 
# 351 "./src/lib/src/Partie5.c" 3 4
                                                                     0 
# 351 "./src/lib/src/Partie5.c"
                                                                           && comparePoints(down1,previousPosition1) == 
# 351 "./src/lib/src/Partie5.c" 3 4
                                                                                                                        0
# 351 "./src/lib/src/Partie5.c"
                                                                                                                             )
                    {
                        ajouterMaillon(ptrTetePath1,creeMaillon(down1));
                    }
                    else if(compareCouleur(cLeft1,palette->pal[0]) == 
# 355 "./src/lib/src/Partie5.c" 3 4
                                                                     0 
# 355 "./src/lib/src/Partie5.c"
                                                                           && comparePoints(left1,previousPosition1) == 
# 355 "./src/lib/src/Partie5.c" 3 4
                                                                                                                        0
# 355 "./src/lib/src/Partie5.c"
                                                                                                                             )
                    {
                        ajouterMaillon(ptrTetePath1,creeMaillon(left1));
                    }

                    if(current1->next == 
# 360 "./src/lib/src/Partie5.c" 3 4
                                        ((void *)0)
# 360 "./src/lib/src/Partie5.c"
                                            )
                    {
                        while(ptrTetePath1){
                            MaillonTraj *tmp = ptrTetePath1->next;
                            libereMaillon(&ptrTetePath1);
                            ptrTetePath1=tmp;
                        }
                        current1 = 
# 367 "./src/lib/src/Partie5.c" 3 4
                                  ((void *)0)
# 367 "./src/lib/src/Partie5.c"
                                      ;
                        previousPosition1 = (Point){0,0};
                    }
                    else
                    {
                        previousPosition1 = (Point){current1->position.x,current1->position.y};
                        current1 = current1->next;
                    }
                }

                if(ptrTetePath2)
                {
                    if(compareCouleur(cLeft2,palette->pal[0]) == 
# 379 "./src/lib/src/Partie5.c" 3 4
                                                                0 
# 379 "./src/lib/src/Partie5.c"
                                                                      && comparePoints(left2,previousPosition2) == 
# 379 "./src/lib/src/Partie5.c" 3 4
                                                                                                                   0
# 379 "./src/lib/src/Partie5.c"
                                                                                                                        )
                    {
                        if (current1 == 
# 381 "./src/lib/src/Partie5.c" 3 4
                                       ((void *)0) 
# 381 "./src/lib/src/Partie5.c"
                                            || (comparePoints(left2,current1->position) == 
# 381 "./src/lib/src/Partie5.c" 3 4
                                                                                           0
# 381 "./src/lib/src/Partie5.c"
                                                                                                ))
                        {
                            ajouterMaillon(ptrTetePath2,creeMaillon(left2));
                        }
                    }
                    else if(compareCouleur(cDown2,palette->pal[0]) == 
# 386 "./src/lib/src/Partie5.c" 3 4
                                                                     0 
# 386 "./src/lib/src/Partie5.c"
                                                                           && comparePoints(down2,previousPosition2) == 
# 386 "./src/lib/src/Partie5.c" 3 4
                                                                                                                        0
# 386 "./src/lib/src/Partie5.c"
                                                                                                                             )
                    {
                        if (current1 == 
# 388 "./src/lib/src/Partie5.c" 3 4
                                       ((void *)0) 
# 388 "./src/lib/src/Partie5.c"
                                            || (comparePoints(down2,current1->position) == 
# 388 "./src/lib/src/Partie5.c" 3 4
                                                                                           0
# 388 "./src/lib/src/Partie5.c"
                                                                                                ))
                        {
                            ajouterMaillon(ptrTetePath2,creeMaillon(down2));
                        }
                    }
                    else if(compareCouleur(cRight2,palette->pal[0]) == 
# 393 "./src/lib/src/Partie5.c" 3 4
                                                                      0 
# 393 "./src/lib/src/Partie5.c"
                                                                            && comparePoints(right2,previousPosition2) == 
# 393 "./src/lib/src/Partie5.c" 3 4
                                                                                                                          0
# 393 "./src/lib/src/Partie5.c"
                                                                                                                               )
                    {
                        if (current1 == 
# 395 "./src/lib/src/Partie5.c" 3 4
                                       ((void *)0) 
# 395 "./src/lib/src/Partie5.c"
                                            || (comparePoints(right2,current1->position) == 
# 395 "./src/lib/src/Partie5.c" 3 4
                                                                                            0
# 395 "./src/lib/src/Partie5.c"
                                                                                                 ))
                        {
                            ajouterMaillon(ptrTetePath2,creeMaillon(right2));
                        }
                    }
                    else if(compareCouleur(cUp2,palette->pal[0]) == 
# 400 "./src/lib/src/Partie5.c" 3 4
                                                                   0 
# 400 "./src/lib/src/Partie5.c"
                                                                         && comparePoints(up2,previousPosition2) == 
# 400 "./src/lib/src/Partie5.c" 3 4
                                                                                                                    0
# 400 "./src/lib/src/Partie5.c"
                                                                                                                         )
                    {
                        if (current1 == 
# 402 "./src/lib/src/Partie5.c" 3 4
                                       ((void *)0) 
# 402 "./src/lib/src/Partie5.c"
                                            || (comparePoints(up2,current1->position) == 
# 402 "./src/lib/src/Partie5.c" 3 4
                                                                                         0
# 402 "./src/lib/src/Partie5.c"
                                                                                              ))
                        {
                            ajouterMaillon(ptrTetePath2,creeMaillon(up2));
                        }
                    }

                    if(current2->next == 
# 408 "./src/lib/src/Partie5.c" 3 4
                                        ((void *)0)
# 408 "./src/lib/src/Partie5.c"
                                            )
                    {
                        while(ptrTetePath2){
                            MaillonTraj *tmp = ptrTetePath2->next;
                            libereMaillon(&ptrTetePath2);
                            ptrTetePath2=tmp;
                        }
                        current2 = 
# 415 "./src/lib/src/Partie5.c" 3 4
                                  ((void *)0)
# 415 "./src/lib/src/Partie5.c"
                                      ;
                        previousPosition2 = (Point){0,0};
                    }
                    else
                    {
                        previousPosition2 = (Point){current2->position.x,current2->position.y};
                        current2 = current2->next;
                    }
                }

                if(ptrTetePath3)
                {
                    if(compareCouleur(cLeft3,palette->pal[0]) == 
# 427 "./src/lib/src/Partie5.c" 3 4
                                                                0 
# 427 "./src/lib/src/Partie5.c"
                                                                      && comparePoints(left3,previousPosition3) == 
# 427 "./src/lib/src/Partie5.c" 3 4
                                                                                                                   0
# 427 "./src/lib/src/Partie5.c"
                                                                                                                        )
                    {
                        if (current1 == 
# 429 "./src/lib/src/Partie5.c" 3 4
                                       ((void *)0) 
# 429 "./src/lib/src/Partie5.c"
                                            || (comparePoints(left3,current1->position) == 
# 429 "./src/lib/src/Partie5.c" 3 4
                                                                                           0
# 429 "./src/lib/src/Partie5.c"
                                                                                                ))
                        {
                            if (current2 == 
# 431 "./src/lib/src/Partie5.c" 3 4
                                           ((void *)0) 
# 431 "./src/lib/src/Partie5.c"
                                                || (comparePoints(left3,current2->position) == 
# 431 "./src/lib/src/Partie5.c" 3 4
                                                                                               0
# 431 "./src/lib/src/Partie5.c"
                                                                                                    ))
                            {
                                ajouterMaillon(ptrTetePath3,creeMaillon(left3));
                            }
                        }


                    }
                    else if(compareCouleur(cDown3,palette->pal[0]) == 
# 439 "./src/lib/src/Partie5.c" 3 4
                                                                     0 
# 439 "./src/lib/src/Partie5.c"
                                                                           && comparePoints(down3,previousPosition3) == 
# 439 "./src/lib/src/Partie5.c" 3 4
                                                                                                                        0
# 439 "./src/lib/src/Partie5.c"
                                                                                                                             )
                    {
                        if (current1 == 
# 441 "./src/lib/src/Partie5.c" 3 4
                                       ((void *)0) 
# 441 "./src/lib/src/Partie5.c"
                                            || (comparePoints(down3,current1->position) == 
# 441 "./src/lib/src/Partie5.c" 3 4
                                                                                           0
# 441 "./src/lib/src/Partie5.c"
                                                                                                ))
                        {
                            if (current2 == 
# 443 "./src/lib/src/Partie5.c" 3 4
                                           ((void *)0) 
# 443 "./src/lib/src/Partie5.c"
                                                || (comparePoints(down3,current2->position) == 
# 443 "./src/lib/src/Partie5.c" 3 4
                                                                                               0
# 443 "./src/lib/src/Partie5.c"
                                                                                                    ))
                            {
                                ajouterMaillon(ptrTetePath3,creeMaillon(down3));
                            }
                        }
                    }
                    else if(compareCouleur(cRight3,palette->pal[0]) == 
# 449 "./src/lib/src/Partie5.c" 3 4
                                                                      0 
# 449 "./src/lib/src/Partie5.c"
                                                                            && comparePoints(right3,previousPosition3) == 
# 449 "./src/lib/src/Partie5.c" 3 4
                                                                                                                          0
# 449 "./src/lib/src/Partie5.c"
                                                                                                                               )
                    {
                        if (current1 == 
# 451 "./src/lib/src/Partie5.c" 3 4
                                       ((void *)0) 
# 451 "./src/lib/src/Partie5.c"
                                            || (comparePoints(right3,current1->position) == 
# 451 "./src/lib/src/Partie5.c" 3 4
                                                                                            0
# 451 "./src/lib/src/Partie5.c"
                                                                                                 ))
                        {
                            if (current2 == 
# 453 "./src/lib/src/Partie5.c" 3 4
                                           ((void *)0) 
# 453 "./src/lib/src/Partie5.c"
                                                || (comparePoints(right3,current2->position) == 
# 453 "./src/lib/src/Partie5.c" 3 4
                                                                                                0
# 453 "./src/lib/src/Partie5.c"
                                                                                                     ))
                            {
                                ajouterMaillon(ptrTetePath3,creeMaillon(right3));
                            }
                        }
                    }
                    else if(compareCouleur(cUp3,palette->pal[0]) == 
# 459 "./src/lib/src/Partie5.c" 3 4
                                                                   0 
# 459 "./src/lib/src/Partie5.c"
                                                                         && comparePoints(up3,previousPosition3) == 
# 459 "./src/lib/src/Partie5.c" 3 4
                                                                                                                    0
# 459 "./src/lib/src/Partie5.c"
                                                                                                                         )
                    {
                        if (current1 == 
# 461 "./src/lib/src/Partie5.c" 3 4
                                       ((void *)0) 
# 461 "./src/lib/src/Partie5.c"
                                            || (comparePoints(up3,current1->position) == 
# 461 "./src/lib/src/Partie5.c" 3 4
                                                                                         0
# 461 "./src/lib/src/Partie5.c"
                                                                                              ))
                        {
                            if (current2 == 
# 463 "./src/lib/src/Partie5.c" 3 4
                                           ((void *)0) 
# 463 "./src/lib/src/Partie5.c"
                                                || (comparePoints(up3,current2->position) == 
# 463 "./src/lib/src/Partie5.c" 3 4
                                                                                             0
# 463 "./src/lib/src/Partie5.c"
                                                                                                  ))
                            {
                                ajouterMaillon(ptrTetePath3,creeMaillon(up3));
                            }
                        }
                    }
                    if(current3->next == 
# 469 "./src/lib/src/Partie5.c" 3 4
                                        ((void *)0)
# 469 "./src/lib/src/Partie5.c"
                                            ){
                        while(ptrTetePath3){
                            MaillonTraj *tmp = ptrTetePath3->next;
                            libereMaillon(&ptrTetePath3);
                            ptrTetePath3=tmp;
                        }
                        current3 = 
# 475 "./src/lib/src/Partie5.c" 3 4
                                  ((void *)0)
# 475 "./src/lib/src/Partie5.c"
                                      ;
                        previousPosition3 = (Point){0,0};
                    }
                    else
                    {
                        previousPosition3 = (Point){current3->position.x,current3->position.y};
                        current3 = current3->next;
                    }

                }

                if(ptrTetePath4)
                {
                   if(compareCouleur(cLeft4,palette->pal[0]) == 
# 488 "./src/lib/src/Partie5.c" 3 4
                                                               0 
# 488 "./src/lib/src/Partie5.c"
                                                                     && comparePoints(left4,previousPosition4) == 
# 488 "./src/lib/src/Partie5.c" 3 4
                                                                                                                  0
# 488 "./src/lib/src/Partie5.c"
                                                                                                                       )
                    {
                        if (current1 == 
# 490 "./src/lib/src/Partie5.c" 3 4
                                       ((void *)0) 
# 490 "./src/lib/src/Partie5.c"
                                            || (comparePoints(left4,current1->position) == 
# 490 "./src/lib/src/Partie5.c" 3 4
                                                                                           0
# 490 "./src/lib/src/Partie5.c"
                                                                                                ))
                        {
                            if (current2 == 
# 492 "./src/lib/src/Partie5.c" 3 4
                                           ((void *)0) 
# 492 "./src/lib/src/Partie5.c"
                                                || (comparePoints(left4,current2->position) == 
# 492 "./src/lib/src/Partie5.c" 3 4
                                                                                               0
# 492 "./src/lib/src/Partie5.c"
                                                                                                    ))
                            {
                                if (current3 == 
# 494 "./src/lib/src/Partie5.c" 3 4
                                               ((void *)0) 
# 494 "./src/lib/src/Partie5.c"
                                                    || (comparePoints(left4,current3->position) == 
# 494 "./src/lib/src/Partie5.c" 3 4
                                                                                                   0
# 494 "./src/lib/src/Partie5.c"
                                                                                                        ))
                                {
                                    ajouterMaillon(ptrTetePath4,creeMaillon(left4));
                                }
                            }
                        }
                    }
                    else if(compareCouleur(cDown4,palette->pal[0]) == 
# 501 "./src/lib/src/Partie5.c" 3 4
                                                                     0 
# 501 "./src/lib/src/Partie5.c"
                                                                           && comparePoints(down4,previousPosition4) == 
# 501 "./src/lib/src/Partie5.c" 3 4
                                                                                                                        0
# 501 "./src/lib/src/Partie5.c"
                                                                                                                             )
                    {
                        if (current1 == 
# 503 "./src/lib/src/Partie5.c" 3 4
                                       ((void *)0) 
# 503 "./src/lib/src/Partie5.c"
                                            || (comparePoints(down4,current1->position) == 
# 503 "./src/lib/src/Partie5.c" 3 4
                                                                                           0
# 503 "./src/lib/src/Partie5.c"
                                                                                                ))
                        {
                            if (current2 == 
# 505 "./src/lib/src/Partie5.c" 3 4
                                           ((void *)0) 
# 505 "./src/lib/src/Partie5.c"
                                                || (comparePoints(down4,current2->position) == 
# 505 "./src/lib/src/Partie5.c" 3 4
                                                                                               0
# 505 "./src/lib/src/Partie5.c"
                                                                                                    ))
                            {
                                if (current3 == 
# 507 "./src/lib/src/Partie5.c" 3 4
                                               ((void *)0) 
# 507 "./src/lib/src/Partie5.c"
                                                    || (comparePoints(down4,current3->position) == 
# 507 "./src/lib/src/Partie5.c" 3 4
                                                                                                   0
# 507 "./src/lib/src/Partie5.c"
                                                                                                        ))
                                {
                                    ajouterMaillon(ptrTetePath4,creeMaillon(down4));
                                }
                            }
                        }
                    }
                    else if(compareCouleur(cRight4,palette->pal[0]) == 
# 514 "./src/lib/src/Partie5.c" 3 4
                                                                      0 
# 514 "./src/lib/src/Partie5.c"
                                                                            && comparePoints(right4,previousPosition4) == 
# 514 "./src/lib/src/Partie5.c" 3 4
                                                                                                                          0
# 514 "./src/lib/src/Partie5.c"
                                                                                                                               )
                    {
                        if (current1 == 
# 516 "./src/lib/src/Partie5.c" 3 4
                                       ((void *)0) 
# 516 "./src/lib/src/Partie5.c"
                                            || (comparePoints(right4,current1->position) == 
# 516 "./src/lib/src/Partie5.c" 3 4
                                                                                            0
# 516 "./src/lib/src/Partie5.c"
                                                                                                 ))
                        {
                            if (current2 == 
# 518 "./src/lib/src/Partie5.c" 3 4
                                           ((void *)0) 
# 518 "./src/lib/src/Partie5.c"
                                                || (comparePoints(right4,current2->position) == 
# 518 "./src/lib/src/Partie5.c" 3 4
                                                                                                0
# 518 "./src/lib/src/Partie5.c"
                                                                                                     ))
                            {
                                if (current3 == 
# 520 "./src/lib/src/Partie5.c" 3 4
                                               ((void *)0) 
# 520 "./src/lib/src/Partie5.c"
                                                    || (comparePoints(right4,current3->position) == 
# 520 "./src/lib/src/Partie5.c" 3 4
                                                                                                    0
# 520 "./src/lib/src/Partie5.c"
                                                                                                         ))
                                {
                                    ajouterMaillon(ptrTetePath4,creeMaillon(right4));
                                }
                            }
                        }
                    }
                    else if(compareCouleur(cUp4,palette->pal[0]) == 
# 527 "./src/lib/src/Partie5.c" 3 4
                                                                   0 
# 527 "./src/lib/src/Partie5.c"
                                                                         && comparePoints(up4,previousPosition4) == 
# 527 "./src/lib/src/Partie5.c" 3 4
                                                                                                                    0
# 527 "./src/lib/src/Partie5.c"
                                                                                                                         )
                    {
                        if (current1 == 
# 529 "./src/lib/src/Partie5.c" 3 4
                                       ((void *)0) 
# 529 "./src/lib/src/Partie5.c"
                                            || (comparePoints(up4,current1->position) == 
# 529 "./src/lib/src/Partie5.c" 3 4
                                                                                         0
# 529 "./src/lib/src/Partie5.c"
                                                                                              ))
                        {
                            if (current2 == 
# 531 "./src/lib/src/Partie5.c" 3 4
                                           ((void *)0) 
# 531 "./src/lib/src/Partie5.c"
                                                || (comparePoints(up4,current2->position) == 
# 531 "./src/lib/src/Partie5.c" 3 4
                                                                                             0
# 531 "./src/lib/src/Partie5.c"
                                                                                                  ))
                            {
                                if (current3 == 
# 533 "./src/lib/src/Partie5.c" 3 4
                                               ((void *)0) 
# 533 "./src/lib/src/Partie5.c"
                                                    || (comparePoints(up4,current3->position) == 
# 533 "./src/lib/src/Partie5.c" 3 4
                                                                                                 0
# 533 "./src/lib/src/Partie5.c"
                                                                                                      ))
                                {
                                    ajouterMaillon(ptrTetePath4,creeMaillon(up4));
                                }
                            }
                        }
                    }

                    if(current4->next == 
# 541 "./src/lib/src/Partie5.c" 3 4
                                        ((void *)0)
# 541 "./src/lib/src/Partie5.c"
                                            )
                    {
                        while(ptrTetePath4){
                            MaillonTraj *tmp = ptrTetePath4->next;
                            libereMaillon(&ptrTetePath4);
                            ptrTetePath4=tmp;
                        }
                        current4 = 
# 548 "./src/lib/src/Partie5.c" 3 4
                                  ((void *)0)
# 548 "./src/lib/src/Partie5.c"
                                      ;
                        previousPosition4 = (Point){0,0};
                    }
                    else
                    {
                        previousPosition4 = (Point){current4->position.x,current4->position.y};
                        current4 = current4->next;
                    }

                }

                if(current1 == 
# 559 "./src/lib/src/Partie5.c" 3 4
                              ((void *)0) 
# 559 "./src/lib/src/Partie5.c"
                                   && current2 == 
# 559 "./src/lib/src/Partie5.c" 3 4
                                                  ((void *)0) 
# 559 "./src/lib/src/Partie5.c"
                                                       && current3 == 
# 559 "./src/lib/src/Partie5.c" 3 4
                                                                      ((void *)0) 
# 559 "./src/lib/src/Partie5.c"
                                                                           && current4 == 
# 559 "./src/lib/src/Partie5.c" 3 4
                                                                                          ((void *)0)
# 559 "./src/lib/src/Partie5.c"
                                                                                              ) return 
# 559 "./src/lib/src/Partie5.c" 3 4
                                                                                                       ((void *)0)
# 559 "./src/lib/src/Partie5.c"
                                                                                                           ;

                MaillonTraj *parcoursMainSegment = current->next;
                while (parcoursMainSegment)
                {

                    if(current1 && comparePoints(current1->position,secondSeg->position))
                    {

                        current->next = ptrTetePath1;
                        current1->next = secondSeg->next;
                        secondSeg->next =
# 570 "./src/lib/src/Partie5.c" 3 4
                                        ((void *)0)
# 570 "./src/lib/src/Partie5.c"
                                            ;
                        while(firstSeg){
                            MaillonTraj *tmp = firstSeg->next;
                            libereMaillon(&firstSeg);
                            firstSeg=tmp;
                        }
                        current = current1->next;

                        while(ptrTetePath2){
                            MaillonTraj *tmp = ptrTetePath2->next;
                            libereMaillon(&ptrTetePath2);
                            ptrTetePath2=tmp;
                        }
                        while(ptrTetePath3){
                            MaillonTraj *tmp = ptrTetePath3->next;
                            libereMaillon(&ptrTetePath3);
                            ptrTetePath3=tmp;
                        }
                        while(ptrTetePath4){
                            MaillonTraj *tmp = ptrTetePath4->next;
                            libereMaillon(&ptrTetePath4);
                            ptrTetePath4=tmp;
                        }

                        goto reloop;
                    }
                    else if(current2 && comparePoints(current2->position,secondSeg->position))
                    {

                        current->next = ptrTetePath2;
                        current2->next = secondSeg->next;
                        secondSeg->next =
# 601 "./src/lib/src/Partie5.c" 3 4
                                        ((void *)0)
# 601 "./src/lib/src/Partie5.c"
                                            ;
                        while(firstSeg){
                            MaillonTraj *tmp = firstSeg->next;
                            libereMaillon(&firstSeg);
                            firstSeg=tmp;
                        }
                        current = current2->next;

                        while(ptrTetePath1){
                            MaillonTraj *tmp = ptrTetePath1->next;
                            libereMaillon(&ptrTetePath1);
                            ptrTetePath1=tmp;
                        }
                        while(ptrTetePath3){
                            MaillonTraj *tmp = ptrTetePath3->next;
                            libereMaillon(&ptrTetePath3);
                            ptrTetePath3=tmp;
                        }
                        while(ptrTetePath4){
                            MaillonTraj *tmp = ptrTetePath4->next;
                            libereMaillon(&ptrTetePath4);
                            ptrTetePath4=tmp;
                        }



                        goto reloop;
                    }
                    else if(current3 && comparePoints(current3->position,secondSeg->position))
                    {

                        current->next = ptrTetePath3;
                        current3->next = secondSeg->next;

                        secondSeg->next =
# 635 "./src/lib/src/Partie5.c" 3 4
                                        ((void *)0)
# 635 "./src/lib/src/Partie5.c"
                                            ;
                        while(firstSeg){
                            MaillonTraj *tmp = firstSeg->next;
                            libereMaillon(&firstSeg);
                            firstSeg=tmp;
                        }
                        current = current3->next;

                        while(ptrTetePath1){
                            MaillonTraj *tmp = ptrTetePath1->next;
                            libereMaillon(&ptrTetePath1);
                            ptrTetePath1=tmp;
                        }
                        while(ptrTetePath2){
                            MaillonTraj *tmp = ptrTetePath2->next;
                            libereMaillon(&ptrTetePath2);
                            ptrTetePath2=tmp;
                        }
                        while(ptrTetePath4){
                            MaillonTraj *tmp = ptrTetePath4->next;
                            libereMaillon(&ptrTetePath4);
                            ptrTetePath4=tmp;
                        }

                        goto reloop;
                    }
                    else if(current4 && comparePoints(current4->position,secondSeg->position))
                    {

                        current->next = ptrTetePath4;
                        current4->next = secondSeg->next;

                        secondSeg->next =
# 667 "./src/lib/src/Partie5.c" 3 4
                                        ((void *)0)
# 667 "./src/lib/src/Partie5.c"
                                            ;
                        while(firstSeg){
                            MaillonTraj *tmp = firstSeg->next;
                            libereMaillon(&firstSeg);
                            firstSeg=tmp;
                        }
                        current = current4->next;

                        while(ptrTetePath1){
                            MaillonTraj *tmp = ptrTetePath1->next;
                            libereMaillon(&ptrTetePath1);
                            ptrTetePath1=tmp;
                        }
                        while(ptrTetePath2){
                            MaillonTraj *tmp = ptrTetePath2->next;
                            libereMaillon(&ptrTetePath2);
                            ptrTetePath2=tmp;
                        }
                        while(ptrTetePath3){
                            MaillonTraj *tmp = ptrTetePath3->next;
                            libereMaillon(&ptrTetePath3);
                            ptrTetePath3=tmp;
                        }

                        goto reloop;
                    }
                    parcoursMainSegment=parcoursMainSegment->next;
                }
            }
        }
        current=current->next;
    }



    MaillonTraj *optiTraj = creeMaillon(robot->ptrTete->position);
    MaillonTraj *maillonPrecedent = optiTraj;
    current = robot->ptrTete;
    while (current)
    {

        MaillonTraj *currentBis = current->next;
        if(currentBis && currentBis->next)
        {
            MaillonTraj *lastAvailable = currentBis->next;
            while(currentBis->next)
            {
                if(compareCouleur(palette->pal[0],estCouleurUniformeDansCarre(imgSansTrajectoires,current->position.x,current->position.y,currentBis->next->position.x,currentBis->next->position.y)))
                {
                    lastAvailable = currentBis;
                }
                currentBis=currentBis->next;
            }
            if(lastAvailable || lastAvailable && comparePoints(arrivee,lastAvailable->position))
            {
                depart = current->position;
                arrivee = lastAvailable->position;

                if(depart.x == arrivee.x)
                {
                    Point positionCourante = depart;
                    while (!comparePoints(positionCourante,arrivee))
                    {
                        if(positionCourante.x < arrivee.x){
                            positionCourante = (Point){positionCourante.x+1,positionCourante.y};
                            ajouterMaillon(optiTraj,creeMaillon(positionCourante));

                        }
                        else if( positionCourante.x > arrivee.x){
                            positionCourante = (Point){positionCourante.x-1,positionCourante.y};
                            ajouterMaillon(optiTraj,creeMaillon(positionCourante));

                        }
                        else if(positionCourante.y < arrivee.y){
                            positionCourante = (Point){positionCourante.x,positionCourante.y+1};
                            ajouterMaillon(optiTraj,creeMaillon(positionCourante));

                        }
                        else if(positionCourante.y > arrivee.y){
                            positionCourante = (Point){positionCourante.x,positionCourante.y-1};
                            ajouterMaillon(optiTraj,creeMaillon(positionCourante));
                        }
                        else break;
                    }

                }
                else
                {
                    float a = ((float) arrivee.y-(float) depart.y)/((float) arrivee.x-(float) depart.x);
                    float b = (float) depart.y - (float) a* (float) depart.x;
                    for (int x=depart.x;(depart.x>arrivee.x)?x>=arrivee.x:x<=arrivee.x;(depart.x>arrivee.x)?x--:x++)
                    {
                        int y = a*x+b;
                        MaillonTraj *nouveauMaillon = creeMaillon((Point){x,y});

                        Point positionPrecedente = (Point){maillonPrecedent->position.x,maillonPrecedent->position.y};
                        Point positionCourante = (Point){x,y};


                        if(positionVoisinage(positionPrecedente, positionCourante) != 8){

                            while( positionVoisinage(positionPrecedente, positionCourante) != 1 &&
                                    positionVoisinage(positionPrecedente, positionCourante) != 3 &&
                                    positionVoisinage(positionPrecedente, positionCourante) != 5 &&
                                    positionVoisinage(positionPrecedente, positionCourante) != 7){

                                if(positionPrecedente.x < positionCourante.x){
                                    positionPrecedente = (Point){positionPrecedente.x+1,positionPrecedente.y};
                                    ajouterMaillon(optiTraj,creeMaillon(positionPrecedente));

                                }
                                else if( positionPrecedente.x > positionCourante.x){
                                    positionPrecedente = (Point){positionPrecedente.x-1,positionPrecedente.y};
                                    ajouterMaillon(optiTraj,creeMaillon(positionPrecedente));

                                }
                                else if(positionPrecedente.y < positionCourante.y){
                                    positionPrecedente = (Point){positionPrecedente.x,positionPrecedente.y+1};
                                    ajouterMaillon(optiTraj,creeMaillon(positionPrecedente));

                                }
                                else if( positionPrecedente.y > positionCourante.y){
                                    positionPrecedente = (Point){positionPrecedente.x,positionPrecedente.y-1};
                                    ajouterMaillon(optiTraj,creeMaillon(positionPrecedente));
                                }
                                else break;
                            }
                        }

                        ajouterMaillon(optiTraj,nouveauMaillon);
                        maillonPrecedent = nouveauMaillon;

                    }
                }
                for(;maillonPrecedent->next;maillonPrecedent=maillonPrecedent->next);
                current = lastAvailable;
            }
        }
        current=current->next;
    }
    while (robot->ptrTete)
    {
        MaillonTraj *tmp = robot->ptrTete->next;
        libereMaillon(&robot->ptrTete);
        robot->ptrTete = tmp;
    }

    robot->ptrTete = optiTraj;
    return robot;
}

void dessineMotif0(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 818 "./src/lib/src/Partie5.c" 3 4
                                                                                                                    _Bool 
# 818 "./src/lib/src/Partie5.c"
                                                                                                                         trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);

    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);

    }
}

void dessineMotif1(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 842 "./src/lib/src/Partie5.c" 3 4
                                                                                                                    _Bool 
# 842 "./src/lib/src/Partie5.c"
                                                                                                                         trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y+carreSO.largeurMotif+3;
            for (int x = carreSO.origine.x; x < carreSO.origine.x +2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y+carreSO.largeurMotif-4;
            for (int x = carreSO.origine.x; x < carreSO.origine.x+ 2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }


    }
}

void dessineMotif2(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 888 "./src/lib/src/Partie5.c" 3 4
                                                                                                                    _Bool 
# 888 "./src/lib/src/Partie5.c"
                                                                                                                         trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif-4;
            for (int y = carreSO.origine.y; y < carreSO.origine.y+ 2*carreSO.largeurMotif; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif+3;
            for (int y = carreSO.origine.y; y < carreSO.origine.y+ 2*carreSO.largeurMotif; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
}

void dessineMotif3(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 932 "./src/lib/src/Partie5.c" 3 4
                                                                                                                    _Bool 
# 932 "./src/lib/src/Partie5.c"
                                                                                                                         trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif-4;
            for (int y = carreSO.origine.y; y < carreSO.origine.y+ carreSO.largeurMotif+3; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int y = carreSO.origine.y+carreSO.largeurMotif+3;
            for (x = carreSO.origine.x+carreSO.largeurMotif-4; x < carreSO.origine.x+ 2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif+3;
            for (int y = carreSO.origine.y; y < carreSO.origine.y+ carreSO.largeurMotif-4; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int y = carreSO.origine.y+carreSO.largeurMotif-4;
            for (x = carreSO.origine.x+carreSO.largeurMotif+3; x < carreSO.origine.x+ 2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
}

void dessineMotif4(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 994 "./src/lib/src/Partie5.c" 3 4
                                                                                                                    _Bool 
# 994 "./src/lib/src/Partie5.c"
                                                                                                                         trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif-4;
            for (int y = carreSO.origine.y+2*carreSO.largeurMotif; y > carreSO.origine.y + carreSO.largeurMotif-4; y--)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int y = carreSO.origine.y + carreSO.largeurMotif-4;
            for (x = carreSO.origine.x+carreSO.largeurMotif-4; x < carreSO.origine.x+ 2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif+3;
            for (int y = carreSO.origine.y+2*carreSO.largeurMotif; y > carreSO.origine.y+carreSO.largeurMotif+3; y--)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int y = carreSO.origine.y+carreSO.largeurMotif+3;
            for (x = carreSO.origine.x+carreSO.largeurMotif+3; x < carreSO.origine.x+ 2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
}

void dessineMotif5(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 1056 "./src/lib/src/Partie5.c" 3 4
                                                                                                                    _Bool 
# 1056 "./src/lib/src/Partie5.c"
                                                                                                                         trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y + carreSO.largeurMotif-4;
            for (int x = carreSO.origine.x; x < carreSO.origine.x+carreSO.largeurMotif+3; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int x = carreSO.origine.x+carreSO.largeurMotif+3;
            for (y = carreSO.origine.y + carreSO.largeurMotif-4; y < carreSO.origine.y + 2*carreSO.largeurMotif; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

        }
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y+carreSO.largeurMotif+3;
            for (int x = carreSO.origine.x; x < carreSO.origine.x+carreSO.largeurMotif-4; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int x = carreSO.origine.x+carreSO.largeurMotif-4;
            for (int y = carreSO.origine.y+carreSO.largeurMotif+3; y < carreSO.origine.y+ 2*carreSO.largeurMotif; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
}

void dessineMotif6(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const 
# 1119 "./src/lib/src/Partie5.c" 3 4
                                                                                                                    _Bool 
# 1119 "./src/lib/src/Partie5.c"
                                                                                                                         trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y+carreSO.largeurMotif+3;
            for (int x = carreSO.origine.x; x < carreSO.origine.x+carreSO.largeurMotif+3; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int x = carreSO.origine.x+carreSO.largeurMotif+3;
            for (int y = carreSO.origine.y+carreSO.largeurMotif+3; y >= carreSO.origine.y; y--)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

        }

    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y+carreSO.largeurMotif-4;
            for (int x = carreSO.origine.x; x < carreSO.origine.x+carreSO.largeurMotif-4; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int x = carreSO.origine.x+carreSO.largeurMotif-4;
            for (int y = carreSO.origine.y+carreSO.largeurMotif-4; y >= carreSO.origine.y; y--)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
}

void dessineMotifNoeud(const Image *img,const Palette palette, const noeudMotif *noeud, const int formeId, const 
# 1183 "./src/lib/src/Partie5.c" 3 4
                                                                                                                _Bool 
# 1183 "./src/lib/src/Partie5.c"
                                                                                                                     trajectoire){
    Emplacement emplacement = (Emplacement) {(Point){noeud->x,noeud->y},32};
    switch (formeId)
    {
    case 0:
        dessineMotif0(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 1:
        dessineMotif1(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 2:
        dessineMotif2(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 3:
        dessineMotif3(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 4:
        dessineMotif4(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 5:
        dessineMotif5(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 6:
        dessineMotif6(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    default:
        break;
    }
}

void dessineCarte(const Image *img, const Palette palette, const noeudMotif *racine, const 
# 1213 "./src/lib/src/Partie5.c" 3 4
                                                                                          _Bool 
# 1213 "./src/lib/src/Partie5.c"
                                                                                               trajectoire){
    if(racine == 
# 1214 "./src/lib/src/Partie5.c" 3 4
                ((void *)0)
# 1214 "./src/lib/src/Partie5.c"
                    )
        return;
    dessineMotifNoeud(img,palette,racine,racine->motif.forme,trajectoire);
    dessineCarte(img,palette,racine->NO,trajectoire);
    dessineCarte(img,palette,racine->NE,trajectoire);
    dessineCarte(img,palette,racine->SO,trajectoire);
    dessineCarte(img,palette,racine->SE,trajectoire);
}

void sauveDescriptionChemin(Robot *robot,char* path){

    FILE * fp;


    fp = fopen (path,"w");
    int i = 0;
    MaillonTraj *current = robot->ptrTete;
    while (current)
    {
        i++;
        current=current->next;
    }


    fprintf (fp, "%d mouvements\n",i);
    fprintf (fp, "Depart: x=%d y=%d\n",robot->depart.x,robot->depart.y);
    fprintf (fp, "Arrivee: x=%d y=%d",robot->arrivee.x,robot->arrivee.y);


    current = robot->ptrTete;
    char mouvementCode;
    while (current->next)
    {
        switch (positionVoisinage(current->next->position,current->position))
        {
        case 1:
            mouvementCode = '1';
            break;

        case 3:
            mouvementCode = '3';
            break;
        case 5:
            mouvementCode = '2';
            break;
        case 7:
            mouvementCode = '4';
            break;
        case 8:
            current=current->next;
            continue;
        default:
            mouvementCode = '?';
            break;
        }
        fprintf (fp, "\n%c",mouvementCode);
        current=current->next;
    }


    fclose (fp);
}
