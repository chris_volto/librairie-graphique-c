# 1 "./src/lib_graphic/src/Image.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "./src/lib_graphic/src/Image.c"
# 1 "./src/lib_graphic/src/../include/Image.h" 1




    typedef struct Image {
        int largeur, hauteur;

        short int **rouge, **vert, **bleu, **gris;
    } Image;
# 18 "./src/lib_graphic/src/../include/Image.h"
    Image *alloueImage(int largeur, int hauteur);






    void libereImage(Image **ptrImage);







    Image *chargeImage(char *nom);






    void sauveImage(Image *monImage, char *nom);






    void sauveImageNG(Image *monImage, char *nom);






    Image *dupliqueImage(Image *monImage);







    Image *differenceImage(Image *image1, Image *image2);
# 2 "./src/lib_graphic/src/Image.c" 2
# 1 "./src/lib_graphic/src/../include/BmpLib.h" 1





# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdbool.h" 1 3 4
# 7 "./src/lib_graphic/src/../include/BmpLib.h" 2





typedef struct
{
 int largeurImage;
 int hauteurImage;
 unsigned char *donneesRGB;
} DonneesImageRGB;


void libereDonneesImageRGB(DonneesImageRGB **structure);



DonneesImageRGB *lisBMPRGB(char *nom);




# 28 "./src/lib_graphic/src/../include/BmpLib.h" 3 4
_Bool 
# 28 "./src/lib_graphic/src/../include/BmpLib.h"
    ecrisBMPRGB_Dans(DonneesImageRGB *donneesImage, char *nom);
# 3 "./src/lib_graphic/src/Image.c" 2
# 1 "c:\\mingw\\include\\stdlib.h" 1 3
# 34 "c:\\mingw\\include\\stdlib.h" 3
       
# 35 "c:\\mingw\\include\\stdlib.h" 3
# 49 "c:\\mingw\\include\\stdlib.h" 3
# 1 "c:\\mingw\\include\\_mingw.h" 1 3
# 55 "c:\\mingw\\include\\_mingw.h" 3
       
# 56 "c:\\mingw\\include\\_mingw.h" 3
# 66 "c:\\mingw\\include\\_mingw.h" 3
# 1 "c:\\mingw\\include\\msvcrtver.h" 1 3
# 35 "c:\\mingw\\include\\msvcrtver.h" 3
       
# 36 "c:\\mingw\\include\\msvcrtver.h" 3
# 67 "c:\\mingw\\include\\_mingw.h" 2 3






# 1 "c:\\mingw\\include\\w32api.h" 1 3
# 35 "c:\\mingw\\include\\w32api.h" 3
       
# 36 "c:\\mingw\\include\\w32api.h" 3
# 59 "c:\\mingw\\include\\w32api.h" 3
# 1 "c:\\mingw\\include\\sdkddkver.h" 1 3
# 35 "c:\\mingw\\include\\sdkddkver.h" 3
       
# 36 "c:\\mingw\\include\\sdkddkver.h" 3
# 60 "c:\\mingw\\include\\w32api.h" 2 3
# 74 "c:\\mingw\\include\\_mingw.h" 2 3
# 174 "c:\\mingw\\include\\_mingw.h" 3
# 1 "c:\\mingw\\include\\features.h" 1 3
# 39 "c:\\mingw\\include\\features.h" 3
       
# 40 "c:\\mingw\\include\\features.h" 3
# 175 "c:\\mingw\\include\\_mingw.h" 2 3
# 50 "c:\\mingw\\include\\stdlib.h" 2 3





# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 216 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4

# 216 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4
typedef unsigned int size_t;
# 328 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4
typedef short unsigned int wchar_t;
# 56 "c:\\mingw\\include\\stdlib.h" 2 3
# 90 "c:\\mingw\\include\\stdlib.h" 3

# 99 "c:\\mingw\\include\\stdlib.h" 3
extern int _argc;
extern char **_argv;




extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) int *__p___argc(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) char ***__p___argv(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t ***__p___wargv(void);
# 142 "c:\\mingw\\include\\stdlib.h" 3
   extern __attribute__((__dllimport__)) int __mb_cur_max;
# 166 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int *_errno(void);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int *__doserrno(void);







extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) char ***__p__environ(void);

extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t ***__p__wenviron(void);
# 202 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) int _sys_nerr;
# 227 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) char *_sys_errlist[];
# 238 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__osver(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__winver(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__winmajor(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__winminor(void);
# 250 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) unsigned int _osver;
extern __attribute__((__dllimport__)) unsigned int _winver;
extern __attribute__((__dllimport__)) unsigned int _winmajor;
extern __attribute__((__dllimport__)) unsigned int _winminor;
# 289 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char **__p__pgmptr(void);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t **__p__wpgmptr(void);
# 325 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) int _fmode;
# 335 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int atoi (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long atol (const char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double strtod (const char *, char **);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double atof (const char *);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double _wtof (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wtoi (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long _wtol (const wchar_t *);
# 378 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__))
float strtof (const char *__restrict__, char **__restrict__);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
long double strtold (const char *__restrict__, char **__restrict__);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long strtol (const char *, char **, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned long strtoul (const char *, char **, int);







 __attribute__((__cdecl__)) __attribute__((__nothrow__))
long wcstol (const wchar_t *, wchar_t **, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
unsigned long wcstoul (const wchar_t *, wchar_t **, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double wcstod (const wchar_t *, wchar_t **);





__attribute__((__cdecl__)) __attribute__((__nothrow__))
float wcstof (const wchar_t *__restrict__, wchar_t **__restrict__);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
long double wcstold (const wchar_t *__restrict__, wchar_t **__restrict__);
# 451 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_wgetenv (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wputenv (const wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _wsearchenv (const wchar_t *, const wchar_t *, wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wsystem (const wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _wmakepath (wchar_t *, const wchar_t *, const wchar_t *, const wchar_t *,
    const wchar_t *
  );

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _wsplitpath (const wchar_t *, wchar_t *, wchar_t *, wchar_t *, wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
wchar_t *_wfullpath (wchar_t *, const wchar_t *, size_t);





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t wcstombs (char *, const wchar_t *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wctomb (char *, wchar_t);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int mblen (const char *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t mbstowcs (wchar_t *, const char *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int mbtowc (wchar_t *, const char *, size_t);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int rand (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void srand (unsigned int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void abort (void) __attribute__((__noreturn__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void exit (int) __attribute__((__noreturn__));



int __attribute__((__cdecl__)) __attribute__((__nothrow__)) atexit (void (*)(void));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int system (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *getenv (const char *);






# 1 "c:\\mingw\\include\\alloca.h" 1 3
# 43 "c:\\mingw\\include\\alloca.h" 3
       
# 44 "c:\\mingw\\include\\alloca.h" 3
# 54 "c:\\mingw\\include\\alloca.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 55 "c:\\mingw\\include\\alloca.h" 2 3


# 80 "c:\\mingw\\include\\alloca.h" 3
void *alloca( size_t );







void *_alloca( size_t );



# 500 "c:\\mingw\\include\\stdlib.h" 2 3


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *calloc (size_t, size_t) __attribute__((__malloc__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *malloc (size_t) __attribute__((__malloc__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *realloc (void *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void free (void *);
# 514 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) void *__mingw_realloc (void *, size_t);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) void __mingw_free (void *);






 __attribute__((__cdecl__)) void *bsearch
(const void *, const void *, size_t, size_t, int (*)(const void *, const void *));

 __attribute__((__cdecl__)) void qsort
(void *, size_t, size_t, int (*)(const void *, const void *));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int abs (int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long labs (long) __attribute__((__const__));
# 538 "c:\\mingw\\include\\stdlib.h" 3
typedef struct { int quot, rem; } div_t;
typedef struct { long quot, rem; } ldiv_t;

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) div_t div (int, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) ldiv_t ldiv (long, long) __attribute__((__const__));






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _exit (int) __attribute__((__noreturn__));





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long long _atoi64 (const char *);
# 564 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _beep (unsigned int, unsigned int) __attribute__((__deprecated__));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _seterrormode (int) __attribute__((__deprecated__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _sleep (unsigned long) __attribute__((__deprecated__));



typedef int (* _onexit_t)(void);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) _onexit_t _onexit( _onexit_t );

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _putenv (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _searchenv (const char *, const char *, char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_ecvt (double, int, int *, int *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_fcvt (double, int, int *, int *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_gcvt (double, int, char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _makepath (char *, const char *, const char *, const char *, const char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _splitpath (const char *, char *, char *, char *, char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_fullpath (char*, const char*, size_t);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_itoa (int, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_ltoa (long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_ultoa(unsigned long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_itow (int, wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_ltow (long, wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_ultow (unsigned long, wchar_t *, int);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* _i64toa (long long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* _ui64toa (unsigned long long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long long _wtoi64 (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t* _i64tow (long long, wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t* _ui64tow (unsigned long long, wchar_t *, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int (_rotl)(unsigned int, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int (_rotr)(unsigned int, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned long (_lrotl)(unsigned long, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned long (_lrotr)(unsigned long, int) __attribute__((__const__));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _set_error_mode (int);
# 647 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putenv (const char*);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void searchenv (const char*, const char*, char*);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* itoa (int, char*, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* ltoa (long, char*, int);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* ecvt (double, int, int*, int*);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* fcvt (double, int, int*, int*);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* gcvt (double, int, char*);
# 668 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) void _Exit(int) __attribute__((__noreturn__));






typedef struct { long long quot, rem; } lldiv_t;
__attribute__((__cdecl__)) __attribute__((__nothrow__)) lldiv_t lldiv (long long, long long) __attribute__((__const__));

__attribute__((__cdecl__)) __attribute__((__nothrow__)) long long llabs (long long);
# 689 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__))
long long strtoll (const char *__restrict__, char **__restrict, int);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
unsigned long long strtoull (const char *__restrict__, char **__restrict__, int);





__attribute__((__cdecl__)) __attribute__((__nothrow__)) long long atoll (const char *);
# 745 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) long long wtoll (const wchar_t *);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) char *lltoa (long long, char *, int);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) char *ulltoa (unsigned long long , char *, int);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) wchar_t *lltow (long long, wchar_t *, int);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) wchar_t *ulltow (unsigned long long, wchar_t *, int);
# 785 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int mkstemp (char *);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int __mingw_mkstemp (int, char *);
# 827 "c:\\mingw\\include\\stdlib.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int mkstemp (char *__filename_template)
{ return __mingw_mkstemp( 0, __filename_template ); }
# 838 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) char *mkdtemp (char *);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) char *__mingw_mkdtemp (char *);

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) char *mkdtemp (char *__dirname_template)
{ return __mingw_mkdtemp( __dirname_template ); }






__attribute__((__cdecl__)) __attribute__((__nothrow__)) int setenv( const char *, const char *, int );
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int unsetenv( const char * );

__attribute__((__cdecl__)) __attribute__((__nothrow__)) int __mingw_setenv( const char *, const char *, int );

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int setenv( const char *__n, const char *__v, int __f )
{ return __mingw_setenv( __n, __v, __f ); }

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int unsetenv( const char *__name )
{ return __mingw_setenv( __name, ((void *)0), 1 ); }





# 4 "./src/lib_graphic/src/Image.c" 2


# 5 "./src/lib_graphic/src/Image.c"
Image *alloueImage(const int largeur,const int hauteur){

    Image* img = malloc(sizeof(Image));

    img->hauteur = hauteur;
    img->largeur = largeur;


    img->bleu = (short int**) malloc(sizeof(short int*)*largeur);
    img->rouge = (short int**) malloc(sizeof(short int*)*largeur);
    img->vert = (short int**) malloc(sizeof(short int*)*largeur);
    img->gris = (short int**) malloc(sizeof(short int*)*largeur);


    for(int x = 0; x < largeur; x++){
        img->bleu[x] = (short int*) malloc(sizeof(short int)*hauteur);
        img->rouge[x] = (short int*) malloc(sizeof(short int)*hauteur);
        img->vert[x] = (short int*) malloc(sizeof(short int)*hauteur);
        img->gris[x] = (short int*) malloc(sizeof(short int)*hauteur);
    }

    return img;
}

void libereImage(Image **ptrImage){

    for(int x = 0; x < (*ptrImage)->largeur; x++){
        free((*ptrImage)->bleu[x]);
        free((*ptrImage)->rouge[x]);
        free((*ptrImage)->vert[x]);
        free((*ptrImage)->gris[x]);
    }

    free((*ptrImage)->bleu);
    free((*ptrImage)->rouge);
    free((*ptrImage)->vert);
    free((*ptrImage)->gris);

    free(*ptrImage);
    *ptrImage = 
# 44 "./src/lib_graphic/src/Image.c" 3 4
               ((void *)0)
# 44 "./src/lib_graphic/src/Image.c"
                   ;

}

Image *chargeImage(char *path){

    if(path == 
# 50 "./src/lib_graphic/src/Image.c" 3 4
              ((void *)0)
# 50 "./src/lib_graphic/src/Image.c"
                  )
        return 
# 51 "./src/lib_graphic/src/Image.c" 3 4
              ((void *)0)
# 51 "./src/lib_graphic/src/Image.c"
                  ;

    DonneesImageRGB* img = lisBMPRGB(path);


    if(img == 
# 56 "./src/lib_graphic/src/Image.c" 3 4
             ((void *)0)
# 56 "./src/lib_graphic/src/Image.c"
                 )
        return 
# 57 "./src/lib_graphic/src/Image.c" 3 4
              ((void *)0)
# 57 "./src/lib_graphic/src/Image.c"
                  ;

    Image* result = alloueImage(img->largeurImage,img->hauteurImage);

    int i = 0;
    for(int y = 0; y < img->hauteurImage; y++){
        for(int x = 0; x < img->largeurImage; x++){

            result->rouge[x][y] = img->donneesRGB[i+2];
            result->vert[x][y] = img->donneesRGB[i+1];
            result->bleu[x][y] = img->donneesRGB[i];


            result->gris[x][y] = 0.2125*result->rouge[x][y] +0.7154*result->vert[x][y] + 0.0721*result->bleu[x][y];
            i += 3;
        }
    }

    free(img->donneesRGB);
    free(img);
    return result;
}

void sauveImage(Image *monImage, char *nom){

    DonneesImageRGB* img = (DonneesImageRGB*) malloc(sizeof(DonneesImageRGB));

    img->hauteurImage = monImage->hauteur;
    img->largeurImage = monImage->largeur;
    img->donneesRGB = (unsigned char *)malloc((unsigned int)monImage->largeur*(unsigned int)monImage->hauteur*3*sizeof(unsigned char));

    int i = 0;
    for(int y = 0; y < img->hauteurImage; y++){
        for(int x = 0; x < img->largeurImage; x++){
            img->donneesRGB[i] =monImage->bleu[x][y];
            img->donneesRGB[i+1] = monImage->vert[x][y];
            img->donneesRGB[i+2] =monImage->rouge[x][y];
            i +=3;
        }
    }

    ecrisBMPRGB_Dans(img,nom);
    free(img->donneesRGB);
    free(img);
}

void sauveImageNG(Image *monImage, char *nom){
    DonneesImageRGB* img = (DonneesImageRGB*) malloc(sizeof(DonneesImageRGB));

    img->hauteurImage = monImage->hauteur;
    img->largeurImage = monImage->largeur;
    img->donneesRGB = (unsigned char *)malloc((unsigned int)monImage->largeur*(unsigned int)monImage->hauteur*3);
    int i = 0;
    for(int y = 0; y < img->hauteurImage; y++){
        for(int x = 0; x < img->largeurImage; x++){
            img->donneesRGB[i] = 0.2125*monImage->rouge[x][y] +0.7154*monImage->vert[x][y] + 0.0721*monImage->bleu[x][y];
            img->donneesRGB[i+1] = 0.2125*monImage->rouge[x][y] +0.7154*monImage->vert[x][y] + 0.0721*monImage->bleu[x][y];
            img->donneesRGB[i+2] = 0.2125*monImage->rouge[x][y] +0.7154*monImage->vert[x][y] + 0.0721*monImage->bleu[x][y];
            i +=3;

        }
    }

    ecrisBMPRGB_Dans(img,nom);
    free(img);
}

Image *dupliqueImage(Image *monImage){

    Image* img = alloueImage(monImage->largeur,monImage->hauteur);

    for(int y = 0; y < monImage->hauteur; y++){
        for(int x = 0; x < monImage->largeur; x++){
            img->rouge[x][y] = monImage->rouge[x][y];
            img->bleu[x][y] = monImage->bleu[x][y];
            img->vert[x][y] = monImage->vert[x][y];
            img->gris[x][y] = monImage->gris[x][y];
        }
    }

    return img;
}

Image *differenceImage(Image *image1, Image *image2){

    Image *img = alloueImage(image1->largeur,image1->hauteur);

    for(int y = 0; y < image1->hauteur; y++){
        for(int x = 0; x < image1->largeur; x++){
            img->rouge[x][y] = image2->rouge[x][y] - image1->rouge[x][y];
            img->bleu[x][y] = image2->bleu[x][y] - image1->bleu[x][y];
            img->vert[x][y] = image2->vert[x][y] - image1->vert[x][y];
            img->gris[x][y] = image2->gris[x][y] - image1->gris[x][y];
        }
    }

    return img;
}
