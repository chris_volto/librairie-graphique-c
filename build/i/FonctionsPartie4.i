# 1 "./src/lib/src/FonctionsPartie4.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "./src/lib/src/FonctionsPartie4.c"
# 1 "./src/lib/src/../include/FonctionsPartie4.h" 1







# 1 "./src/lib/src/../include/Image.h" 1



    typedef struct Image {
        int largeur, hauteur;
        short int **rouge, **vert, **bleu, **gris;
    } Image;

    Image *alloueImage(int largeur, int hauteur);
    void libereImage(Image **ptrImage);
    Image *chargeImage(char *nom);
    void sauveImage(Image *monImage, char *nom);
    void sauveImageNG(Image *monImage, char *nom);
    Image *dupliqueImage(Image *monImage);
    Image *differenceImage(Image *image1, Image *image2);
# 9 "./src/lib/src/../include/FonctionsPartie4.h" 2
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdbool.h" 1 3 4
# 10 "./src/lib/src/../include/FonctionsPartie4.h" 2
# 1 "c:\\mingw\\include\\stdio.h" 1 3
# 38 "c:\\mingw\\include\\stdio.h" 3
       
# 39 "c:\\mingw\\include\\stdio.h" 3
# 55 "c:\\mingw\\include\\stdio.h" 3
# 1 "c:\\mingw\\include\\_mingw.h" 1 3
# 55 "c:\\mingw\\include\\_mingw.h" 3
       
# 56 "c:\\mingw\\include\\_mingw.h" 3
# 66 "c:\\mingw\\include\\_mingw.h" 3
# 1 "c:\\mingw\\include\\msvcrtver.h" 1 3
# 35 "c:\\mingw\\include\\msvcrtver.h" 3
       
# 36 "c:\\mingw\\include\\msvcrtver.h" 3
# 67 "c:\\mingw\\include\\_mingw.h" 2 3






# 1 "c:\\mingw\\include\\w32api.h" 1 3
# 35 "c:\\mingw\\include\\w32api.h" 3
       
# 36 "c:\\mingw\\include\\w32api.h" 3
# 59 "c:\\mingw\\include\\w32api.h" 3
# 1 "c:\\mingw\\include\\sdkddkver.h" 1 3
# 35 "c:\\mingw\\include\\sdkddkver.h" 3
       
# 36 "c:\\mingw\\include\\sdkddkver.h" 3
# 60 "c:\\mingw\\include\\w32api.h" 2 3
# 74 "c:\\mingw\\include\\_mingw.h" 2 3
# 174 "c:\\mingw\\include\\_mingw.h" 3
# 1 "c:\\mingw\\include\\features.h" 1 3
# 39 "c:\\mingw\\include\\features.h" 3
       
# 40 "c:\\mingw\\include\\features.h" 3
# 175 "c:\\mingw\\include\\_mingw.h" 2 3
# 56 "c:\\mingw\\include\\stdio.h" 2 3
# 68 "c:\\mingw\\include\\stdio.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 216 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4

# 216 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4
typedef unsigned int size_t;
# 328 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4
typedef short unsigned int wchar_t;
# 357 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4
typedef short unsigned int wint_t;
# 69 "c:\\mingw\\include\\stdio.h" 2 3
# 95 "c:\\mingw\\include\\stdio.h" 3
# 1 "c:\\mingw\\include\\sys/types.h" 1 3
# 34 "c:\\mingw\\include\\sys/types.h" 3
       
# 35 "c:\\mingw\\include\\sys/types.h" 3
# 62 "c:\\mingw\\include\\sys/types.h" 3
  typedef long __off32_t;




  typedef __off32_t _off_t;







  typedef _off_t off_t;
# 91 "c:\\mingw\\include\\sys/types.h" 3
  typedef long long __off64_t;






  typedef __off64_t off64_t;
# 115 "c:\\mingw\\include\\sys/types.h" 3
  typedef int _ssize_t;







  typedef _ssize_t ssize_t;
# 96 "c:\\mingw\\include\\stdio.h" 2 3






# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 1 3 4
# 40 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 3 4
typedef __builtin_va_list __gnuc_va_list;
# 103 "c:\\mingw\\include\\stdio.h" 2 3
# 210 "c:\\mingw\\include\\stdio.h" 3
typedef struct _iobuf
{
  char *_ptr;
  int _cnt;
  char *_base;
  int _flag;
  int _file;
  int _charbuf;
  int _bufsiz;
  char *_tmpfname;
} FILE;
# 239 "c:\\mingw\\include\\stdio.h" 3
extern __attribute__((__dllimport__)) FILE _iob[];
# 252 "c:\\mingw\\include\\stdio.h" 3








 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * fopen (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * freopen (const char *, const char *, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fflush (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fclose (FILE *);






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int remove (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int rename (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * tmpfile (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char * tmpnam (char *);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_tempnam (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _rmtmp (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _unlink (const char *);
# 289 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char * tempnam (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int rmtmp (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int unlink (const char *);



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int setvbuf (FILE *, char *, int, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void setbuf (FILE *, char *);
# 342 "c:\\mingw\\include\\stdio.h" 3
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,2,3))) __mingw_fprintf(FILE*, const char*, ...);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,1,2))) __mingw_printf(const char*, ...);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,2,3))) __mingw_sprintf(char*, const char*, ...);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,3,4))) __mingw_snprintf(char*, size_t, const char*, ...);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,2,0))) __mingw_vfprintf(FILE*, const char*, __builtin_va_list);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,1,0))) __mingw_vprintf(const char*, __builtin_va_list);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,2,0))) __mingw_vsprintf(char*, const char*, __builtin_va_list);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,3,0))) __mingw_vsnprintf(char*, size_t, const char*, __builtin_va_list);
# 376 "c:\\mingw\\include\\stdio.h" 3
extern unsigned int _mingw_output_format_control( unsigned int, unsigned int );
# 461 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fprintf (FILE *, const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int printf (const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int sprintf (char *, const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vfprintf (FILE *, const char *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vprintf (const char *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vsprintf (char *, const char *, __builtin_va_list);
# 479 "c:\\mingw\\include\\stdio.h" 3
static __inline__ __attribute__((__cdecl__)) __attribute__((__nothrow__))
int snprintf (char *__buf, size_t __len, const char *__format, ...)
{
  register int __retval;
  __builtin_va_list __local_argv; __builtin_va_start( __local_argv, __format );
  __retval = __mingw_vsnprintf( __buf, __len, __format, __local_argv );
  __builtin_va_end( __local_argv );
  return __retval;
}

static __inline__ __attribute__((__cdecl__)) __attribute__((__nothrow__))
int vsnprintf (char *__buf, size_t __len, const char *__format, __builtin_va_list __local_argv)
{
  return __mingw_vsnprintf( __buf, __len, __format, __local_argv );
}
# 513 "c:\\mingw\\include\\stdio.h" 3
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,2,3))) __msvcrt_fprintf(FILE *, const char *, ...);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,1,2))) __msvcrt_printf(const char *, ...);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,2,3))) __msvcrt_sprintf(char *, const char *, ...);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,2,0))) __msvcrt_vfprintf(FILE *, const char *, __builtin_va_list);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,1,0))) __msvcrt_vprintf(const char *, __builtin_va_list);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,2,0))) __msvcrt_vsprintf(char *, const char *, __builtin_va_list);






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _snprintf (char *, size_t, const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _vsnprintf (char *, size_t, const char *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _vscprintf (const char *, __builtin_va_list);
# 536 "c:\\mingw\\include\\stdio.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,3,4)))
int snprintf (char *, size_t, const char *, ...);

__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,3,0)))
int vsnprintf (char *, size_t, const char *, __builtin_va_list);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vscanf (const char * __restrict__, __builtin_va_list);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vfscanf (FILE * __restrict__, const char * __restrict__, __builtin_va_list);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vsscanf (const char * __restrict__, const char * __restrict__, __builtin_va_list);
# 679 "c:\\mingw\\include\\stdio.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) ssize_t
getdelim (char ** __restrict__, size_t * __restrict__, int, FILE * __restrict__);

__attribute__((__cdecl__)) __attribute__((__nothrow__)) ssize_t
getline (char ** __restrict__, size_t * __restrict__, FILE * __restrict__);
# 699 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fscanf (FILE *, const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int scanf (const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int sscanf (const char *, const char *, ...);



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fgetc (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char * fgets (char *, int, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fputc (int, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fputs (const char *, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char * gets (char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int puts (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int ungetc (int, FILE *);
# 720 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _filbuf (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _flsbuf (int, FILE *);



extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getc (FILE *);
extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getc (FILE * __F)
{
  return (--__F->_cnt >= 0)
    ? (int) (unsigned char) *__F->_ptr++
    : _filbuf (__F);
}

extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putc (int, FILE *);
extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putc (int __c, FILE * __F)
{
  return (--__F->_cnt >= 0)
    ? (int) (unsigned char) (*__F->_ptr++ = (char)__c)
    : _flsbuf (__c, __F);
}

extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getchar (void);
extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getchar (void)
{
  return (--(&_iob[0])->_cnt >= 0)
    ? (int) (unsigned char) *(&_iob[0])->_ptr++
    : _filbuf ((&_iob[0]));
}

extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putchar(int);
extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putchar(int __c)
{
  return (--(&_iob[1])->_cnt >= 0)
    ? (int) (unsigned char) (*(&_iob[1])->_ptr++ = (char)__c)
    : _flsbuf (__c, (&_iob[1]));}
# 767 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t fread (void *, size_t, size_t, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t fwrite (const void *, size_t, size_t, FILE *);



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fseek (FILE *, long, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long ftell (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void rewind (FILE *);
# 821 "c:\\mingw\\include\\stdio.h" 3
typedef union { long long __value; __off64_t __offset; } fpos_t;




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fgetpos (FILE *, fpos_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fsetpos (FILE *, const fpos_t *);
# 862 "c:\\mingw\\include\\stdio.h" 3
int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __mingw_fseeki64 (FILE *, long long, int);
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fseeki64 (FILE *__f, long long __o, int __w)
{ return __mingw_fseeki64 (__f, __o, __w); }


long long __attribute__((__cdecl__)) __attribute__((__nothrow__)) __mingw_ftelli64 (FILE *);
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__cdecl__)) long long __attribute__((__nothrow__)) _ftelli64 (FILE *__file )
{ return __mingw_ftelli64 (__file); }





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int feof (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int ferror (FILE *);
# 886 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void clearerr (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void perror (const char *);





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _popen (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _pclose (FILE *);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * popen (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int pclose (FILE *);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _flushall (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fgetchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fputchar (int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _fdopen (int, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fileno (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fcloseall (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _fsopen (const char *, const char *, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _getmaxstdio (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _setmaxstdio (int);
# 936 "c:\\mingw\\include\\stdio.h" 3
unsigned int __attribute__((__cdecl__)) __mingw_get_output_format (void);
unsigned int __attribute__((__cdecl__)) __mingw_set_output_format (unsigned int);







int __attribute__((__cdecl__)) __mingw_get_printf_count_output (void);
int __attribute__((__cdecl__)) __mingw_set_printf_count_output (int);
# 962 "c:\\mingw\\include\\stdio.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) unsigned int __attribute__((__cdecl__)) _get_output_format (void)
{ return __mingw_get_output_format (); }

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) unsigned int __attribute__((__cdecl__)) _set_output_format (unsigned int __style)
{ return __mingw_set_output_format (__style); }
# 987 "c:\\mingw\\include\\stdio.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) int __attribute__((__cdecl__)) _get_printf_count_output (void)
{ return 0 ? 1 : __mingw_get_printf_count_output (); }

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) int __attribute__((__cdecl__)) _set_printf_count_output (int __mode)
{ return 0 ? 1 : __mingw_set_printf_count_output (__mode); }



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fgetchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fputchar (int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * fdopen (int, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fileno (FILE *);
# 1007 "c:\\mingw\\include\\stdio.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) FILE * __attribute__((__cdecl__)) __attribute__((__nothrow__)) fopen64 (const char *, const char *);
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
FILE * __attribute__((__cdecl__)) __attribute__((__nothrow__)) fopen64 (const char * filename, const char * mode)
{ return fopen (filename, mode); }

int __attribute__((__cdecl__)) __attribute__((__nothrow__)) fseeko64 (FILE *, __off64_t, int);
# 1028 "c:\\mingw\\include\\stdio.h" 3
__off64_t __attribute__((__cdecl__)) __attribute__((__nothrow__)) ftello64 (FILE *);
# 1041 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fwprintf (FILE *, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wprintf (const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vfwprintf (FILE *, const wchar_t *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vwprintf (const wchar_t *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _snwprintf (wchar_t *, size_t, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _vscwprintf (const wchar_t *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _vsnwprintf (wchar_t *, size_t, const wchar_t *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fwscanf (FILE *, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wscanf (const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int swscanf (const wchar_t *, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t fgetwc (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t fputwc (wchar_t, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t ungetwc (wchar_t, FILE *);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int swprintf (wchar_t *, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vswprintf (wchar_t *, const wchar_t *, __builtin_va_list);



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t * fgetws (wchar_t *, int, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fputws (const wchar_t *, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t getwc (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t getwchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t putwc (wint_t, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t putwchar (wint_t);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t * _getws (wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _putws (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wfdopen(int, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wfopen (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wfreopen (const wchar_t *, const wchar_t *, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wfsopen (const wchar_t *, const wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t * _wtmpnam (wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t * _wtempnam (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wrename (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wremove (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _wperror (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wpopen (const wchar_t *, const wchar_t *);






__attribute__((__cdecl__)) __attribute__((__nothrow__)) int snwprintf (wchar_t *, size_t, const wchar_t *, ...);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int vsnwprintf (wchar_t *, size_t, const wchar_t *, __builtin_va_list);
# 1099 "c:\\mingw\\include\\stdio.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int vwscanf (const wchar_t *__restrict__, __builtin_va_list);
__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vfwscanf (FILE *__restrict__, const wchar_t *__restrict__, __builtin_va_list);
__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vswscanf (const wchar_t *__restrict__, const wchar_t * __restrict__, __builtin_va_list);






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * wpopen (const wchar_t *, const wchar_t *);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t _fgetwchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t _fputwchar (wint_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _getw (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _putw (int, FILE *);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t fgetwchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t fputwchar (wint_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getw (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putw (int, FILE *);





# 11 "./src/lib/src/../include/FonctionsPartie4.h" 2
# 20 "./src/lib/src/../include/FonctionsPartie4.h"
    
# 20 "./src/lib/src/../include/FonctionsPartie4.h"
   typedef struct Couleur{

        short int rouge, vert, bleu;
    }Couleur;






    typedef struct Palette {
        int nb;
        Couleur *pal;
    }Palette;





    typedef struct Motif{

        short int forme;
        short int config;

        int cfond;
        int cobjet;
    }Motif;







    typedef struct noeudMotif{

        struct noeudMotif *NO;
        struct noeudMotif *NE;
        struct noeudMotif *SO;
        struct noeudMotif *SE;

        Motif motif;
        int x,y;
        int largeur;

    }noeudMotif;







    Palette* creePalette(Image *img);
# 85 "./src/lib/src/../include/FonctionsPartie4.h"
    Motif identifieMotif(Image *img, Palette *palette, int x, int y, int largeur);
# 97 "./src/lib/src/../include/FonctionsPartie4.h"
    noeudMotif* creeArbreMotifs(Image *img, Palette *palette, int largeur, int x, int y);





        void fprintfArbre(FILE *fp, noeudMotif* ptrRcn);







    void sauveDescriptionImage(noeudMotif *rcn, Palette* pal);






    void affichePalette(Palette *pal);

    
# 120 "./src/lib/src/../include/FonctionsPartie4.h" 3 4
   _Bool 
# 120 "./src/lib/src/../include/FonctionsPartie4.h"
        compareCouleur (Couleur c1, Couleur c2);
    
# 121 "./src/lib/src/../include/FonctionsPartie4.h" 3 4
   _Bool 
# 121 "./src/lib/src/../include/FonctionsPartie4.h"
        compareCouleurVariadic (Couleur c1, Couleur c2, int nbreArgumentsOptionnels, ...);
    
# 122 "./src/lib/src/../include/FonctionsPartie4.h" 3 4
   _Bool 
# 122 "./src/lib/src/../include/FonctionsPartie4.h"
        sontDesCouleursVariadic(int nbreArguments, ...);
    
# 123 "./src/lib/src/../include/FonctionsPartie4.h" 3 4
   _Bool 
# 123 "./src/lib/src/../include/FonctionsPartie4.h"
        comparePalette(Palette p1, Palette p2);
    
# 124 "./src/lib/src/../include/FonctionsPartie4.h" 3 4
   _Bool 
# 124 "./src/lib/src/../include/FonctionsPartie4.h"
        compareMotif(Motif m1, Motif m2);
    
# 125 "./src/lib/src/../include/FonctionsPartie4.h" 3 4
   _Bool 
# 125 "./src/lib/src/../include/FonctionsPartie4.h"
        estCouleurDans(Couleur *arr, int arrSize, Couleur couleur);
    void libereArbreMotif(noeudMotif *ptrRcn);
    Couleur estCouleurUniformeDansCarre(Image *img, int x1, int y1, int x2, int y2);
    Couleur estCouleurUniforme(Image *img, int depart_x, int depart_y, int largeur, int hauteur);
# 2 "./src/lib/src/FonctionsPartie4.c" 2
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 1 3 4
# 99 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 3 4

# 99 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 3 4
typedef __gnuc_va_list va_list;
# 3 "./src/lib/src/FonctionsPartie4.c" 2
# 1 "c:\\mingw\\include\\stdlib.h" 1 3
# 34 "c:\\mingw\\include\\stdlib.h" 3
       
# 35 "c:\\mingw\\include\\stdlib.h" 3
# 55 "c:\\mingw\\include\\stdlib.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 56 "c:\\mingw\\include\\stdlib.h" 2 3
# 90 "c:\\mingw\\include\\stdlib.h" 3

# 99 "c:\\mingw\\include\\stdlib.h" 3
extern int _argc;
extern char **_argv;




extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) int *__p___argc(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) char ***__p___argv(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t ***__p___wargv(void);
# 142 "c:\\mingw\\include\\stdlib.h" 3
   extern __attribute__((__dllimport__)) int __mb_cur_max;
# 166 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int *_errno(void);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int *__doserrno(void);







extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) char ***__p__environ(void);

extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t ***__p__wenviron(void);
# 202 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) int _sys_nerr;
# 227 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) char *_sys_errlist[];
# 238 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__osver(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__winver(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__winmajor(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__winminor(void);
# 250 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) unsigned int _osver;
extern __attribute__((__dllimport__)) unsigned int _winver;
extern __attribute__((__dllimport__)) unsigned int _winmajor;
extern __attribute__((__dllimport__)) unsigned int _winminor;
# 289 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char **__p__pgmptr(void);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t **__p__wpgmptr(void);
# 325 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) int _fmode;
# 335 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int atoi (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long atol (const char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double strtod (const char *, char **);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double atof (const char *);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double _wtof (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wtoi (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long _wtol (const wchar_t *);
# 378 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__))
float strtof (const char *__restrict__, char **__restrict__);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
long double strtold (const char *__restrict__, char **__restrict__);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long strtol (const char *, char **, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned long strtoul (const char *, char **, int);







 __attribute__((__cdecl__)) __attribute__((__nothrow__))
long wcstol (const wchar_t *, wchar_t **, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
unsigned long wcstoul (const wchar_t *, wchar_t **, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double wcstod (const wchar_t *, wchar_t **);





__attribute__((__cdecl__)) __attribute__((__nothrow__))
float wcstof (const wchar_t *__restrict__, wchar_t **__restrict__);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
long double wcstold (const wchar_t *__restrict__, wchar_t **__restrict__);
# 451 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_wgetenv (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wputenv (const wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _wsearchenv (const wchar_t *, const wchar_t *, wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wsystem (const wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _wmakepath (wchar_t *, const wchar_t *, const wchar_t *, const wchar_t *,
    const wchar_t *
  );

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _wsplitpath (const wchar_t *, wchar_t *, wchar_t *, wchar_t *, wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
wchar_t *_wfullpath (wchar_t *, const wchar_t *, size_t);





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t wcstombs (char *, const wchar_t *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wctomb (char *, wchar_t);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int mblen (const char *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t mbstowcs (wchar_t *, const char *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int mbtowc (wchar_t *, const char *, size_t);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int rand (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void srand (unsigned int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void abort (void) __attribute__((__noreturn__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void exit (int) __attribute__((__noreturn__));



int __attribute__((__cdecl__)) __attribute__((__nothrow__)) atexit (void (*)(void));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int system (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *getenv (const char *);






# 1 "c:\\mingw\\include\\alloca.h" 1 3
# 43 "c:\\mingw\\include\\alloca.h" 3
       
# 44 "c:\\mingw\\include\\alloca.h" 3
# 54 "c:\\mingw\\include\\alloca.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 55 "c:\\mingw\\include\\alloca.h" 2 3


# 80 "c:\\mingw\\include\\alloca.h" 3
void *alloca( size_t );







void *_alloca( size_t );



# 500 "c:\\mingw\\include\\stdlib.h" 2 3


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *calloc (size_t, size_t) __attribute__((__malloc__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *malloc (size_t) __attribute__((__malloc__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *realloc (void *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void free (void *);
# 514 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) void *__mingw_realloc (void *, size_t);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) void __mingw_free (void *);






 __attribute__((__cdecl__)) void *bsearch
(const void *, const void *, size_t, size_t, int (*)(const void *, const void *));

 __attribute__((__cdecl__)) void qsort
(void *, size_t, size_t, int (*)(const void *, const void *));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int abs (int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long labs (long) __attribute__((__const__));
# 538 "c:\\mingw\\include\\stdlib.h" 3
typedef struct { int quot, rem; } div_t;
typedef struct { long quot, rem; } ldiv_t;

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) div_t div (int, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) ldiv_t ldiv (long, long) __attribute__((__const__));






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _exit (int) __attribute__((__noreturn__));





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long long _atoi64 (const char *);
# 564 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _beep (unsigned int, unsigned int) __attribute__((__deprecated__));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _seterrormode (int) __attribute__((__deprecated__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _sleep (unsigned long) __attribute__((__deprecated__));



typedef int (* _onexit_t)(void);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) _onexit_t _onexit( _onexit_t );

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _putenv (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _searchenv (const char *, const char *, char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_ecvt (double, int, int *, int *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_fcvt (double, int, int *, int *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_gcvt (double, int, char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _makepath (char *, const char *, const char *, const char *, const char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _splitpath (const char *, char *, char *, char *, char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_fullpath (char*, const char*, size_t);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_itoa (int, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_ltoa (long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_ultoa(unsigned long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_itow (int, wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_ltow (long, wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_ultow (unsigned long, wchar_t *, int);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* _i64toa (long long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* _ui64toa (unsigned long long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long long _wtoi64 (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t* _i64tow (long long, wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t* _ui64tow (unsigned long long, wchar_t *, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int (_rotl)(unsigned int, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int (_rotr)(unsigned int, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned long (_lrotl)(unsigned long, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned long (_lrotr)(unsigned long, int) __attribute__((__const__));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _set_error_mode (int);
# 647 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putenv (const char*);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void searchenv (const char*, const char*, char*);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* itoa (int, char*, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* ltoa (long, char*, int);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* ecvt (double, int, int*, int*);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* fcvt (double, int, int*, int*);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* gcvt (double, int, char*);
# 668 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) void _Exit(int) __attribute__((__noreturn__));






typedef struct { long long quot, rem; } lldiv_t;
__attribute__((__cdecl__)) __attribute__((__nothrow__)) lldiv_t lldiv (long long, long long) __attribute__((__const__));

__attribute__((__cdecl__)) __attribute__((__nothrow__)) long long llabs (long long);
# 689 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__))
long long strtoll (const char *__restrict__, char **__restrict, int);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
unsigned long long strtoull (const char *__restrict__, char **__restrict__, int);





__attribute__((__cdecl__)) __attribute__((__nothrow__)) long long atoll (const char *);
# 745 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) long long wtoll (const wchar_t *);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) char *lltoa (long long, char *, int);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) char *ulltoa (unsigned long long , char *, int);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) wchar_t *lltow (long long, wchar_t *, int);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) wchar_t *ulltow (unsigned long long, wchar_t *, int);
# 785 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int mkstemp (char *);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int __mingw_mkstemp (int, char *);
# 827 "c:\\mingw\\include\\stdlib.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int mkstemp (char *__filename_template)
{ return __mingw_mkstemp( 0, __filename_template ); }
# 838 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) char *mkdtemp (char *);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) char *__mingw_mkdtemp (char *);

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) char *mkdtemp (char *__dirname_template)
{ return __mingw_mkdtemp( __dirname_template ); }






__attribute__((__cdecl__)) __attribute__((__nothrow__)) int setenv( const char *, const char *, int );
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int unsetenv( const char * );

__attribute__((__cdecl__)) __attribute__((__nothrow__)) int __mingw_setenv( const char *, const char *, int );

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int setenv( const char *__n, const char *__v, int __f )
{ return __mingw_setenv( __n, __v, __f ); }

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int unsetenv( const char *__name )
{ return __mingw_setenv( __name, ((void *)0), 1 ); }





# 4 "./src/lib/src/FonctionsPartie4.c" 2

_Bool 
# 5 "./src/lib/src/FonctionsPartie4.c"
    compareCouleur (Couleur c1, Couleur c2)
{
    if(c1.rouge==c2.rouge&&c1.vert==c2.vert&&c1.bleu==c2.bleu) return 
# 7 "./src/lib/src/FonctionsPartie4.c" 3 4
                                                                     1
# 7 "./src/lib/src/FonctionsPartie4.c"
                                                                         ;
    return 
# 8 "./src/lib/src/FonctionsPartie4.c" 3 4
          0
# 8 "./src/lib/src/FonctionsPartie4.c"
               ;
}


# 11 "./src/lib/src/FonctionsPartie4.c" 3 4
_Bool 
# 11 "./src/lib/src/FonctionsPartie4.c"
    compareCouleurVariadic (Couleur c1, Couleur c2, int nbreArgumentsOptionnels, ...)
{
    if(c1.rouge!=c2.rouge||c1.vert!=c2.vert||c1.bleu!=c2.bleu) return 
# 13 "./src/lib/src/FonctionsPartie4.c" 3 4
                                                                     0
# 13 "./src/lib/src/FonctionsPartie4.c"
                                                                          ;

    va_list ap;
    
# 16 "./src/lib/src/FonctionsPartie4.c" 3 4
   __builtin_va_start(
# 16 "./src/lib/src/FonctionsPartie4.c"
   ap
# 16 "./src/lib/src/FonctionsPartie4.c" 3 4
   ,
# 16 "./src/lib/src/FonctionsPartie4.c"
   nbreArgumentsOptionnels
# 16 "./src/lib/src/FonctionsPartie4.c" 3 4
   )
# 16 "./src/lib/src/FonctionsPartie4.c"
                                         ;
    Couleur tmp;
    for (int i = 0; i < nbreArgumentsOptionnels; i++){
        tmp = 
# 19 "./src/lib/src/FonctionsPartie4.c" 3 4
             __builtin_va_arg(
# 19 "./src/lib/src/FonctionsPartie4.c"
             ap
# 19 "./src/lib/src/FonctionsPartie4.c" 3 4
             ,
# 19 "./src/lib/src/FonctionsPartie4.c"
             Couleur
# 19 "./src/lib/src/FonctionsPartie4.c" 3 4
             )
# 19 "./src/lib/src/FonctionsPartie4.c"
                                 ;
        if(c1.rouge!=tmp.rouge||c1.vert!=tmp.vert||c1.bleu!=tmp.bleu) return 
# 20 "./src/lib/src/FonctionsPartie4.c" 3 4
                                                                            0
# 20 "./src/lib/src/FonctionsPartie4.c"
                                                                                 ;
    }
    
# 22 "./src/lib/src/FonctionsPartie4.c" 3 4
   __builtin_va_end(
# 22 "./src/lib/src/FonctionsPartie4.c"
   ap
# 22 "./src/lib/src/FonctionsPartie4.c" 3 4
   )
# 22 "./src/lib/src/FonctionsPartie4.c"
              ;
    return 
# 23 "./src/lib/src/FonctionsPartie4.c" 3 4
          1
# 23 "./src/lib/src/FonctionsPartie4.c"
              ;
}


# 26 "./src/lib/src/FonctionsPartie4.c" 3 4
_Bool 
# 26 "./src/lib/src/FonctionsPartie4.c"
    sontDesCouleursVariadic(int nbreArguments, ...){
    va_list ap;
    
# 28 "./src/lib/src/FonctionsPartie4.c" 3 4
   __builtin_va_start(
# 28 "./src/lib/src/FonctionsPartie4.c"
   ap
# 28 "./src/lib/src/FonctionsPartie4.c" 3 4
   ,
# 28 "./src/lib/src/FonctionsPartie4.c"
   nbreArguments
# 28 "./src/lib/src/FonctionsPartie4.c" 3 4
   )
# 28 "./src/lib/src/FonctionsPartie4.c"
                               ;

    for (int i = 0; i < nbreArguments; i++)
        if(compareCouleur(
# 31 "./src/lib/src/FonctionsPartie4.c" 3 4
                         __builtin_va_arg(
# 31 "./src/lib/src/FonctionsPartie4.c"
                         ap
# 31 "./src/lib/src/FonctionsPartie4.c" 3 4
                         ,
# 31 "./src/lib/src/FonctionsPartie4.c"
                         Couleur
# 31 "./src/lib/src/FonctionsPartie4.c" 3 4
                         )
# 31 "./src/lib/src/FonctionsPartie4.c"
                                             , (Couleur) {.rouge = -1, .vert = -1, .bleu = -1}) == 
# 31 "./src/lib/src/FonctionsPartie4.c" 3 4
                                                                   1
# 31 "./src/lib/src/FonctionsPartie4.c"
                                                                       ) return 
# 31 "./src/lib/src/FonctionsPartie4.c" 3 4
                                                                                0
# 31 "./src/lib/src/FonctionsPartie4.c"
                                                                                     ;
    
# 32 "./src/lib/src/FonctionsPartie4.c" 3 4
   __builtin_va_end(
# 32 "./src/lib/src/FonctionsPartie4.c"
   ap
# 32 "./src/lib/src/FonctionsPartie4.c" 3 4
   )
# 32 "./src/lib/src/FonctionsPartie4.c"
              ;
    return 
# 33 "./src/lib/src/FonctionsPartie4.c" 3 4
          1
# 33 "./src/lib/src/FonctionsPartie4.c"
              ;
}


# 36 "./src/lib/src/FonctionsPartie4.c" 3 4
_Bool 
# 36 "./src/lib/src/FonctionsPartie4.c"
    comparePalette(Palette p1, Palette p2)
{
    if(p1.nb != p2.nb) return 
# 38 "./src/lib/src/FonctionsPartie4.c" 3 4
                             0
# 38 "./src/lib/src/FonctionsPartie4.c"
                                  ;
    for(int i = 0; i < p1.nb; i++) if(!compareCouleur(p1.pal[i],p2.pal[i])) return 
# 39 "./src/lib/src/FonctionsPartie4.c" 3 4
                                                                                  0
# 39 "./src/lib/src/FonctionsPartie4.c"
                                                                                       ;
    return 
# 40 "./src/lib/src/FonctionsPartie4.c" 3 4
          1
# 40 "./src/lib/src/FonctionsPartie4.c"
              ;
}


# 43 "./src/lib/src/FonctionsPartie4.c" 3 4
_Bool 
# 43 "./src/lib/src/FonctionsPartie4.c"
    compareMotif(Motif m1, Motif m2)
{
    return (m1.forme==m2.forme&&m1.config==m2.config&&m1.cfond==m2.cfond&&m1.cobjet==m2.cobjet)?
# 45 "./src/lib/src/FonctionsPartie4.c" 3 4
                                                                                               1
# 45 "./src/lib/src/FonctionsPartie4.c"
                                                                                                   :
# 45 "./src/lib/src/FonctionsPartie4.c" 3 4
                                                                                                    0
# 45 "./src/lib/src/FonctionsPartie4.c"
                                                                                                         ;
}


# 48 "./src/lib/src/FonctionsPartie4.c" 3 4
_Bool 
# 48 "./src/lib/src/FonctionsPartie4.c"
    estCouleurDans(Couleur *arr, int arrSize, Couleur couleur){

    if(arr == 
# 50 "./src/lib/src/FonctionsPartie4.c" 3 4
             ((void *)0)
# 50 "./src/lib/src/FonctionsPartie4.c"
                 )
        return 
# 51 "./src/lib/src/FonctionsPartie4.c" 3 4
              0
# 51 "./src/lib/src/FonctionsPartie4.c"
                   ;

    for (int i = 0; i < arrSize; i++)
    {
        if(compareCouleur(arr[i],couleur))
            return 
# 56 "./src/lib/src/FonctionsPartie4.c" 3 4
                  1
# 56 "./src/lib/src/FonctionsPartie4.c"
                      ;
    }

    return 
# 59 "./src/lib/src/FonctionsPartie4.c" 3 4
          0
# 59 "./src/lib/src/FonctionsPartie4.c"
               ;
}

Palette* creePalette(Image *img){
    Palette* ret = (Palette*) malloc(sizeof(Palette));

    ret->nb = 0;
    ret->pal = 
# 66 "./src/lib/src/FonctionsPartie4.c" 3 4
              ((void *)0)
# 66 "./src/lib/src/FonctionsPartie4.c"
                  ;

    for (int x = 0; x < img->largeur; x++)
    {
        for (int y = 0; y < img->hauteur; y++)
        {
            if(!estCouleurDans(ret->pal, ret->nb, (Couleur) { .rouge = img->rouge[x][y], .vert = img->vert[x][y], .bleu = img->bleu[x][y]})){

                Couleur tmp[ret->nb];
                for(int i = 0; i < ret->nb; i++)
                    tmp[i] = ret->pal[i];

                ret->nb++;
                ret->pal = (Couleur*) realloc(ret->pal, ret->nb * sizeof(Couleur));

                for(int i = 0; i < ret->nb-1; i++)
                    ret->pal[i] = tmp[i];

                ret->pal[ret->nb-1] = (Couleur) { .rouge = img->rouge[x][y], .vert = img->vert[x][y], .bleu = img->bleu[x][y]};
            }
        }
    }

    return ret;
}

void affichePalette(Palette *pal){
    if(pal == 
# 93 "./src/lib/src/FonctionsPartie4.c" 3 4
             ((void *)0)
# 93 "./src/lib/src/FonctionsPartie4.c"
                 )
        return;

    printf("Nombre de couleurs: %d\n", pal->nb);


    for (int i = 0; i < pal->nb; i++)
    {
        printf("R:%d - V:%d - B:%d\n", pal->pal[i].rouge, pal->pal[i].vert, pal->pal[i].bleu);
    }

}
Couleur estCouleurUniformeDansCarre(Image *img, int x1, int y1, int x2, int y2){
    Couleur couleur = (Couleur) {.rouge = img->rouge[x1][y1], .vert = img->vert[x1][y1], .bleu = img->bleu[x1][y1]};

    for(int y = y1; (y1>y2)?y+3>=y2:y-3<=y2; (y1>y2)?y--:y++){
        for(int x = x1; (x1>x2)?x+3>=x2:x-3<=x2; (x1>x2)?x--:x++){
            Couleur couleurCourante = (Couleur) {.rouge = img->rouge[x][y], .vert = img->vert[x][y], .bleu = img->bleu[x][y]};
            if(compareCouleur(couleur,couleurCourante) == 
# 111 "./src/lib/src/FonctionsPartie4.c" 3 4
                                                         0
# 111 "./src/lib/src/FonctionsPartie4.c"
                                                              )
                return (Couleur) {.rouge = -1, .vert = -1, .bleu = -1};
        }
    }

    return couleur;
}

Couleur estCouleurUniforme(Image *img, int depart_x, int depart_y, int largeur, int hauteur){
    Couleur couleur = (Couleur) {.rouge = img->rouge[depart_x][depart_y], .vert = img->vert[depart_x][depart_y], .bleu = img->bleu[depart_x][depart_y]};

    for(int y = depart_y; y < hauteur+depart_y; y++){
        for(int x = depart_x; x < largeur+depart_x; x++){
            Couleur couleurCourante = (Couleur) {.rouge = img->rouge[x][y], .vert = img->vert[x][y], .bleu = img->bleu[x][y]};
            if(compareCouleur(couleur,couleurCourante) == 
# 125 "./src/lib/src/FonctionsPartie4.c" 3 4
                                                         0
# 125 "./src/lib/src/FonctionsPartie4.c"
                                                              )
                return (Couleur) {.rouge = -1, .vert = -1, .bleu = -1};
        }
    }

    return couleur;
}

Motif determineConfig(Palette *palette, int forme, Couleur couleurNO, Couleur autreCouleur){

    for(int i = 0; i < palette->nb; i++){
        if(compareCouleur(couleurNO, palette->pal[i])){

            if(i == 0){
                int cobjet = -1;
                for(int j = 0; j < palette->nb; j++){
                    if(compareCouleur(autreCouleur, palette->pal[j])){
                        cobjet = j;
                        break;
                    }
                }
                return (Motif){.forme = forme, .config = (forme == 5)?1:0, .cfond = i, .cobjet = cobjet};
            }
            else{
                int cfond = -1;
                for(int j = 0; j < palette->nb; j++){
                    if(compareCouleur(autreCouleur, palette->pal[j])){
                        cfond = j;
                        break;
                    }
                }
                return (Motif){.forme = forme, .config = (forme == 5)?0:1, .cfond = cfond, .cobjet = i};
            }
        }
    }

    return (Motif) {.forme = -1, .config = -1, .cfond = -1, .cobjet = -1};
}

Motif identifieMotif(Image *img, Palette *palette, int depart_x, int depart_y, int largeur){

    if(largeur != 32)
        return (Motif) {.forme = -1, .config = -1, .cfond = -1, .cobjet = -1};

    Couleur couleurNO, couleurNE, couleurSO, couleurSE;

    couleurNO = estCouleurUniforme(img,depart_x,depart_y+16,largeur/2,largeur/2);
    couleurNE = estCouleurUniforme(img,depart_x+16,depart_y+16,largeur/2,largeur/2);
    couleurSO = estCouleurUniforme(img,depart_x,depart_y,largeur/2,largeur/2);
    couleurSE = estCouleurUniforme(img,depart_x+16,depart_y,largeur/2,largeur/2);


    if(sontDesCouleursVariadic(4,couleurNO, couleurNE, couleurSO, couleurSE)){


        if(compareCouleurVariadic(couleurSO,couleurNO,2,couleurNE,couleurSE)){
            Motif ret = determineConfig(palette,0, couleurNO, (Couleur) {.rouge = -1, .vert = -1, .bleu = -1});
            ret.forme = 0;
            return ret;
        }


        if(compareCouleur(couleurSO,couleurSE) && compareCouleur(couleurNO,couleurNE) && compareCouleur(couleurNO,couleurSO) == 
# 187 "./src/lib/src/FonctionsPartie4.c" 3 4
                                                                                                                               0
# 187 "./src/lib/src/FonctionsPartie4.c"
                                                                                                                                    ){
            Motif ret = determineConfig(palette,1, couleurNO, couleurSO);
            ret.forme = 1;
            return ret;
        }

        if(compareCouleur(couleurSO,couleurNO) && compareCouleur(couleurSE,couleurNE) && compareCouleur(couleurSO,couleurSE) == 
# 193 "./src/lib/src/FonctionsPartie4.c" 3 4
                                                                                                                               0
# 193 "./src/lib/src/FonctionsPartie4.c"
                                                                                                                                    ){
            Motif ret = determineConfig(palette,2, couleurNO, couleurNE);
            ret.forme = 2;
            return ret;
        }


        if (compareCouleurVariadic(couleurSO,couleurNO,1,couleurNE) && compareCouleur(couleurSO,couleurSE) == 
# 200 "./src/lib/src/FonctionsPartie4.c" 3 4
                                                                                                             0
# 200 "./src/lib/src/FonctionsPartie4.c"
                                                                                                                  ){
            Motif ret = determineConfig(palette,3, couleurNO, couleurSE);
            ret.forme = 3;
            return ret;
        }


        if(compareCouleurVariadic(couleurSO,couleurNO,1,couleurSE) && compareCouleur(couleurNO,couleurNE) == 
# 207 "./src/lib/src/FonctionsPartie4.c" 3 4
                                                                                                            0
# 207 "./src/lib/src/FonctionsPartie4.c"
                                                                                                                 ){
            Motif ret = determineConfig(palette,4, couleurNO, couleurNE);
            ret.forme = 4;
            return ret;
        }


        if(compareCouleurVariadic(couleurSO,couleurNE,1,couleurSE) && compareCouleur(couleurNO,couleurNE) == 
# 214 "./src/lib/src/FonctionsPartie4.c" 3 4
                                                                                                            0
# 214 "./src/lib/src/FonctionsPartie4.c"
                                                                                                                 ){
            Motif ret = determineConfig(palette,5, couleurNO, couleurNE);
            ret.forme = 5;
            return ret;
        }


        if(compareCouleurVariadic(couleurNO,couleurNE,1,couleurSE) && compareCouleur(couleurSO,couleurSE) == 
# 221 "./src/lib/src/FonctionsPartie4.c" 3 4
                                                                                                            0
# 221 "./src/lib/src/FonctionsPartie4.c"
                                                                                                                 ){
            Motif ret = determineConfig(palette,6, couleurNO, couleurSO);
            ret.forme = 6;
            return ret;
        }

    }

    return (Motif) {.forme = -1, .config = -1, .cfond = -1, .cobjet = -1};
}

noeudMotif* creeArbreMotifs(Image *img, Palette *palette, int largeur, int x, int y){
    noeudMotif *root = (noeudMotif*) malloc(sizeof(noeudMotif));

    root->largeur = largeur;
    root->x = x;
    root->y = y;
    root->motif = identifieMotif(img,palette,x,y,largeur);

    if(compareMotif(root->motif, (Motif) {.forme = -1, .config = -1, .cfond = -1, .cobjet = -1})){
        root->NO = creeArbreMotifs( img,
                                    palette,
                                    largeur/2,
                                    x,
                                    y+largeur/2);

        root->NE = creeArbreMotifs( img,
                                    palette,
                                    largeur/2,
                                    x+largeur/2,
                                    y+largeur/2);

        root->SO = creeArbreMotifs( img,
                                    palette,
                                    largeur/2,
                                    x,
                                    y);

        root->SE = creeArbreMotifs( img,
                                    palette,
                                    largeur/2,
                                    x+largeur/2,
                                    y);
    }
    else{
        root->NO = 
# 266 "./src/lib/src/FonctionsPartie4.c" 3 4
                  ((void *)0)
# 266 "./src/lib/src/FonctionsPartie4.c"
                      ;
        root->NE = 
# 267 "./src/lib/src/FonctionsPartie4.c" 3 4
                  ((void *)0)
# 267 "./src/lib/src/FonctionsPartie4.c"
                      ;
        root->SE = 
# 268 "./src/lib/src/FonctionsPartie4.c" 3 4
                  ((void *)0)
# 268 "./src/lib/src/FonctionsPartie4.c"
                      ;
        root->SO = 
# 269 "./src/lib/src/FonctionsPartie4.c" 3 4
                  ((void *)0)
# 269 "./src/lib/src/FonctionsPartie4.c"
                      ;
        return root;
    }

    return root;
}

void libereArbreMotif(noeudMotif *ptrRcn)
{
    if(ptrRcn)
    {
        libereArbreMotif(ptrRcn->NE);
        libereArbreMotif(ptrRcn->NO);
        libereArbreMotif(ptrRcn->SE);
        libereArbreMotif(ptrRcn->SO);
        free(ptrRcn);
    }
}

void fprintfArbre(FILE *fp, noeudMotif* ptrRcn){
    if(ptrRcn){
        if(!compareMotif(ptrRcn->motif,(Motif) {.forme = -1, .config = -1, .cfond = -1, .cobjet = -1}))
            fprintf(fp, "\n%d %d %d%d %d %d",ptrRcn->x,ptrRcn->y,ptrRcn->motif.config,ptrRcn->motif.forme,ptrRcn->motif.cfond,ptrRcn->motif.cobjet);
        fprintfArbre(fp, ptrRcn->NO);
        fprintfArbre(fp, ptrRcn->NE);
        fprintfArbre(fp, ptrRcn->SO);
        fprintfArbre(fp, ptrRcn->SE);
    }
}

void sauveDescriptionImage(noeudMotif *rcn, Palette* pal){

    FILE * fp;


    fp = fopen ("./resources/file/description_image.txt","w");


    fprintf (fp, "%d %d\n",rcn->largeur, 32);
    fprintf (fp, "%d",pal->nb);
    for(int i = 0; i < pal->nb; i++)
        fprintf (fp, "\n%d %d %d",pal->pal[i].rouge,pal->pal[i].vert,pal->pal[i].bleu);

    fprintfArbre(fp, rcn);


    fclose (fp);
}
