	.file	"Button.c"
	.text
	.section .rdata,"dr"
LC0:
	.ascii "bouton appuye\12\0"
	.text
	.globl	_simple_callback
	.def	_simple_callback;	.scl	2;	.type	32;	.endef
_simple_callback:
LFB26:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	$LC0, (%esp)
	call	_SDL_Log
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE26:
	.section .rdata,"dr"
	.align 4
LC1:
	.ascii "Invalid pre-set robot positions.\0"
LC2:
	.ascii "Erreur.\0"
	.text
	.globl	_trigger_error_callback
	.def	_trigger_error_callback;	.scl	2;	.type	32;	.endef
_trigger_error_callback:
LFB27:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, 12(%esp)
	movl	$LC1, 8(%esp)
	movl	$LC2, 4(%esp)
	movl	$16, (%esp)
	call	_SDL_ShowSimpleMessageBox
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE27:
	.globl	_affiche_custom_win_callback
	.def	_affiche_custom_win_callback;	.scl	2;	.type	32;	.endef
_affiche_custom_win_callback:
LFB28:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	8(%ebp), %eax
	movl	%eax, -12(%ebp)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$8, %eax
	movl	(%eax), %eax
	movl	$1, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	$1025, (%esp)
	call	_SDL_FlushEvent
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE28:
	.section .rdata,"dr"
	.align 4
LC3:
	.ascii "RESET\12------ TEST TEXTFIELD ------\0"
LC4:
	.ascii "------ TEST TEXTFIELD ------\0"
LC5:
	.ascii "\12\12> Le scroll est possible\0"
LC6:
	.ascii "\12\12> var example :%d\0"
	.align 4
LC7:
	.ascii "\12\12>    Lorem Ipsum is simply dummy text of the printing and typesetting industry.    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s\0"
	.text
	.globl	_affiche_textfield_win_callback
	.def	_affiche_textfield_win_callback;	.scl	2;	.type	32;	.endef
_affiche_textfield_win_callback:
LFB29:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%ebx
	subl	$532, %esp
	.cfi_offset 3, -12
	movl	8(%ebp), %eax
	movl	%eax, -12(%ebp)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	28(%eax), %eax
	testl	%eax, %eax
	je	L5
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %ebx
	movl	$LC3, (%esp)
	call	_createString
	movl	%eax, 28(%ebx)
	jmp	L6
L5:
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %ebx
	movl	$LC4, (%esp)
	call	_createString
	movl	%eax, 28(%ebx)
L6:
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -16(%ebp)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$8, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	$1, (%eax)
	movl	$LC5, 4(%esp)
	leal	-516(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-516(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$42, 8(%esp)
	movl	$LC6, 4(%esp)
	leal	-516(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-516(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC7, 4(%esp)
	leal	-516(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-516(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$1025, (%esp)
	call	_SDL_FlushEvent
	nop
	addl	$532, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE29:
	.globl	_initButton
	.def	_initButton;	.scl	2;	.type	32;	.endef
_initButton:
LFB30:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$56, (%esp)
	call	_malloc
	movl	%eax, -12(%ebp)
	movl	-12(%ebp), %eax
	movl	52(%ebp), %edx
	movl	%edx, (%eax)
	movl	-12(%ebp), %eax
	movl	$0, 4(%eax)
	movl	-12(%ebp), %eax
	movl	8(%ebp), %edx
	movl	%edx, 8(%eax)
	movl	-12(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%edx, 12(%eax)
	movl	-12(%ebp), %eax
	movl	28(%ebp), %edx
	movl	%edx, 28(%eax)
	movl	-12(%ebp), %eax
	movl	40(%ebp), %edx
	movl	%edx, 48(%eax)
	movl	-12(%ebp), %eax
	movl	32(%ebp), %edx
	movl	%edx, 32(%eax)
	movl	-12(%ebp), %eax
	movl	36(%ebp), %edx
	movl	%edx, 36(%eax)
	movl	-12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%edx, 16(%eax)
	movl	-12(%ebp), %eax
	movl	20(%ebp), %edx
	movl	%edx, 20(%eax)
	movl	-12(%ebp), %eax
	movl	24(%ebp), %edx
	movl	%edx, 24(%eax)
	movl	-12(%ebp), %eax
	movl	44(%ebp), %edx
	movl	%edx, 40(%eax)
	movl	-12(%ebp), %eax
	movl	48(%ebp), %edx
	movl	%edx, 44(%eax)
	movl	-12(%ebp), %eax
	movl	56(%ebp), %edx
	movl	%edx, 52(%eax)
	movl	-12(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE30:
	.globl	_initButtons
	.def	_initButtons;	.scl	2;	.type	32;	.endef
_initButtons:
LFB31:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	leal	12(%ebp), %eax
	movl	%eax, -20(%ebp)
	movl	8(%ebp), %eax
	addl	$1, %eax
	sall	$2, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, -16(%ebp)
	movl	$0, -12(%ebp)
	jmp	L10
L11:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-16(%ebp), %eax
	leal	(%edx,%eax), %ecx
	movl	-20(%ebp), %eax
	leal	4(%eax), %edx
	movl	%edx, -20(%ebp)
	movl	(%eax), %eax
	movl	%eax, (%ecx)
	addl	$1, -12(%ebp)
L10:
	movl	-12(%ebp), %eax
	cmpl	8(%ebp), %eax
	jl	L11
	movl	8(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-16(%ebp), %eax
	addl	%edx, %eax
	movl	$0, (%eax)
	movl	-16(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE31:
	.globl	_isOverButton
	.def	_isOverButton;	.scl	2;	.type	32;	.endef
_isOverButton:
LFB32:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	movl	_appState, %eax
	movl	28(%eax), %edx
	movl	12(%ebp), %eax
	movl	4(%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jl	L14
	movl	_appState, %eax
	movl	28(%eax), %edx
	movl	12(%ebp), %eax
	movl	4(%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	addl	%eax, %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jg	L14
	movl	_appState, %eax
	movl	32(%eax), %edx
	movl	12(%ebp), %eax
	movl	8(%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jl	L14
	movl	_appState, %eax
	movl	32(%eax), %edx
	movl	12(%ebp), %eax
	movl	8(%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	addl	%eax, %ecx
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jg	L14
	movl	$1, %eax
	jmp	L15
L14:
	movl	$0, %eax
L15:
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE32:
	.globl	_freeButton
	.def	_freeButton;	.scl	2;	.type	32;	.endef
_freeButton:
LFB33:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE33:
	.globl	_freeButtons
	.def	_freeButtons;	.scl	2;	.type	32;	.endef
_freeButtons:
LFB34:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$0, -12(%ebp)
	jmp	L18
L19:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_freeButton
	addl	$1, -12(%ebp)
L18:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L19
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE34:
	.ident	"GCC: (MinGW.org GCC-8.2.0-5) 8.2.0"
	.def	_SDL_Log;	.scl	2;	.type	32;	.endef
	.def	_SDL_ShowSimpleMessageBox;	.scl	2;	.type	32;	.endef
	.def	_SDL_FlushEvent;	.scl	2;	.type	32;	.endef
	.def	_createString;	.scl	2;	.type	32;	.endef
	.def	_sprintf;	.scl	2;	.type	32;	.endef
	.def	_addText;	.scl	2;	.type	32;	.endef
	.def	_update;	.scl	2;	.type	32;	.endef
	.def	_malloc;	.scl	2;	.type	32;	.endef
	.def	_free;	.scl	2;	.type	32;	.endef
