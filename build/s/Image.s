	.file	"Image.c"
	.text
	.globl	_alloueImage
	.def	_alloueImage;	.scl	2;	.type	32;	.endef
_alloueImage:
LFB4:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%ebx
	subl	$36, %esp
	.cfi_offset 3, -12
	movl	$24, (%esp)
	call	_malloc
	movl	%eax, -16(%ebp)
	movl	-16(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%edx, 4(%eax)
	movl	-16(%ebp), %eax
	movl	8(%ebp), %edx
	movl	%edx, (%eax)
	movl	8(%ebp), %eax
	sall	$2, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, %edx
	movl	-16(%ebp), %eax
	movl	%edx, 16(%eax)
	movl	8(%ebp), %eax
	sall	$2, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, %edx
	movl	-16(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	8(%ebp), %eax
	sall	$2, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, %edx
	movl	-16(%ebp), %eax
	movl	%edx, 12(%eax)
	movl	8(%ebp), %eax
	sall	$2, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, %edx
	movl	-16(%ebp), %eax
	movl	%edx, 20(%eax)
	movl	$0, -12(%ebp)
	jmp	L2
L3:
	movl	12(%ebp), %eax
	leal	(%eax,%eax), %edx
	movl	-16(%ebp), %eax
	movl	16(%eax), %eax
	movl	-12(%ebp), %ecx
	sall	$2, %ecx
	leal	(%eax,%ecx), %ebx
	movl	%edx, (%esp)
	call	_malloc
	movl	%eax, (%ebx)
	movl	12(%ebp), %eax
	leal	(%eax,%eax), %edx
	movl	-16(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %ecx
	sall	$2, %ecx
	leal	(%eax,%ecx), %ebx
	movl	%edx, (%esp)
	call	_malloc
	movl	%eax, (%ebx)
	movl	12(%ebp), %eax
	leal	(%eax,%eax), %edx
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	-12(%ebp), %ecx
	sall	$2, %ecx
	leal	(%eax,%ecx), %ebx
	movl	%edx, (%esp)
	call	_malloc
	movl	%eax, (%ebx)
	movl	12(%ebp), %eax
	leal	(%eax,%eax), %edx
	movl	-16(%ebp), %eax
	movl	20(%eax), %eax
	movl	-12(%ebp), %ecx
	sall	$2, %ecx
	leal	(%eax,%ecx), %ebx
	movl	%edx, (%esp)
	call	_malloc
	movl	%eax, (%ebx)
	addl	$1, -12(%ebp)
L2:
	movl	-12(%ebp), %eax
	cmpl	8(%ebp), %eax
	jl	L3
	movl	-16(%ebp), %eax
	addl	$36, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE4:
	.globl	_libereImage
	.def	_libereImage;	.scl	2;	.type	32;	.endef
_libereImage:
LFB5:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$0, -12(%ebp)
	jmp	L6
L7:
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	16(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	12(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	20(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	addl	$1, -12(%ebp)
L6:
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	(%eax), %eax
	cmpl	%eax, -12(%ebp)
	jl	L7
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	16(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	12(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	20(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	8(%ebp), %eax
	movl	$0, (%eax)
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE5:
	.globl	_chargeImage
	.def	_chargeImage;	.scl	2;	.type	32;	.endef
_chargeImage:
LFB6:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$72, %esp
	cmpl	$0, 8(%ebp)
	jne	L9
	movl	$0, %eax
	jmp	L10
L9:
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_lisBMPRGB
	movl	%eax, -24(%ebp)
	cmpl	$0, -24(%ebp)
	jne	L11
	movl	$0, %eax
	jmp	L10
L11:
	movl	-24(%ebp), %eax
	movl	4(%eax), %edx
	movl	-24(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_alloueImage
	movl	%eax, -28(%ebp)
	movl	$0, -12(%ebp)
	movl	$0, -16(%ebp)
	jmp	L12
L15:
	movl	$0, -20(%ebp)
	jmp	L13
L14:
	movl	-24(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	addl	$2, %edx
	addl	%edx, %eax
	movzbl	(%eax), %ecx
	movl	-28(%ebp), %eax
	movl	8(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzbl	%cl, %eax
	movw	%ax, (%edx)
	movl	-24(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	addl	$1, %edx
	addl	%edx, %eax
	movzbl	(%eax), %ecx
	movl	-28(%ebp), %eax
	movl	12(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzbl	%cl, %eax
	movw	%ax, (%edx)
	movl	-24(%ebp), %eax
	movl	8(%eax), %edx
	movl	-12(%ebp), %eax
	addl	%edx, %eax
	movzbl	(%eax), %ecx
	movl	-28(%ebp), %eax
	movl	16(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzbl	%cl, %eax
	movw	%ax, (%edx)
	movl	-28(%ebp), %eax
	movl	8(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -48(%ebp)
	filds	-48(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	-28(%ebp), %eax
	movl	12(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -48(%ebp)
	filds	-48(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	-28(%ebp), %eax
	movl	16(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -48(%ebp)
	filds	-48(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	-28(%ebp), %eax
	movl	20(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-42(%ebp)
	movzwl	-42(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -44(%ebp)
	fldcw	-44(%ebp)
	fistps	-46(%ebp)
	fldcw	-42(%ebp)
	movzwl	-46(%ebp), %eax
	movw	%ax, (%edx)
	addl	$3, -12(%ebp)
	addl	$1, -20(%ebp)
L13:
	movl	-24(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -20(%ebp)
	jl	L14
	addl	$1, -16(%ebp)
L12:
	movl	-24(%ebp), %eax
	movl	4(%eax), %eax
	cmpl	%eax, -16(%ebp)
	jl	L15
	movl	-24(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-28(%ebp), %eax
L10:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE6:
	.globl	_sauveImage
	.def	_sauveImage;	.scl	2;	.type	32;	.endef
_sauveImage:
LFB7:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$12, (%esp)
	call	_malloc
	movl	%eax, -24(%ebp)
	movl	8(%ebp), %eax
	movl	4(%eax), %edx
	movl	-24(%ebp), %eax
	movl	%edx, 4(%eax)
	movl	8(%ebp), %eax
	movl	(%eax), %edx
	movl	-24(%ebp), %eax
	movl	%edx, (%eax)
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, %edx
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	imull	%eax, %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, %edx
	movl	-24(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	$0, -12(%ebp)
	movl	$0, -16(%ebp)
	jmp	L17
L20:
	movl	$0, -20(%ebp)
	jmp	L18
L19:
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	-24(%ebp), %eax
	movl	8(%eax), %edx
	movl	-12(%ebp), %eax
	addl	%edx, %eax
	movl	%ecx, %edx
	movb	%dl, (%eax)
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	-24(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	addl	$1, %edx
	addl	%edx, %eax
	movl	%ecx, %edx
	movb	%dl, (%eax)
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	-24(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	addl	$2, %edx
	addl	%edx, %eax
	movl	%ecx, %edx
	movb	%dl, (%eax)
	addl	$3, -12(%ebp)
	addl	$1, -20(%ebp)
L18:
	movl	-24(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -20(%ebp)
	jl	L19
	addl	$1, -16(%ebp)
L17:
	movl	-24(%ebp), %eax
	movl	4(%eax), %eax
	cmpl	%eax, -16(%ebp)
	jl	L20
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_ecrisBMPRGB_Dans
	movl	-24(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE7:
	.globl	_sauveImageNG
	.def	_sauveImageNG;	.scl	2;	.type	32;	.endef
_sauveImageNG:
LFB8:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$56, %esp
	movl	$12, (%esp)
	call	_malloc
	movl	%eax, -24(%ebp)
	movl	8(%ebp), %eax
	movl	4(%eax), %edx
	movl	-24(%ebp), %eax
	movl	%edx, 4(%eax)
	movl	8(%ebp), %eax
	movl	(%eax), %edx
	movl	-24(%ebp), %eax
	movl	%edx, (%eax)
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, %edx
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	imull	%eax, %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, %edx
	movl	-24(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	$0, -12(%ebp)
	movl	$0, -16(%ebp)
	jmp	L22
L25:
	movl	$0, -20(%ebp)
	jmp	L23
L24:
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -32(%ebp)
	filds	-32(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -32(%ebp)
	filds	-32(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -32(%ebp)
	filds	-32(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	-24(%ebp), %eax
	movl	8(%eax), %edx
	movl	-12(%ebp), %eax
	addl	%eax, %edx
	fnstcw	-26(%ebp)
	movzwl	-26(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -28(%ebp)
	fldcw	-28(%ebp)
	fistps	-30(%ebp)
	fldcw	-26(%ebp)
	movzwl	-30(%ebp), %eax
	movb	%al, (%edx)
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -32(%ebp)
	filds	-32(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -32(%ebp)
	filds	-32(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -32(%ebp)
	filds	-32(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	-24(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	addl	$1, %edx
	addl	%edx, %eax
	fldcw	-28(%ebp)
	fistps	-30(%ebp)
	fldcw	-26(%ebp)
	movzwl	-30(%ebp), %edx
	movb	%dl, (%eax)
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -32(%ebp)
	filds	-32(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -32(%ebp)
	filds	-32(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -32(%ebp)
	filds	-32(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	-24(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	addl	$2, %edx
	addl	%edx, %eax
	fldcw	-28(%ebp)
	fistps	-30(%ebp)
	fldcw	-26(%ebp)
	movzwl	-30(%ebp), %edx
	movb	%dl, (%eax)
	addl	$3, -12(%ebp)
	addl	$1, -20(%ebp)
L23:
	movl	-24(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -20(%ebp)
	jl	L24
	addl	$1, -16(%ebp)
L22:
	movl	-24(%ebp), %eax
	movl	4(%eax), %eax
	cmpl	%eax, -16(%ebp)
	jl	L25
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_ecrisBMPRGB_Dans
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE8:
	.globl	_dupliqueImage
	.def	_dupliqueImage;	.scl	2;	.type	32;	.endef
_dupliqueImage:
LFB9:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	8(%ebp), %eax
	movl	4(%eax), %edx
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_alloueImage
	movl	%eax, -20(%ebp)
	movl	$0, -12(%ebp)
	jmp	L27
L30:
	movl	$0, -16(%ebp)
	jmp	L28
L29:
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	leal	(%eax,%edx), %ecx
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	leal	(%eax,%edx), %ecx
	movl	-20(%ebp), %eax
	movl	16(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	leal	(%eax,%edx), %ecx
	movl	-20(%ebp), %eax
	movl	12(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	leal	(%eax,%edx), %ecx
	movl	-20(%ebp), %eax
	movl	20(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	addl	$1, -16(%ebp)
L28:
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -16(%ebp)
	jl	L29
	addl	$1, -12(%ebp)
L27:
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	cmpl	%eax, -12(%ebp)
	jl	L30
	movl	-20(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE9:
	.globl	_differenceImage
	.def	_differenceImage;	.scl	2;	.type	32;	.endef
_differenceImage:
LFB10:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	8(%ebp), %eax
	movl	4(%eax), %edx
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_alloueImage
	movl	%eax, -20(%ebp)
	movl	$0, -12(%ebp)
	jmp	L33
L36:
	movl	$0, -16(%ebp)
	jmp	L34
L35:
	movl	12(%ebp), %eax
	movl	8(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movl	%eax, %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	subl	%eax, %ecx
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movl	%ecx, %edx
	movw	%dx, (%eax)
	movl	12(%ebp), %eax
	movl	16(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movl	%eax, %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	subl	%eax, %ecx
	movl	-20(%ebp), %eax
	movl	16(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movl	%ecx, %edx
	movw	%dx, (%eax)
	movl	12(%ebp), %eax
	movl	12(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movl	%eax, %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	subl	%eax, %ecx
	movl	-20(%ebp), %eax
	movl	12(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movl	%ecx, %edx
	movw	%dx, (%eax)
	movl	12(%ebp), %eax
	movl	20(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movl	%eax, %ecx
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	subl	%eax, %ecx
	movl	-20(%ebp), %eax
	movl	20(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movl	%ecx, %edx
	movw	%dx, (%eax)
	addl	$1, -16(%ebp)
L34:
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -16(%ebp)
	jl	L35
	addl	$1, -12(%ebp)
L33:
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	cmpl	%eax, -12(%ebp)
	jl	L36
	movl	-20(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE10:
	.section .rdata,"dr"
	.align 8
LC0:
	.long	858993459
	.long	1070281523
	.align 8
LC1:
	.long	-1972248982
	.long	1072096398
	.align 8
LC2:
	.long	1175103052
	.long	1068660005
	.ident	"GCC: (MinGW.org GCC-8.2.0-5) 8.2.0"
	.def	_malloc;	.scl	2;	.type	32;	.endef
	.def	_free;	.scl	2;	.type	32;	.endef
	.def	_lisBMPRGB;	.scl	2;	.type	32;	.endef
	.def	_ecrisBMPRGB_Dans;	.scl	2;	.type	32;	.endef
