	.file	"App.c"
	.text
	.comm	_appState, 4, 2
.lcomm _SDL_Started,4,4
.lcomm _TTF_STARTED,4,4
	.globl	_cleanEverything
	.def	_cleanEverything;	.scl	2;	.type	32;	.endef
_cleanEverything:
LFB16:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	_appState, %eax
	testl	%eax, %eax
	je	L2
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	%eax, (%esp)
	call	_freeGuis
	movl	_appState, %eax
	movl	%eax, (%esp)
	call	_free
L2:
	movl	_TTF_STARTED, %eax
	testl	%eax, %eax
	je	L3
	call	_TTF_Quit
L3:
	movl	_SDL_Started, %eax
	testl	%eax, %eax
	je	L5
	call	_SDL_Quit
L5:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE16:
	.section .rdata,"dr"
LC0:
	.ascii "ERREUR : %s > %s\12\0"
	.text
	.globl	_SDL_ExitWithError
	.def	_SDL_ExitWithError;	.scl	2;	.type	32;	.endef
_SDL_ExitWithError:
LFB17:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	call	_SDL_GetError
	movl	%eax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	$LC0, (%esp)
	call	_SDL_Log
	call	_cleanEverything
	movl	$1, (%esp)
	call	_exit
	.cfi_endproc
LFE17:
	.globl	_TTF_ExitWithError
	.def	_TTF_ExitWithError;	.scl	2;	.type	32;	.endef
_TTF_ExitWithError:
LFB18:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	call	_SDL_GetError
	movl	%eax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	$LC0, (%esp)
	call	_SDL_Log
	call	_cleanEverything
	movl	$1, (%esp)
	call	_exit
	.cfi_endproc
LFE18:
	.section .rdata,"dr"
LC1:
	.ascii "Impossible d'init SDL\0"
LC2:
	.ascii "Impossible d'init TTF\0"
LC3:
	.ascii "Impossible de malloc appState\0"
LC4:
	.ascii "Impossible de malloc gui**\0"
LC5:
	.ascii "Impossible d'init un gui\0"
	.text
	.globl	_initialization
	.def	_initialization;	.scl	2;	.type	32;	.endef
_initialization:
LFB19:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%ebx
	subl	$84, %esp
	.cfi_offset 3, -12
	movl	$32, (%esp)
	call	_SDL_Init
	testl	%eax, %eax
	je	L9
	movl	$LC1, (%esp)
	call	_SDL_ExitWithError
L9:
	movl	$1, _SDL_Started
	call	_TTF_Init
	testl	%eax, %eax
	je	L10
	movl	$LC2, (%esp)
	call	_TTF_ExitWithError
L10:
	movl	$1, _TTF_STARTED
	movl	$72, (%esp)
	call	_malloc
	movl	%eax, _appState
	movl	_appState, %eax
	testl	%eax, %eax
	jne	L11
	movl	$LC3, (%esp)
	call	_SDL_ExitWithError
L11:
	movl	_appState, %eax
	movl	$0, 64(%eax)
	movl	8(%ebp), %eax
	addl	$1, %eax
	sall	$2, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, -20(%ebp)
	cmpl	$0, -20(%ebp)
	jne	L12
	movl	$LC4, (%esp)
	call	_SDL_ExitWithError
L12:
	movl	$0, -12(%ebp)
	jmp	L13
L14:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-20(%ebp), %eax
	addl	%edx, %eax
	movl	$0, (%eax)
	addl	$1, -12(%ebp)
L13:
	movl	-12(%ebp), %eax
	cmpl	8(%ebp), %eax
	jle	L14
	movl	_appState, %eax
	movl	-20(%ebp), %edx
	movl	%edx, 64(%eax)
	leal	12(%ebp), %eax
	movl	%eax, -56(%ebp)
	movl	$0, -16(%ebp)
	jmp	L15
L18:
	movl	-56(%ebp), %eax
	leal	32(%eax), %edx
	movl	%edx, -56(%ebp)
	movl	(%eax), %edx
	movl	%edx, -52(%ebp)
	movl	4(%eax), %edx
	movl	%edx, -48(%ebp)
	movl	8(%eax), %edx
	movl	%edx, -44(%ebp)
	movl	12(%eax), %edx
	movl	%edx, -40(%ebp)
	movl	16(%eax), %edx
	movl	%edx, -36(%ebp)
	movl	20(%eax), %edx
	movl	%edx, -32(%ebp)
	movl	24(%eax), %edx
	movl	%edx, -28(%ebp)
	movl	28(%eax), %eax
	movl	%eax, -24(%ebp)
	movl	-16(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-20(%ebp), %eax
	leal	(%edx,%eax), %ebx
	movl	-52(%ebp), %eax
	movl	%eax, (%esp)
	movl	-48(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-40(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-36(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-28(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, 28(%esp)
	call	_initGui
	movl	%eax, (%ebx)
	movl	-16(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-20(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L16
	movl	$LC5, (%esp)
	call	_SDL_ExitWithError
L16:
	cmpl	$0, -16(%ebp)
	jne	L17
	movl	-16(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-20(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	$1, (%eax)
L17:
	addl	$1, -16(%ebp)
L15:
	movl	-16(%ebp), %eax
	cmpl	8(%ebp), %eax
	jl	L18
	movl	_appState, %eax
	movl	$1, (%eax)
	nop
	addl	$84, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE19:
	.section .rdata,"dr"
	.align 4
LC6:
	.ascii "Impossible de charger la couleur de blend\0"
	.align 4
LC7:
	.ascii "Impossible de charger la couleur de rendu\0"
LC8:
	.ascii "Impossible de dessiner.\0"
	.align 4
LC9:
	.ascii "Impossible de creer la surface\0"
	.align 4
LC10:
	.ascii "Impossible de creer la texture\0"
	.align 4
LC11:
	.ascii "Impossible de charger la texture\0"
	.align 4
LC12:
	.ascii "Impossible d'afficher la texture\0"
	.align 4
LC13:
	.ascii "Impossible de creer une surface a partir de la font.\0"
	.align 4
LC14:
	.ascii "Impossible de creer une texture a partir de la surface.\0"
	.align 4
LC15:
	.ascii "Impossible de render la texture.\0"
	.text
	.globl	_update
	.def	_update;	.scl	2;	.type	32;	.endef
_update:
LFB20:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%esi
	pushl	%ebx
	subl	$368, %esp
	.cfi_offset 6, -12
	.cfi_offset 3, -16
	movl	_appState, %eax
	movl	64(%eax), %eax
	testl	%eax, %eax
	je	L91
	movl	$0, -12(%ebp)
	jmp	L21
L89:
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -64(%ebp)
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	je	L22
	movl	-64(%ebp), %eax
	movl	20(%eax), %eax
	testl	%eax, %eax
	je	L22
	movl	$0, -16(%ebp)
	jmp	L23
L88:
	movl	-64(%ebp), %eax
	movl	20(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -68(%ebp)
	movl	-68(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	je	L24
	movl	-68(%ebp), %eax
	movl	4(%eax), %eax
	testl	%eax, %eax
	je	L24
	movl	$0, -20(%ebp)
	jmp	L25
L87:
	movl	-68(%ebp), %eax
	movl	4(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -72(%ebp)
	movl	-72(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	je	L26
	movl	-72(%ebp), %eax
	movl	28(%eax), %eax
	movl	%eax, -160(%ebp)
	movl	-72(%ebp), %eax
	movl	24(%eax), %eax
	movl	%eax, -164(%ebp)
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	movl	$1, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_SetRenderDrawBlendMode
	testl	%eax, %eax
	je	L27
	movl	$LC6, (%esp)
	call	_SDL_ExitWithError
L27:
	movzbl	-157(%ebp), %eax
	movzbl	%al, %esi
	movzbl	-158(%ebp), %eax
	movzbl	%al, %ebx
	movzbl	-159(%ebp), %eax
	movzbl	%al, %ecx
	movzbl	-160(%ebp), %eax
	movzbl	%al, %edx
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	movl	%esi, 16(%esp)
	movl	%ebx, 12(%esp)
	movl	%ecx, 8(%esp)
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_SetRenderDrawColor
	testl	%eax, %eax
	je	L28
	movl	$LC7, (%esp)
	call	_SDL_ExitWithError
L28:
	movl	-72(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, -180(%ebp)
	movl	-72(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -176(%ebp)
	movl	-72(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, -172(%ebp)
	movl	-72(%ebp), %eax
	movl	16(%eax), %eax
	movl	%eax, -168(%ebp)
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	leal	-180(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_RenderFillRect
	testl	%eax, %eax
	je	L29
	movl	$LC8, (%esp)
	call	_SDL_ExitWithError
L29:
	movzbl	-161(%ebp), %eax
	movzbl	%al, %esi
	movzbl	-162(%ebp), %eax
	movzbl	%al, %ebx
	movzbl	-163(%ebp), %eax
	movzbl	%al, %ecx
	movzbl	-164(%ebp), %eax
	movzbl	%al, %edx
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	movl	%esi, 16(%esp)
	movl	%ebx, 12(%esp)
	movl	%ecx, 8(%esp)
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_SetRenderDrawColor
	testl	%eax, %eax
	je	L30
	movl	$LC7, (%esp)
	call	_SDL_ExitWithError
L30:
	movl	-72(%ebp), %eax
	movl	4(%eax), %edx
	movl	-72(%ebp), %eax
	movl	20(%eax), %eax
	addl	%edx, %eax
	movl	%eax, -196(%ebp)
	movl	-72(%ebp), %eax
	movl	8(%eax), %edx
	movl	-72(%ebp), %eax
	movl	20(%eax), %eax
	addl	%edx, %eax
	movl	%eax, -192(%ebp)
	movl	-72(%ebp), %eax
	movl	12(%eax), %edx
	movl	-72(%ebp), %eax
	movl	20(%eax), %eax
	addl	%eax, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -188(%ebp)
	movl	-72(%ebp), %eax
	movl	16(%eax), %edx
	movl	-72(%ebp), %eax
	movl	20(%eax), %eax
	addl	%eax, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -184(%ebp)
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	leal	-196(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_RenderFillRect
	testl	%eax, %eax
	je	L31
	movl	$LC8, (%esp)
	call	_SDL_ExitWithError
L31:
	movl	-72(%ebp), %eax
	movl	32(%eax), %eax
	testl	%eax, %eax
	je	L32
	movl	$0, -24(%ebp)
	jmp	L33
L43:
	movl	-72(%ebp), %eax
	movl	32(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -76(%ebp)
	movl	-76(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	je	L34
	movl	-76(%ebp), %eax
	movl	24(%eax), %eax
	testl	%eax, %eax
	je	L34
	movl	-76(%ebp), %eax
	movl	24(%eax), %eax
	movl	%eax, -80(%ebp)
	movl	$8, -84(%ebp)
	movl	-84(%ebp), %eax
	movl	$-16777216, %edx
	movl	%eax, %ecx
	shrl	%cl, %edx
	movl	%edx, %eax
	movl	%eax, -88(%ebp)
	movl	-84(%ebp), %eax
	movl	$16711680, %edx
	movl	%eax, %ecx
	sarl	%cl, %edx
	movl	%edx, %eax
	movl	%eax, -92(%ebp)
	movl	-84(%ebp), %eax
	movl	$65280, %edx
	movl	%eax, %ecx
	sarl	%cl, %edx
	movl	%edx, %eax
	movl	%eax, -96(%ebp)
	movl	-84(%ebp), %eax
	movl	$255, %edx
	movl	%eax, %ecx
	sarl	%cl, %edx
	movl	%edx, %eax
	movl	%eax, -100(%ebp)
	movl	$24, -104(%ebp)
	movl	-80(%ebp), %eax
	movl	(%eax), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	%eax, -108(%ebp)
	movl	-80(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, %edx
	movl	-80(%ebp), %eax
	movl	4(%eax), %eax
	imull	%eax, %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, -112(%ebp)
	movl	$0, -28(%ebp)
	movl	-80(%ebp), %eax
	movl	4(%eax), %eax
	subl	$1, %eax
	movl	%eax, -32(%ebp)
	jmp	L35
L38:
	movl	$0, -36(%ebp)
	jmp	L36
L37:
	movl	-80(%ebp), %eax
	movl	16(%eax), %eax
	movl	-36(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-32(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	-28(%ebp), %edx
	movl	-112(%ebp), %eax
	addl	%edx, %eax
	movl	%ecx, %edx
	movb	%dl, (%eax)
	movl	-80(%ebp), %eax
	movl	12(%eax), %eax
	movl	-36(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-32(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	-28(%ebp), %eax
	leal	1(%eax), %edx
	movl	-112(%ebp), %eax
	addl	%edx, %eax
	movl	%ecx, %edx
	movb	%dl, (%eax)
	movl	-80(%ebp), %eax
	movl	8(%eax), %eax
	movl	-36(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-32(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	-28(%ebp), %eax
	leal	2(%eax), %edx
	movl	-112(%ebp), %eax
	addl	%edx, %eax
	movl	%ecx, %edx
	movb	%dl, (%eax)
	addl	$3, -28(%ebp)
	addl	$1, -36(%ebp)
L36:
	movl	-80(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -36(%ebp)
	jl	L37
	subl	$1, -32(%ebp)
L35:
	cmpl	$0, -32(%ebp)
	jns	L38
	movl	-80(%ebp), %eax
	movl	4(%eax), %edx
	movl	-80(%ebp), %eax
	movl	(%eax), %eax
	movl	-100(%ebp), %ecx
	movl	%ecx, 32(%esp)
	movl	-96(%ebp), %ecx
	movl	%ecx, 28(%esp)
	movl	-92(%ebp), %ecx
	movl	%ecx, 24(%esp)
	movl	-88(%ebp), %ecx
	movl	%ecx, 20(%esp)
	movl	-108(%ebp), %ecx
	movl	%ecx, 16(%esp)
	movl	-104(%ebp), %ecx
	movl	%ecx, 12(%esp)
	movl	%edx, 8(%esp)
	movl	%eax, 4(%esp)
	movl	-112(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_CreateRGBSurfaceFrom
	movl	%eax, -116(%ebp)
	cmpl	$0, -116(%ebp)
	jne	L39
	movl	$LC9, (%esp)
	call	_SDL_ExitWithError
L39:
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	movl	-116(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_CreateTextureFromSurface
	movl	%eax, -120(%ebp)
	movl	-116(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_FreeSurface
	cmpl	$0, -120(%ebp)
	jne	L40
	movl	$LC10, (%esp)
	call	_SDL_ExitWithError
L40:
	movl	-72(%ebp), %eax
	movl	4(%eax), %edx
	movl	-76(%ebp), %eax
	movl	8(%eax), %eax
	addl	%edx, %eax
	movl	%eax, -212(%ebp)
	movl	-72(%ebp), %eax
	movl	8(%eax), %edx
	movl	-76(%ebp), %eax
	movl	12(%eax), %eax
	addl	%edx, %eax
	movl	%eax, -208(%ebp)
	movl	-76(%ebp), %eax
	movl	16(%eax), %eax
	movl	%eax, -204(%ebp)
	movl	-76(%ebp), %eax
	movl	20(%eax), %eax
	movl	%eax, -200(%ebp)
	leal	-212(%ebp), %eax
	addl	$12, %eax
	movl	%eax, 16(%esp)
	leal	-212(%ebp), %eax
	addl	$8, %eax
	movl	%eax, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	-120(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_QueryTexture
	testl	%eax, %eax
	je	L41
	movl	-120(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_DestroyTexture
	movl	$LC11, (%esp)
	call	_SDL_ExitWithError
L41:
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	leal	-212(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	$0, 8(%esp)
	movl	-120(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_RenderCopy
	testl	%eax, %eax
	je	L42
	movl	-120(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_DestroyTexture
	movl	$LC12, (%esp)
	call	_SDL_ExitWithError
L42:
	movl	-112(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-120(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_DestroyTexture
L34:
	addl	$1, -24(%ebp)
L33:
	movl	-72(%ebp), %eax
	movl	32(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L43
L32:
	movl	-72(%ebp), %eax
	movl	36(%eax), %eax
	testl	%eax, %eax
	je	L44
	movl	$0, -40(%ebp)
	jmp	L45
L58:
	movl	-72(%ebp), %eax
	movl	36(%eax), %eax
	movl	-40(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -124(%ebp)
	movl	-124(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	je	L46
	movl	-124(%ebp), %eax
	movl	4(%eax), %eax
	testl	%eax, %eax
	jne	L46
	movl	-124(%ebp), %eax
	movl	44(%eax), %eax
	movl	%eax, -160(%ebp)
	movl	-124(%ebp), %eax
	movl	40(%eax), %eax
	movl	%eax, -164(%ebp)
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	movl	$1, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_SetRenderDrawBlendMode
	testl	%eax, %eax
	je	L47
	movl	$LC6, (%esp)
	call	_SDL_ExitWithError
L47:
	movzbl	-157(%ebp), %eax
	movzbl	%al, %esi
	movzbl	-158(%ebp), %eax
	movzbl	%al, %ebx
	movzbl	-159(%ebp), %eax
	movzbl	%al, %ecx
	movzbl	-160(%ebp), %eax
	movzbl	%al, %edx
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	movl	%esi, 16(%esp)
	movl	%ebx, 12(%esp)
	movl	%ecx, 8(%esp)
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_SetRenderDrawColor
	testl	%eax, %eax
	je	L48
	movl	$LC7, (%esp)
	call	_SDL_ExitWithError
L48:
	movl	-72(%ebp), %eax
	movl	4(%eax), %edx
	movl	-124(%ebp), %eax
	movl	8(%eax), %eax
	addl	%edx, %eax
	movl	%eax, -228(%ebp)
	movl	-72(%ebp), %eax
	movl	8(%eax), %edx
	movl	-124(%ebp), %eax
	movl	12(%eax), %eax
	addl	%edx, %eax
	movl	%eax, -224(%ebp)
	movl	-124(%ebp), %eax
	movl	16(%eax), %eax
	movl	%eax, -220(%ebp)
	movl	-124(%ebp), %eax
	movl	20(%eax), %eax
	movl	%eax, -216(%ebp)
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	leal	-228(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_RenderFillRect
	testl	%eax, %eax
	je	L49
	movl	$LC8, (%esp)
	call	_SDL_ExitWithError
L49:
	movzbl	-161(%ebp), %eax
	movzbl	%al, %esi
	movzbl	-162(%ebp), %eax
	movzbl	%al, %ebx
	movzbl	-163(%ebp), %eax
	movzbl	%al, %ecx
	movzbl	-164(%ebp), %eax
	movzbl	%al, %edx
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	movl	%esi, 16(%esp)
	movl	%ebx, 12(%esp)
	movl	%ecx, 8(%esp)
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_SetRenderDrawColor
	testl	%eax, %eax
	je	L50
	movl	$LC7, (%esp)
	call	_SDL_ExitWithError
L50:
	movl	-228(%ebp), %edx
	movl	-124(%ebp), %eax
	movl	24(%eax), %eax
	addl	%edx, %eax
	movl	%eax, -244(%ebp)
	movl	-224(%ebp), %edx
	movl	-124(%ebp), %eax
	movl	24(%eax), %eax
	addl	%edx, %eax
	movl	%eax, -240(%ebp)
	movl	-220(%ebp), %edx
	movl	-124(%ebp), %eax
	movl	24(%eax), %eax
	addl	%eax, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -236(%ebp)
	movl	-216(%ebp), %edx
	movl	-124(%ebp), %eax
	movl	24(%eax), %eax
	addl	%eax, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -232(%ebp)
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	leal	-244(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_RenderFillRect
	testl	%eax, %eax
	je	L51
	movl	$LC8, (%esp)
	call	_SDL_ExitWithError
L51:
	movl	-124(%ebp), %eax
	movl	32(%eax), %eax
	testl	%eax, %eax
	je	L46
	movl	-124(%ebp), %eax
	movl	36(%eax), %edx
	movl	-124(%ebp), %eax
	movl	32(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_TTF_OpenFont
	movl	%eax, -128(%ebp)
	cmpl	$0, -128(%ebp)
	je	L46
	movl	-124(%ebp), %eax
	movl	28(%eax), %eax
	movl	-124(%ebp), %edx
	movl	48(%edx), %edx
	movl	%edx, 8(%esp)
	movl	%eax, 4(%esp)
	movl	-128(%ebp), %eax
	movl	%eax, (%esp)
	call	_TTF_RenderText_Solid
	movl	%eax, -132(%ebp)
	cmpl	$0, -132(%ebp)
	jne	L54
	movl	$LC13, (%esp)
	call	_SDL_ExitWithError
L54:
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	movl	-132(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_CreateTextureFromSurface
	movl	%eax, -136(%ebp)
	cmpl	$0, -136(%ebp)
	jne	L55
	movl	-132(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_FreeSurface
	movl	$LC14, (%esp)
	call	_SDL_ExitWithError
L55:
	movl	-228(%ebp), %edx
	movl	-220(%ebp), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%ecx, %eax
	sarl	%eax
	addl	%eax, %edx
	movl	-132(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%ecx, %eax
	sarl	%eax
	negl	%eax
	addl	%edx, %eax
	movl	%eax, -260(%ebp)
	movl	-224(%ebp), %edx
	movl	-216(%ebp), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%ecx, %eax
	sarl	%eax
	addl	%eax, %edx
	movl	-132(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%ecx, %eax
	sarl	%eax
	negl	%eax
	addl	%edx, %eax
	movl	%eax, -256(%ebp)
	movl	-132(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -252(%ebp)
	movl	-132(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, -248(%ebp)
	leal	-260(%ebp), %eax
	addl	$12, %eax
	movl	%eax, 16(%esp)
	leal	-260(%ebp), %eax
	addl	$8, %eax
	movl	%eax, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	-136(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_QueryTexture
	testl	%eax, %eax
	je	L56
	movl	$LC11, (%esp)
	call	_SDL_ExitWithError
L56:
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	leal	-260(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	$0, 8(%esp)
	movl	-136(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_RenderCopy
	testl	%eax, %eax
	je	L57
	movl	-132(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_FreeSurface
	movl	-136(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_DestroyTexture
	movl	$LC15, (%esp)
	call	_SDL_ExitWithError
L57:
	movl	-132(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_FreeSurface
	movl	-136(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_DestroyTexture
	movl	-128(%ebp), %eax
	movl	%eax, (%esp)
	call	_TTF_CloseFont
L46:
	addl	$1, -40(%ebp)
L45:
	movl	-72(%ebp), %eax
	movl	36(%eax), %eax
	movl	-40(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L58
L44:
	movl	-72(%ebp), %eax
	movl	40(%eax), %eax
	testl	%eax, %eax
	je	L26
	movl	$0, -44(%ebp)
	jmp	L60
L86:
	movl	-72(%ebp), %eax
	movl	40(%eax), %eax
	movl	-44(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -140(%ebp)
	movl	-140(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	je	L61
	movl	-140(%ebp), %eax
	movl	44(%eax), %eax
	movl	%eax, -160(%ebp)
	movl	-140(%ebp), %eax
	movl	40(%eax), %eax
	movl	%eax, -164(%ebp)
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	movl	$1, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_SetRenderDrawBlendMode
	testl	%eax, %eax
	je	L62
	movl	$LC6, (%esp)
	call	_SDL_ExitWithError
L62:
	movzbl	-157(%ebp), %eax
	movzbl	%al, %esi
	movzbl	-158(%ebp), %eax
	movzbl	%al, %ebx
	movzbl	-159(%ebp), %eax
	movzbl	%al, %ecx
	movzbl	-160(%ebp), %eax
	movzbl	%al, %edx
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	movl	%esi, 16(%esp)
	movl	%ebx, 12(%esp)
	movl	%ecx, 8(%esp)
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_SetRenderDrawColor
	testl	%eax, %eax
	je	L63
	movl	$LC7, (%esp)
	call	_SDL_ExitWithError
L63:
	movl	-72(%ebp), %eax
	movl	4(%eax), %edx
	movl	-140(%ebp), %eax
	movl	4(%eax), %eax
	addl	%edx, %eax
	addl	$5, %eax
	movl	%eax, -276(%ebp)
	movl	-72(%ebp), %eax
	movl	8(%eax), %edx
	movl	-140(%ebp), %eax
	movl	8(%eax), %eax
	addl	%edx, %eax
	addl	$5, %eax
	movl	%eax, -272(%ebp)
	movl	-140(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, -268(%ebp)
	movl	-140(%ebp), %eax
	movl	16(%eax), %eax
	movl	%eax, -264(%ebp)
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	leal	-276(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_RenderFillRect
	testl	%eax, %eax
	je	L64
	movl	$LC8, (%esp)
	call	_SDL_ExitWithError
L64:
	movzbl	-161(%ebp), %eax
	movzbl	%al, %esi
	movzbl	-162(%ebp), %eax
	movzbl	%al, %ebx
	movzbl	-163(%ebp), %eax
	movzbl	%al, %ecx
	movzbl	-164(%ebp), %eax
	movzbl	%al, %edx
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	movl	%esi, 16(%esp)
	movl	%ebx, 12(%esp)
	movl	%ecx, 8(%esp)
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_SetRenderDrawColor
	testl	%eax, %eax
	je	L65
	movl	$LC7, (%esp)
	call	_SDL_ExitWithError
L65:
	movl	-276(%ebp), %edx
	movl	-140(%ebp), %eax
	movl	20(%eax), %eax
	addl	%edx, %eax
	movl	%eax, -292(%ebp)
	movl	-272(%ebp), %edx
	movl	-140(%ebp), %eax
	movl	20(%eax), %eax
	addl	%edx, %eax
	movl	%eax, -288(%ebp)
	movl	-268(%ebp), %edx
	movl	-140(%ebp), %eax
	movl	20(%eax), %eax
	addl	%eax, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -284(%ebp)
	movl	-264(%ebp), %edx
	movl	-140(%ebp), %eax
	movl	20(%eax), %eax
	addl	%eax, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -280(%ebp)
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	leal	-292(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_RenderFillRect
	testl	%eax, %eax
	je	L66
	movl	$LC8, (%esp)
	call	_SDL_ExitWithError
L66:
	movl	-140(%ebp), %eax
	movl	32(%eax), %eax
	testl	%eax, %eax
	je	L61
	movl	-140(%ebp), %eax
	movl	28(%eax), %eax
	testl	%eax, %eax
	je	L61
	movl	-140(%ebp), %eax
	movl	36(%eax), %edx
	movl	-140(%ebp), %eax
	movl	32(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_TTF_OpenFont
	movl	%eax, -144(%ebp)
	cmpl	$0, -144(%ebp)
	je	L92
	movl	$0, -48(%ebp)
	movl	$0, -52(%ebp)
	movl	$0, -56(%ebp)
	movl	$0, -60(%ebp)
	jmp	L69
L85:
	movl	-140(%ebp), %eax
	movl	28(%eax), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	movzbl	(%eax), %eax
	cmpb	$10, %al
	je	L70
	movl	-140(%ebp), %eax
	movl	28(%eax), %eax
	movl	-60(%ebp), %edx
	addl	$1, %edx
	addl	%edx, %eax
	movzbl	(%eax), %eax
	testb	%al, %al
	je	L70
	movl	-52(%ebp), %eax
	subl	-48(%ebp), %eax
	leal	1(%eax), %edx
	movl	-140(%ebp), %eax
	movl	36(%eax), %eax
	imull	%edx, %eax
	movl	%eax, -316(%ebp)
	fildl	-316(%ebp)
	fldl	LC16
	fdivrp	%st, %st(1)
	movl	-140(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, -316(%ebp)
	fildl	-316(%ebp)
	fxch	%st(1)
	fcompp
	fnstsw	%ax
	sahf
	jbe	L90
L70:
	movl	-140(%ebp), %eax
	movl	28(%eax), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	movzbl	(%eax), %eax
	cmpb	$10, %al
	jne	L73
	movl	-60(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -52(%ebp)
L73:
	movl	-140(%ebp), %eax
	movl	28(%eax), %eax
	movl	%eax, 8(%esp)
	movl	-52(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_extractSubString
	movl	%eax, -148(%ebp)
	movl	-140(%ebp), %eax
	movl	28(%eax), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	movzbl	(%eax), %eax
	cmpb	$10, %al
	je	L74
	movl	-52(%ebp), %eax
	subl	-48(%ebp), %eax
	leal	1(%eax), %edx
	movl	-140(%ebp), %eax
	movl	36(%eax), %eax
	imull	%edx, %eax
	movl	%eax, -316(%ebp)
	fildl	-316(%ebp)
	fldl	LC17
	fdivrp	%st, %st(1)
	movl	-140(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, -316(%ebp)
	fildl	-316(%ebp)
	fxch	%st(1)
	fcompp
	fnstsw	%ax
	sahf
	jbe	L75
L74:
	movl	-60(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -48(%ebp)
	movl	-60(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -52(%ebp)
L75:
	movl	-148(%ebp), %eax
	movzbl	(%eax), %eax
	testb	%al, %al
	jne	L77
	addl	$1, -56(%ebp)
	jmp	L78
L77:
	movl	-140(%ebp), %eax
	movl	24(%eax), %eax
	cmpl	%eax, -56(%ebp)
	jge	L79
	addl	$1, -56(%ebp)
	movl	-148(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	jmp	L78
L79:
	movl	-140(%ebp), %eax
	movl	48(%eax), %eax
	movl	%eax, 8(%esp)
	movl	-148(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-144(%ebp), %eax
	movl	%eax, (%esp)
	call	_TTF_RenderText_Solid
	movl	%eax, -152(%ebp)
	cmpl	$0, -152(%ebp)
	jne	L80
	movl	$LC13, (%esp)
	call	_SDL_ExitWithError
L80:
	movl	-140(%ebp), %eax
	movl	36(%eax), %edx
	movl	-56(%ebp), %eax
	leal	1(%eax), %ecx
	movl	-140(%ebp), %eax
	movl	24(%eax), %eax
	subl	%eax, %ecx
	movl	%ecx, %eax
	imull	%eax, %edx
	movl	-264(%ebp), %eax
	cmpl	%eax, %edx
	jge	L81
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	movl	-152(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_CreateTextureFromSurface
	movl	%eax, -156(%ebp)
	cmpl	$0, -156(%ebp)
	jne	L82
	movl	-152(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_FreeSurface
	movl	$LC14, (%esp)
	call	_SDL_ExitWithError
L82:
	movl	-276(%ebp), %eax
	movl	%eax, -308(%ebp)
	movl	-272(%ebp), %edx
	movl	-140(%ebp), %eax
	movl	36(%eax), %ecx
	movl	-140(%ebp), %eax
	movl	24(%eax), %eax
	movl	-56(%ebp), %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	imull	%ecx, %eax
	addl	%edx, %eax
	movl	%eax, -304(%ebp)
	movl	-152(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -300(%ebp)
	movl	-152(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, -296(%ebp)
	leal	-308(%ebp), %eax
	addl	$12, %eax
	movl	%eax, 16(%esp)
	leal	-308(%ebp), %eax
	addl	$8, %eax
	movl	%eax, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	-156(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_QueryTexture
	testl	%eax, %eax
	je	L83
	movl	$LC11, (%esp)
	call	_SDL_ExitWithError
L83:
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	leal	-308(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	$0, 8(%esp)
	movl	-156(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_RenderCopy
	testl	%eax, %eax
	je	L84
	movl	-152(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_FreeSurface
	movl	-156(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_DestroyTexture
	movl	$LC15, (%esp)
	call	_SDL_ExitWithError
L84:
	movl	-156(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_DestroyTexture
L81:
	movl	-152(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_FreeSurface
	addl	$1, -56(%ebp)
	movl	-148(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	jmp	L78
L90:
	addl	$1, -52(%ebp)
L78:
	addl	$1, -60(%ebp)
L69:
	movl	-140(%ebp), %eax
	movl	28(%eax), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	movzbl	(%eax), %eax
	testb	%al, %al
	jne	L85
	movl	-144(%ebp), %eax
	movl	%eax, (%esp)
	call	_TTF_CloseFont
	jmp	L61
L92:
	nop
L61:
	addl	$1, -44(%ebp)
L60:
	movl	-72(%ebp), %eax
	movl	40(%eax), %eax
	movl	-44(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L86
L26:
	addl	$1, -20(%ebp)
L25:
	movl	-68(%ebp), %eax
	movl	4(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L87
L24:
	addl	$1, -16(%ebp)
L23:
	movl	-64(%ebp), %eax
	movl	20(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L88
L22:
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	movl	%eax, (%esp)
	call	_SDL_RenderPresent
	addl	$1, -12(%ebp)
L21:
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L89
L91:
	nop
	addl	$368, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE20:
	.globl	_eventHandler
	.def	_eventHandler;	.scl	2;	.type	32;	.endef
_eventHandler:
LFB21:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$168, %esp
	movl	_appState, %eax
	movl	8(%eax), %edx
	movl	%edx, -144(%ebp)
	movl	12(%eax), %edx
	movl	%edx, -140(%ebp)
	movl	16(%eax), %edx
	movl	%edx, -136(%ebp)
	movl	20(%eax), %edx
	movl	%edx, -132(%ebp)
	movl	24(%eax), %edx
	movl	%edx, -128(%ebp)
	movl	28(%eax), %edx
	movl	%edx, -124(%ebp)
	movl	32(%eax), %edx
	movl	%edx, -120(%ebp)
	movl	36(%eax), %edx
	movl	%edx, -116(%ebp)
	movl	40(%eax), %edx
	movl	%edx, -112(%ebp)
	movl	44(%eax), %edx
	movl	%edx, -108(%ebp)
	movl	48(%eax), %edx
	movl	%edx, -104(%ebp)
	movl	52(%eax), %edx
	movl	%edx, -100(%ebp)
	movl	56(%eax), %edx
	movl	%edx, -96(%ebp)
	movl	60(%eax), %eax
	movl	%eax, -92(%ebp)
	movl	-144(%ebp), %eax
	cmpl	$768, %eax
	je	L94
	cmpl	$768, %eax
	ja	L95
	cmpl	$512, %eax
	je	L96
	jmp	L143
L95:
	cmpl	$1026, %eax
	je	L98
	cmpl	$1027, %eax
	je	L99
	jmp	L143
L94:
	movl	-124(%ebp), %eax
	cmpl	$113, %eax
	jne	L139
	movl	_appState, %eax
	movl	$0, (%eax)
	jmp	L101
L139:
	nop
L101:
	jmp	L97
L99:
	movl	_appState, %eax
	movl	64(%eax), %eax
	testl	%eax, %eax
	je	L140
	movl	$0, -12(%ebp)
	jmp	L103
L115:
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -52(%ebp)
	movl	-52(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	je	L104
	movl	-52(%ebp), %eax
	movl	20(%eax), %eax
	testl	%eax, %eax
	je	L104
	movl	$0, -16(%ebp)
	jmp	L105
L114:
	movl	-52(%ebp), %eax
	movl	20(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -56(%ebp)
	movl	-56(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	je	L106
	movl	-56(%ebp), %eax
	movl	4(%eax), %eax
	testl	%eax, %eax
	je	L106
	movl	$0, -20(%ebp)
	jmp	L107
L113:
	movl	-56(%ebp), %eax
	movl	4(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -60(%ebp)
	movl	-60(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	je	L108
	movl	-60(%ebp), %eax
	movl	40(%eax), %eax
	testl	%eax, %eax
	je	L108
	movl	$0, -24(%ebp)
	jmp	L109
L112:
	movl	-60(%ebp), %eax
	movl	40(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -64(%ebp)
	leal	-152(%ebp), %eax
	movl	%eax, 4(%esp)
	leal	-148(%ebp), %eax
	movl	%eax, (%esp)
	call	_SDL_GetMouseState
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	cmpl	$1, %eax
	jne	L110
	movl	-60(%ebp), %eax
	movl	4(%eax), %edx
	movl	-64(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %edx
	movl	-148(%ebp), %eax
	cmpl	%eax, %edx
	jg	L110
	movl	-60(%ebp), %eax
	movl	4(%eax), %edx
	movl	-64(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	12(%eax), %eax
	addl	%eax, %edx
	movl	-148(%ebp), %eax
	cmpl	%eax, %edx
	jl	L110
	movl	-60(%ebp), %eax
	movl	8(%eax), %edx
	movl	-64(%ebp), %eax
	movl	8(%eax), %eax
	addl	%eax, %edx
	movl	-152(%ebp), %eax
	cmpl	%eax, %edx
	jg	L110
	movl	-60(%ebp), %eax
	movl	8(%eax), %edx
	movl	-64(%ebp), %eax
	movl	8(%eax), %eax
	addl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	16(%eax), %eax
	addl	%eax, %edx
	movl	-152(%ebp), %eax
	cmpl	%eax, %edx
	jl	L110
	movl	-52(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, (%esp)
	call	_SDL_GetWindowID
	movl	%eax, %edx
	movl	-136(%ebp), %eax
	cmpl	%eax, %edx
	jne	L110
	movl	-124(%ebp), %eax
	testl	%eax, %eax
	jle	L111
	movl	-64(%ebp), %eax
	movl	24(%eax), %eax
	testl	%eax, %eax
	jle	L111
	movl	-64(%ebp), %eax
	movl	24(%eax), %eax
	leal	-1(%eax), %edx
	movl	-64(%ebp), %eax
	movl	%edx, 24(%eax)
	jmp	L110
L111:
	movl	-124(%ebp), %eax
	testl	%eax, %eax
	jns	L110
	movl	-64(%ebp), %eax
	movl	24(%eax), %eax
	leal	1(%eax), %edx
	movl	-64(%ebp), %eax
	movl	%edx, 24(%eax)
L110:
	addl	$1, -24(%ebp)
L109:
	movl	-60(%ebp), %eax
	movl	40(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L112
L108:
	addl	$1, -20(%ebp)
L107:
	movl	-56(%ebp), %eax
	movl	4(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L113
L106:
	addl	$1, -16(%ebp)
L105:
	movl	-52(%ebp), %eax
	movl	20(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L114
L104:
	addl	$1, -12(%ebp)
L103:
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L115
	jmp	L140
L98:
	movl	_appState, %eax
	movl	64(%eax), %eax
	testl	%eax, %eax
	je	L141
	movl	$0, -28(%ebp)
	jmp	L117
L132:
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-28(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -68(%ebp)
	movl	-68(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	je	L118
	movl	-68(%ebp), %eax
	movl	20(%eax), %eax
	testl	%eax, %eax
	je	L118
	movl	$0, -32(%ebp)
	jmp	L119
L131:
	movl	-68(%ebp), %eax
	movl	20(%eax), %eax
	movl	-32(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -72(%ebp)
	movl	-72(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	je	L120
	movl	-72(%ebp), %eax
	movl	4(%eax), %eax
	testl	%eax, %eax
	je	L120
	movl	$0, -36(%ebp)
	jmp	L121
L130:
	movl	-72(%ebp), %eax
	movl	4(%eax), %eax
	movl	-36(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -76(%ebp)
	movl	-76(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	je	L122
	movl	-76(%ebp), %eax
	movl	32(%eax), %eax
	testl	%eax, %eax
	je	L123
	movl	$0, -40(%ebp)
	jmp	L124
L126:
	movl	-76(%ebp), %eax
	movl	32(%eax), %eax
	movl	-40(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -80(%ebp)
	movl	-80(%ebp), %eax
	movl	28(%eax), %eax
	testl	%eax, %eax
	je	L125
	movl	-80(%ebp), %eax
	movl	(%eax), %eax
	cmpl	$1, %eax
	jne	L125
	movl	-80(%ebp), %eax
	movl	4(%eax), %eax
	testl	%eax, %eax
	jne	L125
	movl	-76(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-80(%ebp), %eax
	movl	%eax, (%esp)
	call	_isOverDisplaySurface
	testl	%eax, %eax
	je	L125
	movl	-68(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, (%esp)
	call	_SDL_GetWindowID
	movl	%eax, %edx
	movl	-136(%ebp), %eax
	cmpl	%eax, %edx
	jne	L125
	movl	-80(%ebp), %eax
	movl	28(%eax), %eax
	movl	-76(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	-68(%ebp), %edx
	movl	%edx, (%esp)
	call	*%eax
L125:
	addl	$1, -40(%ebp)
L124:
	movl	-76(%ebp), %eax
	movl	32(%eax), %eax
	movl	-40(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L126
L123:
	movl	-76(%ebp), %eax
	movl	36(%eax), %eax
	testl	%eax, %eax
	je	L122
	movl	$0, -44(%ebp)
	jmp	L127
L129:
	movl	-76(%ebp), %eax
	movl	36(%eax), %eax
	movl	-44(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -84(%ebp)
	movl	-84(%ebp), %eax
	movl	52(%eax), %eax
	testl	%eax, %eax
	je	L128
	movl	-84(%ebp), %eax
	movl	(%eax), %eax
	cmpl	$1, %eax
	jne	L128
	movl	-84(%ebp), %eax
	movl	4(%eax), %eax
	testl	%eax, %eax
	jne	L128
	movl	-76(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-84(%ebp), %eax
	movl	%eax, (%esp)
	call	_isOverButton
	testl	%eax, %eax
	je	L128
	movl	-68(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, (%esp)
	call	_SDL_GetWindowID
	movl	%eax, %edx
	movl	-136(%ebp), %eax
	cmpl	%eax, %edx
	jne	L128
	movl	-84(%ebp), %eax
	movl	52(%eax), %eax
	movl	-76(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	-68(%ebp), %edx
	movl	%edx, (%esp)
	call	*%eax
L128:
	addl	$1, -44(%ebp)
L127:
	movl	-76(%ebp), %eax
	movl	36(%eax), %eax
	movl	-44(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L129
L122:
	addl	$1, -36(%ebp)
L121:
	movl	-72(%ebp), %eax
	movl	4(%eax), %eax
	movl	-36(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L130
L120:
	addl	$1, -32(%ebp)
L119:
	movl	-68(%ebp), %eax
	movl	20(%eax), %eax
	movl	-32(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L131
L118:
	addl	$1, -28(%ebp)
L117:
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-28(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L132
	jmp	L141
L96:
	movzbl	-132(%ebp), %eax
	cmpb	$14, %al
	jne	L142
	movl	$0, -48(%ebp)
	jmp	L134
L138:
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-48(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	12(%eax), %eax
	movl	%eax, (%esp)
	call	_SDL_GetWindowID
	movl	%eax, %edx
	movl	-136(%ebp), %eax
	cmpl	%eax, %edx
	jne	L135
	cmpl	$0, -48(%ebp)
	jne	L136
	movl	_appState, %eax
	movl	$0, (%eax)
	jmp	L133
L136:
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-48(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_removeGui
	jmp	L133
L135:
	addl	$1, -48(%ebp)
L134:
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-48(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L138
	jmp	L142
L133:
	jmp	L142
L140:
	nop
	jmp	L143
L141:
	nop
	jmp	L143
L142:
	nop
L97:
L143:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE21:
	.section .rdata,"dr"
	.align 8
LC16:
	.long	858993459
	.long	1073427251
	.align 8
LC17:
	.long	0
	.long	1073217536
	.ident	"GCC: (MinGW.org GCC-8.2.0-5) 8.2.0"
	.def	_freeGuis;	.scl	2;	.type	32;	.endef
	.def	_free;	.scl	2;	.type	32;	.endef
	.def	_TTF_Quit;	.scl	2;	.type	32;	.endef
	.def	_SDL_Quit;	.scl	2;	.type	32;	.endef
	.def	_SDL_GetError;	.scl	2;	.type	32;	.endef
	.def	_SDL_Log;	.scl	2;	.type	32;	.endef
	.def	_exit;	.scl	2;	.type	32;	.endef
	.def	_SDL_Init;	.scl	2;	.type	32;	.endef
	.def	_TTF_Init;	.scl	2;	.type	32;	.endef
	.def	_malloc;	.scl	2;	.type	32;	.endef
	.def	_initGui;	.scl	2;	.type	32;	.endef
	.def	_SDL_SetRenderDrawBlendMode;	.scl	2;	.type	32;	.endef
	.def	_SDL_SetRenderDrawColor;	.scl	2;	.type	32;	.endef
	.def	_SDL_RenderFillRect;	.scl	2;	.type	32;	.endef
	.def	_SDL_CreateRGBSurfaceFrom;	.scl	2;	.type	32;	.endef
	.def	_SDL_CreateTextureFromSurface;	.scl	2;	.type	32;	.endef
	.def	_SDL_FreeSurface;	.scl	2;	.type	32;	.endef
	.def	_SDL_QueryTexture;	.scl	2;	.type	32;	.endef
	.def	_SDL_DestroyTexture;	.scl	2;	.type	32;	.endef
	.def	_SDL_RenderCopy;	.scl	2;	.type	32;	.endef
	.def	_TTF_OpenFont;	.scl	2;	.type	32;	.endef
	.def	_TTF_RenderText_Solid;	.scl	2;	.type	32;	.endef
	.def	_TTF_CloseFont;	.scl	2;	.type	32;	.endef
	.def	_extractSubString;	.scl	2;	.type	32;	.endef
	.def	_SDL_RenderPresent;	.scl	2;	.type	32;	.endef
	.def	_SDL_GetMouseState;	.scl	2;	.type	32;	.endef
	.def	_SDL_GetWindowID;	.scl	2;	.type	32;	.endef
	.def	_isOverDisplaySurface;	.scl	2;	.type	32;	.endef
	.def	_isOverButton;	.scl	2;	.type	32;	.endef
	.def	_removeGui;	.scl	2;	.type	32;	.endef
