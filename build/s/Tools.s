	.file	"Tools.c"
	.text
	.globl	_comparePoints
	.def	_comparePoints;	.scl	2;	.type	32;	.endef
_comparePoints:
LFB26:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	movl	8(%ebp), %edx
	movl	16(%ebp), %eax
	cmpl	%eax, %edx
	jne	L2
	movl	12(%ebp), %edx
	movl	20(%ebp), %eax
	cmpl	%eax, %edx
	je	L3
L2:
	movl	$0, %eax
	jmp	L4
L3:
	movl	$1, %eax
L4:
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE26:
	.globl	_positionVoisinage
	.def	_positionVoisinage;	.scl	2;	.type	32;	.endef
_positionVoisinage:
LFB27:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$80, %esp
	movl	16(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -8(%ebp)
	movl	20(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -4(%ebp)
	movl	16(%ebp), %eax
	movl	%eax, -16(%ebp)
	movl	20(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -12(%ebp)
	movl	16(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -24(%ebp)
	movl	20(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -20(%ebp)
	movl	16(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -32(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -28(%ebp)
	movl	16(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -40(%ebp)
	movl	20(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -36(%ebp)
	movl	16(%ebp), %eax
	movl	%eax, -48(%ebp)
	movl	20(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -44(%ebp)
	movl	16(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -56(%ebp)
	movl	20(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -52(%ebp)
	movl	16(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -64(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -60(%ebp)
	movl	-8(%ebp), %eax
	movl	-4(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L6
	movl	$0, %eax
	jmp	L16
L6:
	movl	-16(%ebp), %eax
	movl	-12(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L8
	movl	$1, %eax
	jmp	L16
L8:
	movl	-24(%ebp), %eax
	movl	-20(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L9
	movl	$2, %eax
	jmp	L16
L9:
	movl	-32(%ebp), %eax
	movl	-28(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L10
	movl	$3, %eax
	jmp	L16
L10:
	movl	-40(%ebp), %eax
	movl	-36(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L11
	movl	$4, %eax
	jmp	L16
L11:
	movl	-48(%ebp), %eax
	movl	-44(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L12
	movl	$5, %eax
	jmp	L16
L12:
	movl	-56(%ebp), %eax
	movl	-52(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L13
	movl	$6, %eax
	jmp	L16
L13:
	movl	-64(%ebp), %eax
	movl	-60(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L14
	movl	$7, %eax
	jmp	L16
L14:
	movl	16(%ebp), %eax
	movl	20(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L15
	movl	$8, %eax
	jmp	L16
L15:
	movl	$-1, %eax
L16:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE27:
	.globl	_comparePointsVariadic
	.def	_comparePointsVariadic;	.scl	2;	.type	32;	.endef
_comparePointsVariadic:
LFB28:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movl	8(%ebp), %edx
	movl	16(%ebp), %eax
	cmpl	%eax, %edx
	jne	L18
	movl	12(%ebp), %edx
	movl	20(%ebp), %eax
	cmpl	%eax, %edx
	je	L19
L18:
	movl	$0, %eax
	jmp	L25
L19:
	leal	28(%ebp), %eax
	movl	%eax, -8(%ebp)
	movl	$0, -4(%ebp)
	jmp	L21
L24:
	movl	-8(%ebp), %eax
	leal	8(%eax), %edx
	movl	%edx, -8(%ebp)
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, -16(%ebp)
	movl	%edx, -12(%ebp)
	movl	8(%ebp), %edx
	movl	-16(%ebp), %eax
	cmpl	%eax, %edx
	jne	L22
	movl	12(%ebp), %edx
	movl	-12(%ebp), %eax
	cmpl	%eax, %edx
	je	L23
L22:
	movl	$0, %eax
	jmp	L25
L23:
	addl	$1, -4(%ebp)
L21:
	movl	-4(%ebp), %eax
	cmpl	24(%ebp), %eax
	jl	L24
	movl	$1, %eax
L25:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE28:
	.globl	_creeMaillon
	.def	_creeMaillon;	.scl	2;	.type	32;	.endef
_creeMaillon:
LFB29:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$12, (%esp)
	call	_malloc
	movl	%eax, -12(%ebp)
	movl	8(%ebp), %ecx
	movl	12(%ebp), %edx
	movl	-12(%ebp), %eax
	movl	%ecx, (%eax)
	movl	-12(%ebp), %eax
	movl	%edx, 4(%eax)
	movl	-12(%ebp), %eax
	movl	$0, 8(%eax)
	movl	-12(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE29:
	.globl	_insererMaillonAvant
	.def	_insererMaillonAvant;	.scl	2;	.type	32;	.endef
_insererMaillonAvant:
LFB30:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movl	8(%ebp), %eax
	movl	%eax, -4(%ebp)
	movl	$0, -8(%ebp)
	jmp	L29
L32:
	movl	-8(%ebp), %eax
	addl	$1, %eax
	cmpl	%eax, 12(%ebp)
	jne	L30
	movl	-4(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -12(%ebp)
	movl	-4(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%edx, 8(%eax)
	movl	16(%ebp), %eax
	movl	-12(%ebp), %edx
	movl	%edx, 8(%eax)
	jmp	L28
L30:
	addl	$1, -8(%ebp)
	movl	-4(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -4(%ebp)
L29:
	cmpl	$0, -4(%ebp)
	jne	L32
L28:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE30:
	.globl	_ajouterMaillon
	.def	_ajouterMaillon;	.scl	2;	.type	32;	.endef
_ajouterMaillon:
LFB31:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movl	8(%ebp), %eax
	movl	%eax, -4(%ebp)
	jmp	L34
L35:
	movl	-4(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -4(%ebp)
L34:
	movl	-4(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L35
	movl	-4(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%edx, 8(%eax)
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE31:
	.globl	_libereMaillon
	.def	_libereMaillon;	.scl	2;	.type	32;	.endef
_libereMaillon:
LFB32:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	je	L37
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_free
L37:
	movl	$0, 8(%ebp)
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE32:
	.globl	_trouverIndexParValeur
	.def	_trouverIndexParValeur;	.scl	2;	.type	32;	.endef
_trouverIndexParValeur:
LFB33:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movl	8(%ebp), %eax
	movl	%eax, -4(%ebp)
	movl	$0, -8(%ebp)
	jmp	L39
L42:
	movl	-4(%ebp), %eax
	movl	(%eax), %edx
	movl	12(%ebp), %eax
	cmpl	%eax, %edx
	jne	L40
	movl	-4(%ebp), %eax
	movl	4(%eax), %edx
	movl	16(%ebp), %eax
	cmpl	%eax, %edx
	jne	L40
	movl	-8(%ebp), %eax
	jmp	L41
L40:
	addl	$1, -8(%ebp)
	movl	-4(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -4(%ebp)
L39:
	movl	-4(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L42
	movl	$-1, %eax
L41:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE33:
	.globl	_supprimerMaillonParValeur
	.def	_supprimerMaillonParValeur;	.scl	2;	.type	32;	.endef
_supprimerMaillonParValeur:
LFB34:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	8(%ebp), %eax
	movl	%eax, -12(%ebp)
	jmp	L44
L47:
	movl	-12(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -16(%ebp)
	movl	-16(%ebp), %eax
	movl	(%eax), %edx
	movl	12(%ebp), %eax
	cmpl	%eax, %edx
	jne	L45
	movl	-16(%ebp), %eax
	movl	4(%eax), %edx
	movl	16(%ebp), %eax
	cmpl	%eax, %edx
	jne	L45
	movl	-12(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -20(%ebp)
	movl	-12(%ebp), %eax
	movl	8(%eax), %eax
	movl	8(%eax), %edx
	movl	-12(%ebp), %eax
	movl	%edx, 8(%eax)
	leal	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	jmp	L43
L45:
	movl	-12(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -12(%ebp)
L44:
	movl	-12(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L47
L43:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE34:
	.globl	_supprimerMaillon
	.def	_supprimerMaillon;	.scl	2;	.type	32;	.endef
_supprimerMaillon:
LFB35:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	8(%ebp), %eax
	movl	%eax, -12(%ebp)
	jmp	L49
L50:
	movl	-12(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -12(%ebp)
L49:
	movl	-12(%ebp), %eax
	movl	8(%eax), %edx
	movl	12(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, %edx
	jne	L50
	movl	-12(%ebp), %eax
	movl	8(%eax), %eax
	movl	8(%eax), %eax
	movl	%eax, -16(%ebp)
	movl	12(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-12(%ebp), %eax
	movl	-16(%ebp), %edx
	movl	%edx, 8(%eax)
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE35:
	.globl	_dessineCarre
	.def	_dessineCarre;	.scl	2;	.type	32;	.endef
_dessineCarre:
LFB36:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	20(%ebp), %eax
	movl	%eax, -4(%ebp)
	jmp	L52
L55:
	movl	24(%ebp), %eax
	movl	%eax, -8(%ebp)
	jmp	L53
L54:
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	12(%ebp), %eax
	movw	%ax, (%edx)
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	14(%ebp), %eax
	movw	%ax, (%edx)
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	16(%ebp), %eax
	movw	%ax, (%edx)
	movzwl	12(%ebp), %eax
	movw	%ax, -24(%ebp)
	filds	-24(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movzwl	14(%ebp), %eax
	movw	%ax, -24(%ebp)
	filds	-24(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movzwl	16(%ebp), %eax
	movw	%ax, -24(%ebp)
	filds	-24(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-18(%ebp)
	movzwl	-18(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -20(%ebp)
	fldcw	-20(%ebp)
	fistps	-22(%ebp)
	fldcw	-18(%ebp)
	movzwl	-22(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -8(%ebp)
L53:
	movl	24(%ebp), %edx
	movl	28(%ebp), %eax
	addl	%eax, %edx
	movl	-8(%ebp), %eax
	cmpl	%eax, %edx
	ja	L54
	addl	$1, -4(%ebp)
L52:
	movl	20(%ebp), %edx
	movl	28(%ebp), %eax
	addl	%eax, %edx
	movl	-4(%ebp), %eax
	cmpl	%eax, %edx
	ja	L55
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE36:
	.globl	_calculeTrajectoire
	.def	_calculeTrajectoire;	.scl	2;	.type	32;	.endef
_calculeTrajectoire:
LFB37:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%esi
	pushl	%ebx
	subl	$640, %esp
	.cfi_offset 6, -12
	.cfi_offset 3, -16
	movl	20(%ebp), %eax
	leal	3(%eax), %edx
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	subl	$1, %eax
	cmpl	%eax, %edx
	ja	L57
	movl	24(%ebp), %eax
	leal	3(%eax), %edx
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	subl	$1, %eax
	cmpl	%eax, %edx
	ja	L57
	movl	28(%ebp), %eax
	leal	3(%eax), %edx
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	subl	$1, %eax
	cmpl	%eax, %edx
	ja	L57
	movl	32(%ebp), %eax
	leal	3(%eax), %edx
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	subl	$1, %eax
	cmpl	%eax, %edx
	ja	L57
	movl	24(%ebp), %eax
	subl	$3, %eax
	movl	%eax, %ecx
	movl	20(%ebp), %eax
	subl	$3, %eax
	leal	-226(%ebp), %edx
	movl	$7, 20(%esp)
	movl	$7, 16(%esp)
	movl	%ecx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_estCouleurUniforme
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	-226(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-222(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	(%edx), %eax
	movl	%eax, (%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	jne	L57
	movl	32(%ebp), %eax
	subl	$3, %eax
	movl	%eax, %ecx
	movl	28(%ebp), %eax
	subl	$3, %eax
	leal	-220(%ebp), %edx
	movl	$7, 20(%esp)
	movl	$7, 16(%esp)
	movl	%ecx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_estCouleurUniforme
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	-220(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-216(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	(%edx), %eax
	movl	%eax, (%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L58
L57:
	movl	$0, %eax
	jmp	L59
L58:
	movl	$28, (%esp)
	call	_malloc
	movl	%eax, -64(%ebp)
	movl	20(%ebp), %edx
	movl	24(%ebp), %ecx
	movl	-64(%ebp), %eax
	movl	%edx, 12(%eax)
	movl	-64(%ebp), %eax
	movl	%ecx, 16(%eax)
	movl	28(%ebp), %edx
	movl	32(%ebp), %ecx
	movl	-64(%ebp), %eax
	movl	%edx, 20(%eax)
	movl	-64(%ebp), %eax
	movl	%ecx, 24(%eax)
	movl	20(%ebp), %edx
	movl	24(%ebp), %ecx
	movl	-64(%ebp), %eax
	movl	%edx, 4(%eax)
	movl	-64(%ebp), %eax
	movl	%ecx, 8(%eax)
	movl	-64(%ebp), %eax
	movl	16(%eax), %edx
	movl	12(%eax), %eax
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	%edx, (%eax)
	movl	20(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jne	L60
	movl	20(%ebp), %eax
	movl	24(%ebp), %edx
	movl	%eax, -236(%ebp)
	movl	%edx, -232(%ebp)
	jmp	L61
L67:
	movl	-236(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jnb	L62
	movl	-236(%ebp), %eax
	leal	1(%eax), %edx
	movl	-232(%ebp), %eax
	movl	%edx, -236(%ebp)
	movl	%eax, -232(%ebp)
	movl	-236(%ebp), %eax
	movl	-232(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L61
L62:
	movl	-236(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L63
	movl	-236(%ebp), %eax
	leal	-1(%eax), %edx
	movl	-232(%ebp), %eax
	movl	%edx, -236(%ebp)
	movl	%eax, -232(%ebp)
	movl	-236(%ebp), %eax
	movl	-232(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L61
L63:
	movl	-232(%ebp), %edx
	movl	32(%ebp), %eax
	cmpl	%eax, %edx
	jnb	L64
	movl	-236(%ebp), %edx
	movl	-232(%ebp), %eax
	addl	$1, %eax
	movl	%edx, -236(%ebp)
	movl	%eax, -232(%ebp)
	movl	-236(%ebp), %eax
	movl	-232(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L61
L64:
	movl	-232(%ebp), %edx
	movl	32(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L229
	movl	-236(%ebp), %edx
	movl	-232(%ebp), %eax
	subl	$1, %eax
	movl	%edx, -236(%ebp)
	movl	%eax, -232(%ebp)
	movl	-236(%ebp), %eax
	movl	-232(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
L61:
	movl	28(%ebp), %eax
	movl	32(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-236(%ebp), %eax
	movl	-232(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	jne	L67
	jmp	L68
L60:
	movl	32(%ebp), %eax
	movl	%eax, -608(%ebp)
	movl	$0, -604(%ebp)
	fildq	-608(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	movl	24(%ebp), %eax
	movl	%eax, -608(%ebp)
	movl	$0, -604(%ebp)
	fildq	-608(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	fsubrp	%st, %st(1)
	movl	28(%ebp), %eax
	movl	%eax, -608(%ebp)
	movl	$0, -604(%ebp)
	fildq	-608(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -608(%ebp)
	movl	$0, -604(%ebp)
	fildq	-608(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	fsubrp	%st, %st(1)
	fdivrp	%st, %st(1)
	fstps	-68(%ebp)
	movl	24(%ebp), %eax
	movl	%eax, -608(%ebp)
	movl	$0, -604(%ebp)
	fildq	-608(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -608(%ebp)
	movl	$0, -604(%ebp)
	fildq	-608(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	fmuls	-68(%ebp)
	fsubrp	%st, %st(1)
	fstps	-72(%ebp)
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -12(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -16(%ebp)
	jmp	L69
L80:
	fildl	-16(%ebp)
	fmuls	-68(%ebp)
	fadds	-72(%ebp)
	fnstcw	-558(%ebp)
	movzwl	-558(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -560(%ebp)
	fldcw	-560(%ebp)
	fistpl	-76(%ebp)
	fldcw	-558(%ebp)
	movl	-16(%ebp), %eax
	movl	%eax, %ebx
	movl	-76(%ebp), %eax
	movl	%eax, %esi
	movl	%ebx, (%esp)
	movl	%esi, 4(%esp)
	call	_creeMaillon
	movl	%eax, -80(%ebp)
	movl	-12(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -244(%ebp)
	movl	-12(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, -240(%ebp)
	movl	-16(%ebp), %eax
	movl	%eax, -252(%ebp)
	movl	-76(%ebp), %eax
	movl	%eax, -248(%ebp)
	movl	-252(%ebp), %eax
	movl	-248(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$8, %eax
	je	L70
	jmp	L71
L76:
	movl	-244(%ebp), %edx
	movl	-252(%ebp), %eax
	cmpl	%eax, %edx
	jnb	L72
	movl	-244(%ebp), %eax
	leal	1(%eax), %edx
	movl	-240(%ebp), %eax
	movl	%edx, -244(%ebp)
	movl	%eax, -240(%ebp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L71
L72:
	movl	-244(%ebp), %edx
	movl	-252(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L73
	movl	-244(%ebp), %eax
	leal	-1(%eax), %edx
	movl	-240(%ebp), %eax
	movl	%edx, -244(%ebp)
	movl	%eax, -240(%ebp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L71
L73:
	movl	-240(%ebp), %edx
	movl	-248(%ebp), %eax
	cmpl	%eax, %edx
	jnb	L74
	movl	-244(%ebp), %edx
	movl	-240(%ebp), %eax
	addl	$1, %eax
	movl	%edx, -244(%ebp)
	movl	%eax, -240(%ebp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L71
L74:
	movl	-240(%ebp), %edx
	movl	-248(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L230
	movl	-244(%ebp), %edx
	movl	-240(%ebp), %eax
	subl	$1, %eax
	movl	%edx, -244(%ebp)
	movl	%eax, -240(%ebp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
L71:
	movl	-252(%ebp), %eax
	movl	-248(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$1, %eax
	je	L70
	movl	-252(%ebp), %eax
	movl	-248(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$3, %eax
	je	L70
	movl	-252(%ebp), %eax
	movl	-248(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$5, %eax
	je	L70
	movl	-252(%ebp), %eax
	movl	-248(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$7, %eax
	jne	L76
	jmp	L70
L230:
	nop
L70:
	movl	-64(%ebp), %eax
	movl	(%eax), %edx
	movl	-80(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_ajouterMaillon
	movl	-80(%ebp), %eax
	movl	%eax, -12(%ebp)
	movl	20(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L77
	movl	-16(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -16(%ebp)
	jmp	L69
L77:
	movl	-16(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -16(%ebp)
L69:
	movl	20(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L78
	movl	28(%ebp), %edx
	movl	-16(%ebp), %eax
	cmpl	%eax, %edx
	setbe	%al
	jmp	L79
L78:
	movl	28(%ebp), %edx
	movl	-16(%ebp), %eax
	cmpl	%eax, %edx
	setnb	%al
L79:
	testb	%al, %al
	jne	L80
	jmp	L68
L229:
	nop
L68:
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -20(%ebp)
L81:
	jmp	L82
L196:
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	subl	$4, %eax
	movl	%eax, %ecx
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	subl	$4, %eax
	leal	-214(%ebp), %edx
	movl	$9, 20(%esp)
	movl	$9, 16(%esp)
	movl	%ecx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_estCouleurUniforme
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	-214(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-210(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	(%edx), %eax
	movl	%eax, (%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L83
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -256(%ebp)
	movl	-256(%ebp), %eax
	movl	%eax, -24(%ebp)
	jmp	L84
L85:
	movl	-24(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -24(%ebp)
	movl	-24(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L84
	movl	$0, %eax
	jmp	L59
L84:
	movl	-24(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	subl	$4, %eax
	movl	%eax, %ecx
	movl	-24(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	subl	$4, %eax
	leal	-208(%ebp), %edx
	movl	$9, 20(%esp)
	movl	$9, 16(%esp)
	movl	%ecx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_estCouleurUniforme
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	-208(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-204(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	(%edx), %eax
	movl	%eax, (%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	jne	L85
	movl	-24(%ebp), %eax
	movl	%eax, -84(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -264(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -260(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -272(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -268(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -280(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -276(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -288(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -284(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -600(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -596(%ebp)
	movl	-600(%ebp), %eax
	movl	-596(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, -292(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -592(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -588(%ebp)
	movl	-592(%ebp), %eax
	movl	-588(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, -296(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -584(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -580(%ebp)
	movl	-584(%ebp), %eax
	movl	-580(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, -300(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -576(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -572(%ebp)
	movl	-576(%ebp), %eax
	movl	-572(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, -304(%ebp)
	movl	-292(%ebp), %eax
	movl	%eax, -28(%ebp)
	movl	-296(%ebp), %eax
	movl	%eax, -32(%ebp)
	movl	-300(%ebp), %eax
	movl	%eax, -36(%ebp)
	movl	-304(%ebp), %eax
	movl	%eax, -40(%ebp)
	movl	$0, -88(%ebp)
	movb	$0, -89(%ebp)
	jmp	L86
L195:
	cmpl	$0, -28(%ebp)
	je	L87
	movl	-28(%ebp), %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	addl	$1, %eax
	movl	%edx, -312(%ebp)
	movl	%eax, -308(%ebp)
	movl	-28(%ebp), %eax
	movl	(%eax), %eax
	leal	1(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -320(%ebp)
	movl	%eax, -316(%ebp)
	movl	-28(%ebp), %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	subl	$1, %eax
	movl	%edx, -328(%ebp)
	movl	%eax, -324(%ebp)
	movl	-28(%ebp), %eax
	movl	(%eax), %eax
	leal	-1(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -336(%ebp)
	movl	%eax, -332(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-312(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-308(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-312(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-308(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-312(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-308(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -438(%ebp)
	movw	%cx, -436(%ebp)
	movw	%ax, -434(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-320(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-316(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-320(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-316(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-320(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-316(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -444(%ebp)
	movw	%cx, -442(%ebp)
	movw	%ax, -440(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-328(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-324(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-328(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-324(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-328(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-324(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -450(%ebp)
	movw	%cx, -448(%ebp)
	movw	%ax, -446(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-336(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-332(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-336(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-332(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-336(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-332(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -456(%ebp)
	movw	%cx, -454(%ebp)
	movw	%ax, -452(%ebp)
L87:
	cmpl	$0, -32(%ebp)
	je	L88
	movl	-32(%ebp), %eax
	movl	(%eax), %edx
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	addl	$1, %eax
	movl	%edx, -344(%ebp)
	movl	%eax, -340(%ebp)
	movl	-32(%ebp), %eax
	movl	(%eax), %eax
	leal	1(%eax), %edx
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -352(%ebp)
	movl	%eax, -348(%ebp)
	movl	-32(%ebp), %eax
	movl	(%eax), %edx
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	subl	$1, %eax
	movl	%edx, -360(%ebp)
	movl	%eax, -356(%ebp)
	movl	-32(%ebp), %eax
	movl	(%eax), %eax
	leal	-1(%eax), %edx
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -368(%ebp)
	movl	%eax, -364(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-344(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-340(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-344(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-340(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-344(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-340(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -462(%ebp)
	movw	%cx, -460(%ebp)
	movw	%ax, -458(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-352(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-348(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-352(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-348(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-352(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-348(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -468(%ebp)
	movw	%cx, -466(%ebp)
	movw	%ax, -464(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-360(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-356(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-360(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-356(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-360(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-356(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -474(%ebp)
	movw	%cx, -472(%ebp)
	movw	%ax, -470(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-368(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-364(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-368(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-364(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-368(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-364(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -480(%ebp)
	movw	%cx, -478(%ebp)
	movw	%ax, -476(%ebp)
L88:
	cmpl	$0, -36(%ebp)
	je	L89
	movl	-36(%ebp), %eax
	movl	(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	addl	$1, %eax
	movl	%edx, -376(%ebp)
	movl	%eax, -372(%ebp)
	movl	-36(%ebp), %eax
	movl	(%eax), %eax
	leal	1(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -384(%ebp)
	movl	%eax, -380(%ebp)
	movl	-36(%ebp), %eax
	movl	(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	subl	$1, %eax
	movl	%edx, -392(%ebp)
	movl	%eax, -388(%ebp)
	movl	-36(%ebp), %eax
	movl	(%eax), %eax
	leal	-1(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -400(%ebp)
	movl	%eax, -396(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-376(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-372(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-376(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-372(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-376(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-372(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -486(%ebp)
	movw	%cx, -484(%ebp)
	movw	%ax, -482(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-384(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-380(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-384(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-380(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-384(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-380(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -492(%ebp)
	movw	%cx, -490(%ebp)
	movw	%ax, -488(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-392(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-388(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-392(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-388(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-392(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-388(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -498(%ebp)
	movw	%cx, -496(%ebp)
	movw	%ax, -494(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-400(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-396(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-400(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-396(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-400(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-396(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -504(%ebp)
	movw	%cx, -502(%ebp)
	movw	%ax, -500(%ebp)
L89:
	cmpl	$0, -40(%ebp)
	je	L90
	movl	-40(%ebp), %eax
	movl	(%eax), %edx
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	addl	$1, %eax
	movl	%edx, -408(%ebp)
	movl	%eax, -404(%ebp)
	movl	-40(%ebp), %eax
	movl	(%eax), %eax
	leal	1(%eax), %edx
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -416(%ebp)
	movl	%eax, -412(%ebp)
	movl	-40(%ebp), %eax
	movl	(%eax), %edx
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	subl	$1, %eax
	movl	%edx, -424(%ebp)
	movl	%eax, -420(%ebp)
	movl	-40(%ebp), %eax
	movl	(%eax), %eax
	leal	-1(%eax), %edx
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -432(%ebp)
	movl	%eax, -428(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-408(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-404(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-408(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-404(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-408(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-404(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -510(%ebp)
	movw	%cx, -508(%ebp)
	movw	%ax, -506(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-416(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-412(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-416(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-412(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-416(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-412(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -516(%ebp)
	movw	%cx, -514(%ebp)
	movw	%ax, -512(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-424(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-420(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-424(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-420(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-424(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-420(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -522(%ebp)
	movw	%cx, -520(%ebp)
	movw	%ax, -518(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-432(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-428(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-432(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-428(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-432(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-428(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -528(%ebp)
	movw	%cx, -526(%ebp)
	movw	%ax, -524(%ebp)
L90:
	movl	-292(%ebp), %eax
	testl	%eax, %eax
	je	L91
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-438(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-434(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L92
	movl	-264(%ebp), %eax
	movl	-260(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-312(%ebp), %eax
	movl	-308(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L92
	movl	-312(%ebp), %eax
	movl	-308(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-292(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L93
L92:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-444(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-440(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L94
	movl	-264(%ebp), %eax
	movl	-260(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-320(%ebp), %eax
	movl	-316(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L94
	movl	-320(%ebp), %eax
	movl	-316(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-292(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L93
L94:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-450(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-446(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L95
	movl	-264(%ebp), %eax
	movl	-260(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-328(%ebp), %eax
	movl	-324(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L95
	movl	-328(%ebp), %eax
	movl	-324(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-292(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L93
L95:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-456(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-452(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L93
	movl	-264(%ebp), %eax
	movl	-260(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-336(%ebp), %eax
	movl	-332(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L93
	movl	-336(%ebp), %eax
	movl	-332(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-292(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
L93:
	movl	-28(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L96
	jmp	L97
L98:
	movl	-292(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -96(%ebp)
	leal	-292(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-96(%ebp), %eax
	movl	%eax, -292(%ebp)
L97:
	movl	-292(%ebp), %eax
	testl	%eax, %eax
	jne	L98
	movl	$0, -28(%ebp)
	movl	$0, -264(%ebp)
	movl	$0, -260(%ebp)
	jmp	L91
L96:
	movl	-28(%ebp), %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -264(%ebp)
	movl	%eax, -260(%ebp)
	movl	-28(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -28(%ebp)
L91:
	movl	-296(%ebp), %eax
	testl	%eax, %eax
	je	L99
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-480(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-476(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L100
	movl	-272(%ebp), %eax
	movl	-268(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-368(%ebp), %eax
	movl	-364(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L100
	cmpl	$0, -28(%ebp)
	je	L101
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-368(%ebp), %eax
	movl	-364(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L231
L101:
	movl	-368(%ebp), %eax
	movl	-364(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-296(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L231
L100:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-474(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-470(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L104
	movl	-272(%ebp), %eax
	movl	-268(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-360(%ebp), %eax
	movl	-356(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L104
	cmpl	$0, -28(%ebp)
	je	L105
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-360(%ebp), %eax
	movl	-356(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L232
L105:
	movl	-360(%ebp), %eax
	movl	-356(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-296(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L232
L104:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-468(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-464(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L107
	movl	-272(%ebp), %eax
	movl	-268(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-352(%ebp), %eax
	movl	-348(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L107
	cmpl	$0, -28(%ebp)
	je	L108
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-352(%ebp), %eax
	movl	-348(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L233
L108:
	movl	-352(%ebp), %eax
	movl	-348(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-296(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L233
L107:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-462(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-458(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L103
	movl	-272(%ebp), %eax
	movl	-268(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-344(%ebp), %eax
	movl	-340(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L103
	cmpl	$0, -28(%ebp)
	je	L110
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-344(%ebp), %eax
	movl	-340(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L103
L110:
	movl	-344(%ebp), %eax
	movl	-340(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-296(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L103
L231:
	nop
	jmp	L103
L232:
	nop
	jmp	L103
L233:
	nop
L103:
	movl	-32(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L111
	jmp	L112
L113:
	movl	-296(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -100(%ebp)
	leal	-296(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-100(%ebp), %eax
	movl	%eax, -296(%ebp)
L112:
	movl	-296(%ebp), %eax
	testl	%eax, %eax
	jne	L113
	movl	$0, -32(%ebp)
	movl	$0, -272(%ebp)
	movl	$0, -268(%ebp)
	jmp	L99
L111:
	movl	-32(%ebp), %eax
	movl	(%eax), %edx
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -272(%ebp)
	movl	%eax, -268(%ebp)
	movl	-32(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -32(%ebp)
L99:
	movl	-300(%ebp), %eax
	testl	%eax, %eax
	je	L114
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-504(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-500(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L115
	movl	-280(%ebp), %eax
	movl	-276(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-400(%ebp), %eax
	movl	-396(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L115
	cmpl	$0, -28(%ebp)
	je	L116
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-400(%ebp), %eax
	movl	-396(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L234
L116:
	cmpl	$0, -32(%ebp)
	je	L118
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-400(%ebp), %eax
	movl	-396(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L234
L118:
	movl	-400(%ebp), %eax
	movl	-396(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-300(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L234
L115:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-498(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-494(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L120
	movl	-280(%ebp), %eax
	movl	-276(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-392(%ebp), %eax
	movl	-388(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L120
	cmpl	$0, -28(%ebp)
	je	L121
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-392(%ebp), %eax
	movl	-388(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L235
L121:
	cmpl	$0, -32(%ebp)
	je	L123
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-392(%ebp), %eax
	movl	-388(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L235
L123:
	movl	-392(%ebp), %eax
	movl	-388(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-300(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L235
L120:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-492(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-488(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L124
	movl	-280(%ebp), %eax
	movl	-276(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-384(%ebp), %eax
	movl	-380(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L124
	cmpl	$0, -28(%ebp)
	je	L125
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-384(%ebp), %eax
	movl	-380(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L236
L125:
	cmpl	$0, -32(%ebp)
	je	L127
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-384(%ebp), %eax
	movl	-380(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L236
L127:
	movl	-384(%ebp), %eax
	movl	-380(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-300(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L236
L124:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-486(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-482(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L119
	movl	-280(%ebp), %eax
	movl	-276(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-376(%ebp), %eax
	movl	-372(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L119
	cmpl	$0, -28(%ebp)
	je	L128
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-376(%ebp), %eax
	movl	-372(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L119
L128:
	cmpl	$0, -32(%ebp)
	je	L129
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-376(%ebp), %eax
	movl	-372(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L119
L129:
	movl	-376(%ebp), %eax
	movl	-372(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-300(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L119
L234:
	nop
	jmp	L119
L235:
	nop
	jmp	L119
L236:
	nop
L119:
	movl	-36(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L130
	jmp	L131
L132:
	movl	-300(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -104(%ebp)
	leal	-300(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-104(%ebp), %eax
	movl	%eax, -300(%ebp)
L131:
	movl	-300(%ebp), %eax
	testl	%eax, %eax
	jne	L132
	movl	$0, -36(%ebp)
	movl	$0, -280(%ebp)
	movl	$0, -276(%ebp)
	jmp	L114
L130:
	movl	-36(%ebp), %eax
	movl	(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -280(%ebp)
	movl	%eax, -276(%ebp)
	movl	-36(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -36(%ebp)
L114:
	movl	-304(%ebp), %eax
	testl	%eax, %eax
	je	L133
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-528(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-524(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L134
	movl	-288(%ebp), %eax
	movl	-284(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-432(%ebp), %eax
	movl	-428(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L134
	cmpl	$0, -28(%ebp)
	je	L135
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-432(%ebp), %eax
	movl	-428(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L237
L135:
	cmpl	$0, -32(%ebp)
	je	L137
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-432(%ebp), %eax
	movl	-428(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L237
L137:
	cmpl	$0, -36(%ebp)
	je	L138
	movl	-36(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-432(%ebp), %eax
	movl	-428(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L237
L138:
	movl	-432(%ebp), %eax
	movl	-428(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-304(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L237
L134:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-522(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-518(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L140
	movl	-288(%ebp), %eax
	movl	-284(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-424(%ebp), %eax
	movl	-420(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L140
	cmpl	$0, -28(%ebp)
	je	L141
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-424(%ebp), %eax
	movl	-420(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L238
L141:
	cmpl	$0, -32(%ebp)
	je	L143
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-424(%ebp), %eax
	movl	-420(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L238
L143:
	cmpl	$0, -36(%ebp)
	je	L144
	movl	-36(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-424(%ebp), %eax
	movl	-420(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L238
L144:
	movl	-424(%ebp), %eax
	movl	-420(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-304(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L238
L140:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-516(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-512(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L145
	movl	-288(%ebp), %eax
	movl	-284(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-416(%ebp), %eax
	movl	-412(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L145
	cmpl	$0, -28(%ebp)
	je	L146
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-416(%ebp), %eax
	movl	-412(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L239
L146:
	cmpl	$0, -32(%ebp)
	je	L148
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-416(%ebp), %eax
	movl	-412(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L239
L148:
	cmpl	$0, -36(%ebp)
	je	L149
	movl	-36(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-416(%ebp), %eax
	movl	-412(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L239
L149:
	movl	-416(%ebp), %eax
	movl	-412(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-304(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L239
L145:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-510(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-506(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L139
	movl	-288(%ebp), %eax
	movl	-284(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-408(%ebp), %eax
	movl	-404(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L139
	cmpl	$0, -28(%ebp)
	je	L150
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-408(%ebp), %eax
	movl	-404(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L139
L150:
	cmpl	$0, -32(%ebp)
	je	L151
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-408(%ebp), %eax
	movl	-404(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L139
L151:
	cmpl	$0, -36(%ebp)
	je	L152
	movl	-36(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-408(%ebp), %eax
	movl	-404(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L139
L152:
	movl	-408(%ebp), %eax
	movl	-404(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-304(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L139
L237:
	nop
	jmp	L139
L238:
	nop
	jmp	L139
L239:
	nop
L139:
	movl	-40(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L153
	jmp	L154
L155:
	movl	-304(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -108(%ebp)
	leal	-304(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-108(%ebp), %eax
	movl	%eax, -304(%ebp)
L154:
	movl	-304(%ebp), %eax
	testl	%eax, %eax
	jne	L155
	movl	$0, -40(%ebp)
	movl	$0, -288(%ebp)
	movl	$0, -284(%ebp)
	jmp	L133
L153:
	movl	-40(%ebp), %eax
	movl	(%eax), %edx
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -288(%ebp)
	movl	%eax, -284(%ebp)
	movl	-40(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -40(%ebp)
L133:
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -44(%ebp)
	jmp	L156
L194:
	cmpl	$0, -28(%ebp)
	je	L157
	movl	-84(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L157
	movl	-292(%ebp), %edx
	movl	-20(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-84(%ebp), %eax
	movl	8(%eax), %edx
	movl	-28(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-84(%ebp), %eax
	movl	$0, 8(%eax)
	jmp	L158
L159:
	movl	-256(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -112(%ebp)
	leal	-256(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-112(%ebp), %eax
	movl	%eax, -256(%ebp)
L158:
	movl	-256(%ebp), %eax
	testl	%eax, %eax
	jne	L159
	movl	-28(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -20(%ebp)
	jmp	L160
L161:
	movl	-296(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -116(%ebp)
	leal	-296(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-116(%ebp), %eax
	movl	%eax, -296(%ebp)
L160:
	movl	-296(%ebp), %eax
	testl	%eax, %eax
	jne	L161
	jmp	L162
L163:
	movl	-300(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -120(%ebp)
	leal	-300(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-120(%ebp), %eax
	movl	%eax, -300(%ebp)
L162:
	movl	-300(%ebp), %eax
	testl	%eax, %eax
	jne	L163
	jmp	L164
L165:
	movl	-304(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -124(%ebp)
	leal	-304(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-124(%ebp), %eax
	movl	%eax, -304(%ebp)
L164:
	movl	-304(%ebp), %eax
	testl	%eax, %eax
	jne	L165
	jmp	L81
L157:
	cmpl	$0, -32(%ebp)
	je	L167
	movl	-84(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L167
	movl	-296(%ebp), %edx
	movl	-20(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-84(%ebp), %eax
	movl	8(%eax), %edx
	movl	-32(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-84(%ebp), %eax
	movl	$0, 8(%eax)
	jmp	L168
L169:
	movl	-256(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -128(%ebp)
	leal	-256(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-128(%ebp), %eax
	movl	%eax, -256(%ebp)
L168:
	movl	-256(%ebp), %eax
	testl	%eax, %eax
	jne	L169
	movl	-32(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -20(%ebp)
	jmp	L170
L171:
	movl	-292(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -132(%ebp)
	leal	-292(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-132(%ebp), %eax
	movl	%eax, -292(%ebp)
L170:
	movl	-292(%ebp), %eax
	testl	%eax, %eax
	jne	L171
	jmp	L172
L173:
	movl	-300(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -136(%ebp)
	leal	-300(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-136(%ebp), %eax
	movl	%eax, -300(%ebp)
L172:
	movl	-300(%ebp), %eax
	testl	%eax, %eax
	jne	L173
	jmp	L174
L175:
	movl	-304(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -140(%ebp)
	leal	-304(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-140(%ebp), %eax
	movl	%eax, -304(%ebp)
L174:
	movl	-304(%ebp), %eax
	testl	%eax, %eax
	jne	L175
	jmp	L81
L167:
	cmpl	$0, -36(%ebp)
	je	L176
	movl	-84(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-36(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L176
	movl	-300(%ebp), %edx
	movl	-20(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-84(%ebp), %eax
	movl	8(%eax), %edx
	movl	-36(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-84(%ebp), %eax
	movl	$0, 8(%eax)
	jmp	L177
L178:
	movl	-256(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -144(%ebp)
	leal	-256(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-144(%ebp), %eax
	movl	%eax, -256(%ebp)
L177:
	movl	-256(%ebp), %eax
	testl	%eax, %eax
	jne	L178
	movl	-36(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -20(%ebp)
	jmp	L179
L180:
	movl	-292(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -148(%ebp)
	leal	-292(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-148(%ebp), %eax
	movl	%eax, -292(%ebp)
L179:
	movl	-292(%ebp), %eax
	testl	%eax, %eax
	jne	L180
	jmp	L181
L182:
	movl	-296(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -152(%ebp)
	leal	-296(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-152(%ebp), %eax
	movl	%eax, -296(%ebp)
L181:
	movl	-296(%ebp), %eax
	testl	%eax, %eax
	jne	L182
	jmp	L183
L184:
	movl	-304(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -156(%ebp)
	leal	-304(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-156(%ebp), %eax
	movl	%eax, -304(%ebp)
L183:
	movl	-304(%ebp), %eax
	testl	%eax, %eax
	jne	L184
	jmp	L81
L176:
	cmpl	$0, -40(%ebp)
	je	L185
	movl	-84(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-40(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L185
	movl	-304(%ebp), %edx
	movl	-20(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-84(%ebp), %eax
	movl	8(%eax), %edx
	movl	-40(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-84(%ebp), %eax
	movl	$0, 8(%eax)
	jmp	L186
L187:
	movl	-256(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -160(%ebp)
	leal	-256(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-160(%ebp), %eax
	movl	%eax, -256(%ebp)
L186:
	movl	-256(%ebp), %eax
	testl	%eax, %eax
	jne	L187
	movl	-40(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -20(%ebp)
	jmp	L188
L189:
	movl	-292(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -164(%ebp)
	leal	-292(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-164(%ebp), %eax
	movl	%eax, -292(%ebp)
L188:
	movl	-292(%ebp), %eax
	testl	%eax, %eax
	jne	L189
	jmp	L190
L191:
	movl	-296(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -168(%ebp)
	leal	-296(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-168(%ebp), %eax
	movl	%eax, -296(%ebp)
L190:
	movl	-296(%ebp), %eax
	testl	%eax, %eax
	jne	L191
	jmp	L192
L193:
	movl	-300(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -172(%ebp)
	leal	-300(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-172(%ebp), %eax
	movl	%eax, -300(%ebp)
L192:
	movl	-300(%ebp), %eax
	testl	%eax, %eax
	jne	L193
	jmp	L81
L185:
	movl	-44(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -44(%ebp)
L156:
	cmpl	$0, -44(%ebp)
	jne	L194
L86:
	movzbl	-89(%ebp), %eax
	xorl	$1, %eax
	testb	%al, %al
	jne	L195
L83:
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -20(%ebp)
L82:
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L196
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, -176(%ebp)
	movl	-176(%ebp), %eax
	movl	%eax, -48(%ebp)
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -20(%ebp)
	jmp	L197
L226:
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -52(%ebp)
	cmpl	$0, -52(%ebp)
	je	L198
	movl	-52(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	je	L198
	movl	-52(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -56(%ebp)
	jmp	L199
L201:
	movl	-52(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, %esi
	movl	-52(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, %ebx
	movl	-20(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, %ecx
	movl	-20(%ebp), %eax
	movl	(%eax), %eax
	leal	-202(%ebp), %edx
	movl	%esi, 20(%esp)
	movl	%ebx, 16(%esp)
	movl	%ecx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_estCouleurUniformeDansCarre
	movl	16(%ebp), %eax
	movl	4(%eax), %eax
	movl	-202(%ebp), %edx
	movl	%edx, 8(%esp)
	movzwl	-198(%ebp), %edx
	movw	%dx, 12(%esp)
	movl	(%eax), %edx
	movl	%edx, (%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	testb	%al, %al
	je	L200
	movl	-52(%ebp), %eax
	movl	%eax, -56(%ebp)
L200:
	movl	-52(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -52(%ebp)
L199:
	movl	-52(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L201
	cmpl	$0, -56(%ebp)
	jne	L202
	cmpl	$0, -56(%ebp)
	je	L198
	movl	-56(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	28(%ebp), %eax
	movl	32(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L198
L202:
	movl	-20(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 20(%ebp)
	movl	%edx, 24(%ebp)
	movl	-56(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 28(%ebp)
	movl	%edx, 32(%ebp)
	movl	20(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jne	L203
	movl	20(%ebp), %eax
	movl	24(%ebp), %edx
	movl	%eax, -536(%ebp)
	movl	%edx, -532(%ebp)
	jmp	L204
L210:
	movl	-536(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jnb	L205
	movl	-536(%ebp), %eax
	leal	1(%eax), %edx
	movl	-532(%ebp), %eax
	movl	%edx, -536(%ebp)
	movl	%eax, -532(%ebp)
	movl	-536(%ebp), %eax
	movl	-532(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L204
L205:
	movl	-536(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L206
	movl	-536(%ebp), %eax
	leal	-1(%eax), %edx
	movl	-532(%ebp), %eax
	movl	%edx, -536(%ebp)
	movl	%eax, -532(%ebp)
	movl	-536(%ebp), %eax
	movl	-532(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L204
L206:
	movl	-532(%ebp), %edx
	movl	32(%ebp), %eax
	cmpl	%eax, %edx
	jnb	L207
	movl	-536(%ebp), %eax
	movl	-532(%ebp), %edx
	addl	$1, %edx
	movl	%eax, -536(%ebp)
	movl	%edx, -532(%ebp)
	movl	-536(%ebp), %eax
	movl	-532(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L204
L207:
	movl	-532(%ebp), %edx
	movl	32(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L240
	movl	-536(%ebp), %eax
	movl	-532(%ebp), %edx
	subl	$1, %edx
	movl	%eax, -536(%ebp)
	movl	%edx, -532(%ebp)
	movl	-536(%ebp), %eax
	movl	-532(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
L204:
	movl	28(%ebp), %eax
	movl	32(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-536(%ebp), %eax
	movl	-532(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	jne	L210
	jmp	L224
L203:
	movl	32(%ebp), %eax
	movl	%eax, -576(%ebp)
	movl	$0, -572(%ebp)
	fildq	-576(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	movl	24(%ebp), %eax
	movl	%eax, -576(%ebp)
	movl	$0, -572(%ebp)
	fildq	-576(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	fsubrp	%st, %st(1)
	movl	28(%ebp), %eax
	movl	%eax, -576(%ebp)
	movl	$0, -572(%ebp)
	fildq	-576(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -576(%ebp)
	movl	$0, -572(%ebp)
	fildq	-576(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	fsubrp	%st, %st(1)
	fdivrp	%st, %st(1)
	fstps	-180(%ebp)
	movl	24(%ebp), %eax
	movl	%eax, -576(%ebp)
	movl	$0, -572(%ebp)
	fildq	-576(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -576(%ebp)
	movl	$0, -572(%ebp)
	fildq	-576(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	fmuls	-180(%ebp)
	fsubrp	%st, %st(1)
	fstps	-184(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -60(%ebp)
	jmp	L212
L223:
	fildl	-60(%ebp)
	fmuls	-180(%ebp)
	fadds	-184(%ebp)
	fnstcw	-558(%ebp)
	movzwl	-558(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -560(%ebp)
	fldcw	-560(%ebp)
	fistpl	-188(%ebp)
	fldcw	-558(%ebp)
	movl	-60(%ebp), %eax
	movl	%eax, -568(%ebp)
	movl	-188(%ebp), %eax
	movl	%eax, -564(%ebp)
	movl	-568(%ebp), %eax
	movl	-564(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, -192(%ebp)
	movl	-48(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -544(%ebp)
	movl	-48(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, -540(%ebp)
	movl	-60(%ebp), %eax
	movl	%eax, -552(%ebp)
	movl	-188(%ebp), %eax
	movl	%eax, -548(%ebp)
	movl	-552(%ebp), %eax
	movl	-548(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$8, %eax
	je	L213
	jmp	L214
L219:
	movl	-544(%ebp), %edx
	movl	-552(%ebp), %eax
	cmpl	%eax, %edx
	jnb	L215
	movl	-544(%ebp), %eax
	leal	1(%eax), %edx
	movl	-540(%ebp), %eax
	movl	%edx, -544(%ebp)
	movl	%eax, -540(%ebp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L214
L215:
	movl	-544(%ebp), %edx
	movl	-552(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L216
	movl	-544(%ebp), %eax
	leal	-1(%eax), %edx
	movl	-540(%ebp), %eax
	movl	%edx, -544(%ebp)
	movl	%eax, -540(%ebp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L214
L216:
	movl	-540(%ebp), %edx
	movl	-548(%ebp), %eax
	cmpl	%eax, %edx
	jnb	L217
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	addl	$1, %edx
	movl	%eax, -544(%ebp)
	movl	%edx, -540(%ebp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L214
L217:
	movl	-540(%ebp), %edx
	movl	-548(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L241
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	subl	$1, %edx
	movl	%eax, -544(%ebp)
	movl	%edx, -540(%ebp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
L214:
	movl	-552(%ebp), %eax
	movl	-548(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$1, %eax
	je	L213
	movl	-552(%ebp), %eax
	movl	-548(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$3, %eax
	je	L213
	movl	-552(%ebp), %eax
	movl	-548(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$5, %eax
	je	L213
	movl	-552(%ebp), %eax
	movl	-548(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$7, %eax
	jne	L219
	jmp	L213
L241:
	nop
L213:
	movl	-192(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
	movl	-192(%ebp), %eax
	movl	%eax, -48(%ebp)
	movl	20(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L220
	movl	-60(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -60(%ebp)
	jmp	L212
L220:
	movl	-60(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -60(%ebp)
L212:
	movl	20(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L221
	movl	28(%ebp), %edx
	movl	-60(%ebp), %eax
	cmpl	%eax, %edx
	setbe	%al
	jmp	L222
L221:
	movl	28(%ebp), %edx
	movl	-60(%ebp), %eax
	cmpl	%eax, %edx
	setnb	%al
L222:
	testb	%al, %al
	jne	L223
	jmp	L224
L240:
	nop
	jmp	L224
L225:
	movl	-48(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -48(%ebp)
L224:
	movl	-48(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L225
	movl	-56(%ebp), %eax
	movl	%eax, -20(%ebp)
L198:
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -20(%ebp)
L197:
	cmpl	$0, -20(%ebp)
	jne	L226
	jmp	L227
L228:
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	%eax, -196(%ebp)
	movl	-64(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-64(%ebp), %eax
	movl	-196(%ebp), %edx
	movl	%edx, (%eax)
L227:
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L228
	movl	-64(%ebp), %eax
	movl	-176(%ebp), %edx
	movl	%edx, (%eax)
	movl	-64(%ebp), %eax
L59:
	addl	$640, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE37:
	.globl	_dessineMotif0
	.def	_dessineMotif0;	.scl	2;	.type	32;	.endef
_dessineMotif0:
LFB38:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$80, %esp
	movl	44(%ebp), %eax
	movb	%al, -52(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -12(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -8(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -4(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -24(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -20(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -16(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -36(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -32(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -28(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -48(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -44(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -40(%ebp)
	movzwl	22(%ebp), %eax
	testw	%ax, %ax
	jne	L243
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-12(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-8(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-4(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-24(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-16(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-36(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-28(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-48(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	jmp	L245
L243:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-12(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-8(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-4(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-24(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-16(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-36(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-28(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-48(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
L245:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE38:
	.globl	_dessineMotif1
	.def	_dessineMotif1;	.scl	2;	.type	32;	.endef
_dessineMotif1:
LFB39:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$104, %esp
	movl	44(%ebp), %eax
	movb	%al, -68(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -28(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -24(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -20(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -40(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -36(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -32(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -52(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -48(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -44(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -64(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -60(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -56(%ebp)
	movzwl	22(%ebp), %eax
	testw	%ax, %ax
	jne	L247
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-28(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-24(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-40(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-36(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-52(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-48(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-64(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -68(%ebp)
	je	L254
	movl	-48(%ebp), %edx
	movl	-44(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -12(%ebp)
	movl	-52(%ebp), %eax
	movl	%eax, -4(%ebp)
	jmp	L249
L250:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-66(%ebp)
	movzwl	-66(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -70(%ebp)
	fldcw	-70(%ebp)
	fistps	-72(%ebp)
	fldcw	-66(%ebp)
	movzwl	-72(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -4(%ebp)
L249:
	movl	-52(%ebp), %eax
	movl	-44(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-4(%ebp), %eax
	cmpl	%eax, %edx
	ja	L250
	jmp	L254
L247:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-28(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-24(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-40(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-36(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-52(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-48(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-64(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -68(%ebp)
	je	L254
	movl	-48(%ebp), %edx
	movl	-44(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -16(%ebp)
	movl	-52(%ebp), %eax
	movl	%eax, -8(%ebp)
	jmp	L252
L253:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-66(%ebp)
	movzwl	-66(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -70(%ebp)
	fldcw	-70(%ebp)
	fistps	-72(%ebp)
	fldcw	-66(%ebp)
	movzwl	-72(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -8(%ebp)
L252:
	movl	-52(%ebp), %eax
	movl	-44(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-8(%ebp), %eax
	cmpl	%eax, %edx
	ja	L253
L254:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE39:
	.globl	_dessineMotif2
	.def	_dessineMotif2;	.scl	2;	.type	32;	.endef
_dessineMotif2:
LFB40:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$104, %esp
	movl	44(%ebp), %eax
	movb	%al, -68(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -28(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -24(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -20(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -40(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -36(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -32(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -52(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -48(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -44(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -64(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -60(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -56(%ebp)
	movzwl	22(%ebp), %eax
	testw	%ax, %ax
	jne	L256
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-28(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-24(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-40(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-36(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-52(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-48(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-64(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -68(%ebp)
	je	L263
	movl	-52(%ebp), %edx
	movl	-44(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -12(%ebp)
	movl	-48(%ebp), %eax
	movl	%eax, -4(%ebp)
	jmp	L258
L259:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-66(%ebp)
	movzwl	-66(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -70(%ebp)
	fldcw	-70(%ebp)
	fistps	-72(%ebp)
	fldcw	-66(%ebp)
	movzwl	-72(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -4(%ebp)
L258:
	movl	-48(%ebp), %eax
	movl	-44(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-4(%ebp), %eax
	cmpl	%eax, %edx
	ja	L259
	jmp	L263
L256:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-28(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-24(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-40(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-36(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-52(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-48(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-64(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -68(%ebp)
	je	L263
	movl	-52(%ebp), %edx
	movl	-44(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -16(%ebp)
	movl	-48(%ebp), %eax
	movl	%eax, -8(%ebp)
	jmp	L261
L262:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-66(%ebp)
	movzwl	-66(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -70(%ebp)
	fldcw	-70(%ebp)
	fistps	-72(%ebp)
	fldcw	-66(%ebp)
	movzwl	-72(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -8(%ebp)
L261:
	movl	-48(%ebp), %eax
	movl	-44(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-8(%ebp), %eax
	cmpl	%eax, %edx
	ja	L262
L263:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE40:
	.globl	_dessineMotif3
	.def	_dessineMotif3;	.scl	2;	.type	32;	.endef
_dessineMotif3:
LFB41:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$120, %esp
	movl	44(%ebp), %eax
	movb	%al, -84(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -36(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -32(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -28(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -48(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -44(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -40(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -60(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -56(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -52(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -72(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -68(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -64(%ebp)
	movzwl	22(%ebp), %eax
	testw	%ax, %ax
	jne	L265
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-36(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-28(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-48(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-60(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-52(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-72(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-68(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-64(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -84(%ebp)
	je	L276
	movl	-60(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -4(%ebp)
	movl	-56(%ebp), %eax
	movl	%eax, -8(%ebp)
	jmp	L267
L268:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -8(%ebp)
L267:
	movl	-56(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	leal	3(%eax), %edx
	movl	-8(%ebp), %eax
	cmpl	%eax, %edx
	ja	L268
	movl	-56(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -20(%ebp)
	movl	-60(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -4(%ebp)
	jmp	L269
L270:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -4(%ebp)
L269:
	movl	-60(%ebp), %eax
	movl	-52(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-4(%ebp), %eax
	cmpl	%eax, %edx
	ja	L270
	jmp	L276
L265:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-36(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-28(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-48(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-60(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-52(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-72(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-68(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-64(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -84(%ebp)
	je	L276
	movl	-60(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -12(%ebp)
	movl	-56(%ebp), %eax
	movl	%eax, -16(%ebp)
	jmp	L272
L273:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -16(%ebp)
L272:
	movl	-56(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	leal	-4(%eax), %edx
	movl	-16(%ebp), %eax
	cmpl	%eax, %edx
	ja	L273
	movl	-56(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -24(%ebp)
	movl	-60(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -12(%ebp)
	jmp	L274
L275:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -12(%ebp)
L274:
	movl	-60(%ebp), %eax
	movl	-52(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-12(%ebp), %eax
	cmpl	%eax, %edx
	ja	L275
L276:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE41:
	.globl	_dessineMotif4
	.def	_dessineMotif4;	.scl	2;	.type	32;	.endef
_dessineMotif4:
LFB42:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$120, %esp
	movl	44(%ebp), %eax
	movb	%al, -84(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -36(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -32(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -28(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -48(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -44(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -40(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -60(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -56(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -52(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -72(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -68(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -64(%ebp)
	movzwl	22(%ebp), %eax
	testw	%ax, %ax
	jne	L278
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-36(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-28(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-48(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-60(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-52(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-72(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-68(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-64(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -84(%ebp)
	je	L289
	movl	-60(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -4(%ebp)
	movl	-56(%ebp), %eax
	movl	-52(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movl	%eax, -8(%ebp)
	jmp	L280
L281:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	subl	$1, -8(%ebp)
L280:
	movl	-56(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	leal	-4(%eax), %edx
	movl	-8(%ebp), %eax
	cmpl	%eax, %edx
	jb	L281
	movl	-56(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -20(%ebp)
	movl	-60(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -4(%ebp)
	jmp	L282
L283:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -4(%ebp)
L282:
	movl	-60(%ebp), %eax
	movl	-52(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-4(%ebp), %eax
	cmpl	%eax, %edx
	ja	L283
	jmp	L289
L278:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-36(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-28(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-48(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-60(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-52(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-72(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-68(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-64(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -84(%ebp)
	je	L289
	movl	-60(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -12(%ebp)
	movl	-56(%ebp), %eax
	movl	-52(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movl	%eax, -16(%ebp)
	jmp	L285
L286:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	subl	$1, -16(%ebp)
L285:
	movl	-56(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	leal	3(%eax), %edx
	movl	-16(%ebp), %eax
	cmpl	%eax, %edx
	jb	L286
	movl	-56(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -24(%ebp)
	movl	-60(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -12(%ebp)
	jmp	L287
L288:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -12(%ebp)
L287:
	movl	-60(%ebp), %eax
	movl	-52(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-12(%ebp), %eax
	cmpl	%eax, %edx
	ja	L288
L289:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE42:
	.globl	_dessineMotif5
	.def	_dessineMotif5;	.scl	2;	.type	32;	.endef
_dessineMotif5:
LFB43:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$120, %esp
	movl	44(%ebp), %eax
	movb	%al, -84(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -40(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -36(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -32(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -52(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -48(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -44(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -64(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -60(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -56(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -76(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -72(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -68(%ebp)
	movzwl	22(%ebp), %eax
	testw	%ax, %ax
	jne	L291
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-40(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-36(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-52(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-48(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-64(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-76(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-72(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-68(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -84(%ebp)
	je	L302
	movl	-60(%ebp), %edx
	movl	-56(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -4(%ebp)
	movl	-64(%ebp), %eax
	movl	%eax, -8(%ebp)
	jmp	L293
L294:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -8(%ebp)
L293:
	movl	-64(%ebp), %edx
	movl	-56(%ebp), %eax
	addl	%edx, %eax
	leal	3(%eax), %edx
	movl	-8(%ebp), %eax
	cmpl	%eax, %edx
	ja	L294
	movl	-64(%ebp), %edx
	movl	-56(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -20(%ebp)
	movl	-60(%ebp), %edx
	movl	-56(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -4(%ebp)
	jmp	L295
L296:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -4(%ebp)
L295:
	movl	-60(%ebp), %eax
	movl	-56(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-4(%ebp), %eax
	cmpl	%eax, %edx
	ja	L296
	jmp	L302
L291:
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-40(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-36(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-52(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-48(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-64(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-76(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-72(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-68(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -84(%ebp)
	je	L302
	movl	-60(%ebp), %edx
	movl	-56(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -24(%ebp)
	movl	-64(%ebp), %eax
	movl	%eax, -12(%ebp)
	jmp	L298
L299:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -12(%ebp)
L298:
	movl	-64(%ebp), %edx
	movl	-56(%ebp), %eax
	addl	%edx, %eax
	leal	-4(%eax), %edx
	movl	-12(%ebp), %eax
	cmpl	%eax, %edx
	ja	L299
	movl	-64(%ebp), %edx
	movl	-56(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -28(%ebp)
	movl	-60(%ebp), %edx
	movl	-56(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -16(%ebp)
	jmp	L300
L301:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-28(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-28(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-28(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-28(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -16(%ebp)
L300:
	movl	-60(%ebp), %eax
	movl	-56(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-16(%ebp), %eax
	cmpl	%eax, %edx
	ja	L301
L302:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE43:
	.globl	_dessineMotif6
	.def	_dessineMotif6;	.scl	2;	.type	32;	.endef
_dessineMotif6:
LFB44:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$120, %esp
	movl	44(%ebp), %eax
	movb	%al, -84(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -44(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -40(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -36(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -56(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -52(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -48(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -68(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -64(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -60(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -80(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -76(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -72(%ebp)
	movzwl	22(%ebp), %eax
	testw	%ax, %ax
	jne	L304
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-44(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-36(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-56(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-52(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-48(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-68(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-64(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-80(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-76(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-72(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -84(%ebp)
	je	L315
	movl	-64(%ebp), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -20(%ebp)
	movl	-68(%ebp), %eax
	movl	%eax, -4(%ebp)
	jmp	L306
L307:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -4(%ebp)
L306:
	movl	-68(%ebp), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	leal	3(%eax), %edx
	movl	-4(%ebp), %eax
	cmpl	%eax, %edx
	ja	L307
	movl	-68(%ebp), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -24(%ebp)
	movl	-64(%ebp), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -8(%ebp)
	jmp	L308
L309:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	subl	$1, -8(%ebp)
L308:
	movl	-64(%ebp), %edx
	movl	-8(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L309
	jmp	L315
L304:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-44(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-36(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-56(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-52(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-48(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-68(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-64(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-80(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-76(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-72(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -84(%ebp)
	je	L315
	movl	-64(%ebp), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -28(%ebp)
	movl	-68(%ebp), %eax
	movl	%eax, -12(%ebp)
	jmp	L311
L312:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-28(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-28(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-28(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-28(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -12(%ebp)
L311:
	movl	-68(%ebp), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	leal	-4(%eax), %edx
	movl	-12(%ebp), %eax
	cmpl	%eax, %edx
	ja	L312
	movl	-68(%ebp), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -32(%ebp)
	movl	-64(%ebp), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -16(%ebp)
	jmp	L313
L314:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-32(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-32(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-32(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-32(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	subl	$1, -16(%ebp)
L313:
	movl	-64(%ebp), %edx
	movl	-16(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L314
L315:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE44:
	.globl	_dessineMotifNoeud
	.def	_dessineMotifNoeud;	.scl	2;	.type	32;	.endef
_dessineMotifNoeud:
LFB45:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$64, %esp
	movl	28(%ebp), %eax
	movb	%al, -20(%ebp)
	movl	20(%ebp), %eax
	movl	28(%eax), %eax
	movl	%eax, -12(%ebp)
	movl	20(%ebp), %eax
	movl	32(%eax), %eax
	movl	%eax, -8(%ebp)
	movl	$32, -4(%ebp)
	cmpl	$6, 24(%ebp)
	ja	L327
	movl	24(%ebp), %eax
	sall	$2, %eax
	addl	$L319, %eax
	movl	(%eax), %eax
	jmp	*%eax
	.section .rdata,"dr"
	.align 4
L319:
	.long	L325
	.long	L324
	.long	L323
	.long	L322
	.long	L321
	.long	L320
	.long	L318
	.text
L325:
	movzbl	-20(%ebp), %eax
	movl	%eax, 36(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-8(%ebp), %eax
	movl	%eax, 28(%esp)
	movl	-4(%ebp), %eax
	movl	%eax, 32(%esp)
	movl	20(%ebp), %eax
	movl	16(%eax), %edx
	movl	%edx, 12(%esp)
	movl	20(%eax), %edx
	movl	%edx, 16(%esp)
	movl	24(%eax), %eax
	movl	%eax, 20(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineMotif0
	jmp	L326
L324:
	movzbl	-20(%ebp), %eax
	movl	%eax, 36(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-8(%ebp), %eax
	movl	%eax, 28(%esp)
	movl	-4(%ebp), %eax
	movl	%eax, 32(%esp)
	movl	20(%ebp), %eax
	movl	16(%eax), %edx
	movl	%edx, 12(%esp)
	movl	20(%eax), %edx
	movl	%edx, 16(%esp)
	movl	24(%eax), %eax
	movl	%eax, 20(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineMotif1
	jmp	L326
L323:
	movzbl	-20(%ebp), %eax
	movl	%eax, 36(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-8(%ebp), %eax
	movl	%eax, 28(%esp)
	movl	-4(%ebp), %eax
	movl	%eax, 32(%esp)
	movl	20(%ebp), %eax
	movl	16(%eax), %edx
	movl	%edx, 12(%esp)
	movl	20(%eax), %edx
	movl	%edx, 16(%esp)
	movl	24(%eax), %eax
	movl	%eax, 20(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineMotif2
	jmp	L326
L322:
	movzbl	-20(%ebp), %eax
	movl	%eax, 36(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-8(%ebp), %eax
	movl	%eax, 28(%esp)
	movl	-4(%ebp), %eax
	movl	%eax, 32(%esp)
	movl	20(%ebp), %eax
	movl	16(%eax), %edx
	movl	%edx, 12(%esp)
	movl	20(%eax), %edx
	movl	%edx, 16(%esp)
	movl	24(%eax), %eax
	movl	%eax, 20(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineMotif3
	jmp	L326
L321:
	movzbl	-20(%ebp), %eax
	movl	%eax, 36(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-8(%ebp), %eax
	movl	%eax, 28(%esp)
	movl	-4(%ebp), %eax
	movl	%eax, 32(%esp)
	movl	20(%ebp), %eax
	movl	16(%eax), %edx
	movl	%edx, 12(%esp)
	movl	20(%eax), %edx
	movl	%edx, 16(%esp)
	movl	24(%eax), %eax
	movl	%eax, 20(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineMotif4
	jmp	L326
L320:
	movzbl	-20(%ebp), %eax
	movl	%eax, 36(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-8(%ebp), %eax
	movl	%eax, 28(%esp)
	movl	-4(%ebp), %eax
	movl	%eax, 32(%esp)
	movl	20(%ebp), %eax
	movl	16(%eax), %edx
	movl	%edx, 12(%esp)
	movl	20(%eax), %edx
	movl	%edx, 16(%esp)
	movl	24(%eax), %eax
	movl	%eax, 20(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineMotif5
	jmp	L326
L318:
	movzbl	-20(%ebp), %eax
	movl	%eax, 36(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-8(%ebp), %eax
	movl	%eax, 28(%esp)
	movl	-4(%ebp), %eax
	movl	%eax, 32(%esp)
	movl	20(%ebp), %eax
	movl	16(%eax), %edx
	movl	%edx, 12(%esp)
	movl	20(%eax), %edx
	movl	%edx, 16(%esp)
	movl	24(%eax), %eax
	movl	%eax, 20(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineMotif6
	jmp	L326
L327:
	nop
L326:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE45:
	.globl	_dessineCarte
	.def	_dessineCarte;	.scl	2;	.type	32;	.endef
_dessineCarte:
LFB46:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$56, %esp
	movl	24(%ebp), %eax
	movb	%al, -12(%ebp)
	cmpl	$0, 20(%ebp)
	je	L331
	movzbl	-12(%ebp), %edx
	movl	20(%ebp), %eax
	movzwl	16(%eax), %eax
	cwtl
	movl	%edx, 20(%esp)
	movl	%eax, 16(%esp)
	movl	20(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineMotifNoeud
	movzbl	-12(%ebp), %edx
	movl	20(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 16(%esp)
	movl	%eax, 12(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarte
	movzbl	-12(%ebp), %edx
	movl	20(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, 16(%esp)
	movl	%eax, 12(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarte
	movzbl	-12(%ebp), %edx
	movl	20(%ebp), %eax
	movl	8(%eax), %eax
	movl	%edx, 16(%esp)
	movl	%eax, 12(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarte
	movzbl	-12(%ebp), %edx
	movl	20(%ebp), %eax
	movl	12(%eax), %eax
	movl	%edx, 16(%esp)
	movl	%eax, 12(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarte
	jmp	L328
L331:
	nop
L328:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE46:
	.section .rdata,"dr"
LC4:
	.ascii "w\0"
	.align 4
LC5:
	.ascii "./resources/file/description_robopath.txt\0"
LC6:
	.ascii "%d mouvements\12\0"
LC7:
	.ascii "Depart: x=%d y=%d\12\0"
LC8:
	.ascii "Arrivee: x=%d y=%d\0"
LC9:
	.ascii "\12%c\0"
	.text
	.globl	_sauveDescriptionChemin
	.def	_sauveDescriptionChemin;	.scl	2;	.type	32;	.endef
_sauveDescriptionChemin:
LFB47:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$LC4, 4(%esp)
	movl	$LC5, (%esp)
	call	_fopen
	movl	%eax, -24(%ebp)
	movl	$0, -12(%ebp)
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -16(%ebp)
	jmp	L333
L334:
	addl	$1, -12(%ebp)
	movl	-16(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -16(%ebp)
L333:
	cmpl	$0, -16(%ebp)
	jne	L334
	movl	-12(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC6, 4(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintf
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC7, 4(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintf
	movl	8(%ebp), %eax
	movl	24(%eax), %edx
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC8, 4(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintf
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -16(%ebp)
	jmp	L335
L344:
	movl	-16(%ebp), %eax
	movl	8(%eax), %ecx
	movl	-16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	(%ecx), %eax
	movl	4(%ecx), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$8, %eax
	ja	L336
	movl	L338(,%eax,4), %eax
	jmp	*%eax
	.section .rdata,"dr"
	.align 4
L338:
	.long	L336
	.long	L342
	.long	L336
	.long	L341
	.long	L336
	.long	L340
	.long	L336
	.long	L339
	.long	L337
	.text
L342:
	movb	$49, -17(%ebp)
	jmp	L343
L341:
	movb	$51, -17(%ebp)
	jmp	L343
L340:
	movb	$50, -17(%ebp)
	jmp	L343
L339:
	movb	$52, -17(%ebp)
	jmp	L343
L337:
	movl	-16(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -16(%ebp)
	jmp	L335
L336:
	movb	$63, -17(%ebp)
	nop
L343:
	movsbl	-17(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintf
	movl	-16(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -16(%ebp)
L335:
	movl	-16(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L344
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE47:
	.section .rdata,"dr"
	.align 8
LC0:
	.long	858993459
	.long	1070281523
	.align 8
LC1:
	.long	-1972248982
	.long	1072096398
	.align 8
LC2:
	.long	1175103052
	.long	1068660005
	.ident	"GCC: (MinGW.org GCC-8.2.0-5) 8.2.0"
	.def	_malloc;	.scl	2;	.type	32;	.endef
	.def	_free;	.scl	2;	.type	32;	.endef
	.def	_estCouleurUniforme;	.scl	2;	.type	32;	.endef
	.def	_compareCouleur;	.scl	2;	.type	32;	.endef
	.def	_estCouleurUniformeDansCarre;	.scl	2;	.type	32;	.endef
	.def	_fopen;	.scl	2;	.type	32;	.endef
	.def	_fprintf;	.scl	2;	.type	32;	.endef
	.def	_fclose;	.scl	2;	.type	32;	.endef
