	.file	"AppState.c"
	.text
	.section .rdata,"dr"
LC0:
	.ascii "Projet Librairie Graphique\0"
LC1:
	.ascii "./resources/font/sans.ttf\0"
LC2:
	.ascii "Example home page\0"
LC3:
	.ascii "Bouton 2\0"
LC4:
	.ascii "Retour home\0"
LC5:
	.ascii "Auteur - VOLTO Christophe\0"
LC6:
	.ascii "cant init\0"
LC7:
	.ascii "Clean exit :) .\0"
	.text
	.globl	_SDL_main
	.def	_SDL_main;	.scl	2;	.type	32;	.endef
_SDL_main:
LFB13:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	subl	$188, %esp
	.cfi_offset 7, -12
	.cfi_offset 6, -16
	.cfi_offset 3, -20
	movl	$LC0, -56(%ebp)
	movl	$805240832, -52(%ebp)
	movl	$805240832, -48(%ebp)
	movl	$900, -44(%ebp)
	movl	$900, -40(%ebp)
	movl	$0, -36(%ebp)
	movl	$4, -32(%ebp)
	movb	$-1, %dl
	movl	$-1, %eax
	movb	%al, %dh
	movl	%edx, %eax
	orl	$16711680, %eax
	movl	%eax, %edx
	movl	%edx, %eax
	orl	$-16777216, %eax
	movl	%eax, %edx
	movb	$0, %cl
	movl	$0, %eax
	movb	%al, %ch
	movl	%ecx, %eax
	andl	$-16711681, %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	andl	$16777215, %eax
	movl	%eax, %ecx
	movb	$0, %bl
	movl	$0, %eax
	movb	%al, %bh
	movl	%ebx, %eax
	andl	$-16711681, %eax
	movl	%eax, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ebx
	movl	%edx, 44(%esp)
	movl	%ecx, 40(%esp)
	movl	%ebx, 36(%esp)
	movl	$18, 32(%esp)
	movl	$LC1, 28(%esp)
	movl	$0, 24(%esp)
	movl	$0, 20(%esp)
	movl	$802, 16(%esp)
	movl	$646, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initTextArea
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initTextAreas
	movl	%eax, %edx
	movb	$0, -136(%ebp)
	movl	$0, %eax
	movl	-136(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ecx
	movb	$0, -132(%ebp)
	movl	$0, %eax
	movl	-132(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	%edx, 40(%esp)
	movl	$0, 36(%esp)
	movl	$0, 32(%esp)
	movl	$2, 28(%esp)
	movl	%ecx, 24(%esp)
	movl	%eax, 20(%esp)
	movl	$802, 16(%esp)
	movl	$652, 12(%esp)
	movl	$98, 8(%esp)
	movl	$0, 4(%esp)
	movl	$0, (%esp)
	call	_initFrame
	movl	%eax, %edi
	movb	$0, -128(%ebp)
	movl	$0, %eax
	movl	-128(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	andl	$16777215, %eax
	movl	%eax, %ecx
	movb	$0, -124(%ebp)
	movl	$0, %eax
	movl	-124(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	andl	$16777215, %eax
	movl	%eax, %edx
	movb	$0, -120(%ebp)
	movl	$0, %eax
	movl	-120(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	movl	%eax, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ebx
	movl	$0, 48(%esp)
	movl	$1, 44(%esp)
	movl	%ecx, 40(%esp)
	movl	%edx, 36(%esp)
	movl	%ebx, 32(%esp)
	movl	$25, 28(%esp)
	movl	$LC1, 24(%esp)
	movl	$LC2, 20(%esp)
	movl	$0, 16(%esp)
	movl	$802, 12(%esp)
	movl	$652, 8(%esp)
	movl	$0, 4(%esp)
	movl	$0, (%esp)
	call	_initButton
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initButtons
	movl	%eax, %edx
	movb	$0, -116(%ebp)
	movl	$0, %eax
	movl	-116(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ecx
	movb	$-56, -112(%ebp)
	movl	$-56, %eax
	movl	-112(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$13107200, %eax
	orl	$-16777216, %eax
	movl	$0, 40(%esp)
	movl	%edx, 36(%esp)
	movl	$0, 32(%esp)
	movl	$2, 28(%esp)
	movl	%ecx, 24(%esp)
	movl	%eax, 20(%esp)
	movl	$802, 16(%esp)
	movl	$652, 12(%esp)
	movl	$98, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, %esi
	movb	$106, -76(%ebp)
	movl	$106, %eax
	movl	-76(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	orl	$6946816, %eax
	orl	$-16777216, %eax
	movl	%eax, -76(%ebp)
	movb	$-16, -108(%ebp)
	movl	$-16, %eax
	movl	-108(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$15728640, %eax
	orl	$-16777216, %eax
	movl	%eax, %ecx
	movb	$0, -104(%ebp)
	movl	$0, %eax
	movl	-104(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, %ebx
	movl	$_affiche_textfield_win_callback, 48(%esp)
	movl	$1, 44(%esp)
	movl	-76(%ebp), %eax
	movl	%eax, 40(%esp)
	movl	%ecx, 36(%esp)
	movl	%ebx, 32(%esp)
	movl	$15, 28(%esp)
	movl	$LC1, 24(%esp)
	movl	$LC3, 20(%esp)
	movl	$2, 16(%esp)
	movl	$70, 12(%esp)
	movl	$200, 8(%esp)
	movl	$167, 4(%esp)
	movl	$25, (%esp)
	call	_initButton
	movl	%eax, %ebx
	movb	$106, -72(%ebp)
	movl	$106, %eax
	movl	-72(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$6946816, %eax
	orl	$-16777216, %eax
	movl	%eax, -72(%ebp)
	movb	$-16, -68(%ebp)
	movl	$-16, %eax
	movl	-68(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	orl	$15728640, %eax
	orl	$-16777216, %eax
	movl	%eax, -68(%ebp)
	movb	$0, -100(%ebp)
	movl	$0, %eax
	movl	-100(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	movl	%eax, %edx
	orl	$-16777216, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	movl	$_affiche_custom_win_callback, 48(%esp)
	movl	$1, 44(%esp)
	movl	-72(%ebp), %ecx
	movl	%ecx, 40(%esp)
	movl	-68(%ebp), %eax
	movl	%eax, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$15, 28(%esp)
	movl	$LC1, 24(%esp)
	movl	$LC4, 20(%esp)
	movl	$2, 16(%esp)
	movl	$70, 12(%esp)
	movl	$200, 8(%esp)
	movl	$33, 4(%esp)
	movl	$25, (%esp)
	call	_initButton
	movl	%ebx, 8(%esp)
	movl	%eax, 4(%esp)
	movl	$2, (%esp)
	call	_initButtons
	movl	%eax, %edx
	movb	$0, -96(%ebp)
	movl	$0, %eax
	movl	-96(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ecx
	movb	$-16, -92(%ebp)
	movl	$-16, %eax
	movl	-92(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$15728640, %eax
	orl	$-16777216, %eax
	movl	$0, 40(%esp)
	movl	%edx, 36(%esp)
	movl	$0, 32(%esp)
	movl	$2, 28(%esp)
	movl	%ecx, 24(%esp)
	movl	%eax, 20(%esp)
	movl	$900, 16(%esp)
	movl	$250, 12(%esp)
	movl	$98, 8(%esp)
	movl	$650, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, %ebx
	movb	$0, -64(%ebp)
	movl	$0, %eax
	movl	-64(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	andl	$16777215, %eax
	movl	%eax, -64(%ebp)
	movb	$0, -88(%ebp)
	movl	$0, %eax
	movl	-88(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	andl	$16777215, %eax
	movl	%eax, %ecx
	movb	$0, -84(%ebp)
	movl	$0, %eax
	movl	-84(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	orl	$-16777216, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	movl	$0, 48(%esp)
	movl	$1, 44(%esp)
	movl	-64(%ebp), %eax
	movl	%eax, 40(%esp)
	movl	%ecx, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$25, 28(%esp)
	movl	$LC1, 24(%esp)
	movl	$LC5, 20(%esp)
	movl	$0, 16(%esp)
	movl	$100, 12(%esp)
	movl	$900, 8(%esp)
	movl	$0, 4(%esp)
	movl	$0, (%esp)
	call	_initButton
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initButtons
	movl	%eax, %edx
	movb	$0, -60(%ebp)
	movl	$0, %eax
	movl	-60(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	%eax, -60(%ebp)
	movb	$-16, -80(%ebp)
	movl	$-16, %eax
	movl	-80(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$15728640, %eax
	orl	$-16777216, %eax
	movl	$0, 40(%esp)
	movl	%edx, 36(%esp)
	movl	$0, 32(%esp)
	movl	$2, 28(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 24(%esp)
	movl	%eax, 20(%esp)
	movl	$100, 16(%esp)
	movl	$900, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%edi, 16(%esp)
	movl	%esi, 12(%esp)
	movl	%ebx, 8(%esp)
	movl	%eax, 4(%esp)
	movl	$4, (%esp)
	call	_initFrames
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initView
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initViews
	movl	%eax, -28(%ebp)
	movl	-56(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-52(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-48(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-40(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-36(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, 28(%esp)
	movl	-28(%ebp), %eax
	movl	%eax, 32(%esp)
	movl	$1, (%esp)
	call	_initialization
	movl	_appState, %eax
	testl	%eax, %eax
	jne	L3
	movl	$LC6, (%esp)
	call	_SDL_ExitWithError
	jmp	L3
L5:
	movl	_appState, %eax
	addl	$8, %eax
	movl	%eax, (%esp)
	call	_SDL_WaitEvent
	testl	%eax, %eax
	je	L4
	call	_eventHandler
L4:
	call	_update
L3:
	movl	_appState, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L5
	call	_cleanEverything
	movl	$LC7, (%esp)
	call	_SDL_Log
	movl	$0, %eax
	addl	$188, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%edi
	.cfi_restore 7
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE13:
	.ident	"GCC: (MinGW.org GCC-8.2.0-5) 8.2.0"
	.def	_initTextArea;	.scl	2;	.type	32;	.endef
	.def	_initTextAreas;	.scl	2;	.type	32;	.endef
	.def	_initFrame;	.scl	2;	.type	32;	.endef
	.def	_initButton;	.scl	2;	.type	32;	.endef
	.def	_initButtons;	.scl	2;	.type	32;	.endef
	.def	_affiche_textfield_win_callback;	.scl	2;	.type	32;	.endef
	.def	_affiche_custom_win_callback;	.scl	2;	.type	32;	.endef
	.def	_initFrames;	.scl	2;	.type	32;	.endef
	.def	_initView;	.scl	2;	.type	32;	.endef
	.def	_initViews;	.scl	2;	.type	32;	.endef
	.def	_initialization;	.scl	2;	.type	32;	.endef
	.def	_SDL_ExitWithError;	.scl	2;	.type	32;	.endef
	.def	_SDL_WaitEvent;	.scl	2;	.type	32;	.endef
	.def	_eventHandler;	.scl	2;	.type	32;	.endef
	.def	_update;	.scl	2;	.type	32;	.endef
	.def	_cleanEverything;	.scl	2;	.type	32;	.endef
	.def	_SDL_Log;	.scl	2;	.type	32;	.endef
