	.file	"FonctionsPartie4.c"
	.text
	.globl	_compareCouleur
	.def	_compareCouleur;	.scl	2;	.type	32;	.endef
_compareCouleur:
LFB17:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	movzwl	8(%ebp), %edx
	movzwl	16(%ebp), %eax
	cmpw	%ax, %dx
	jne	L2
	movzwl	10(%ebp), %edx
	movzwl	18(%ebp), %eax
	cmpw	%ax, %dx
	jne	L2
	movzwl	12(%ebp), %edx
	movzwl	20(%ebp), %eax
	cmpw	%ax, %dx
	jne	L2
	movl	$1, %eax
	jmp	L3
L2:
	movl	$0, %eax
L3:
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE17:
	.globl	_compareCouleurVariadic
	.def	_compareCouleurVariadic;	.scl	2;	.type	32;	.endef
_compareCouleurVariadic:
LFB18:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movzwl	8(%ebp), %edx
	movzwl	16(%ebp), %eax
	cmpw	%ax, %dx
	jne	L5
	movzwl	10(%ebp), %edx
	movzwl	18(%ebp), %eax
	cmpw	%ax, %dx
	jne	L5
	movzwl	12(%ebp), %edx
	movzwl	20(%ebp), %eax
	cmpw	%ax, %dx
	je	L6
L5:
	movl	$0, %eax
	jmp	L12
L6:
	leal	28(%ebp), %eax
	movl	%eax, -8(%ebp)
	movl	$0, -4(%ebp)
	jmp	L8
L11:
	movl	-8(%ebp), %eax
	leal	8(%eax), %edx
	movl	%edx, -8(%ebp)
	movl	(%eax), %edx
	movl	%edx, -14(%ebp)
	movzwl	4(%eax), %eax
	movw	%ax, -10(%ebp)
	movzwl	8(%ebp), %edx
	movzwl	-14(%ebp), %eax
	cmpw	%ax, %dx
	jne	L9
	movzwl	10(%ebp), %edx
	movzwl	-12(%ebp), %eax
	cmpw	%ax, %dx
	jne	L9
	movzwl	12(%ebp), %edx
	movzwl	-10(%ebp), %eax
	cmpw	%ax, %dx
	je	L10
L9:
	movl	$0, %eax
	jmp	L12
L10:
	addl	$1, -4(%ebp)
L8:
	movl	-4(%ebp), %eax
	cmpl	24(%ebp), %eax
	jl	L11
	movl	$1, %eax
L12:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE18:
	.globl	_sontDesCouleursVariadic
	.def	_sontDesCouleursVariadic;	.scl	2;	.type	32;	.endef
_sontDesCouleursVariadic:
LFB19:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$48, %esp
	leal	12(%ebp), %eax
	movl	%eax, -20(%ebp)
	movl	$0, -4(%ebp)
	jmp	L14
L17:
	movw	$-1, -16(%ebp)
	movw	$-1, -14(%ebp)
	movw	$-1, -12(%ebp)
	movl	-20(%ebp), %eax
	leal	8(%eax), %edx
	movl	%edx, -20(%ebp)
	movl	(%eax), %edx
	movl	%edx, -10(%ebp)
	movzwl	4(%eax), %eax
	movw	%ax, -6(%ebp)
	movl	-16(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-12(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-10(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-6(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	testb	%al, %al
	je	L15
	movl	$0, %eax
	jmp	L18
L15:
	addl	$1, -4(%ebp)
L14:
	movl	-4(%ebp), %eax
	cmpl	8(%ebp), %eax
	jl	L17
	movl	$1, %eax
L18:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE19:
	.globl	_comparePalette
	.def	_comparePalette;	.scl	2;	.type	32;	.endef
_comparePalette:
LFB20:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%ebx
	subl	$32, %esp
	.cfi_offset 3, -12
	movl	8(%ebp), %edx
	movl	16(%ebp), %eax
	cmpl	%eax, %edx
	je	L20
	movl	$0, %eax
	jmp	L21
L20:
	movl	$0, -8(%ebp)
	jmp	L22
L24:
	movl	20(%ebp), %ecx
	movl	-8(%ebp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	leal	(%ecx,%eax), %edx
	movl	12(%ebp), %ebx
	movl	-8(%ebp), %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%ebx, %eax
	movl	(%edx), %ecx
	movl	%ecx, 8(%esp)
	movzwl	4(%edx), %edx
	movw	%dx, 12(%esp)
	movl	(%eax), %edx
	movl	%edx, (%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L23
	movl	$0, %eax
	jmp	L21
L23:
	addl	$1, -8(%ebp)
L22:
	movl	8(%ebp), %eax
	cmpl	%eax, -8(%ebp)
	jl	L24
	movl	$1, %eax
L21:
	addl	$32, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE20:
	.globl	_compareMotif
	.def	_compareMotif;	.scl	2;	.type	32;	.endef
_compareMotif:
LFB21:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	movzwl	8(%ebp), %edx
	movzwl	20(%ebp), %eax
	cmpw	%ax, %dx
	jne	L26
	movzwl	10(%ebp), %edx
	movzwl	22(%ebp), %eax
	cmpw	%ax, %dx
	jne	L26
	movl	12(%ebp), %edx
	movl	24(%ebp), %eax
	cmpl	%eax, %edx
	jne	L26
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jne	L26
	movl	$1, %eax
	jmp	L27
L26:
	movl	$0, %eax
L27:
	andl	$1, %eax
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE21:
	.globl	_estCouleurDans
	.def	_estCouleurDans;	.scl	2;	.type	32;	.endef
_estCouleurDans:
LFB22:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$32, %esp
	cmpl	$0, 8(%ebp)
	jne	L30
	movl	$0, %eax
	jmp	L31
L30:
	movl	$0, -4(%ebp)
	jmp	L32
L34:
	movl	-4(%ebp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	16(%ebp), %edx
	movl	%edx, 8(%esp)
	movzwl	20(%ebp), %edx
	movw	%dx, 12(%esp)
	movl	(%eax), %edx
	movl	%edx, (%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	testb	%al, %al
	je	L33
	movl	$1, %eax
	jmp	L31
L33:
	addl	$1, -4(%ebp)
L32:
	movl	-4(%ebp), %eax
	cmpl	12(%ebp), %eax
	jl	L34
	movl	$0, %eax
L31:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE22:
	.globl	_creePalette
	.def	_creePalette;	.scl	2;	.type	32;	.endef
_creePalette:
LFB23:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	subl	$76, %esp
	.cfi_offset 7, -12
	.cfi_offset 6, -16
	.cfi_offset 3, -20
	movl	$8, (%esp)
	call	_malloc
	movl	%eax, -44(%ebp)
	movl	-44(%ebp), %eax
	movl	$0, (%eax)
	movl	-44(%ebp), %eax
	movl	$0, 4(%eax)
	movl	$0, -40(%ebp)
	jmp	L36
L44:
	movl	$0, -36(%ebp)
	jmp	L37
L43:
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-40(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-36(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -58(%ebp)
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-40(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-36(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -56(%ebp)
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-40(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-36(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -54(%ebp)
	movl	-44(%ebp), %eax
	movl	(%eax), %edx
	movl	-44(%ebp), %eax
	movl	4(%eax), %eax
	movl	-58(%ebp), %ecx
	movl	%ecx, 8(%esp)
	movzwl	-54(%ebp), %ecx
	movw	%cx, 12(%esp)
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_estCouleurDans
	xorl	$1, %eax
	testb	%al, %al
	je	L38
	movl	%esp, %eax
	movl	%eax, %edi
	movl	-44(%ebp), %eax
	movl	(%eax), %ecx
	leal	-1(%ecx), %eax
	movl	%eax, -48(%ebp)
	movl	%ecx, %eax
	movl	$0, %edx
	imull	$48, %edx, %esi
	imull	$0, %eax, %ebx
	addl	%esi, %ebx
	movl	$48, %esi
	mull	%esi
	addl	%edx, %ebx
	movl	%ebx, %edx
	movl	%ecx, %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	movl	%ecx, %eax
	movl	$0, %edx
	imull	$48, %edx, %esi
	imull	$0, %eax, %ebx
	addl	%esi, %ebx
	movl	$48, %esi
	mull	%esi
	addl	%edx, %ebx
	movl	%ebx, %edx
	movl	%ecx, %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movl	$16, %eax
	subl	$1, %eax
	addl	%edx, %eax
	movl	$16, %esi
	movl	$0, %edx
	divl	%esi
	imull	$16, %eax, %eax
	call	___chkstk_ms
	subl	%eax, %esp
	leal	16(%esp), %eax
	addl	$1, %eax
	shrl	%eax
	addl	%eax, %eax
	movl	%eax, -52(%ebp)
	movl	$0, -32(%ebp)
	jmp	L39
L40:
	movl	-44(%ebp), %eax
	movl	4(%eax), %ecx
	movl	-32(%ebp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	leal	(%ecx,%eax), %edx
	movl	-52(%ebp), %ebx
	movl	-32(%ebp), %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%ebx, %eax
	movl	(%edx), %ecx
	movl	%ecx, (%eax)
	movzwl	4(%edx), %edx
	movw	%dx, 4(%eax)
	addl	$1, -32(%ebp)
L39:
	movl	-44(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -32(%ebp)
	jl	L40
	movl	-44(%ebp), %eax
	movl	(%eax), %eax
	leal	1(%eax), %edx
	movl	-44(%ebp), %eax
	movl	%edx, (%eax)
	movl	-44(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movl	-44(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_realloc
	movl	%eax, %edx
	movl	-44(%ebp), %eax
	movl	%edx, 4(%eax)
	movl	$0, -28(%ebp)
	jmp	L41
L42:
	movl	-44(%ebp), %eax
	movl	4(%eax), %ecx
	movl	-28(%ebp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	leal	(%ecx,%eax), %edx
	movl	-52(%ebp), %ebx
	movl	-28(%ebp), %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%ebx, %eax
	movl	(%eax), %ecx
	movl	%ecx, (%edx)
	movzwl	4(%eax), %eax
	movw	%ax, 4(%edx)
	addl	$1, -28(%ebp)
L41:
	movl	-44(%ebp), %eax
	movl	(%eax), %eax
	subl	$1, %eax
	cmpl	%eax, -28(%ebp)
	jl	L42
	movl	-44(%ebp), %eax
	movl	4(%eax), %edx
	movl	-44(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	subl	$6, %eax
	addl	%edx, %eax
	movl	8(%ebp), %edx
	movl	8(%edx), %edx
	movl	-40(%ebp), %ecx
	sall	$2, %ecx
	addl	%ecx, %edx
	movl	(%edx), %edx
	movl	-36(%ebp), %ecx
	addl	%ecx, %ecx
	addl	%ecx, %edx
	movzwl	(%edx), %ebx
	movl	8(%ebp), %edx
	movl	12(%edx), %edx
	movl	-40(%ebp), %ecx
	sall	$2, %ecx
	addl	%ecx, %edx
	movl	(%edx), %edx
	movl	-36(%ebp), %ecx
	addl	%ecx, %ecx
	addl	%ecx, %edx
	movzwl	(%edx), %ecx
	movl	8(%ebp), %edx
	movl	16(%edx), %edx
	movl	-40(%ebp), %esi
	sall	$2, %esi
	addl	%esi, %edx
	movl	(%edx), %edx
	movl	-36(%ebp), %esi
	addl	%esi, %esi
	addl	%esi, %edx
	movzwl	(%edx), %edx
	movw	%bx, (%eax)
	movw	%cx, 2(%eax)
	movw	%dx, 4(%eax)
	movl	%edi, %esp
L38:
	addl	$1, -36(%ebp)
L37:
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	cmpl	%eax, -36(%ebp)
	jl	L43
	addl	$1, -40(%ebp)
L36:
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -40(%ebp)
	jl	L44
	movl	-44(%ebp), %eax
	leal	-12(%ebp), %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%edi
	.cfi_restore 7
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE23:
	.section .rdata,"dr"
LC0:
	.ascii "Nombre de couleurs: %d\12\0"
LC1:
	.ascii "R:%d - V:%d - B:%d\12\0"
	.text
	.globl	_affichePalette
	.def	_affichePalette;	.scl	2;	.type	32;	.endef
_affichePalette:
LFB24:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%esi
	pushl	%ebx
	subl	$32, %esp
	.cfi_offset 6, -12
	.cfi_offset 3, -16
	cmpl	$0, 8(%ebp)
	je	L51
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, 4(%esp)
	movl	$LC0, (%esp)
	call	_printf
	movl	$0, -12(%ebp)
	jmp	L49
L50:
	movl	8(%ebp), %eax
	movl	4(%eax), %ecx
	movl	-12(%ebp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	movzwl	4(%eax), %eax
	movswl	%ax, %ebx
	movl	8(%ebp), %eax
	movl	4(%eax), %ecx
	movl	-12(%ebp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	movzwl	2(%eax), %eax
	movswl	%ax, %ecx
	movl	8(%ebp), %eax
	movl	4(%eax), %esi
	movl	-12(%ebp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	addl	%esi, %eax
	movzwl	(%eax), %eax
	cwtl
	movl	%ebx, 12(%esp)
	movl	%ecx, 8(%esp)
	movl	%eax, 4(%esp)
	movl	$LC1, (%esp)
	call	_printf
	addl	$1, -12(%ebp)
L49:
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -12(%ebp)
	jl	L50
	jmp	L46
L51:
	nop
L46:
	addl	$32, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE24:
	.globl	_estCouleurUniformeDansCarre
	.def	_estCouleurUniformeDansCarre;	.scl	2;	.type	32;	.endef
_estCouleurUniformeDansCarre:
LFB25:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$48, %esp
	movl	12(%ebp), %eax
	movl	8(%eax), %eax
	movl	16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	20(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -14(%ebp)
	movl	12(%ebp), %eax
	movl	12(%eax), %eax
	movl	16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	20(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -12(%ebp)
	movl	12(%ebp), %eax
	movl	16(%eax), %eax
	movl	16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	20(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -10(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -4(%ebp)
	jmp	L53
L64:
	movl	16(%ebp), %eax
	movl	%eax, -8(%ebp)
	jmp	L54
L60:
	movl	12(%ebp), %eax
	movl	8(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -20(%ebp)
	movl	12(%ebp), %eax
	movl	12(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -18(%ebp)
	movl	12(%ebp), %eax
	movl	16(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -16(%ebp)
	movl	-20(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-16(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-14(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-10(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L55
	movl	8(%ebp), %eax
	movw	$-1, (%eax)
	movl	8(%ebp), %eax
	movw	$-1, 2(%eax)
	movl	8(%ebp), %eax
	movw	$-1, 4(%eax)
	jmp	L52
L55:
	movl	16(%ebp), %eax
	cmpl	24(%ebp), %eax
	jle	L57
	movl	-8(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -8(%ebp)
	jmp	L54
L57:
	movl	-8(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -8(%ebp)
L54:
	movl	16(%ebp), %eax
	cmpl	24(%ebp), %eax
	jle	L58
	movl	-8(%ebp), %eax
	addl	$3, %eax
	cmpl	%eax, 24(%ebp)
	setle	%al
	jmp	L59
L58:
	movl	-8(%ebp), %eax
	subl	$3, %eax
	cmpl	%eax, 24(%ebp)
	setge	%al
L59:
	testb	%al, %al
	jne	L60
	movl	20(%ebp), %eax
	cmpl	28(%ebp), %eax
	jle	L61
	movl	-4(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -4(%ebp)
	jmp	L53
L61:
	movl	-4(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -4(%ebp)
L53:
	movl	20(%ebp), %eax
	cmpl	28(%ebp), %eax
	jle	L62
	movl	-4(%ebp), %eax
	addl	$3, %eax
	cmpl	%eax, 28(%ebp)
	setle	%al
	jmp	L63
L62:
	movl	-4(%ebp), %eax
	subl	$3, %eax
	cmpl	%eax, 28(%ebp)
	setge	%al
L63:
	testb	%al, %al
	jne	L64
	movl	8(%ebp), %eax
	movl	-14(%ebp), %edx
	movl	%edx, (%eax)
	movzwl	-10(%ebp), %edx
	movw	%dx, 4(%eax)
L52:
	movl	8(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE25:
	.globl	_estCouleurUniforme
	.def	_estCouleurUniforme;	.scl	2;	.type	32;	.endef
_estCouleurUniforme:
LFB26:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$48, %esp
	movl	12(%ebp), %eax
	movl	8(%eax), %eax
	movl	16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	20(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -14(%ebp)
	movl	12(%ebp), %eax
	movl	12(%eax), %eax
	movl	16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	20(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -12(%ebp)
	movl	12(%ebp), %eax
	movl	16(%eax), %eax
	movl	16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	20(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -10(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -4(%ebp)
	jmp	L67
L72:
	movl	16(%ebp), %eax
	movl	%eax, -8(%ebp)
	jmp	L68
L71:
	movl	12(%ebp), %eax
	movl	8(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -20(%ebp)
	movl	12(%ebp), %eax
	movl	12(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -18(%ebp)
	movl	12(%ebp), %eax
	movl	16(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -16(%ebp)
	movl	-20(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-16(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-14(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-10(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L69
	movl	8(%ebp), %eax
	movw	$-1, (%eax)
	movl	8(%ebp), %eax
	movw	$-1, 2(%eax)
	movl	8(%ebp), %eax
	movw	$-1, 4(%eax)
	jmp	L66
L69:
	addl	$1, -8(%ebp)
L68:
	movl	24(%ebp), %edx
	movl	16(%ebp), %eax
	addl	%edx, %eax
	cmpl	%eax, -8(%ebp)
	jl	L71
	addl	$1, -4(%ebp)
L67:
	movl	28(%ebp), %edx
	movl	20(%ebp), %eax
	addl	%edx, %eax
	cmpl	%eax, -4(%ebp)
	jl	L72
	movl	8(%ebp), %eax
	movl	-14(%ebp), %edx
	movl	%edx, (%eax)
	movzwl	-10(%ebp), %edx
	movw	%dx, 4(%eax)
L66:
	movl	8(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE26:
	.globl	_determineConfig
	.def	_determineConfig;	.scl	2;	.type	32;	.endef
_determineConfig:
LFB27:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$48, %esp
	movl	$0, -4(%ebp)
	jmp	L75
L87:
	movl	12(%ebp), %eax
	movl	4(%eax), %ecx
	movl	-4(%ebp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	movl	(%eax), %edx
	movl	%edx, 8(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 12(%esp)
	movl	20(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	24(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	testb	%al, %al
	je	L76
	cmpl	$0, -4(%ebp)
	jne	L77
	movl	$-1, -8(%ebp)
	movl	$0, -12(%ebp)
	jmp	L78
L81:
	movl	12(%ebp), %eax
	movl	4(%eax), %ecx
	movl	-12(%ebp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	movl	(%eax), %edx
	movl	%edx, 8(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 12(%esp)
	movl	28(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	32(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	testb	%al, %al
	je	L79
	movl	-12(%ebp), %eax
	movl	%eax, -8(%ebp)
	jmp	L80
L79:
	addl	$1, -12(%ebp)
L78:
	movl	12(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -12(%ebp)
	jl	L81
L80:
	movl	16(%ebp), %eax
	movl	%eax, %ecx
	cmpl	$5, 16(%ebp)
	sete	%al
	movzbl	%al, %edx
	movl	8(%ebp), %eax
	movw	%cx, (%eax)
	movl	8(%ebp), %eax
	movw	%dx, 2(%eax)
	movl	8(%ebp), %eax
	movl	-4(%ebp), %edx
	movl	%edx, 4(%eax)
	movl	8(%ebp), %eax
	movl	-8(%ebp), %edx
	movl	%edx, 8(%eax)
	jmp	L74
L77:
	movl	$-1, -16(%ebp)
	movl	$0, -20(%ebp)
	jmp	L83
L86:
	movl	12(%ebp), %eax
	movl	4(%eax), %ecx
	movl	-20(%ebp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	movl	(%eax), %edx
	movl	%edx, 8(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 12(%esp)
	movl	28(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	32(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	testb	%al, %al
	je	L84
	movl	-20(%ebp), %eax
	movl	%eax, -16(%ebp)
	jmp	L85
L84:
	addl	$1, -20(%ebp)
L83:
	movl	12(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -20(%ebp)
	jl	L86
L85:
	movl	16(%ebp), %eax
	movl	%eax, %ecx
	cmpl	$5, 16(%ebp)
	setne	%al
	movzbl	%al, %edx
	movl	8(%ebp), %eax
	movw	%cx, (%eax)
	movl	8(%ebp), %eax
	movw	%dx, 2(%eax)
	movl	8(%ebp), %eax
	movl	-16(%ebp), %edx
	movl	%edx, 4(%eax)
	movl	8(%ebp), %eax
	movl	-4(%ebp), %edx
	movl	%edx, 8(%eax)
	jmp	L74
L76:
	addl	$1, -4(%ebp)
L75:
	movl	12(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -4(%ebp)
	jl	L87
	movl	8(%ebp), %eax
	movw	$-1, (%eax)
	movl	8(%ebp), %eax
	movw	$-1, 2(%eax)
	movl	8(%ebp), %eax
	movl	$-1, 4(%eax)
	movl	8(%ebp), %eax
	movl	$-1, 8(%eax)
L74:
	movl	8(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE27:
	.globl	_identifieMotif
	.def	_identifieMotif;	.scl	2;	.type	32;	.endef
_identifieMotif:
LFB28:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%esi
	pushl	%ebx
	subl	$164, %esp
	.cfi_offset 6, -12
	.cfi_offset 3, -16
	cmpl	$32, 28(%ebp)
	je	L89
	movl	8(%ebp), %eax
	movw	$-1, (%eax)
	movl	8(%ebp), %eax
	movw	$-1, 2(%eax)
	movl	8(%ebp), %eax
	movl	$-1, 4(%eax)
	movl	8(%ebp), %eax
	movl	$-1, 8(%eax)
	jmp	L88
L89:
	movl	28(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %ebx
	movl	28(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %ecx
	movl	24(%ebp), %eax
	leal	16(%eax), %edx
	leal	-20(%ebp), %eax
	movl	%ebx, 20(%esp)
	movl	%ecx, 16(%esp)
	movl	%edx, 12(%esp)
	movl	20(%ebp), %edx
	movl	%edx, 8(%esp)
	movl	12(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_estCouleurUniforme
	movl	28(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %esi
	movl	28(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %ebx
	movl	24(%ebp), %eax
	leal	16(%eax), %ecx
	movl	20(%ebp), %eax
	leal	16(%eax), %edx
	leal	-26(%ebp), %eax
	movl	%esi, 20(%esp)
	movl	%ebx, 16(%esp)
	movl	%ecx, 12(%esp)
	movl	%edx, 8(%esp)
	movl	12(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_estCouleurUniforme
	movl	28(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %ecx
	movl	28(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %edx
	leal	-32(%ebp), %eax
	movl	%ecx, 20(%esp)
	movl	%edx, 16(%esp)
	movl	24(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	20(%ebp), %edx
	movl	%edx, 8(%esp)
	movl	12(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_estCouleurUniforme
	movl	28(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %ebx
	movl	28(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %edx
	movl	20(%ebp), %eax
	leal	16(%eax), %ecx
	leal	-38(%ebp), %eax
	movl	%ebx, 20(%esp)
	movl	%edx, 16(%esp)
	movl	24(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	%ecx, 8(%esp)
	movl	12(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_estCouleurUniforme
	movl	-38(%ebp), %eax
	movl	%eax, 28(%esp)
	movzwl	-34(%ebp), %eax
	movw	%ax, 32(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, 20(%esp)
	movzwl	-28(%ebp), %eax
	movw	%ax, 24(%esp)
	movl	-26(%ebp), %eax
	movl	%eax, 12(%esp)
	movzwl	-22(%ebp), %eax
	movw	%ax, 16(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, 4(%esp)
	movzwl	-16(%ebp), %eax
	movw	%ax, 8(%esp)
	movl	$4, (%esp)
	call	_sontDesCouleursVariadic
	testb	%al, %al
	je	L91
	movl	-38(%ebp), %eax
	movl	%eax, 28(%esp)
	movzwl	-34(%ebp), %eax
	movw	%ax, 32(%esp)
	movl	-26(%ebp), %eax
	movl	%eax, 20(%esp)
	movzwl	-22(%ebp), %eax
	movw	%ax, 24(%esp)
	movl	$2, 16(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-16(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-28(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleurVariadic
	testb	%al, %al
	je	L92
	movw	$-1, -14(%ebp)
	movw	$-1, -12(%ebp)
	movw	$-1, -10(%ebp)
	leal	-52(%ebp), %eax
	movl	-14(%ebp), %edx
	movl	%edx, 20(%esp)
	movzwl	-10(%ebp), %edx
	movw	%dx, 24(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 12(%esp)
	movzwl	-16(%ebp), %edx
	movw	%dx, 16(%esp)
	movl	$0, 8(%esp)
	movl	16(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_determineConfig
	movw	$0, -52(%ebp)
	movl	8(%ebp), %eax
	movl	-52(%ebp), %edx
	movl	%edx, (%eax)
	movl	-48(%ebp), %edx
	movl	%edx, 4(%eax)
	movl	-44(%ebp), %edx
	movl	%edx, 8(%eax)
	jmp	L88
L92:
	movl	-38(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-34(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-28(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	testb	%al, %al
	je	L93
	movl	-26(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-22(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-16(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	testb	%al, %al
	je	L93
	movl	-32(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-28(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-16(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L93
	leal	-64(%ebp), %eax
	movl	-32(%ebp), %edx
	movl	%edx, 20(%esp)
	movzwl	-28(%ebp), %edx
	movw	%dx, 24(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 12(%esp)
	movzwl	-16(%ebp), %edx
	movw	%dx, 16(%esp)
	movl	$1, 8(%esp)
	movl	16(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_determineConfig
	movw	$1, -64(%ebp)
	movl	8(%ebp), %eax
	movl	-64(%ebp), %edx
	movl	%edx, (%eax)
	movl	-60(%ebp), %edx
	movl	%edx, 4(%eax)
	movl	-56(%ebp), %edx
	movl	%edx, 8(%eax)
	jmp	L88
L93:
	movl	-20(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-16(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-28(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	testb	%al, %al
	je	L94
	movl	-26(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-22(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-38(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-34(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	testb	%al, %al
	je	L94
	movl	-38(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-34(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-28(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L94
	leal	-76(%ebp), %eax
	movl	-26(%ebp), %edx
	movl	%edx, 20(%esp)
	movzwl	-22(%ebp), %edx
	movw	%dx, 24(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 12(%esp)
	movzwl	-16(%ebp), %edx
	movw	%dx, 16(%esp)
	movl	$2, 8(%esp)
	movl	16(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_determineConfig
	movw	$2, -76(%ebp)
	movl	8(%ebp), %eax
	movl	-76(%ebp), %edx
	movl	%edx, (%eax)
	movl	-72(%ebp), %edx
	movl	%edx, 4(%eax)
	movl	-68(%ebp), %edx
	movl	%edx, 8(%eax)
	jmp	L88
L94:
	movl	-26(%ebp), %eax
	movl	%eax, 20(%esp)
	movzwl	-22(%ebp), %eax
	movw	%ax, 24(%esp)
	movl	$1, 16(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-16(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-28(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleurVariadic
	testb	%al, %al
	je	L95
	movl	-38(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-34(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-28(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L95
	leal	-88(%ebp), %eax
	movl	-38(%ebp), %edx
	movl	%edx, 20(%esp)
	movzwl	-34(%ebp), %edx
	movw	%dx, 24(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 12(%esp)
	movzwl	-16(%ebp), %edx
	movw	%dx, 16(%esp)
	movl	$3, 8(%esp)
	movl	16(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_determineConfig
	movw	$3, -88(%ebp)
	movl	8(%ebp), %eax
	movl	-88(%ebp), %edx
	movl	%edx, (%eax)
	movl	-84(%ebp), %edx
	movl	%edx, 4(%eax)
	movl	-80(%ebp), %edx
	movl	%edx, 8(%eax)
	jmp	L88
L95:
	movl	-38(%ebp), %eax
	movl	%eax, 20(%esp)
	movzwl	-34(%ebp), %eax
	movw	%ax, 24(%esp)
	movl	$1, 16(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-16(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-28(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleurVariadic
	testb	%al, %al
	je	L96
	movl	-26(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-22(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-16(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L96
	leal	-100(%ebp), %eax
	movl	-26(%ebp), %edx
	movl	%edx, 20(%esp)
	movzwl	-22(%ebp), %edx
	movw	%dx, 24(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 12(%esp)
	movzwl	-16(%ebp), %edx
	movw	%dx, 16(%esp)
	movl	$4, 8(%esp)
	movl	16(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_determineConfig
	movw	$4, -100(%ebp)
	movl	8(%ebp), %eax
	movl	-100(%ebp), %edx
	movl	%edx, (%eax)
	movl	-96(%ebp), %edx
	movl	%edx, 4(%eax)
	movl	-92(%ebp), %edx
	movl	%edx, 8(%eax)
	jmp	L88
L96:
	movl	-38(%ebp), %eax
	movl	%eax, 20(%esp)
	movzwl	-34(%ebp), %eax
	movw	%ax, 24(%esp)
	movl	$1, 16(%esp)
	movl	-26(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-22(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-28(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleurVariadic
	testb	%al, %al
	je	L97
	movl	-26(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-22(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-16(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L97
	leal	-112(%ebp), %eax
	movl	-26(%ebp), %edx
	movl	%edx, 20(%esp)
	movzwl	-22(%ebp), %edx
	movw	%dx, 24(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 12(%esp)
	movzwl	-16(%ebp), %edx
	movw	%dx, 16(%esp)
	movl	$5, 8(%esp)
	movl	16(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_determineConfig
	movw	$5, -112(%ebp)
	movl	8(%ebp), %eax
	movl	-112(%ebp), %edx
	movl	%edx, (%eax)
	movl	-108(%ebp), %edx
	movl	%edx, 4(%eax)
	movl	-104(%ebp), %edx
	movl	%edx, 8(%eax)
	jmp	L88
L97:
	movl	-38(%ebp), %eax
	movl	%eax, 20(%esp)
	movzwl	-34(%ebp), %eax
	movw	%ax, 24(%esp)
	movl	$1, 16(%esp)
	movl	-26(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-22(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-16(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleurVariadic
	testb	%al, %al
	je	L91
	movl	-38(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-34(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-28(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L91
	leal	-124(%ebp), %eax
	movl	-32(%ebp), %edx
	movl	%edx, 20(%esp)
	movzwl	-28(%ebp), %edx
	movw	%dx, 24(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 12(%esp)
	movzwl	-16(%ebp), %edx
	movw	%dx, 16(%esp)
	movl	$6, 8(%esp)
	movl	16(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_determineConfig
	movw	$6, -124(%ebp)
	movl	8(%ebp), %eax
	movl	-124(%ebp), %edx
	movl	%edx, (%eax)
	movl	-120(%ebp), %edx
	movl	%edx, 4(%eax)
	movl	-116(%ebp), %edx
	movl	%edx, 8(%eax)
	jmp	L88
L91:
	movl	8(%ebp), %eax
	movw	$-1, (%eax)
	movl	8(%ebp), %eax
	movw	$-1, 2(%eax)
	movl	8(%ebp), %eax
	movl	$-1, 4(%eax)
	movl	8(%ebp), %eax
	movl	$-1, 8(%eax)
L88:
	movl	8(%ebp), %eax
	addl	$164, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE28:
	.globl	_creeArbreMotifs
	.def	_creeArbreMotifs;	.scl	2;	.type	32;	.endef
_creeArbreMotifs:
LFB29:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%ebx
	subl	$68, %esp
	.cfi_offset 3, -12
	movl	$40, (%esp)
	call	_malloc
	movl	%eax, -12(%ebp)
	movl	-12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%edx, 36(%eax)
	movl	-12(%ebp), %eax
	movl	20(%ebp), %edx
	movl	%edx, 28(%eax)
	movl	-12(%ebp), %eax
	movl	24(%ebp), %edx
	movl	%edx, 32(%eax)
	movl	-12(%ebp), %ebx
	leal	-40(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	24(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	20(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	12(%ebp), %edx
	movl	%edx, 8(%esp)
	movl	8(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_identifieMotif
	movl	-40(%ebp), %eax
	movl	%eax, 16(%ebx)
	movl	-36(%ebp), %eax
	movl	%eax, 20(%ebx)
	movl	-32(%ebp), %eax
	movl	%eax, 24(%ebx)
	movw	$-1, -24(%ebp)
	movw	$-1, -22(%ebp)
	movl	$-1, -20(%ebp)
	movl	$-1, -16(%ebp)
	movl	-24(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-12(%ebp), %eax
	movl	16(%eax), %edx
	movl	%edx, (%esp)
	movl	20(%eax), %edx
	movl	%edx, 4(%esp)
	movl	24(%eax), %eax
	movl	%eax, 8(%esp)
	call	_compareMotif
	testb	%al, %al
	je	L100
	movl	16(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %edx
	movl	24(%ebp), %eax
	addl	%eax, %edx
	movl	16(%ebp), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%ecx, %eax
	sarl	%eax
	movl	%eax, %ecx
	movl	%edx, 16(%esp)
	movl	20(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	%ecx, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_creeArbreMotifs
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movl	%edx, (%eax)
	movl	16(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %edx
	movl	24(%ebp), %eax
	leal	(%edx,%eax), %ecx
	movl	16(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %edx
	movl	20(%ebp), %eax
	addl	%eax, %edx
	movl	16(%ebp), %eax
	movl	%eax, %ebx
	shrl	$31, %ebx
	addl	%ebx, %eax
	sarl	%eax
	movl	%ecx, 16(%esp)
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_creeArbreMotifs
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movl	%edx, 4(%eax)
	movl	16(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %edx
	movl	24(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	20(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	%edx, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_creeArbreMotifs
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	16(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %edx
	movl	20(%ebp), %eax
	addl	%eax, %edx
	movl	16(%ebp), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%ecx, %eax
	sarl	%eax
	movl	%eax, %ecx
	movl	24(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	%edx, 12(%esp)
	movl	%ecx, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_creeArbreMotifs
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movl	%edx, 12(%eax)
	movl	-12(%ebp), %eax
	jmp	L102
L100:
	movl	-12(%ebp), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	$0, 4(%eax)
	movl	-12(%ebp), %eax
	movl	$0, 12(%eax)
	movl	-12(%ebp), %eax
	movl	$0, 8(%eax)
	movl	-12(%ebp), %eax
L102:
	addl	$68, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE29:
	.globl	_libereArbreMotif
	.def	_libereArbreMotif;	.scl	2;	.type	32;	.endef
_libereArbreMotif:
LFB30:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	cmpl	$0, 8(%ebp)
	je	L105
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_libereArbreMotif
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_libereArbreMotif
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, (%esp)
	call	_libereArbreMotif
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, (%esp)
	call	_libereArbreMotif
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
L105:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE30:
	.section .rdata,"dr"
LC2:
	.ascii "\12%d %d %d%d %d %d\0"
	.text
	.globl	_fprintfArbre
	.def	_fprintfArbre;	.scl	2;	.type	32;	.endef
_fprintfArbre:
LFB31:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	subl	$60, %esp
	.cfi_offset 7, -12
	.cfi_offset 6, -16
	.cfi_offset 3, -20
	cmpl	$0, 12(%ebp)
	je	L109
	movw	$-1, -36(%ebp)
	movw	$-1, -34(%ebp)
	movl	$-1, -32(%ebp)
	movl	$-1, -28(%ebp)
	movl	-36(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-28(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	12(%ebp), %eax
	movl	16(%eax), %edx
	movl	%edx, (%esp)
	movl	20(%eax), %edx
	movl	%edx, 4(%esp)
	movl	24(%eax), %eax
	movl	%eax, 8(%esp)
	call	_compareMotif
	xorl	$1, %eax
	testb	%al, %al
	je	L108
	movl	12(%ebp), %eax
	movl	24(%eax), %edi
	movl	12(%ebp), %eax
	movl	20(%eax), %esi
	movl	12(%ebp), %eax
	movzwl	16(%eax), %eax
	movswl	%ax, %ebx
	movl	12(%ebp), %eax
	movzwl	18(%eax), %eax
	movswl	%ax, %ecx
	movl	12(%ebp), %eax
	movl	32(%eax), %edx
	movl	12(%ebp), %eax
	movl	28(%eax), %eax
	movl	%edi, 28(%esp)
	movl	%esi, 24(%esp)
	movl	%ebx, 20(%esp)
	movl	%ecx, 16(%esp)
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC2, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintf
L108:
	movl	12(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintfArbre
	movl	12(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintfArbre
	movl	12(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintfArbre
	movl	12(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintfArbre
L109:
	nop
	addl	$60, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%edi
	.cfi_restore 7
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE31:
	.section .rdata,"dr"
LC3:
	.ascii "w\0"
	.align 4
LC4:
	.ascii "./resources/file/description_image.txt\0"
LC5:
	.ascii "%d %d\12\0"
LC6:
	.ascii "%d\0"
LC7:
	.ascii "\12%d %d %d\0"
	.text
	.globl	_sauveDescriptionImage
	.def	_sauveDescriptionImage;	.scl	2;	.type	32;	.endef
_sauveDescriptionImage:
LFB32:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%esi
	pushl	%ebx
	subl	$48, %esp
	.cfi_offset 6, -12
	.cfi_offset 3, -16
	movl	$LC3, 4(%esp)
	movl	$LC4, (%esp)
	call	_fopen
	movl	%eax, -16(%ebp)
	movl	8(%ebp), %eax
	movl	36(%eax), %eax
	movl	$32, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC5, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintf
	movl	12(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	$LC6, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintf
	movl	$0, -12(%ebp)
	jmp	L111
L112:
	movl	12(%ebp), %eax
	movl	4(%eax), %ecx
	movl	-12(%ebp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	movzwl	4(%eax), %eax
	movswl	%ax, %ebx
	movl	12(%ebp), %eax
	movl	4(%eax), %ecx
	movl	-12(%ebp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	movzwl	2(%eax), %eax
	movswl	%ax, %ecx
	movl	12(%ebp), %eax
	movl	4(%eax), %esi
	movl	-12(%ebp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	addl	%esi, %eax
	movzwl	(%eax), %eax
	cwtl
	movl	%ebx, 16(%esp)
	movl	%ecx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC7, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintf
	addl	$1, -12(%ebp)
L111:
	movl	12(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -12(%ebp)
	jl	L112
	movl	8(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintfArbre
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	nop
	addl	$48, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE32:
	.ident	"GCC: (MinGW.org GCC-8.2.0-5) 8.2.0"
	.def	_malloc;	.scl	2;	.type	32;	.endef
	.def	_realloc;	.scl	2;	.type	32;	.endef
	.def	_printf;	.scl	2;	.type	32;	.endef
	.def	_free;	.scl	2;	.type	32;	.endef
	.def	_fprintf;	.scl	2;	.type	32;	.endef
	.def	_fopen;	.scl	2;	.type	32;	.endef
	.def	_fclose;	.scl	2;	.type	32;	.endef
